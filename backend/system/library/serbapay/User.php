<?php


/**
 * The Serbapay_User class represents the current loggedin user 
 */
class Serbapay_User 
{
	/* The ID (name) of the connected provider */
	public $providerId = NULL;

	/* timestamp connection to the provider */
	public $timestamp = NULL; 

	/* user profile, containts the list of fields available in the normalized user profile structure used by SerbapayAuth. */
	public $profile = NULL;

	/**
	* inisialize the user object,
	*/
	function __construct()
	{
		$this->timestamp = time(); 

		$this->profile   = new Serbapay_User_Profile(); 
	}
}
