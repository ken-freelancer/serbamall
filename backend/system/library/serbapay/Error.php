<?php


/**
 * Errors manager
 * 
 * SerbapayAuth errors are stored in Serbapay::storage() and not displayed directly to the end user 
 */
class Serbapay_Error
{
	/**
	* store error in session
	*/
	public static function setError( $message, $code = NULL, $trace = NULL, $previous = NULL )
	{
		Serbapay_Logger::info( "Enter Serbapay_Error::setError( $message )" );

		Serbapay_Auth::storage()->set( "hauth_session.error.status"  , 1         );
		Serbapay_Auth::storage()->set( "hauth_session.error.message" , $message  );
		Serbapay_Auth::storage()->set( "hauth_session.error.code"    , $code     );
		Serbapay_Auth::storage()->set( "hauth_session.error.trace"   , $trace    );
		Serbapay_Auth::storage()->set( "hauth_session.error.previous", $previous );
	}

	/**
	* clear the last error
	*/
	public static function clearError()
	{ 
		Serbapay_Logger::info( "Enter Serbapay_Error::clearError()" );

		Serbapay_Auth::storage()->delete( "hauth_session.error.status"   );
		Serbapay_Auth::storage()->delete( "hauth_session.error.message"  );
		Serbapay_Auth::storage()->delete( "hauth_session.error.code"     );
		Serbapay_Auth::storage()->delete( "hauth_session.error.trace"    );
		Serbapay_Auth::storage()->delete( "hauth_session.error.previous" );
	}

	/**
	* Checks to see if there is a an error. 
	* 
	* @return boolean True if there is an error.
	*/
	public static function hasError()
	{ 
		return (bool) Serbapay_Auth::storage()->get( "hauth_session.error.status" );
	}

	/**
	* return error message 
	*/
	public static function getErrorMessage()
	{ 
		return Serbapay_Auth::storage()->get( "hauth_session.error.message" );
	}

	/**
	* return error code  
	*/
	public static function getErrorCode()
	{ 
		return Serbapay_Auth::storage()->get( "hauth_session.error.code" );
	}

	/**
	* return string detailled error backtrace as string.
	*/
	public static function getErrorTrace()
	{ 
		return Serbapay_Auth::storage()->get( "hauth_session.error.trace" );
	}

	/**
	* @return string detailled error backtrace as string.
	*/
	public static function getErrorPrevious()
	{ 
		return Serbapay_Auth::storage()->get( "hauth_session.error.previous" );
	}
}
