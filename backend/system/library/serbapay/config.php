<?php

// ----------------------------------------------------------------------------------------
//	SerbapayAuth Config file
// ----------------------------------------------------------------------------------------

return
	array(		
		"base_url" => "http://wwww.serbamall.com/index.php?route=serbapay/process",
		
		"providers" => array (			

			"SerbaPay" => array (
				"enabled" => true,
				"keys"    => array ( "id" => "1d3d9fc3-54da-4c4b-8e88-d34e2b1caa12", "secret" => "CKzITSwebpghNj7jIWa3" ),
				"scope"   => "", // optional
				"display" => "popup" // optional
			),
		),

		// if you want to enable logging, set 'debug_mode' to true  then provide a writable file by the web server on "debug_file"
		"debug_mode" => false,

		"debug_file" => "serbapayauth.txt",
	);
