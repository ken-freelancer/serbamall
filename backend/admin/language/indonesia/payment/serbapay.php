<?php
// Heading
$_['heading_title']      = 'SerbaPay';

// Text 
$_['text_payment']       = 'Payment'; 
$_['text_success']       = 'Success: You have modified serbapay account details!';
$_['text_serbapay']       = '<a onclick="window.open(\'http://www.serbapay.com\');"><img src="view/image/payment/serbapay.png" alt="serbapay" title="serbapay" style="border: 1px solid #EEEEEE;" /></a>';
$_['text_sim']           = 'Simulator';
$_['text_test']          = 'Test';
$_['text_live']          = 'Live';
$_['text_payment']       = 'Payment';
$_['text_defered']       = 'Defered';
$_['text_authenticate']  = 'Authenticate';

// Entry
$_['entry_vendor']       = 'Merchant ID:';
$_['entry_password']     = 'Merchant Secret:';
$_['entry_test']         = 'Test Mode:';
$_['entry_transaction']  = 'Transaction Method:';
$_['entry_order_status'] = 'Order Status:';
$_['entry_geo_zone']     = 'Geo Zone:';
$_['entry_status']       = 'Status:';
$_['entry_sort_order']   = 'Sort Order:';

// Error
$_['error_permission']   = 'Warning: You do not have permission to modify payment serbapay !';
$_['error_vendor']       = 'Merchant ID Required!';
$_['error_password']     = 'Merchant Secret Required!';