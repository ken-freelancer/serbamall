<?php

$_['heading_title']       = 'SerbaPay Login';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module SerbaPay Login!';
$_['text_copyright']      = 'OpenCart <a href="#" target="_blank">serbapay</a> Serbapay &copy; %s ';

// Entry
$_['entry_status']        = 'Status';
$_['entry_debug']         = 'Debug Mode';

$_['entry_provider']      = 'Provider';
$_['entry_key']           = 'Key';
$_['entry_secret']        = 'Secret';
$_['entry_scope']         = 'Flags (optional)';
$_['entry_sort_order']    = 'Sort Order';

// Button
$_['button_add_row']      = 'Add Provider';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module SerbaPay Login!';
