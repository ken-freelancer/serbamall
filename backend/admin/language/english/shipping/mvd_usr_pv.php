<?php
// Heading
$_['heading_title']    		= 'Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor';

// Text
$_['text_shipping']    		= 'Shipping';
$_['text_checkout_name']	= 'Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor';
$_['text_edit']				= 'Edit Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor';
$_['text_success']     		= 'Success: You have modified Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor!';
$_['text_combo_text'] 		= 'Combo Box';
$_['text_input_text'] 		= 'Radio';
$_['text_nws_text'] 		= 'Weight Base Shipping';
$_['text_wbsp_text'] 		= 'Weight Base Shipping Plus';
$_['text_tapv_text'] 		= 'Total Amount Per Vendor';

// Entry
$_['entry_global_mode']		= 'Default Mode';
$_['entry_measurement']		= 'Measurement';
$_['entry_module_name'] 	= 'Checkout Name';
$_['entry_display_mode'] 	= 'Display Style';
$_['entry_tax_class']  		= 'Tax Class';
$_['entry_geo_zone']   		= 'Geo Zone';
$_['entry_status']     		= 'Status';
$_['entry_sort_order'] 		= 'Sort Order';

// Error
$_['error_permission'] 		= 'Warning: You do not have permission to modify Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor!';
?>