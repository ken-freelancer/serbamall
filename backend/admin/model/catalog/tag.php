<?php
class ModelCatalogTag extends Model {
	public function addTag($data) {
        //explode tag_name
        $a_Data = explode(',', $data['tag_name']);
        foreach ($a_Data as $data)
        {
            try
            {
                if(!empty($data))
                $this->db->query("INSERT IGNORE INTO `" . DB_PREFIX . "tag` SET `tag_name` = '" . $data . "', language_id = '" . (int) $this->config->get('config_language_id') . "', date_added = NOW(), tag_user_created = '" . $this->user->getId() . "'");
            }
            catch( ErrorException $e ){
                return $e;
            }
        }

        //reconstruct json file
        $this->constructJSON();

	}


	public function deleteTag($tag_id) {
		$this->db->query("DELETE FROM `" . DB_PREFIX . "tag` WHERE tag_id = '" . (int)$tag_id . "'");
        $this->constructJSON();
	}

    protected function constructJSON()
    {
        $sql = "SELECT * FROM " . DB_PREFIX . "tag";
        $query = $this->db->query($sql);

        foreach($query->rows as $row) {
            $posts[] 	= $row['tag_name'];
        }

        $posts = array_values($posts);

        $fp = fopen(__DIR__ .'/../../../merchant/tags.json', 'w');
        fwrite($fp, json_encode($posts));
        fclose($fp);
    }

	public function getTag($tag_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "tag t WHERE t.tag_id = '" . (int)$tag_id . "' AND t.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	}

	public function getTags($data) {
		$sql = "SELECT * FROM " . DB_PREFIX . "tag t WHERE t.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_tag_name'])) {
            $sql .= " AND t.tag_name LIKE '%" . $this->db->escape($data['filter_tag_name']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(t.date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

		$sql .= " ORDER BY t.date_added DESC";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}					

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}


	public function getTotalTags($data = array()) {
		$sql = ("SELECT COUNT(*) AS total FROM `" . DB_PREFIX . "tag`");

        if (!empty($data['filter_tag_name'])) {
            $sql .= " AND tag_name LIKE '%" . $this->db->escape($data['filter_tag_name']) . "%'";
        }

        if (!empty($data['filter_date_added'])) {
            $sql .= " AND DATE(date_added) = DATE('" . $this->db->escape($data['filter_date_added']) . "')";
        }

        $query = $this->db->query($sql);

        return $query->row['total'];
	}		
}