<?php
class ModelCatalogVendor extends Model {
	public function addVendor($data) {
		/*add*/
		$this->db->query("INSERT INTO " . DB_PREFIX . "vendors SET vendor_name = '" . $this->db->escape($data['vendor_name']) . "', company = '" . $this->db->escape($data['company']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', email = '" . $this->db->escape($data['email']) . "', paypal_email = '" . $this->db->escape($data['paypal_email']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', iban = '" . $this->db->escape($data['iban']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_address = '" . $this->db->escape($data['bank_address']) . "', swift_bic = '" . $this->db->escape($data['swift_bic']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', accept_paypal = '" . $this->db->escape($data['accept_paypal']) . "', accept_cheques = '" . $this->db->escape($data['accept_cheques']) . "', accept_bank_transfer = '" . $this->db->escape($data['accept_bank_transfer']) . "', vendor_description = '" . $this->db->escape($data['vendor_description']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', commission_id = '" . (int)$data['commission'] . "', product_limit_id = '" . (int)$data['product_limit'] . "', store_url = '" . $this->db->escape($data['store_url']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$vendor_id = $this->db->getLastId();

        //Multiple Shipping
        if (isset($data['vendor_ultimate_shipping'])) {
            foreach ($data['vendor_ultimate_shipping'] as $vendor_ultimate_shipping) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "vendor_ultimate_shipping SET vendor_id = '" . (int)$vendor_id . "', courier_id = '" . (int)$vendor_ultimate_shipping['courier_id'] . "', shipping_rate = '" . $this->db->escape($vendor_ultimate_shipping['shipping_rate']) . "', geo_zone_id = '" . (int)$vendor_ultimate_shipping['geo_zone_id'] . "'");
            }
        }
        //.Multiple Shipping

        //Multiple Shipping
        $this->db->query("UPDATE " . DB_PREFIX . "vendors SET measurement_id = '" . $this->db->escape($data['measurement_id']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
        //.Multiple Shipping

		if (isset($data['vendor_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_image = '" . $this->db->escape($data['vendor_image']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}
		
		//vendor banner
		if (isset($data['vendor_banner'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_banner = '" . $this->db->escape($data['vendor_banner']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}
		
		//vendor gallery
		if (isset($data['vendor_gallery'])) {
			foreach ($data['vendor_gallery'] as $product_video) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_gallery SET vendor_id = '" . (int)$vendor_id . "', image = '".$data['image']."', sort_order = '" . (int)$vendor_gallery['sort_order'] . "'");
			}
		}
		
		$this->cache->delete('vendor');
	}

	public function editVendor($vendor_id, $data) {
		/*add*/
		$this->db->query("UPDATE " . DB_PREFIX . "vendors SET user_id = '" . (int)$this->db->escape($data['user_id']) . "', vendor_name = '" . $this->db->escape($data['vendor_name']) . "', company = '" . $this->db->escape($data['company']) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', email = '" . $this->db->escape($data['email']) . "', paypal_email = '" . $this->db->escape($data['paypal_email']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', iban = '" . $this->db->escape($data['iban']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_address = '" . $this->db->escape($data['bank_address']) . "', swift_bic = '" . $this->db->escape($data['swift_bic']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', accept_paypal = '" . $this->db->escape($data['accept_paypal']) . "', accept_cheques = '" . $this->db->escape($data['accept_cheques']) . "', accept_bank_transfer = '" . $this->db->escape($data['accept_bank_transfer']) . "', vendor_description = '" . $this->db->escape($data['vendor_description']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', commission_id = '" . (int)$data['commission'] . "', product_limit_id = '" . (int)$data['product_limit'] . "', store_url = '" . $this->db->escape($data['store_url']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE vendor_id = '" . (int)$vendor_id . "'");

        //Multiple Shipping
        $this->db->query("UPDATE " . DB_PREFIX . "vendors SET measurement_id = '" . $this->db->escape($data['measurement_id']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
        //.Multiple Shipping

		if (isset($data['vendor_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_image = '" . $this->db->escape($data['vendor_image']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}
		
		//vendor banner
		if (isset($data['vendor_banner'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_banner = '" . $this->db->escape($data['vendor_banner']) . "' WHERE vendor_id = '" . (int)$vendor_id . "'");
		}
		
		//vendor gallery
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_gallery WHERE vendor_id = '" . (int)$this->db->escape($data['user_id']) . "'");
		
		if (isset($data['vendor_gallery'])) {
			foreach ($data['vendor_gallery'] as $vendor_gallery) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_gallery SET vendor_id = '" . (int)$this->db->escape($data['user_id']) . "', image = '" . $vendor_gallery['image'] . "', sort_order = '" . (int)$vendor_gallery['sort_order'] . "'");
			}
		}

        //Multiple Shipping
        if ($vendor_id) {
            $id_data = $this->ID_Info($vendor_id);

            if (isset($data['vendor_ultimate_shipping'])) {
                $data_id = array();
                $shipping_data = array();

                foreach ($data['vendor_ultimate_shipping'] as $data) {
                    if (isset($data['vutm_shipping_id'])) {
                        $data_id[] =$data['vutm_shipping_id'];
                        $shipping_data[] = array(
                            'vutm_shipping_id' 	=> $data['vutm_shipping_id'],
                            'courier_id' 		=> $data['courier_id'],
                            'shipping_rate' 	=> $data['shipping_rate'],
                            'geo_zone_id' 		=> $data['geo_zone_id']
                        );
                    }
                }

                if ($id_data) {
                    foreach ($id_data as $key => $value) {
                        if (!in_array($value,$data_id)) {
                            $this->db->query("DELETE FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vutm_shipping_id = '" . (int)$value . "'");
                        }
                    }
                }

                foreach ($shipping_data as $data) {
                    if ($data['vutm_shipping_id']) {
                        $this->db->query("UPDATE " . DB_PREFIX . "vendor_ultimate_shipping SET courier_id = '" . (int)$data['courier_id'] . "', shipping_rate = '" . $this->db->escape($data['shipping_rate']) . "', geo_zone_id = '" . (int)$data['geo_zone_id'] . "' WHERE vutm_shipping_id = '" . (int)$data['vutm_shipping_id'] . "'");
                    } else {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "vendor_ultimate_shipping SET vendor_id = '" . (int)$vendor_id . "', courier_id = '" . (int)$data['courier_id'] . "', shipping_rate = '" . $this->db->escape($data['shipping_rate']) . "', geo_zone_id = '" . (int)$data['geo_zone_id'] . "'");
                    }
                }
            } else {
                $this->db->query("DELETE FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vendor_id = '" . (int)$vendor_id . "'");
            }
        }
        //.Multiple Shipping


        $this->cache->delete('vendor');
	}
	
	public function editVendorProfile($user_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "vendors SET firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', telephone = '" . $this->db->escape($data['telephone']) . "', fax = '" . $this->db->escape($data['fax']) . "', email = '" . $this->db->escape($data['email']) . "', paypal_email = '" . $this->db->escape($data['paypal_email']) . "', company_id = '" . $this->db->escape($data['company_id']) . "', iban = '" . $this->db->escape($data['iban']) . "', bank_name = '" . $this->db->escape($data['bank_name']) . "', bank_address = '" . $this->db->escape($data['bank_address']) . "', swift_bic = '" . $this->db->escape($data['swift_bic']) . "', tax_id = '" . $this->db->escape($data['tax_id']) . "', accept_paypal = '" . $this->db->escape($data['accept_paypal']) . "', accept_cheques = '" . $this->db->escape($data['accept_cheques']) . "', accept_bank_transfer = '" . $this->db->escape($data['accept_bank_transfer']) . "', vendor_description = '" . $this->db->escape($data['vendor_description']) . "', address_1 = '" . $this->db->escape($data['address_1']) . "', address_2 = '" . $this->db->escape($data['address_2']) . "', city = '" . $this->db->escape($data['city']) . "', postcode = '" . $this->db->escape($data['postcode']) . "', country_id = '" . (int)$data['country_id'] . "', zone_id = '" . (int)$data['zone_id'] . "', store_url = '" . $this->db->escape($data['store_url']) . "' WHERE user_id = '" . (int)$user_id . "'");

		if (isset($data['vendor_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_image = '" . $this->db->escape($data['vendor_image']) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
		
		//vendor banner
		if (isset($data['vendor_banner'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "vendors SET vendor_banner = '" . $this->db->escape($data['vendor_banner']) . "' WHERE user_id = '" . (int)$user_id . "'");
		}
				
		//vendor gallery
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_gallery WHERE vendor_id = '" . (int)$user_id . "'");
		
		if (isset($data['vendor_gallery'])) {
			foreach ($data['vendor_gallery'] as $vendor_gallery) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_gallery SET vendor_id = '" . (int)$user_id . "', image = '" . $vendor_gallery['image'] . "', sort_order = '" . (int)$vendor_gallery['sort_order'] . "'");
			}
		}

        //Multiple Shipping
        $this->db->query("UPDATE " . DB_PREFIX . "vendors SET measurement_id = '" . $this->db->escape($data['measurement_id']) . "' WHERE user_id = '" . (int)$user_id . "'");

        $my_vendor_id = $this->db->query("SELECT vendor_id FROM " . DB_PREFIX . "vendors WHERE user_id = '" . (int)$user_id . "'");

        if ($my_vendor_id->row) {
            $id_data = $this->ID_Info($my_vendor_id->row['vendor_id']);

            if (isset($data['vendor_ultimate_shipping'])) {
                $data_id = array();
                $shipping_data = array();

                foreach ($data['vendor_ultimate_shipping'] as $data) {
                    if (isset($data['vutm_shipping_id'])) {
                        $data_id[] = $data['vutm_shipping_id'];
                        $shipping_data[] = array(
                            'vutm_shipping_id' 	=>$data['vutm_shipping_id'],
                            'courier_id' 		=>$data['courier_id'],
                            'shipping_rate' 	=>$data['shipping_rate'],
                            'geo_zone_id' 		=>$data['geo_zone_id']
                        );
                    }
                }

                if ($id_data) {
                    foreach ($id_data as $key => $value) {
                        if (!in_array($value,$data_id)) {
                            $this->db->query("DELETE FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vutm_shipping_id = '" . (int)$value . "'");
                        }
                    }
                }

                foreach ($shipping_data as $data) {
                    if ($data['vutm_shipping_id']) {
                        $this->db->query("UPDATE " . DB_PREFIX . "vendor_ultimate_shipping SET courier_id = '" . (int)$data['courier_id'] . "', shipping_rate = '" . $this->db->escape($data['shipping_rate']) . "', geo_zone_id = '" . (int)$data['geo_zone_id'] . "' WHERE vutm_shipping_id = '" . (int)$data['vutm_shipping_id'] . "'");
                    } else {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "vendor_ultimate_shipping SET vendor_id = '" . (int)$my_vendor_id->row['vendor_id'] . "', courier_id = '" . (int)$data['courier_id'] . "', shipping_rate = '" . $this->db->escape($data['shipping_rate']) . "', geo_zone_id = '" . (int)$data['geo_zone_id'] . "'");
                    }
                }
            } else {
                $this->db->query("DELETE FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vendor_id = '" . (int)$my_vendor_id->row['vendor_id'] . "'");
            }
        }
        //.Multiple Shipping

		$this->cache->delete('vendor');
	}
 
	public function deleteVendor($vendor_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendors WHERE vendor_id = '" . (int)$vendor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "user WHERE vendor_permission = '" . (int)$vendor_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_gallery WHERE vendor_id = '" . (int)$vendor_id . "'");	//vendor gallery
	}
	
	public function getCommissionLimits() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission c LEFT JOIN " . DB_PREFIX . "product_limit pc ON (c.product_limit_id = pc.product_limit_id)");
		return $query->rows; 
	}
	
	public function getVendor($vendor_id) {	
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendors WHERE vendor_id = '" . (int)$vendor_id . "'");
		if ($query->row) {
			return $query->row;
		} else {
			false;
		}
	}
	
	public function getVendorProfile($user_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendors WHERE user_id = '" . (int)$user_id . "'");
		return $query->row; 
	}
	
	public function getVendors($data = array()) {
		
		if ($data) {
			$sql = "SELECT *,v.commission_id AS commission_id, c.commission AS commission,v.sort_order as vsort_order FROM " . DB_PREFIX . "vendors v LEFT JOIN " . DB_PREFIX . "commission c ON (v.commission_id = c.commission_id)";
			
			$sort_data = array(
				'vendor_name',
				'commission',
				'vsort_order'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];	
			} else {
				$sql .= " ORDER BY vendor_name";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			
			if (!$vendors_data) {
				$query = $this->db->query("SELECT *,v.commission_id AS commission_id, c.commission AS commission,v.sort_order as sort_order FROM " . DB_PREFIX . "vendors v LEFT JOIN " . DB_PREFIX . "commission c ON (v.commission_id = c.commission_id)");
			
				$vendors_data = $query->rows;

				$this->cache->set('vendor', $vendors_data);
			}

			return $vendors_data;
		}
	}
	public function getTotalVendorsByVendorId($vendors) {
      	$query = $this->db->query("SELECT COUNT(vendor) AS total FROM " . DB_PREFIX . "vendor WHERE vendor = '" . (int)$vendors . "'");
		return $query->row['total'];
	}

	public function getTotalVendors($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendors";
		$query = $this->db->query($sql);

		return $query->row['total'];
	}
	
	public function getByCountryZone($country_id,$zone_id) {
		$query = $this->db->query("SELECT zone FROM " . DB_PREFIX . "zone WHERE country_id = '" . (int)$country_id . "' AND zone_id = '" . (int)$zone_id . "'");
		
		return $query->row;
	}

    //Multiple Shipping
    public function getProductUltmShippings($vendor_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_ultm_shipping WHERE vendor_id = '" . (int)$vendor_id . "' ORDER BY vendor_ultimate_shipping_id");
        return $query->rows;
    }

    public function getVendorUltmShippings($vendor_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vendor_id = '" . (int)$vendor_id . "' ORDER BY vutm_shipping_id");
        return $query->rows;
    }

    public function getVendorUltiShipping() {
        $query = $this->db->query("SELECT vus.vutm_shipping_id as vutm_shipping_id,vus.shipping_rate as shipping_rate,vus.geo_zone_id as geo_zone_id,vs.vendor_id as vendor_id,vs.company as vendor,cr.courier_name as courier,gz.name as geo_zone FROM " . DB_PREFIX . "vendor_ultimate_shipping vus LEFT JOIN  " . DB_PREFIX . "vendors vs ON (vus.vendor_id = vs.vendor_id) LEFT JOIN  " . DB_PREFIX . "courier cr ON(vus.courier_id=cr.courier_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (vus.geo_zone_id=gz.geo_zone_id) ORDER BY vus.vendor_id");
        return $query->rows;
    }

    public function getVDIUltiShipping($vendor_id) {
        $query = $this->db->query("SELECT vus.vutm_shipping_id as vutm_shipping_id,vus.shipping_rate as shipping_rate,vus.geo_zone_id as geo_zone_id,vs.vendor_id as vendor_id,vs.company as vendor,cr.courier_name as courier,gz.name as geo_zone FROM " . DB_PREFIX . "vendor_ultimate_shipping vus LEFT JOIN  " . DB_PREFIX . "vendors vs ON (vus.vendor_id = vs.vendor_id) LEFT JOIN  " . DB_PREFIX . "courier cr ON(vus.courier_id=cr.courier_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (vus.geo_zone_id=gz.geo_zone_id) WHERE vus.vendor_id = '" . (int)$vendor_id . "' ORDER BY vus.vendor_id");
        return $query->rows;
    }

    public function ID_Info($vendor_id) {
        $mydata = array();
        $query = $this->db->query("SELECT vutm_shipping_id FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vendor_id = '" . (int)$vendor_id . "'");

        if ($query->row) {
            foreach ($query->rows as $data) {
                $mydata[] = $data['vutm_shipping_id'];
            }
            return $mydata;
        } else {
            return false;
        }
    }
    //.Multiple Shipping

	//vendor gallery
	public function getVendorGallery($vendors) {
		$vendor_gallery = array();		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor_gallery WHERE vendor_id = '" . (int)$vendors . "'");
		
		return $query->rows;
	}
}
?>