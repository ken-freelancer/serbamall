<?php
class ModelCatalogMVDUSRPV extends Model {
	public function addCourier($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "courier SET courier_name = '" . $this->db->escape($data['courier_name']) . "', description = '" . $this->db->escape($data['description']) . "', flat_rate = '" . $this->db->escape($data['flat_rate']) . "', weight_rate = '" . $this->db->escape($data['weight_rate']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$courier_id = $this->db->getLastId();

		if (isset($data['courier_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "courier SET courier_image = '" . $this->db->escape($data['courier_image']) . "' WHERE courier_id = '" . (int)$courier_id . "'");
		}
		
		$this->cache->delete('courier');
	}

	public function editCourier($courier_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "courier SET courier_name = '" . $this->db->escape($data['courier_name']) . "', description = '" . $this->db->escape($data['description']) . "', flat_rate = '" . $this->db->escape($data['flat_rate']) . "', weight_rate = '" . $this->db->escape($data['weight_rate']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE courier_id = '" . (int)$courier_id . "'");

		if (isset($data['courier_image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "courier SET courier_image = '" . $this->db->escape($data['courier_image']) . "' WHERE courier_id = '" . (int)$courier_id . "'");
		}

		$this->cache->delete('courier');
	}
	
	public function getCourier() {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "courier");
		
		if ($query->row) {
			return $query->rows;
		} else {
			return false;
		}
		
	}
}
?>