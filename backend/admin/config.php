<?php

$basepath   = str_replace('admin', '', dirname(realpath(__FILE__)));

//composer autoload
require $basepath . 'vendor/autoload.php';

if ( file_exists( dirname( __FILE__ ) . '/config-local.php' ) ) {  
    include( dirname( __FILE__ ) . '/config-local.php' );  
}  
else { 

    // HTTP
    define('HTTP_SERVER', 'http://localhost/c2c/admin/');
    define('HTTP_CATALOG', 'http://localhost/c2c/');

    // HTTPS
    define('HTTPS_SERVER', 'http://localhost/c2c/admin/');
    define('HTTPS_CATALOG', 'http://localhost/c2c/');

    // SERBAPAY LOGIN
    define('HTTPS_SERBAPAY', 'http://serbapay.app/');

    // DIR
    define('DIR_APPLICATION', $basepath.'/admin/');
    define('DIR_SYSTEM', $basepath.'/system/');
    define('DIR_DATABASE', $basepath.'/system/database/');
    define('DIR_LANGUAGE', $basepath.'/admin/language/');
    define('DIR_TEMPLATE', $basepath.'/admin/view/template/');
    define('DIR_CONFIG', $basepath.'/system/config/');
    define('DIR_IMAGE', $basepath.'/image/');
    define('DIR_CACHE', $basepath.'/system/cache/');
    define('DIR_DOWNLOAD', $basepath.'/download/');
    define('DIR_LOGS', $basepath.'/system/logs/');
    define('DIR_CATALOG', $basepath.'/catalog/');


    define('REDIS_ENABLE', getenv('REDIS_ENABLE')?getenv('REDIS_ENABLE'):0);
    define('REDIS_HOST', getenv('REDIS_HOST')?getenv('REDIS_HOST'):'192.168.10.10');
    define('REDIS_PORT', getenv('REDIS_PORT')?getenv('REDIS_PORT'):'6379');
    define('REDIS_PORTOCAL', getenv('REDIS_PORTOCAL')?getenv('REDIS_PORTOCAL'):'tcp');

    //cache cron job
    define('CACHE_CRON_ENABLE', getenv('CACHE_CRON_ENABLE')?getenv('CACHE_CRON_ENABLE'):0);

    // DB
    define('DB_DRIVER', 'mysqli');
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', '');
    define('DB_DATABASE', 'c2c');
    define('DB_PREFIX', 'oc_');
}


//REQUIRED REDIS
require $basepath . 'vendor/predis/predis/autoload.php';