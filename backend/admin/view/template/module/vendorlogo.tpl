<?php echo $header; ?>
<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
		<tr>
          <td><?php echo $entry_vendors_image; ?></td>
		  <td><input type="text" name="vw_image" value="<?php if ($vw_image) { echo $vw_image; } else { echo '135'; } ?>" size="8"> <input type="text" name="vh_image" value="<?php if ($vh_image) { echo $vh_image; } else { echo '60'; } ?>" size="8"></td>
		</tr> 
		<tr>
          <td><?php echo $entry_product_image; ?></td>
		  <td><input type="text" name="pvw_image" value="<?php if ($pvw_image) { echo $pvw_image; } else { echo '120'; } ?>" size="8"> <input type="text" name="pvh_image" value="<?php if ($pvh_image) { echo $pvh_image; } else { echo '30'; } ?>" size="8"></td>
		</tr> 
		<tr>
          <td><?php echo $entry_infomation; ?></td>
		  <td><?php if ($vendor_information) { ?>
                <input type="radio" name="vendor_information" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="vendor_information" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="vendor_information" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="vendor_information" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
		</tr>
		<tr>
          <td><?php echo $entry_review; ?></td>
		  <td><?php if ($vendor_review) { ?>
                <input type="radio" name="vendor_review" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="vendor_review" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="vendor_review" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="vendor_review" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
		</tr>
		<tr>
          <td><?php echo $entry_mode; ?></td>
		  <td><?php if ($display_mode) { ?>
                <input type="radio" name="display_mode" value="1" checked="checked" />
                <?php echo $text_yes; ?>
                <input type="radio" name="display_mode" value="0" />
                <?php echo $text_no; ?>
                <?php } else { ?>
                <input type="radio" name="display_mode" value="1" />
                <?php echo $text_yes; ?>
                <input type="radio" name="display_mode" value="0" checked="checked" />
                <?php echo $text_no; ?>
                <?php } ?></td>
            </tr>
		</tr> 
	  	<tr>
          <td><?php echo $entry_vendor; ?></td>
          <td><table>
            <tr>
            <td style="padding: 0;"><select multiple="multiple" id="totalvendors" size="10" style="width: 350px;">
			<?php foreach ($vendors as $vendor) { ?>
			<?php if (!in_array($vendor['vendor_id'],$vendors_selected)) { ?>
			<option value="<?php echo $vendor['vendor_id']; ?>"><?php echo $vendor['vendor_name']; ?></option>
			<?php } } ?>
            </select></td>
            <td style="vertical-align: middle;"><input type="button" value="--&gt;" onclick="addRelated();" />
            <br />
            <input type="button" value="&lt;--" onclick="removeSelectedVendors();" /></td>
            <td style="padding: 0;"><select multiple="multiple" id="SelectedVendors" size="10" style="width: 350px;">
			
			<?php if ($vendors_selected) { ?>
			<?php foreach ($vendors as $vendor) { ?>
			<?php if (in_array($vendor['vendor_id'],$vendors_selected)) { ?>
				<option value="<?php echo $vendor['vendor_id']; ?>"><?php echo $vendor['vendor_name']; ?></option>
			<?php } } } ?>
			</select></td>
          </tr>
	      </table>
          <div id="vendors_selected">
		  <?php if ($vendors_selected) { ?>	
		   <?php foreach ($vendors as $vendor) { ?>
			<?php if (in_array($vendor['vendor_id'],$vendors_selected)) { ?>
			<input type="hidden" name="vendors_selected[]" value="<?php echo $vendor['vendor_id']; ?>" />
		  <?php } } } ?>
        </div></td>
        </tr>
	  </table>
	  <table id="module" class="list">
        <thead>
          <tr>
		    <td class="left"><?php echo $entry_logos; ?></td>
            <td class="left"><?php echo $entry_limit; ?></td>			
			<td class="left"><?php echo $entry_scroll; ?></td>
            <td class="left"><?php echo $entry_image; ?></td>
            <td class="left"><?php echo $entry_layout; ?></td>
            <td class="left"><?php echo $entry_position; ?></td>
            <td class="left"><?php echo $entry_status; ?></td>
            <td class="right"><?php echo $entry_sort_order; ?></td>
            <td></td>
          </tr>
        </thead>
        <?php $module_row = 0; ?>
        <?php foreach ($modules as $module) { ?>
        <tbody id="module-row<?php echo $module_row; ?>">
          <tr>
		    <td class="left"><input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][logos]" value="<?php echo $module['logos']; ?>" size="1" /></td>
            <td class="left"><input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][limit]" value="<?php echo $module['limit']; ?>" size="1" /></td>            
			<td class="left"><input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][scroll]" value="<?php echo $module['scroll']; ?>" size="1" />
			<td class="left"><input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][image_width]" value="<?php echo $module['image_width']; ?>" size="3" />
              <input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][image_height]" value="<?php echo $module['image_height']; ?>" size="3" />
              <?php if (isset($error_image[$module_row])) { ?>
              <span class="error"><?php echo $error_image[$module_row]; ?></span>
              <?php } ?></td>
            <td class="left"><select name="vendorlogo_module[<?php echo $module_row; ?>][layout_id]">
                <?php foreach ($layouts as $layout) { ?>
                <?php if ($layout['layout_id'] == $module['layout_id']) { ?>
                <option value="<?php echo $layout['layout_id']; ?>" selected="selected"><?php echo $layout['name']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $layout['layout_id']; ?>"><?php echo $layout['name']; ?></option>
                <?php } ?>
                <?php } ?>
              </select></td>
            <td class="left"><select name="vendorlogo_module[<?php echo $module_row; ?>][position]">
                <?php if ($module['position'] == 'content_top') { ?>
                <option value="content_top" selected="selected"><?php echo $text_content_top; ?></option>
                <?php } else { ?>
                <option value="content_top"><?php echo $text_content_top; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'content_bottom') { ?>
                <option value="content_bottom" selected="selected"><?php echo $text_content_bottom; ?></option>
                <?php } else { ?>
                <option value="content_bottom"><?php echo $text_content_bottom; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_left') { ?>
                <option value="column_left" selected="selected"><?php echo $text_column_left; ?></option>
                <?php } else { ?>
                <option value="column_left"><?php echo $text_column_left; ?></option>
                <?php } ?>
                <?php if ($module['position'] == 'column_right') { ?>
                <option value="column_right" selected="selected"><?php echo $text_column_right; ?></option>
                <?php } else { ?>
                <option value="column_right"><?php echo $text_column_right; ?></option>
                <?php } ?>
              </select></td>
            <td class="left"><select name="vendorlogo_module[<?php echo $module_row; ?>][status]">
                <?php if ($module['status']) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select></td>
            <td class="right"><input type="text" name="vendorlogo_module[<?php echo $module_row; ?>][sort_order]" value="<?php echo $module['sort_order']; ?>" size="3" /></td>
            <td class="left"><a onclick="$('#module-row<?php echo $module_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $module_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="8"></td>
            <td class="left"><a onclick="addModule();" class="button"><?php echo $button_add_module; ?></a></td>
          </tr>
        </tfoot>
      </table>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
var module_row = <?php echo $module_row; ?>;

function addModule() {	
	html  = '<tbody id="module-row' + module_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><input type="text" name="vendorlogo_module[' + module_row + '][logos]" value="8" size="1" /></td>';
	html += '    <td class="left"><input type="text" name="vendorlogo_module[' + module_row + '][limit]" value="5" size="1" /></td>';	
	html += '    <td class="left"><input type="text" name="vendorlogo_module[' + module_row + '][scroll]" value="5" size="1" /></td>';
	html += '    <td class="left"><input type="text" name="vendorlogo_module[' + module_row + '][image_width]" value="80" size="3" /> <input type="text" name="vendorlogo_module[' + module_row + '][image_height]" value="80" size="3" /></td>';		
	html += '    <td class="left"><select name="vendorlogo_module[' + module_row + '][layout_id]">';
	<?php foreach ($layouts as $layout) { ?>
	html += '      <option value="<?php echo $layout['layout_id']; ?>"><?php echo addslashes($layout['name']); ?></option>';
	<?php } ?>
	html += '    </select></td>';
	html += '    <td class="left"><select name="vendorlogo_module[' + module_row + '][position]">';
	html += '      <option value="content_top"><?php echo $text_content_top; ?></option>';
	html += '      <option value="content_bottom"><?php echo $text_content_bottom; ?></option>';
	html += '      <option value="column_left"><?php echo $text_column_left; ?></option>';
	html += '      <option value="column_right"><?php echo $text_column_right; ?></option>';
	html += '    </select></td>';
	html += '    <td class="left"><select name="vendorlogo_module[' + module_row + '][status]">';
    html += '      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>';
    html += '      <option value="0"><?php echo $text_disabled; ?></option>';
    html += '    </select></td>';
	html += '    <td class="right"><input type="text" name="vendorlogo_module[' + module_row + '][sort_order]" value="" size="3" /></td>';
	html += '    <td class="left"><a onclick="$(\'#module-row' + module_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#module tfoot').before(html);
	
	module_row++;
}
//--></script>
<script type="text/javascript"><!--
function addRelated() {
	$('#totalvendors :selected').each(function() {
		$('#totalvendors option[value=\'' + $(this).attr('value') + '\']').remove();
		$('#SelectedVendors').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
		$('#vendors_selected').append('<input type="hidden" name="vendors_selected[]" value="' + $(this).attr('value') + '" />');
	});
}

function removeSelectedVendors() {
	$('#SelectedVendors :selected').each(function() {
		$(this).remove();
		$('#vendors_selected input[value=\'' + $(this).attr('value') + '\']').remove();
		$('#totalvendors').append('<option value="' + $(this).attr('value') + '">' + $(this).text() + '</option>');
	});
}
//--></script>
<?php echo $footer; ?>