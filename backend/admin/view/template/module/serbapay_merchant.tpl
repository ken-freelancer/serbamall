<?php echo $header; ?>
    <div id="content">
        <div class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
            <?php } ?>
        </div>
        <?php if ($error_warning) { ?>
            <div class="warning"><?php echo $error_warning; ?></div>
        <?php } ?>
        <div class="box">
            <div class="heading">
                <h1><img src="view/image/payment.png" alt="" /> <?php echo $heading_title; ?></h1>
                <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
            </div>
            <div class="content">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                    <div id="tab_general" class="page">
                        <table class="form">

                            <tr>
                                <td><?php echo $entry_status; ?></td>
                                <td><select name="serbapay_merchant_status">
                                        <?php if ($serbapay_merchant_status) { ?>
                                            <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                            <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                            <option value="1"><?php echo $text_enabled; ?></option>
                                            <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select></td>
                            </tr>

                            <tr>
                                <td width="25%"><span class="required">*</span> <?php echo $entry_vendor; ?></td>
                                <td><input type="text" name="serbapay_merchant_id" value="<?php echo $serbapay_merchant_id; ?>" style="width:50%" />
                                    <br />
                                    <?php if ($error_vendor) { ?>
                                        <span class="error"><?php echo $error_vendor; ?></span>
                                    <?php } ?></td>
                            </tr>
                            <tr>
                                <td><span class="required">*</span> <?php echo $entry_password; ?></td>
                                <td><input type="text" name="serbapay_merchant_password" value="<?php echo $serbapay_merchant_password; ?>" style="width:50%" />
                                    <br />
                                    <?php if ($error_password) { ?>
                                        <span class="error"><?php echo $error_password; ?></span>
                                    <?php } ?></td>
                            </tr>


                        </table>
                    </div>
                </form>
            </div>
        </div>
    </div>
<?php echo $footer; ?>