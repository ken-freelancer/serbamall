<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
   <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title_profile; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
    <div id="tabs" class="htabs">
		<a href="#tab-general"><?php echo $tab_general; ?></a>
		<a href="#tab-address"><?php echo $tab_address; ?></a>
		<a href="#tab-gallery">Gallery</a>
		<a href="#tab-finance"><?php echo $tab_finance; ?></a>
        <?php if ($this->config->get('multi_store_activated')) { ?>
            <a href="#tab-payment"><?php echo $tab_payment; ?></a>
        <?php } ?>
        <?php //Multiple Shipping ?>
        <a href="#tab-utm-shipping-setting"><?php echo $tab_utm_setting; ?></a>
        <a href="#tab-utm-shipping"><?php echo $tab_utm_shipping; ?></a>
        <?php //.Multiple Shipping ?>
	</div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div id="tab-general">
	  <table class="form">
	    <tr>
          <td><?php echo $entry_company_id; ?></td>
          <td><input name="company_id" value="<?php echo $company_id; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_tax_id; ?></td>
          <td><input name="tax_id" value="<?php echo $tax_id; ?>" size="25" /></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_vendor_firstname) { ?>
            <span class="error"><?php echo $error_vendor_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_vendor_lastname) { ?>
            <span class="error"><?php echo $error_vendor_lastname; ?></span>
            <?php } ?></td>
        </tr>		
		<tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input name="telephone" value="<?php echo $telephone; ?>" size="25" />
		  <?php if ($error_vendor_telephone) { ?>
            <span class="error"><?php echo $error_vendor_telephone; ?></span>
            <?php } ?>
		  </td>
        </tr>
		<tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input name="email" value="<?php echo $email; ?>" size="25" />
		   <?php if ($error_vendor_email) { ?>
            <span class="error"><?php echo $error_vendor_email; ?></span>
           <?php } ?>
		  </td>
        </tr>
		<tr>
          <td><?php echo $entry_fax; ?></td>
          <td><input name="fax" value="<?php echo $fax; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_description; ?></td>
          <td><textarea name="vendor_description" cols="68" rows="3" ><?php echo $vendor_description; ?></textarea></td>
        </tr>
		<tr>
          <td><?php echo $entry_store_url; ?></td>
          <td><textarea name="store_url" cols="68" rows="3" ><?php echo $store_url; ?></textarea></td>
        </tr>
        <tr>
		  <td><?php echo $entry_image; ?></td>
          <td><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
              <input type="hidden" name="vendor_image" value="<?php echo $vendor_image; ?>" id="image" />
              <a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
        </tr>
		<!--vendor banner-->
		<tr>
		  <td>Image:<br><span class="help">Company Banner(990x150)</span></td>
          <td><div class="image"><img src="<?php echo $banner_thumb; ?>" alt="" id="banner_thumb" /><br />
              <input type="hidden" name="vendor_banner" value="<?php echo $vendor_banner; ?>" id="banner_image" />
              <a onclick="image_upload('banner_image', 'banner_thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#banner_thumb').attr('src', '<?php echo $banner_no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
        </tr>
		</table>
		</div>
		
	  <div id="tab-address">
	  <table class="form">
		<tr>
          <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_vendor_address_1) { ?>
            <span class="error"><?php echo $error_vendor_address_1; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_address_2; ?></td>
          <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_city; ?></td>
          <td><input type="text" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_vendor_city) { ?>
            <span class="error"><?php echo $error_vendor_city; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_vendor_postcode) { ?>
            <span class="error"><?php echo $error_vendor_postcode; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_country; ?></td>
          <td><select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor_profile/zone&token=<?php echo $token; ?>&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
			  <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_vendor_country) { ?>
            <span class="error"><?php echo $error_vendor_country; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td><select name="zone_id">
            </select>
            <?php if ($error_vendor_zone) { ?>
            <span class="error"><?php echo $error_vendor_zone; ?></span>
            <?php } ?></td>
        </tr>
	   </table>
	   </div>
	   
	   <!-- vendor gallery -->
	   <div id="tab-gallery">
          <table id="images" class="list">
            <thead>
              <tr>
                <td class="left">Image:</td>
                <td class="right">Sort Order:</td>
                <td></td>
              </tr>
            </thead>			
            <?php $image_row = 0; ?>
            <?php foreach ($vendor_gallery as $vendor_image) { ?>
			<?php if($image_row > 3){ continue; } ?>
            <tbody id="image-row<?php echo $image_row; ?>">
              <tr>
                <td class="left">
					<div class="image"><img src="<?php echo $vendor_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                    <input type="hidden" name="vendor_gallery[<?php echo $image_row; ?>][image]" value="<?php echo $vendor_image['image']; ?>" id="image<?php echo $image_row; ?>" />
                    <br />
                    <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
					</div>
				</td>
                <td class="right"><input type="text" name="vendor_gallery[<?php echo $image_row; ?>][sort_order]" value="<?php echo $vendor_image['sort_order']; ?>" size="2" /></td>
                <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove(); checkStatus();" class="button">Remove</a></td>
              </tr>
            </tbody>
            <?php $image_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
				<?php if($image_row < 3){ ?>
                <td colspan="2"></td>
                <td class="left"><a onclick="addImage();" id="a_LimitAddButton" class="button">Add Image</a></td>
				<?php } ?>
              </tr>
            </tfoot>
          </table>
        </div>

        <?php //Multiple Shipping ?>
        <div id="tab-utm-shipping-setting">
            <table class="form">
                <tr>
                    <td><?php echo $entry_measurement; ?></td>
                    <td><select name="measurement_id">
                            <?php if (($measurement_id==0) || (empty($measurement_id)) ) { ?>
                                <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                <option value="1"><?php echo $text_nws_text; ?></option>
                                <option value="2"><?php echo $text_wbsp_text; ?></option>
                                <option value="3"><?php echo $text_tapv_text; ?></option>
                            <?php } ?>
                            <?php if ($measurement_id==1) { ?>
                                <option value="0"><?php echo $text_none; ?></option>
                                <option value="1" selected="selected"><?php echo $text_nws_text; ?></option>
                                <option value="2"><?php echo $text_wbsp_text; ?></option>
                                <option value="3"><?php echo $text_tapv_text; ?></option>
                            <?php } ?>
                            <?php if ($measurement_id==2) { ?>
                                <option value="0"><?php echo $text_none; ?></option>
                                <option value="1"><?php echo $text_nws_text; ?></option>
                                <option value="2" selected="selected"><?php echo $text_wbsp_text; ?></option>
                                <option value="3"><?php echo $text_tapv_text; ?></option>
                            <?php } ?>
                            <?php if ($measurement_id==3) { ?>
                                <option value="0"><?php echo $text_none; ?></option>
                                <option value="1"><?php echo $text_nws_text; ?></option>
                                <option value="2"><?php echo $text_wbsp_text; ?></option>
                                <option value="3" selected="selected"><?php echo $text_tapv_text; ?></option>
                            <?php } ?>
                        </select>
                    </td>
                </tr>
            </table>
        </div>
        <div id="tab-utm-shipping">
            <table id="ultm-shipping" class="list">
                <thead>
                <tr>
                    <td class="left"><?php echo $entry_shipping_courier; ?></td>
                    <td class="right"><?php echo $entry_shipping_rate; ?></td>
                    <td class="right"><?php echo $entry_shipping_geozone; ?></td>
                    <td></td>
                </tr>
                </thead>
                <?php $ultm_shipping_row = 0; ?>
                <?php foreach ($vendor_ultimate_shippings as $vendor_ultimate_shipping) { ?>
                    <tbody id="ultm-shipping-row<?php echo $ultm_shipping_row; ?>">
                    <tr>
                        <input type="hidden" name="vendor_ultimate_shipping[<?php echo $ultm_shipping_row; ?>][vutm_shipping_id]" value="<?php echo $vendor_ultimate_shipping['vutm_shipping_id']; ?>" />
                        <td class="left"><select name="vendor_ultimate_shipping[<?php echo $ultm_shipping_row; ?>][courier_id]">
                                <?php foreach ($couriers as $courier) { ?>
                                    <?php if ($courier['courier_id'] == $vendor_ultimate_shipping['courier_id']) { ?>
                                        <option value="<?php echo $courier['courier_id']; ?>" selected="selected"><?php echo $courier['courier_name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                        <td class="right"><input type="text" name="vendor_ultimate_shipping[<?php echo $ultm_shipping_row; ?>][shipping_rate]" value="<?php echo $vendor_ultimate_shipping['shipping_rate']; ?>" /></td>
                        <td class="right"><select name="vendor_ultimate_shipping[<?php echo $ultm_shipping_row; ?>][geo_zone_id]">
                                <?php foreach ($geo_zones as $geo_zone) { ?>
                                    <?php if ($geo_zone['geo_zone_id'] == $vendor_ultimate_shipping['geo_zone_id']) { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>" selected="selected"><?php echo $geo_zone['name']; ?></option>
                                    <?php } else { ?>
                                        <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>
                                    <?php } ?>
                                <?php } ?>
                            </select></td>
                        <td class="left"><a onclick="$('#ultm-shipping-row<?php echo $ultm_shipping_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
                    </tr>
                    </tbody>
                    <?php $ultm_shipping_row++; ?>
                <?php } ?>
                <tfoot>
                <tr>
                    <td colspan="3"></td>
                    <td class="left"><a onclick="add_ultm_Shipping();" class="button"><?php echo $button_add_shipping; ?></a></td>
                </tr>
                </tfoot>
            </table>
        </div>
        <?php //.Multiple Shipping ?>
	   
	  <div id="tab-finance">
	  <table class="form">		
		<tr>
          <td><span class="required">*</span><?php echo $entry_paypal_email; ?></td>
          <td><input name="paypal_email" value="<?php echo $paypal_email; ?>" size="25" />
		  <?php if ($error_vendor_paypal_email) { ?>
            <span class="error"><?php echo $error_vendor_paypal_email; ?></span>
          <?php } ?>
		  </td>
        </tr>
		<tr>
          <td>Bank Account No.<?php //echo $entry_iban; ?></td>
          <td><input name="iban" value="<?php echo $iban; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_bank_name; ?></td>
          <td><input name="bank_name" value="<?php echo $bank_name; ?>" size="25" /></td>
        </tr>
		<?php /* 
		<tr style="display: none;">
          <td><?php echo $entry_bank_addr; ?></td>
          <td><textarea name="bank_address" cols="68" rows="5" ><?php echo $bank_address; ?></textarea></td>
        </tr>
		<tr style="display: none;">
          <td><?php echo $entry_swift_bic; ?></td>
          <td><input name="swift_bic" value="<?php echo $swift_bic; ?>" size="25" /></td>
        </tr>
		*/ ?>
        </table>
		</div>
	   <?php if ($this->config->get('multi_store_activated')) { ?>
	   <div id="tab-payment">
	   <table class="form">
		<tr>
          <td><?php echo $entry_accept_paypal; ?></td>
          <td><select name="accept_paypal">
                  <?php if ($accept_paypal) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
		<tr>
          <td><?php echo $entry_accept_cheques; ?></td>
          <td><select name="accept_cheques">
                  <?php if ($accept_cheques) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
		<tr>
          <td><?php echo $entry_accept_bank_transfer; ?></td>
          <td><select name="accept_bank_transfer">
                  <?php if ($accept_bank_transfer) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
		</table>
		</div>
		<?php } else { ?>
		  <input type="hidden" name="accept_paypal" value="0" /> 
		  <input type="hidden" name="accept_cheques" value="0" />
		  <input type="hidden" name="accept_bank_transfer" value="0" />
	    <?php } ?>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor_profile/zone&token=<?php echo $token; ?>&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="vendor_gallery[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');">Clear</a></div></td>';
	html += '    <td class="right"><input type="text" name="vendor_gallery[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove(); checkStatus();" class="button">Remove</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
	
	if(image_row >= 3)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}
	
}

function checkStatus(){
	
	var stt_image_row = $('#tab-gallery tbody').length;
	console.log(stt_image_row);
	if(stt_image_row >= 3)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}else{
		$('#a_LimitAddButton').css('display', 'inline-block');
	}
	
}
//--></script>

<?php //Multiple Shipping ?>
    <script type="text/javascript"><!--
        var ultm_shipping_row = <?php echo $ultm_shipping_row; ?>;

        function add_ultm_Shipping() {
            html  = '<tbody id="ultm-shipping-row' + ultm_shipping_row + '">';
            html += '  <tr>';
            html += '  <td class="text-left"><select name="vendor_ultimate_shipping[' + ultm_shipping_row + '][courier_id]">';
            <?php foreach ($couriers as $courier) { ?>
            html += '      <option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';
            html += '    <input type="hidden" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][vutm_shipping_id]" value="" />';
            html += '    <td class="right"><input type="text" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][shipping_rate]" value="" /></td>';
            html += '    <td class="right"><select name="vendor_ultimate_shipping[' + ultm_shipping_row + '][geo_zone_id]">';
            <?php foreach ($geo_zones as $geo_zone) { ?>
            html += '      <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';

            html += '    <td class="left"><a onclick="$(\'#ultm-shipping-row' + ultm_shipping_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
            html += '  </tr>';
            html += '</tbody>';

            $('#ultm-shipping tfoot').before(html);
            $('#ultm-shipping-row' + ultm_shipping_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

            ultm_shipping_row++;
        }
        //--></script>
<?php //.Multiple Shipping ?>
<?php echo $footer; ?>