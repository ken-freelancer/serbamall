<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/order.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a href="<?php echo $insert; ?>" class="button"><?php echo $button_insert; ?></a><a onclick="$('form').submit();" class="button"><?php echo $button_delete; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="list">
          <thead>
            <tr>
              <td width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></td>
              <td class="left"><?php if ($sort == 't.tag_name') { ?>
                <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>">Tag Name</a>
                <?php } else { ?>
                <a href="<?php echo $sort_name; ?>"Tag Name</a>
                <?php } ?></td>
                <td class="left"><?php if ($sort == 't.date_added') { ?>
                        <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>">Tag Created</a>
                    <?php } else { ?>
                        <a href="<?php echo $sort_date_added; ?>">Tag Created</a>
                    <?php } ?></td>
                <td></td>
            </tr>
          </thead>
          <tbody>
          <tr class="filter">
              <td></td>
              <td align="left"><input type="text" name="filter_name" value="<?php echo $filter_name; ?>" size="20" style="" /></td>
              <td><input type="text" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
              <td align="right"><a onclick="filter();" class="button">Filter</a></td>
          </tr>
            <?php if ($tags) { ?>
            <?php foreach ($tags as $tag) { ?>
            <tr>
              <td style="text-align: center;"><?php if ($tag['selected']) { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $tag['tag_id']; ?>" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="selected[]" value="<?php echo $tag['tag_id']; ?>" />
                <?php } ?></td>
              <td class="left"><?php echo $tag['name']; ?></td>
              <td class="left"><?php echo $tag['date_added']; ?></td>
                <td></td>
            </tr>
            <?php } ?>
            <?php } else { ?>
            <tr>
              <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </form>
      <div class="pagination"><?php echo $pagination; ?></div>
    </div>
  </div>
</div>

    <script type="text/javascript"><!--
        function filter() {
            url = 'index.php?route=catalog/tag&token=<?php echo $token; ?>';

            var filter_name = $('input[name=\'filter_name\']').attr('value');

            if (filter_name) {
                url += '&filter_name=' + encodeURIComponent(filter_name);
            }

            var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');

            if (filter_date_added) {
                url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
            }


            location = url;
        }
        //--></script>
    <script type="text/javascript"><!--
        $('#form input').keydown(function(e) {
            if (e.keyCode == 13) {
                filter();
            }
        });
        //--></script>
    <script type="text/javascript"><!--
        $(document).ready(function() {
            $('.date').datepicker({dateFormat: 'yy-mm-dd'});
        });
        //--></script>
<?php echo $footer; ?>