<?php echo $header; ?>
    <!-- / select2 -->
    <link href="view/stylesheet/select2/select2.css" media="all" rel="stylesheet" type="text/css" />
    <script src="view/javascript/jquery/select2/select2.js" type="text/javascript"></script>
    <script id="script_select2">
        $(document).ready(function () {
            $.getJSON('<?php echo $json_tag; ?>', function(opts){
                $(".select2-tags").select2({
                    tags: opts,
                    tokenSeparators: [",", " "],
                    placeholder: "Type your tag here... "
                });
            });
        });
    </script>

    <div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/information.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <table class="form">
          <tr>
            <td><span class="required">*</span> Tag Name <br />(Add those not in the list)</td>
            <td>
              <input type="text" class="select2-tags" name="tag_name" id="tag_name" value="" style="width: 100%;" />
              <?php  if (isset($error_name)) {?>
              <span class="error"><?php echo $error_name; ?></span><br />
              <?php } ?>
              </td>
          </tr>
        </table>

      </form>
    </div>
  </div>
</div>
<?php echo $footer; ?>