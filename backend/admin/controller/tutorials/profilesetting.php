<?php

class ControllerTutorialsProfilesetting extends Controller {

private $error = array();

public function index() {

		$this->document->setTitle($this->language->get('heading_title'));

		$this->template = 'tutorials/profile_setting.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

			$this->response->setOutput($this->render());
	}

}

?>