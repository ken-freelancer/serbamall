<?php

class ControllerModuleSerbaPayMerchant extends Controller {

    // Presets
    private $error = array();

    public function index() {

        // Load Dependencies
        $this->load->language('module/serbapay_merchant');
        $this->load->model('setting/setting');

        // Save Incoming Data
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            
            // Edit Settings
            $this->model_setting_setting->editSetting('serbapay_merchant', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->redirect($this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'));
        }

        // Language Init
        $this->data['heading_title'] = strip_tags($this->language->get('heading_title'));

        $this->data['text_enabled'] = $this->language->get('text_enabled');
        $this->data['text_disabled'] = $this->language->get('text_disabled');
        $this->data['text_copyright'] = sprintf($this->language->get('text_copyright'), date('Y'));

        $this->data['entry_status'] = $this->language->get('entry_status');
        $this->data['entry_debug'] = $this->language->get('entry_debug');

        $this->data['entry_vendor'] = $this->language->get('entry_vendor');
        $this->data['entry_password'] = $this->language->get('entry_password');
        $this->data['entry_provider'] = $this->language->get('entry_provider');
        $this->data['entry_key'] = $this->language->get('entry_key');
        $this->data['entry_secret'] = $this->language->get('entry_secret');
        $this->data['entry_scope'] = $this->language->get('entry_scope');
        $this->data['entry_sort_order'] = $this->language->get('entry_sort_order');

        $this->data['button_save'] = $this->language->get('button_save');
        $this->data['button_cancel'] = $this->language->get('button_cancel');
        $this->data['button_add_row'] = $this->language->get('button_add_row');
        $this->data['button_remove'] = $this->language->get('button_remove');

        // Process Errors
         if (isset($this->error['warning'])) {
            $this->data['error_warning'] = $this->error['warning'];
        } else {
            $this->data['error_warning'] = '';
        }
        if (isset($this->error['vendor'])) {
            $this->data['error_vendor'] = $this->error['vendor'];
        } else {
            $this->data['error_vendor'] = '';
        }

        if (isset($this->error['password'])) {
            $this->data['error_password'] = $this->error['password'];
        } else {
            $this->data['error_password'] = '';
        }
        // Generate Breadcrumbs
        $this->data['breadcrumbs'] = array();

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_module'),
            'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->data['breadcrumbs'][] = array(
            'text'      => str_replace('Beta', '', $this->language->get('heading_title')),
            'href'      => $this->url->link('module/serbapay_merchant', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        // Set Page Title
        $this->document->setTitle(str_replace('Beta', '', strip_tags($this->language->get('heading_title'))));


        // Basic Variables
        $this->data['action'] = $this->url->link('module/serbapay_merchant', 'token=' . $this->session->data['token'], 'SSL');
        $this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

        // Process Variables

        if (isset($this->request->post['serbapay_merchant_id'])) {
            $this->data['serbapay_merchant_id'] = $this->request->post['serbapay_merchant_id'];
        } else {
            $this->data['serbapay_merchant_id'] = $this->config->get('serbapay_merchant_id');
        }

        if (isset($this->request->post['serbapay_merchant_password'])) {
            $this->data['serbapay_merchant_password'] = $this->request->post['serbapay_merchant_password'];
        } else {
            $this->data['serbapay_merchant_password'] = $this->config->get('serbapay_merchant_password');
        }

        if (isset($this->request->post['serbapay_merchant_status'])) {
            $this->data['serbapay_merchant_status'] = $this->request->post['serbapay_merchant_status'];
        } else {
            $this->data['serbapay_merchant_status'] = $this->config->get('serbapay_merchant_status');
        }

        // Load Template
        $this->template = 'module/serbapay_merchant.tpl';
        $this->children = array(
            'common/header',
            'common/footer'
        );

        // Rendering
        $this->response->setOutput($this->render());
    }

    private function validate() {

        // Check Permissions
        if (!$this->user->hasPermission('modify', 'module/serbapay_merchant')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        if (!$this->request->post['serbapay_merchant_id']) {
            $this->error['vendor'] = $this->language->get('error_vendor');
        }

        if (!$this->request->post['serbapay_merchant_password']) {
            $this->error['password'] = $this->language->get('error_password');
        }

        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }
}
