<?php
class ControllerShippingMVDUSRPV extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('shipping/mvd_usr_pv');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		
		$checkmytb = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "vendor_ultimate_shipping'");		
		if (!$checkmytb->row) {
			$this->db->query("CREATE TABLE `".DB_PREFIX."product_ultm_shipping` 
			(`product_ultm_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
			  `vutm_shipping_id` int(11) NOT NULL,
			  `product_id` int(11) NOT NULL,
			  PRIMARY KEY (`product_ultm_shipping_id`))
			  ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin AUTO_INCREMENT=1;");
			  
			$this->db->query("CREATE TABLE `".DB_PREFIX."vendor_ultimate_shipping` 
			(`vutm_shipping_id` int(11) NOT NULL AUTO_INCREMENT,
			 `courier_id` int(11) NOT NULL,
			 `vendor_id` int(11) NOT NULL,
			 `priority` int(5) NOT NULL,
			 `shipping_rate` text NOT NULL,
			 `geo_zone_id` int(11) NOT NULL,
			  PRIMARY KEY (`vutm_shipping_id`))
			  ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1;");
			  
			 $this->db->query("ALTER TABLE `".DB_PREFIX."vendors` ADD `measurement_id` TINYINT NOT NULL DEFAULT '0';");
		}
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('mvd_usr_pv', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');		
			$this->redirect($this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_checkout_name'] = $this->language->get('text_checkout_name');
		$this->data['text_combo_text'] = $this->language->get('text_combo_text');
		$this->data['text_input_text'] = $this->language->get('text_input_text');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_edit'] = $this->language->get('text_edit');
		$this->data['text_nws_text'] = $this->language->get('text_nws_text');
		$this->data['text_wbsp_text'] = $this->language->get('text_wbsp_text');
		$this->data['text_tapv_text'] = $this->language->get('text_tapv_text');

		$this->data['entry_display_mode'] = $this->language->get('entry_display_mode');
		$this->data['entry_measurement'] = $this->language->get('entry_measurement');
		$this->data['entry_module_name'] = $this->language->get('entry_module_name');
		$this->data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$this->data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_shipping'),
			'href'      => $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('shipping/mvd_usr_pv', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
		);
		
		$this->data['action'] = $this->url->link('shipping/mvd_usr_pv', 'token=' . $this->session->data['token'], 'SSL');
		$this->data['cancel'] = $this->url->link('extension/shipping', 'token=' . $this->session->data['token'], 'SSL');
		
		if (isset($this->request->post['mvd_usr_pv_checkout_name'])) {
			$this->data['mvd_usr_pv_checkout_name'] = $this->request->post['mvd_usr_pv_checkout_name'];
		} else {
			$this->data['mvd_usr_pv_checkout_name'] = $this->config->get('mvd_usr_pv_checkout_name');
		}
		
		if (isset($this->request->post['mvd_usr_pv_measurement'])) {
			$this->data['mvd_usr_pv_measurement'] = $this->request->post['mvd_usr_pv_measurement'];
		} else {
			$this->data['mvd_usr_pv_measurement'] = $this->config->get('mvd_usr_pv_measurement');
		}
	
		if (isset($this->request->post['mvd_usr_pv_display'])) {
			$this->data['mvd_usr_pv_display'] = $this->request->post['mvd_usr_pv_display'];
		} else {
			$this->data['mvd_usr_pv_display'] = $this->config->get('mvd_usr_pv_display');
		}

		if (isset($this->request->post['mvd_usr_pv_tax_class_id'])) {
			$this->data['mvd_usr_pv_tax_class_id'] = $this->request->post['mvd_usr_pv_tax_class_id'];
		} else {
			$this->data['mvd_usr_pv_tax_class_id'] = $this->config->get('mvd_usr_pv_tax_class_id');
		}

		if (isset($this->request->post['mvd_usr_pv_status'])) {
			$this->data['mvd_usr_pv_status'] = $this->request->post['mvd_usr_pv_status'];
		} else {
			$this->data['mvd_usr_pv_status'] = $this->config->get('mvd_usr_pv_status');
		}
		
		if (isset($this->request->post['mvd_usr_pv_sort_order'])) {
			$this->data['mvd_usr_pv_sort_order'] = $this->request->post['mvd_usr_pv_sort_order'];
		} else {
			$this->data['mvd_usr_pv_sort_order'] = $this->config->get('mvd_usr_pv_sort_order');
		}
		
		$this->load->model('localisation/tax_class');
		$this->data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		
		$this->template = 'shipping/mvd_usr_pv.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'shipping/mvd_usr_pv')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>