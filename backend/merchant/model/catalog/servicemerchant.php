<?php
class ModelCatalogServiceMerchant extends Model {
	public function addServiceMerchant($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "servicemerchant SET
							`contact` = '" . $data['contact'] . "',
							`address` = '" . $data['address'] . "',
							`featured` = '" . (isset($data['featured']) ? (int)$data['featured'] : 0) . "',
							`email` = '" . $data['email'] . "',
							sort_order = '" . (int)$data['sort_order'] . "',
							status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

		$servicemerchant_id = $this->db->getLastId();

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "servicemerchant SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");
		}

		foreach ($data['servicemerchant_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "servicemerchant_description SET servicemerchant_id = '" . (int)$servicemerchant_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "servicemerchant_to_category
				SET servicemerchant_id = '" . (int)$servicemerchant_id . "', category_id = '" . (int)$category_id . "'");
			}
		}

		$this->cache->delete('servicemerchant_category');
	}

	public function editServiceMerchant($servicemerchant_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "servicemerchant SET
		`contact` = '" . $data['contact'] . "',
		`address` = '" . $data['address'] . "',
		`featured` = '" . (isset($data['featured']) ? (int)$data['featured'] : 0) . "',
		`email` = '" . $data['email'] . "',
		sort_order = '" . (int)$data['sort_order'] . "',
		status = '" . (int)$data['status'] . "',
		 date_modified = NOW() WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		if (isset($data['image'])) {
			$this->db->query("UPDATE " . DB_PREFIX . "servicemerchant SET image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "' WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "servicemerchant_description WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		foreach ($data['servicemerchant_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "servicemerchant_description SET servicemerchant_id = '" . (int)$servicemerchant_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "servicemerchant_to_category WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "servicemerchant_to_category
				SET servicemerchant_id = '" . (int)$servicemerchant_id . "', category_id = '" . (int)$category_id . "'");
			}
		}


		$this->cache->delete('servicemerchant_category');
	}

	public function deleteServiceMerchant($servicemerchant_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "servicemerchant WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "servicemerchant_description WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "servicemerchant_to_category WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		$this->cache->delete('servicemerchant_category');
	} 

	public function getServiceMerchant($servicemerchant_id) {
		$query = $this->db->query("SELECT DISTINCT *
				FROM " . DB_PREFIX . "servicemerchant c LEFT JOIN " . DB_PREFIX . "servicemerchant_description cd2
				ON (c.servicemerchant_id = cd2.servicemerchant_id) WHERE c.servicemerchant_id = '" . (int)$servicemerchant_id . "'
				AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

		return $query->row;
	} 

	public function getServiceMerchants($data) {
		$sql = "SELECT c.servicemerchant_id AS servicemerchant_id, cd1.name , c.sort_order
					FROM " . DB_PREFIX . "servicemerchant c
					LEFT JOIN " . DB_PREFIX . "servicemerchant_description cd1 ON (c.servicemerchant_id = cd1.servicemerchant_id)
					WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_name'])) {
			$sql .= " AND cd1.name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
		}

		$sql .= " GROUP BY c.servicemerchant_id ORDER BY name";

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getServiceMerchantDescriptions($servicemerchant_id) {
		$servicemerchant_description_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant_description WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		foreach ($query->rows as $result) {
			$servicemerchant_description_data[$result['language_id']] = array(
				'name'             => $result['name'],
				'meta_keyword'     => $result['meta_keyword'],
				'meta_description' => $result['meta_description'],
				'description'      => $result['description']
			);
		}

		return $servicemerchant_description_data;
	}	

	public function getServiceMerchantFilters($servicemerchant_id) {
		$servicemerchant_filter_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant_filter WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		foreach ($query->rows as $result) {
			$servicemerchant_filter_data[] = $result['filter_id'];
		}

		return $servicemerchant_filter_data;
	}

	public function getServiceMerchantStores($servicemerchant_id) {
		$servicemerchant_store_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant_to_store WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		foreach ($query->rows as $result) {
			$servicemerchant_store_data[] = $result['store_id'];
		}

		return $servicemerchant_store_data;
	}

	public function getServiceMerchantLayouts($servicemerchant_id) {
		$servicemerchant_layout_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant_to_layout WHERE servicemerchant_id = '" . (int)$servicemerchant_id . "'");

		foreach ($query->rows as $result) {
			$servicemerchant_layout_data[$result['store_id']] = $result['layout_id'];
		}

		return $servicemerchant_layout_data;
	}

	public function getServiceMerchantCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant_to_category WHERE servicemerchant_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}


	public function getTotalServiceMerchants() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "servicemerchant");

		return $query->row['total'];
	}	

	public function getTotalServiceMerchantsByImageId($image_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "servicemerchant WHERE image_id = '" . (int)$image_id . "'");

		return $query->row['total'];
	}

}