<?php
class ModelCatalogPrintVoucher extends Model {
    public function getTotalPrintVoucher($data = array()) {
		
		$sql = "SELECT COUNT(DISTINCT pv.voucher_no) AS total FROM " . DB_PREFIX . "pvoucher pv
				LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
				LEFT JOIN " . DB_PREFIX . "vendor vd ON (pd.product_id = vd.vproduct_id) 
				LEFT JOIN " . DB_PREFIX . "vendors vds ON (vd.vendor = vds.vendor_id)";
			


		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";

		if (!empty($data['filter_voucher_no'])) {
			$sql .= " AND pv.voucher_no LIKE '%" . $this->db->escape($data['filter_voucher_no']) . "%'";
		}
		
		if (isset($data['filter_vendor']) && !is_null($data['filter_vendor'])) {
			$sql .= " AND vds.vendor_name LIKE '%" . $this->db->escape($data['filter_vendor']) . "%'";
		}
		
		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (isset($data['filter_created_at']) && !is_null($data['filter_created_at'])) {
			$sql .= " AND pv.created_at = '%" . $this->db->escape($data['filter_created_at']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND pv.status = '" . (int)$data['filter_status'] . "'";
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
	}

	public function getPrintVouchers($data = array()) {
		
		$sql = "SELECT *, pv.status AS pv_status FROM " . DB_PREFIX . "pvoucher pv
				LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id) 
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
				LEFT JOIN " . DB_PREFIX . "vendor vd ON (pd.product_id = vd.vproduct_id) 
				LEFT JOIN " . DB_PREFIX . "vendors vds ON (vd.vendor = vds.vendor_id)"; 
			

		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "'"; 

		if (!empty($data['filter_voucher_no'])) {
			$sql .= " AND pv.voucher_no LIKE '%" . $this->db->escape($data['filter_voucher_no']) . "%'";
		}
		
		if (isset($data['filter_vendor']) && !is_null($data['filter_vendor'])) {
			$sql .= " AND vds.vendor_name LIKE '%" . $this->db->escape($data['filter_vendor']) . "%'";
		}
		
		if (!empty($data['filter_product'])) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_product']) . "%'";
		}

		if (isset($data['filter_created_at']) && !is_null($data['filter_created_at'])) {
			$sql .= " AND pv.created_at = '%" . $this->db->escape($data['filter_created_at']) . "%'";
		}

		if (isset($data['filter_status']) && !is_null($data['filter_status'])) {
			$sql .= " AND pv.status = '" . (int)$data['filter_status'] . "'";
		}

		$sql .= " GROUP BY pv.voucher_no";

		$sort_data = array(
			'pv.voucher_no',
			'vds.vendor_name',
			'pd.name',
			'pv.created_at',
			'pv.status'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY pv.created_at";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}	

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getPrintVoucher($voucher_no) {

		$sql = "SELECT *, pv.status AS pv_status, ct.firstname AS cust_fn, ct.lastname AS cust_ln FROM " . DB_PREFIX . "pvoucher pv
				LEFT JOIN " . DB_PREFIX . "product p ON (p.product_id = pv.product_id)
				LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)
				LEFT JOIN " . DB_PREFIX . "vendor vd ON (pd.product_id = vd.vproduct_id)
				LEFT JOIN " . DB_PREFIX . "vendors vds ON (vd.vendor = vds.vendor_id)
				LEFT JOIN " . DB_PREFIX . "customer ct ON (pv.customer_id = ct.customer_id)
				LEFT JOIN `" . DB_PREFIX . "user` usr ON (pv.updated_by = usr.user_id)
				WHERE pv.voucher_no = '" . $voucher_no .  "'";
		$query = $this->db->query($sql);

		return $query->row;
	}

	public function editPrintVoucher($voucher_no, $data) {

		$sql = "UPDATE " . DB_PREFIX . "pvoucher SET status = '" . $this->db->escape($data['status']) . "',
				updated_by = '" . $this->session->data['user_id'] . "',
				updated_at = NOW() WHERE voucher_no = '" . $voucher_no . "'";

		$query = $this->db->query($sql);

		return $query->row;
	}



}