<?php
class ModelModuleServiceMerchant extends Model {
    public function getTotalServiceMerchants($data = array()) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "servicemerchant");
		return $query->rows;
	}
	
	 public function getSEOKeyword($vendor_id) {
		$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'vendor_id=" . (int)$vendor_id . "'");
		return $query->row;
	}
}