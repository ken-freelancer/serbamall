<?php
class ModelModuleVendorReview extends Model {
	public function addReview($data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_review SET
		author = '" . $this->db->escape($data['name']) . "',
		customer_id = '" . (int)$this->customer->getId() . "',
		vendor_id = '" . (int)$this->db->escape($data['vendor_id'])  . "',
		text = '" . $this->db->escape($data['text']) . "',
		rating = '" . (int)$data['rating'] . "',
		date_added = NOW()");
	}

	public function editReview($review_id, $data) {
		$this->db->query("UPDATE " . DB_PREFIX . "vendor_review SET
							author = '" . $this->db->escape($data['author']) . "',
							vendor_id = '" . $this->db->escape($data['vendor_id']) . "',
							text = '" . $this->db->escape(strip_tags($data['text'])) . "',
							rating = '" . (int)$data['rating'] . "',
							status = '" . (int)$data['status'] . "',
							date_added = NOW() WHERE vreview_id = '" . (int)$review_id . "'");

		$this->cache->delete('product');
	}

	public function deleteReview($review_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "vendor_review WHERE vreview_id = '" . (int)$review_id . "'");

		$this->cache->delete('product');
	}

	public function getReview($review_id) {
		$query = $this->db->query("SELECT DISTINCT *,
				(SELECT vds.vendor_name FROM " . DB_PREFIX . "vendors vds
				WHERE vds.vendor_id = r.vendor_id) AS vendor
				FROM " . DB_PREFIX . "vendor_review r WHERE r.vreview_id = '" . (int)$review_id . "'");

		return $query->row;
	}

	public function getReviews($data = array()) {
		$sql = "SELECT r.vreview_id, vds.vendor_name as name, r.author, r.rating, r.status, r.date_added
				FROM " . DB_PREFIX . "vendor_review r LEFT JOIN " . DB_PREFIX . "vendors vds ON (r.vendor_id = vds.vendor_id)
				";

		$sort_data = array(
			'vds.vendor_name',
			'r.author',
			'r.rating',
			'r.status',
			'r.date_added'
		);	

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];	
		} else {
			$sql .= " ORDER BY r.date_added";	
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}			

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}	

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}																																							  

		$query = $this->db->query($sql);																																				

		return $query->rows;	
	}

	public function getTotalReviews() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendor_review");

		return $query->row['total'];
	}

	public function getTotalReviewsAwaitingApproval() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendor_review WHERE status = '0'");

		return $query->row['total'];
	}

	public function getVendors($data = array()) {

		if ($data) {
			$sql = "SELECT *,v.commission_id AS commission_id, c.commission AS commission,v.sort_order as vsort_order
					FROM " . DB_PREFIX . "vendors v
					LEFT JOIN " . DB_PREFIX . "commission c ON (v.commission_id = c.commission_id)";

			if (!empty($data['filter_name'])) {
				$sql .= " WHERE v.vendor_name LIKE '" . $this->db->escape($data['filter_name']) . "%'";
			}

			$sort_data = array(
				'vendor_name',
				'commission',
				'vsort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY vendor_name";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		}
	}
}
?>