<?php
class ControllerModuleServiceMerchant extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('servicemerchant', $this->request->post);		
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('module/servicemerchant', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		
		$this->data['entry_servicemerchant'] = $this->language->get('entry_servicemerchant');
		$this->data['entry_servicemerchants_image'] = $this->language->get('entry_servicemerchants_image');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_logos'] = $this->language->get('entry_logos');
		$this->data['entry_scroll'] = $this->language->get('entry_scroll');
		$this->data['entry_infomation'] = $this->language->get('entry_infomation');
		$this->data['entry_review'] = $this->language->get('entry_review');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_mode'] = $this->language->get('entry_mode');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/servicemerchant', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		   		
		$this->data['action'] = $this->url->link('module/servicemerchant', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('module/servicemerchant');
		$this->data['servicemerchants'] = $this->model_module_servicemerchant->getTotalservicemerchants();

		if (isset($this->request->post['smw_image'])){
			$this->data['smw_image'] = $this->request->post['smw_image'];
		}else {
			$this->data['smw_image'] = $this->config->get('smw_image');
		}

		if (isset($this->request->post['smh_image'])) {
			$this->data['smh_image'] = $this->request->post['smh_image'];
		} else {
			$this->data['smh_image'] = $this->config->get('smh_image');
		}

		if (isset($this->request->post['servicemerchant_enable'])) {
			$this->data['servicemerchant_enable'] = $this->request->post['servicemerchant_enable'];
		} else {
			$this->data['servicemerchant_enable'] = $this->config->get('servicemerchant_enable');
		}

		$this->data['modules'] = array();
		
		if (isset($this->request->post['servicemerchant_module'])) {
			$this->data['modules'] = $this->request->post['servicemerchant_module'];
		} elseif ($this->config->get('servicemerchant_module')) { 
			$this->data['modules'] = $this->config->get('servicemerchant_module');
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['token'] = $this->session->data['token'];	
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/servicemerchant.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/servicemerchant')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['servicemerchant_module'])) {
			foreach ($this->request->post['servicemerchant_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}