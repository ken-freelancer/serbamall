<?php
/**
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License, Version 3
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 *
 * @category   OpenCart
 * @package    Print Voucher
 * @copyright  Copyright (c) 2015 Ken Teo
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 */
class ControllerModulePrintVoucher extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/printvoucher');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('printvoucher', $this->request->post);		
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('module/printvoucher', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_copyright'] = $this->language->get('text_copyright');

		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_listing_per_page'] = $this->language->get('entry_listing_per_page');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/printvoucher', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		   		
		$this->data['action'] = $this->url->link('module/printvoucher', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		
		if (isset($this->request->post['pv_listing_per_page'])) {
			$this->data['pv_listing_per_page'] = $this->request->post['pv_listing_per_page'];
		} else {
			$this->data['pv_listing_per_page'] = $this->config->get('pv_listing_per_page');
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		if (isset($this->request->post['printvoucher_status'])) {
            $this->data['printvoucher_status'] = $this->request->post['printvoucher_status'];
        } elseif ($this->config->get('printvoucher_status')) {
            $this->data['printvoucher_status'] = $this->config->get('printvoucher_status');
        } else {
            $this->data['printvoucher_status'] = 0;
        }


		$this->data['token'] = $this->session->data['token'];	
		

		$this->template = 'module/printvoucher.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}

	
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/printvoucher')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['printvoucher_module'])) {
			foreach ($this->request->post['printvoucher_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>