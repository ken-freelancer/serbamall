<?php
class ControllerModuleVendorLogo extends Controller {
	private $error = array(); 
	
	public function index() {   
		$this->load->language('module/vendorlogo');

		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
				
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('vendorlogo', $this->request->post);		
			$this->session->data['success'] = $this->language->get('text_success');
			$this->redirect($this->url->link('module/vendorlogo', 'token=' . $this->session->data['token'], 'SSL'));
		}
				
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_content_top'] = $this->language->get('text_content_top');
		$this->data['text_content_bottom'] = $this->language->get('text_content_bottom');		
		$this->data['text_column_left'] = $this->language->get('text_column_left');
		$this->data['text_column_right'] = $this->language->get('text_column_right');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		
		$this->data['entry_vendor'] = $this->language->get('entry_vendor');
		$this->data['entry_vendors_image'] = $this->language->get('entry_vendors_image');
		$this->data['entry_product_image'] = $this->language->get('entry_product_image');
		$this->data['entry_limit'] = $this->language->get('entry_limit');
		$this->data['entry_logos'] = $this->language->get('entry_logos');
		$this->data['entry_scroll'] = $this->language->get('entry_scroll');
		$this->data['entry_infomation'] = $this->language->get('entry_infomation');
		$this->data['entry_review'] = $this->language->get('entry_review');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_layout'] = $this->language->get('entry_layout');
		$this->data['entry_position'] = $this->language->get('entry_position');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_mode'] = $this->language->get('entry_mode');
		
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_add_module'] = $this->language->get('button_add_module');
		$this->data['button_remove'] = $this->language->get('button_remove');
		
 		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}
		
		if (isset($this->error['image'])) {
			$this->data['error_image'] = $this->error['image'];
		} else {
			$this->data['error_image'] = array();
		}
		
  		$this->data['breadcrumbs'] = array();

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		
		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/vendorlogo', 'token=' . $this->session->data['token'], 'SSL'),
      		'separator' => ' :: '
   		);
		   		
		$this->data['action'] = $this->url->link('module/vendorlogo', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');
		
		$this->load->model('module/vendorlogo');
		$this->data['vendors'] = $this->model_module_vendorlogo->getTotalVendors();
		
		if (isset($this->request->post['vw_image'])) {
			$this->data['vw_image'] = $this->request->post['vw_image'];
		} else {
			$this->data['vw_image'] = $this->config->get('vw_image');
		}
		
		if (isset($this->request->post['vh_image'])) {
			$this->data['vh_image'] = $this->request->post['vh_image'];
		} else {
			$this->data['vh_image'] = $this->config->get('vh_image');
		}
		
		if (isset($this->request->post['pvw_image'])) {
			$this->data['pvw_image'] = $this->request->post['pvw_image'];
		} else {
			$this->data['pvw_image'] = $this->config->get('pvw_image');
		}
		
		if (isset($this->request->post['pvh_image'])) {
			$this->data['pvh_image'] = $this->request->post['pvh_image'];
		} else {
			$this->data['pvh_image'] = $this->config->get('pvh_image');
		}
		
		if (isset($this->request->post['vendor_information'])) {
			$this->data['vendor_information'] = $this->request->post['vendor_information'];
		} else {
			$this->data['vendor_information'] = $this->config->get('vendor_information');
		}
		
		if (isset($this->request->post['vendor_review'])) {
			$this->data['vendor_review'] = $this->request->post['vendor_review'];
		} else {
			$this->data['vendor_review'] = $this->config->get('vendor_review');
		}
		
		if (isset($this->request->post['display_mode'])) {
			$this->data['display_mode'] = $this->request->post['display_mode'];
		} else {
			$this->data['display_mode'] = $this->config->get('display_mode');
		}
		
		if (isset($this->request->post['vendors_selected'])) {
			$this->data['vendors_selected'] = $this->request->post['vendors_selected'];
		} elseif ($this->config->get('vendors_selected')) {
			$this->data['vendors_selected'] = $this->config->get('vendors_selected');
		} else { 	
			$this->data['vendors_selected'] = array();
		}
		
		$this->data['modules'] = array();
		
		if (isset($this->request->post['vendorlogo_module'])) {
			$this->data['modules'] = $this->request->post['vendorlogo_module'];
		} elseif ($this->config->get('vendorlogo_module')) { 
			$this->data['modules'] = $this->config->get('vendorlogo_module');
		}
		
		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];
			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}
		
		$this->data['token'] = $this->session->data['token'];	
		$this->load->model('design/layout');
		
		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'module/vendorlogo.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);
				
		$this->response->setOutput($this->render());
	}
	
	
	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/vendorlogo')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if (isset($this->request->post['vendorlogo_module'])) {
			foreach ($this->request->post['vendorlogo_module'] as $key => $value) {
				if (!$value['image_width'] || !$value['image_height']) {
					$this->error['image'][$key] = $this->language->get('error_image');
				}
			}
		}
		
		if (!$this->error) {
			return true;
		} else {
			return false;
		}	
	}
}
?>