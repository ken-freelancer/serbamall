<?php

class ControllerTutorialsAddproduct extends Controller {

private $error = array();

public function index() {

		$this->document->setTitle("Tambah Produk");

		$this->template = 'tutorials/add_product.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

			$this->response->setOutput($this->render());
	}

}

?>