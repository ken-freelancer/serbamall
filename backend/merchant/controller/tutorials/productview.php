<?php

class ControllerTutorialsProductview extends Controller {

private $error = array();

public function index() {

		$this->document->setTitle("Tutorial - Produk yang Dilihat");

		$this->template = 'tutorials/product_view.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

			$this->response->setOutput($this->render());
	}

}

?>