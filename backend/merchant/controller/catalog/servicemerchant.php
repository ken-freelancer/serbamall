<?php 
class ControllerCatalogServiceMerchant extends Controller { 
	private $error = array();

	public function index() {
		$this->language->load('catalog/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/servicemerchant');

		$this->getList();
	}

	public function insert() {
		$this->language->load('catalog/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/servicemerchant');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_servicemerchant->addServiceMerchant($this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'] . $url, 'SSL')); 
		}

		$this->getForm();
	}

	public function update() {
		$this->language->load('catalog/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/servicemerchant');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
			$this->model_catalog_servicemerchant->editServiceMerchant($this->request->get['servicemerchant_id'], $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	public function delete() {
		$this->language->load('catalog/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/servicemerchant');

		if (isset($this->request->post['selected']) && $this->validateDelete()) {
			foreach ($this->request->post['selected'] as $servicemerchant_id) {
				$this->model_catalog_servicemerchant->deleteServiceMerchant($servicemerchant_id);
			}

			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getList();
	}

	public function repair() {
		$this->language->load('catalog/servicemerchant');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/servicemerchant');

		if ($this->validateRepair()) {
			$this->model_catalog_servicemerchant->repairCategories();

			$this->session->data['success'] = $this->language->get('text_success');

			$this->redirect($this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$this->getList();	
	}

	protected function getList() {
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/servicemerchant/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/servicemerchant/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['servicemerchants'] = array();

		$data = array(
			'start' => ($page - 1) * $this->config->get('config_admin_limit'),
			'limit' => $this->config->get('config_admin_limit')
		);

		$servicemerchant_total = $this->model_catalog_servicemerchant->getTotalServiceMerchants();

		$results = $this->model_catalog_servicemerchant->getServiceMerchants($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/servicemerchant/update', 'token=' . $this->session->data['token'] . '&servicemerchant_id=' . $result['servicemerchant_id'] . $url, 'SSL')
			);

			$this->data['servicemerchants'][] = array(
				'servicemerchant_id' => $result['servicemerchant_id'],
				'name'        => $result['name'],
				'sort_order'  => $result['sort_order'],
				'selected'    => isset($this->request->post['selected']) && in_array($result['servicemerchant_id'], $this->request->post['selected']),
				'action'      => $action
			);
		}

		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_no_results'] = $this->language->get('text_no_results');

		$this->data['column_name'] = $this->language->get('column_name');
		$this->data['column_sort_order'] = $this->language->get('column_sort_order');
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['button_insert'] = $this->language->get('button_insert');
		$this->data['button_delete'] = $this->language->get('button_delete');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$pagination = new Pagination();
		$pagination->total = $servicemerchant_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('config_admin_limit');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->template = 'catalog/servicemerchant_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');		
		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_percent'] = $this->language->get('text_percent');
		$this->data['text_amount'] = $this->language->get('text_amount');

		$this->data['entry_name'] = $this->language->get('entry_name');
		$this->data['entry_meta_keyword'] = $this->language->get('entry_meta_keyword');
		$this->data['entry_meta_description'] = $this->language->get('entry_meta_description');
		$this->data['entry_description'] = $this->language->get('entry_description');
		$this->data['entry_parent'] = $this->language->get('entry_parent');
		$this->data['entry_filter'] = $this->language->get('entry_filter');
		$this->data['entry_store'] = $this->language->get('entry_store');
		$this->data['entry_keyword'] = $this->language->get('entry_keyword');
		$this->data['entry_image'] = $this->language->get('entry_image');
		$this->data['entry_top'] = $this->language->get('entry_top');
		$this->data['entry_column'] = $this->language->get('entry_column');		
		$this->data['entry_sort_order'] = $this->language->get('entry_sort_order');
		$this->data['entry_status'] = $this->language->get('entry_status');
		$this->data['entry_layout'] = $this->language->get('entry_layout');

		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');

		$this->data['tab_general'] = $this->language->get('tab_general');
		$this->data['tab_data'] = $this->language->get('tab_data');
		$this->data['tab_design'] = $this->language->get('tab_design');

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->error['name'])) {
			$this->data['error_name'] = $this->error['name'];
		} else {
			$this->data['error_name'] = array();
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['servicemerchant_id'])) {
			$this->data['action'] = $this->url->link('catalog/servicemerchant/insert', 'token=' . $this->session->data['token'], 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/servicemerchant/update', 'token=' . $this->session->data['token'] . '&servicemerchant_id=' . $this->request->get['servicemerchant_id'], 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/servicemerchant', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->get['servicemerchant_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$servicemerchant_info = $this->model_catalog_servicemerchant->getServiceMerchant($this->request->get['servicemerchant_id']);
		}

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('localisation/language');

		$this->data['languages'] = $this->model_localisation_language->getLanguages();

		if (isset($this->request->post['servicemerchant_description'])) {
			$this->data['servicemerchant_description'] = $this->request->post['servicemerchant_description'];
		} elseif (isset($this->request->get['servicemerchant_id'])) {
			$this->data['servicemerchant_description'] = $this->model_catalog_servicemerchant->getServiceMerchantDescriptions($this->request->get['servicemerchant_id']);
		} else {
			$this->data['servicemerchant_description'] = array();
		}


		// Categories
		$this->load->model('catalog/servicemerchant_category');

		if (isset($this->request->post['product_category'])) {
			$categories = $this->request->post['product_category'];
		} elseif (isset($this->request->get['servicemerchant_id'])) {
			$categories = $this->model_catalog_servicemerchant->getServiceMerchantCategories($this->request->get['servicemerchant_id']);
		} else {
			$categories = array();
		}

		$this->data['product_categories'] = array();

		foreach ($categories as $category_id) {
			$category_info = $this->model_catalog_servicemerchant_category->getCategory($category_id);

			if ($category_info) {
				$this->data['product_categories'][] = array(
					'category_id' => $category_info['category_id'],
					'name'        => ($category_info['path'] ? $category_info['path'] . ' &gt; ' : '') . $category_info['name']
				);
			}
		}
		//.Categories


		if (isset($this->request->post['contact'])) {
			$this->data['contact'] = $this->request->post['contact'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['contact'] = $servicemerchant_info['contact'];
		} else {
			$this->data['contact'] = '';
		}

		if (isset($this->request->post['email'])) {
			$this->data['email'] = $this->request->post['email'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['email'] = $servicemerchant_info['email'];
		} else {
			$this->data['email'] = '';
		}

		if (isset($this->request->post['address'])) {
			$this->data['address'] = $this->request->post['address'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['address'] = $servicemerchant_info['address'];
		} else {
			$this->data['address'] = '';
		}

		if (isset($this->request->post['image'])) {
			$this->data['image'] = $this->request->post['image'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['image'] = $servicemerchant_info['image'];
		} else {
			$this->data['image'] = '';
		}

		$this->load->model('tool/image');
		$this->load->model('setting/setting');

		$image_width = $this->config->get('smw_image');
		$image_height = $this->config->get('smh_image');

		if(empty($image_width)){
			$image_width = 100;
		}

		if(empty($image_height)){
			$image_height = 100;
		}

		if (isset($this->request->post['image']) && file_exists(DIR_IMAGE . $this->request->post['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($this->request->post['image'], $image_width, $image_height);
		} elseif (!empty($servicemerchant_info) && $servicemerchant_info['image'] && file_exists(DIR_IMAGE . $servicemerchant_info['image'])) {
			$this->data['thumb'] = $this->model_tool_image->resize($servicemerchant_info['image'], $image_width, $image_height);
		} else {
			$this->data['thumb'] = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);
		}

		$this->data['no_image'] = $this->model_tool_image->resize('no_image.jpg', $image_width, $image_height);

		if (isset($this->request->post['featured'])) {
			$this->data['featured'] = $this->request->post['featured'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['featured'] = $servicemerchant_info['featured'];
		} else {
			$this->data['featured'] = 0;
		}

		if (isset($this->request->post['sort_order'])) {
			$this->data['sort_order'] = $this->request->post['sort_order'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['sort_order'] = $servicemerchant_info['sort_order'];
		} else {
			$this->data['sort_order'] = 0;
		}

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($servicemerchant_info)) {
			$this->data['status'] = $servicemerchant_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->template = 'catalog/servicemerchant_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/servicemerchant')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		foreach ($this->request->post['servicemerchant_description'] as $language_id => $value) {
			if ((utf8_strlen($value['name']) < 2) || (utf8_strlen($value['name']) > 255)) {
				$this->error['name'][$language_id] = $this->language->get('error_name');
			}
		}

		if ((!isset($this->request->post['product_category'])) ) {
			$this->error['product_category'] = "Category is required!";
		}


		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/servicemerchant')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true; 
		} else {
			return false;
		}
	}


	public function autocomplete() {
		$json = array();

		if (isset($this->request->get['filter_name'])) {
			$this->load->model('catalog/servicemerchant');

			$data = array(
				'filter_name' => $this->request->get['filter_name'],
				'start'       => 0,
				'limit'       => 20
			);

			$results = $this->model_catalog_servicemerchant->getCategories($data);

			foreach ($results as $result) {
				$json[] = array(
					'servicemerchant_id' => $result['servicemerchant_id'], 
					'name'        => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8'))
				);
			}		
		}

		$sort_order = array();

		foreach ($json as $key => $value) {
			$sort_order[$key] = $value['name'];
		}

		array_multisort($sort_order, SORT_ASC, $json);

		$this->response->setOutput(json_encode($json));
	}		
}
?>