<?php
/* OpenCart Extension
 * Project Name: Print Voucher
 * @property ModelCatalogPrintVoucher $model_catalog_printvoucher
 */

class ControllerCatalogPv2Ven extends Controller {

	private $error = array(); 

	public function index() {
		$this->language->load('catalog/printvoucher');

		$this->document->setTitle($this->language->get('heading_title')); 

		$this->load->model('catalog/pv2ven');

		$this->getList();
	}

	public function update() {

		$this->language->load('catalog/printvoucher');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('catalog/pv2ven');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			$this->model_catalog_pv2ven->editPrintVoucher($this->request->get['voucher_no'], $this->request->post);
			/*
			 * notification
			 */
			/*if ($this->config->get('vendor_product_edit_approval')) {
				if ($this->config->get('product_notification')) {
					if (!empty($this->request->post['product_name']) && ($this->request->post['status'] == '1') && ($this->request->post['pending_status'] == '5')) {
						$this->approve_notification($this->request->post['product_name'],$this->request->post['vendor']);
					}
				}
			}*/


			$this->session->data['success'] = $this->language->get('text_success');

			$url = '';

			if (isset($this->request->get['filter_voucher_no'])) {
				$url .= '&filter_voucher_no=' . urlencode(html_entity_decode($this->request->get['filter_voucher_no'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['filter_vendor'])) {
				$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
			}


			if (isset($this->request->get['filter_product'])) {
				$url .= '&filter_product=' . $this->request->get['filter_product'];
			}	
			
			if (isset($this->request->get['filter_created_at'])) {
				$url .= '&filter_created_at=' . $this->request->get['filter_created_at'];
			}
			
			if (isset($this->request->get['filter_price'])) {
				$url .= '&filter_price=' . $this->request->get['filter_price'];
			}

			if (isset($this->request->get['filter_quantity'])) {
				$url .= '&filter_quantity=' . $this->request->get['filter_quantity'];
			}	

			if (isset($this->request->get['filter_status'])) {
				$url .= '&filter_status=' . $this->request->get['filter_status'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			$this->redirect($this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . $url, 'SSL'));
		}

		$this->getForm();
	}

	protected function getList() {
		if (isset($this->request->get['filter_voucher_no'])) {
			$filter_voucher_no = $this->request->get['filter_voucher_no'];
		} else {
			$filter_voucher_no = null;
		}

		if (isset($this->request->get['filter_vendor'])) {
			$filter_vendor = $this->request->get['filter_vendor'];
		} else {
			$filter_vendor = null;
		}

		if (isset($this->request->get['filter_product'])) {
			$filter_product = $this->request->get['filter_product'];
		} else {
			$filter_product = null;
		}

		if (isset($this->request->get['filter_created_at'])) {
			$filter_created_at = $this->request->get['filter_created_at'];
		} else {
			$filter_created_at = null;
		}

		if (isset($this->request->get['filter_status'])) {
			$filter_status = $this->request->get['filter_status'];
		} else {
			$filter_status = null;
		}


		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'pvoucher.created_at';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'DESC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		$url = '';

		if (isset($this->request->get['filter_voucher_no'])) {
			$url .= '&filter_voucher_no=' . urlencode(html_entity_decode($this->request->get['filter_voucher_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_created_at'])) {
			$url .= '&filter_created_at=' . $this->request->get['filter_created_at'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		$this->data['insert'] = $this->url->link('catalog/pv2ven/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['copy'] = $this->url->link('catalog/pv2ven/copy', 'token=' . $this->session->data['token'] . $url, 'SSL');
		$this->data['delete'] = $this->url->link('catalog/pv2ven/delete', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['vouchers'] = array();

		$data = array(
			'filter_voucher_no'	  	=> $filter_voucher_no, 
			'filter_vendor'	  		=> $filter_vendor,
			'filter_product'	 	=> $filter_product,
			'filter_created_at'   	=> $filter_created_at,
			'filter_status'  	 	=> $filter_status,
			'sort'            		=> $sort,
			'order'           		=> $order,
			'start'           		=> ($page - 1) * $this->config->get('pv_listing_per_page'),
			'limit'           		=> $this->config->get('pv_listing_per_page'),
		);


		$printvoucher_total = $this->model_catalog_pv2ven->getTotalPrintVoucher($data);

		$results = $this->model_catalog_pv2ven->getPrintVouchers($data);

		foreach ($results as $result) {
			$action = array();

			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('catalog/pv2ven/update', 'token=' . $this->session->data['token'] . '&voucher_no=' . $result['voucher_no'] . $url, 'SSL')
			);

			if ($result['pv_status'] == 0) {
				$status = $this->language->get('txt_status_unused');
			} elseif ($result['pv_status'] == 1) {
				$status = $this->language->get('txt_status_used');
			} else {
				$status = $this->language->get('txt_status_void');
			}
			
			$this->data['vouchers'][] = array(
				'voucher_no' => $result['voucher_no'],
				'vendor_name'=> $result['vendor_name'],
				'name'    	 => $result['name'],
				'created_at' => $result['created_at'],
				'status'     => $status,
				'action'     => $action
			);
		}
		

		$this->data['heading_title'] = $this->language->get('heading_title');		

		$this->data['text_enabled'] = $this->language->get('text_enabled');		
		$this->data['text_disabled'] = $this->language->get('text_disabled');		
		$this->data['text_no_results'] = $this->language->get('text_no_results');		
		
		$this->data['column_voucher_no'] = $this->language->get('column_voucher_no');		
		$this->data['column_vendor'] = $this->language->get('column_vendor');		
		$this->data['column_product'] = $this->language->get('column_product');
		$this->data['column_created_at'] = $this->language->get('column_created_at');
		$this->data['column_status'] = $this->language->get('column_status');		
		$this->data['column_action'] = $this->language->get('column_action');

		$this->data['txt_status_unused'] = $this->language->get('txt_status_unused');
		$this->data['txt_status_used'] = $this->language->get('txt_status_used');
		$this->data['txt_status_void'] = $this->language->get('txt_status_void');

		$this->data['button_copy'] = $this->language->get('button_copy');		
		$this->data['button_insert'] = $this->language->get('button_insert');		
		$this->data['button_delete'] = $this->language->get('button_delete');		
		$this->data['button_filter'] = $this->language->get('button_filter');

		$this->data['token'] = $this->session->data['token'];

		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		if (isset($this->session->data['success'])) {
			$this->data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$this->data['success'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_voucher_no'])) {
			$url .= '&filter_voucher_no=' . urlencode(html_entity_decode($this->request->get['filter_voucher_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_created_at'])) {
			$url .= '&filter_created_at=' . $this->request->get['filter_created_at'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if ($order == 'ASC') {
			$url .= '&order=DESC';
		} else {
			$url .= '&order=ASC';
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['sort_voucher_no'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=pv.voucher_no' . $url, 'SSL');
		$this->data['sort_vendor'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=vds.vendor_name' . $url, 'SSL');
		$this->data['sort_product'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=pd.name' . $url, 'SSL');
		$this->data['sort_created_at'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=pv.created_at' . $url, 'SSL');
		$this->data['sort_status'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=pv.status' . $url, 'SSL');
		$this->data['sort_order'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . '&sort=pv.sort_order' . $url, 'SSL');

		$url = '';

		if (isset($this->request->get['filter_voucher_no'])) {
			$url .= '&filter_voucher_no=' . urlencode(html_entity_decode($this->request->get['filter_voucher_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}
		
		if (isset($this->request->get['filter_created_at'])) {
			$url .= '&filter_created_at=' . $this->request->get['filter_created_at'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		$pagination = new Pagination();
		$pagination->total = $printvoucher_total;
		$pagination->page = $page;
		$pagination->limit = $this->config->get('pv_listing_per_page');
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('catalog/printvoucher', 'token=' . $this->session->data['token'] . $url . '&page={page}', 'SSL');

		$this->data['pagination'] = $pagination->render();

		$this->data['filter_voucher_no'] = $filter_voucher_no;
		$this->data['filter_vendor'] = $filter_vendor;

		$this->data['filter_product'] = $filter_product;
		$this->data['filter_created_at'] = $filter_created_at;
			
		$this->data['filter_status'] = $filter_status;

		$this->data['sort'] = $sort;
		$this->data['order'] = $order;

		$this->template = 'catalog/pv2ven_list.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function getForm() {
		$this->data['heading_title'] = $this->language->get('heading_title');

		$this->data['text_enabled'] = $this->language->get('text_enabled');
		$this->data['text_disabled'] = $this->language->get('text_disabled');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_yes'] = $this->language->get('text_yes');
		$this->data['text_no'] = $this->language->get('text_no');
		$this->data['text_plus'] = $this->language->get('text_plus');
		$this->data['text_minus'] = $this->language->get('text_minus');
		$this->data['text_default'] = $this->language->get('text_default');
		$this->data['text_image_manager'] = $this->language->get('text_image_manager');
		$this->data['text_browse'] = $this->language->get('text_browse');
		$this->data['text_clear'] = $this->language->get('text_clear');
		$this->data['text_option'] = $this->language->get('text_option');
		$this->data['text_option_value'] = $this->language->get('text_option_value');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['text_none'] = $this->language->get('text_none');
		$this->data['text_percent'] = $this->language->get('text_percent');

		$this->data['txt_status_unused'] = $this->language->get('txt_status_unused');
		$this->data['txt_status_used'] = $this->language->get('txt_status_used');
		$this->data['txt_status_void'] = $this->language->get('txt_status_void');
		$this->data['txt_voucher_no'] = $this->language->get('txt_voucher_no');
		$this->data['txt_security_code'] = $this->language->get('txt_security_code');
		$this->data['txt_vendor'] = $this->language->get('txt_vendor');
		$this->data['txt_product'] = $this->language->get('txt_product');
		$this->data['txt_created_at'] = $this->language->get('txt_created_at');
		$this->data['txt_updated_at'] = $this->language->get('txt_updated_at');
		$this->data['txt_status'] = $this->language->get('txt_status');
		$this->data['txt_action'] = $this->language->get('txt_action');
		$this->data['txt_customer_name'] = $this->language->get('txt_customer_name');

		$this->data['entry_status'] = $this->language->get('entry_status');

		$this->data['entry_trial'] = $this->language->get('entry_trial');
		$this->data['entry_trial_price'] = $this->language->get('entry_trial_price');
		$this->data['entry_trial_freq'] = $this->language->get('entry_trial_freq');
		$this->data['entry_trial_length'] = $this->language->get('entry_trial_length');
		$this->data['entry_trial_cycle'] = $this->language->get('entry_trial_cycle');

		$this->data['text_length_day'] = $this->language->get('text_length_day');
		$this->data['text_length_week'] = $this->language->get('text_length_week');
		$this->data['text_length_month'] = $this->language->get('text_length_month');
		$this->data['text_length_month_semi'] = $this->language->get('text_length_month_semi');
		$this->data['text_length_year'] = $this->language->get('text_length_year');
			
		$this->data['button_save'] = $this->language->get('button_save');
		$this->data['button_cancel'] = $this->language->get('button_cancel');
		$this->data['button_remove'] = $this->language->get('button_remove');


		if (isset($this->error['warning'])) {
			$this->data['error_warning'] = $this->error['warning'];
		} else {
			$this->data['error_warning'] = '';
		}

		$url = '';

		if (isset($this->request->get['filter_voucher_no'])) {
			$url .= '&filter_voucher_no=' . urlencode(html_entity_decode($this->request->get['filter_voucher_no'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_vendor'])) {
			$url .= '&filter_vendor=' . urlencode(html_entity_decode($this->request->get['filter_vendor'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_product'])) {
			$url .= '&filter_product=' . urlencode(html_entity_decode($this->request->get['filter_product'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['filter_created_at'])) {
			$url .= '&filter_created_at=' . $this->request->get['filter_created_at'];
		}

		if (isset($this->request->get['filter_status'])) {
			$url .= '&filter_status=' . $this->request->get['filter_status'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . $url, 'SSL'),
			'separator' => ' :: '
		);

		if (!isset($this->request->get['voucher_no'])) {
			$this->data['action'] = $this->url->link('catalog/pv2ven/insert', 'token=' . $this->session->data['token'] . $url, 'SSL');
		} else {
			$this->data['action'] = $this->url->link('catalog/pv2ven/update', 'token=' . $this->session->data['token'] . '&voucher_no=' . $this->request->get['voucher_no'] . $url, 'SSL');
		}

		$this->data['cancel'] = $this->url->link('catalog/pv2ven', 'token=' . $this->session->data['token'] . $url, 'SSL');

		$this->data['voucher_info'] = null;
		if (isset($this->request->get['voucher_no']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
			$this->data['voucher_info'] = $this->model_catalog_pv2ven->getPrintVoucher($this->request->get['voucher_no']);
		}


		$this->data['product_link'] = $this->url->link('catalog/product/update', 'product_id='.$this->data['voucher_info']['product_id'].'&token=' . $this->session->data['token'], 'SSL');

		$this->data['token'] = $this->session->data['token'];

		$this->load->model('setting/setting');
		$this->data['config'] = $this->model_setting_setting->getSetting('config');

		if (isset($this->request->post['status'])) {
			$this->data['status'] = $this->request->post['status'];
		} elseif (!empty($product_info)) {
			$this->data['status'] = $product_info['status'];
		} else {
			$this->data['status'] = 1;
		}

		$this->load->model('design/layout');

		$this->data['layouts'] = $this->model_design_layout->getLayouts();

		$this->template = 'catalog/pv2ven_form.tpl';
		$this->children = array(
			'common/header',
			'common/footer'
		);

		$this->response->setOutput($this->render());
	}

	protected function validateForm() {
		if (!$this->user->hasPermission('modify', 'catalog/pv2ven')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateDelete() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}

	protected function validateCopy() {
		if (!$this->user->hasPermission('modify', 'catalog/product')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if (!$this->error) {
			return true;
		} else {
			return false;
		}
	}


			public function vendor() {
				$this->load->model('catalog/product');
				
				if (isset($this->request->get['vendor_id'])) {
					$vendor_id = $this->request->get['vendor_id'];
				} else {
					$vendor_id = 0;
				}

				$results = $this->model_catalog_product->getVendorsByVendorId($vendor_id);

				$this->response->setOutput(json_encode($results));
			}
			
			public function approve_notification($pname,$vendor_id) {
	
				$this->language->load('mail/email_notification');
				
				$this->load->model('catalog/product');
				
				$vendor_data = $this->model_catalog_product->getVendorData($vendor_id);

				$subject = sprintf($this->language->get('text_subject_approve'),$pname);
				
				$text = sprintf($this->language->get('text_to'), $vendor_data['firstname'] . ' ' . $vendor_data['lastname']) . "<br><br>";				
				$text .= sprintf($this->language->get('text_message_approve'), $pname) . "<br><br>";						
				$text .= $this->language->get('text_thanks') . "<br>";
				$text .= $this->config->get('config_name') . "<br><br>";
				$text .= $this->language->get('text_system');

				$mail = new Mail();
				$mail->protocol = $this->config->get('config_mail_protocol');
				$mail->parameter = $this->config->get('config_mail_parameter');
				$mail->hostname = $this->config->get('config_smtp_host');
				$mail->username = $this->config->get('config_smtp_username');
				$mail->password = $this->config->get('config_smtp_password');
				$mail->port = $this->config->get('config_smtp_port');
				$mail->timeout = $this->config->get('config_smtp_timeout');				
				$mail->setTo($vendor_data['email']);
				$mail->setFrom($this->config->get('config_email'));
				$mail->setSender($this->config->get('config_name'));
				$mail->setSubject(html_entity_decode($subject, ENT_QUOTES, 'UTF-8'));
				$mail->setHtml(html_entity_decode($text, ENT_QUOTES, 'UTF-8'));
				$mail->send();
			}
			
	public function autocomplete() {
		$json = array();

		
			if (isset($this->request->get['filter_voucher_no']) || isset($this->request->get['filter_vendor']) || isset($this->request->get['filter_product']) || isset($this->request->get['filter_category_id'])) {
			
			$this->load->model('catalog/product');
			$this->load->model('catalog/option');

			if (isset($this->request->get['filter_voucher_no'])) {
				$filter_voucher_no = $this->request->get['filter_voucher_no'];
			} else {
				$filter_voucher_no = '';
			}

			if (isset($this->request->get['filter_vendor'])) {
				$filter_vendor = $this->request->get['filter_vendor'];
			} else {
				$filter_vendor = '';
			}


			if (isset($this->request->get['filter_product'])) {
				$filter_product = $this->request->get['filter_product'];
			} else {
				$filter_product = '';
			}
			
			if (isset($this->request->get['limit'])) {
				$limit = $this->request->get['limit'];	
			} else {
				$limit = 20;	
			}			

			$data = array(
				'filter_voucher_no'  => $filter_voucher_no,

			'filter_product'          => $filter_product,
			
				'filter_vendor' => $filter_vendor,
				'start'        => 0,
				'limit'        => $limit
			);

			$results = $this->model_catalog_product->getProducts($data);

			foreach ($results as $result) {
				$option_data = array();

				$product_options = $this->model_catalog_product->getProductOptions($result['product_id']);	

				foreach ($product_options as $product_option) {
					$option_info = $this->model_catalog_option->getOption($product_option['option_id']);

					if ($option_info) {				
						if ($option_info['type'] == 'select' || $option_info['type'] == 'radio' || $option_info['type'] == 'checkbox' || $option_info['type'] == 'image') {
							$option_value_data = array();

							foreach ($product_option['product_option_value'] as $product_option_value) {
								$option_value_info = $this->model_catalog_option->getOptionValue($product_option_value['option_value_id']);

								if ($option_value_info) {
									$option_value_data[] = array(
										'product_option_value_id' => $product_option_value['product_option_value_id'],
										'option_value_id'         => $product_option_value['option_value_id'],
										'name'                    => $option_value_info['name'],
										'price'                   => (float)$product_option_value['price'] ? $this->currency->format($product_option_value['price'], $this->config->get('config_currency')) : false,
										'price_prefix'            => $product_option_value['price_prefix']
									);
								}
							}

							$option_data[] = array(
								'product_option_id' => $product_option['product_option_id'],
								'option_id'         => $product_option['option_id'],
								'name'              => $option_info['name'],
								'type'              => $option_info['type'],
								'option_value'      => $option_value_data,
								'required'          => $product_option['required']
							);	
						} else {
							$option_data[] = array(
								'product_option_id' => $product_option['product_option_id'],
								'option_id'         => $product_option['option_id'],
								'name'              => $option_info['name'],
								'type'              => $option_info['type'],
								'option_value'      => $product_option['option_value'],
								'required'          => $product_option['required']
							);				
						}
					}
				}

				$json[] = array(
					'product_id' => $result['product_id'],

			'sku'        => $result['sku'],
			
					'name'       => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),	
					'model'      => $result['model'],
					'option'     => $option_data,
					'price'      => $result['price']
				);	
			}
		}

		$this->response->setOutput(json_encode($json));
	}
}
?>