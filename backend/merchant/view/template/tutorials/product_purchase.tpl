<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Produk yang Dibeli</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Pilih ‘Produk yang dibeli’ di tab ‘Laporan’.</p>
<p>
	<img alt="" src="view/image/tutorials/product_purchase/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
Kemudian: </br>
1.	Pilih ‘Tanggal Mulai’ untuk mengatur tanggal awal pembelian. </br>
2.	Pilih ‘Tanggal Selesai’ untuk mengatur tanggal akhir pembelian. </br>
3.	Atur ‘Status Pesanan’ menjadi ‘Semua status’. </br>
4.	Tekan ‘Filter’ untuk melihat hasil yang anda cari. </br>

</p>
<p>
	<img alt="" src="view/image/tutorials/product_purchase/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>


<?php echo $footer ?>