<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Pengaturan profil</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Pilih ‘Pengaturan profil’.</p>
<p>
	<img alt="" src="view/image/tutorials/profile_setting/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
Isi informasi anda di tab ‘Umum’ dan ‘Alamat’.

</p>
<p>
	<img alt="" src="view/image/tutorials/profile_setting/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 3:</strong></span></p>
<p>
1.	Pilih ‘Tambahkan gambar’ pada tombol ‘Gambar’ untuk mengunggah gambar. </br>
<p><img alt="" src="view/image/tutorials/profile_setting/3.png" style="width: 500px; height: 300px;" /></p></br>
2.	Klik tombol ‘Pencarian File’.</br></br>
<p><img alt="" src="view/image/tutorials/profile_setting/4.png" style="width: 500px; height: 300px;" /></p></br>
3.	Tekan logo ‘Unggah’.</br></br>
<p><img alt="" src="view/image/tutorials/profile_setting/5.png" style="width: 500px; height: 300px;" /></p></br>
4.	Pilih gambar yang ingin anda unggah.</br>
5.	Tekan tombol ‘Buka’ untuk mengunggah gambar.</br></br>
<p><img alt="" src="view/image/tutorials/profile_setting/6.png" style="width: 500px; height: 300px;" /></p></br>
6.	Klik kiri dua kali gambar yang ingin diunggah.</br></br>
<img alt="" src="view/image/tutorials/profile_setting/7.png" style="width: 500px; height: 300px;" /></p></br>
7.	Tekan tombol ‘Simpan’ untuk menyimpan pengaturan profil.</br></br>
<p><img alt="" src="view/image/tutorials/profile_setting/8.png" style="width: 500px; height: 300px;" /></p></br>
</p>


<?php echo $footer ?>