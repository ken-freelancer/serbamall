<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Cek dan Ubah Pesanan</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Klik ‘Pesanan’ pada tab ‘Penjualan’.</p>
<p>
	<img alt="" src="view/image/tutorials/check_order/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
	Pilih:  </br> 
1.	Klik tab ‘Status’, kemudian ada daftar pilihan, lalu klik ‘Ditunda’. </br>
2.	Klik ‘ Filter’ untuk melihat produk yang ditunda </br>
<p><img alt="" src="view/image/tutorials/check_order/2.png" style="width: 500px; height: 300px;" /></p></br>
3.	Pilih ‘Tampilan’ pada pesanan yang ditunda. </br></br>
<p><img alt="" src="view/image/tutorials/check_order/3.png" style="width: 500px; height: 300px;" /></p></br>
4.	Pilih ‘Riwayat Pesanan’. </br></br>
<p><img alt="" src="view/image/tutorials/check_order/4.png" style="width: 500px; height: 300px;" /></p></br>
5.	Ubah Status Pesanan menjadi ‘Sedang Diproses’. </br></br>
<p><img alt="" src="view/image/tutorials/check_order/5.png" style="width: 500px; height: 300px;" /></p> </br>
6.	Klik ‘Menambahkan Riwayat Sebelumnya’ ketika anda mempersiapkan barang untuk pengiriman. </br></br>
<p><img alt="" src="view/image/tutorials/check_order/6.png" style="width: 500px; height: 300px;" /></p>
</p>
	
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 3:</strong></span></p>
<p>
	Setelah anda mempersiapkan barang dan mengirimkannya kepada pembeli, ikuti langkah berikut: </br>
1.	Ubah status pesanan menjadi ‘Terkirim’.</br>
2.	Masukkan ‘Nomor Melacak Pengiriman’.</br>
3.	Tulis komentar jika anda memberi informasi tambahan kepada pembeli. </br>
4.	Klik ‘Penambahan Riwayat Sebelumnya’ untuk menyelesaikan pesanan. </br>

</p>

<p>
	<img alt="" src="view/image/tutorials/check_order/7.png" style="width: 500px; height: 300px;" /></p>

<?php echo $footer ?>