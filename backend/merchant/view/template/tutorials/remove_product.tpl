<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Hapus Produk</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Klik tab produk, lalu klik ‘Daftar Produk’.</p>
<p>
	<img alt="" src="view/image/tutorials/remove_product/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
1.	Pilih produk yang ingin dihapus. (Beri centang pada kotak) </br>
2.	Klik ‘Hapus Produk’.
</p>
<p>
	<img alt="" src="view/image/tutorials/remove_product/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>


<?php echo $footer ?>