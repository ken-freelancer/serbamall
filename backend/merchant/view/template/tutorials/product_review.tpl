<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Cek Produk Tinjauan</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Pilih tab ‘Tinjauan’.</p>
<p>
	<img alt="" src="view/image/tutorials/product_review/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
Pilih produk yang diinginkan dan klik ‘Tampilan’ di bawah tab ‘Tindakan’.

</p>
<p>
	<img alt="" src="view/image/tutorials/product_review/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 3:</strong></span></p>
<p>
Isilah kotak tinjauan produk sebagai berikut: </br></br>
i.	*Penulis – Nama pembeli. </br>
ii.	*Produk – Nama produk. </br>
iii.	*Teks – Testimoni dari pembeli. </br>
iv.	Rating – Penilaian dari pembeli mengenai produk tersebut.


</p>
<p>
	<img alt="" src="view/image/tutorials/product_review/3.png" style="width: 500px; height: 300px;" /></p>
<p>


<?php echo $footer ?>