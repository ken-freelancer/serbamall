<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Produk yang Dilihat</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Pilih ‘Produk yang dilihat’ di tab ‘Laporan’. </p>
<p>
	<img alt="" src="view/image/tutorials/product_view/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
Anda dapat melihat produk yang dilihat oleh pembeli di layar ini.
</p>
<p>
	<img alt="" src="view/image/tutorials/product_view/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>


<?php echo $footer ?>