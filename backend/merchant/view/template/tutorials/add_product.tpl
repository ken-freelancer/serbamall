<?php echo $header ?>
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Tambah Produk</span>
                        </h1>
                    </div>
                </div>
            </div>
						<p>
							<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
						<p>
							Pilih ‘Tambah Produk’.</p>
						<p>
							<img alt="" src="view/image/tutorials/add_product/1.png" style="width: 500px; height: 300px;" /></p>
						<p>
							&nbsp;</p>
						<p>
							<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
						<p>
							Isilah kotak informasi produk di tab ‘Umum’.</p>
						<p>
							<img alt="" src="view/image/tutorials/add_product/2.png" style="width: 500px; height: 300px;" /></p>
						<p>
							&nbsp;</p>
						<p>
							<span style="color: rgb(255, 0, 0);"><strong>Langkah 3:</strong></span></p>
						<p>
							Isilah harga produk di tab ‘Harga Produk’ </br></br>
							➢	Harga Satuan – Harga produk yang akan dijual  </br>
							➢	Harga Diskon – Harga produk yang akan dijual saat masa promo. Anda dapat memilih tanggal promosi di ‘Tanggal Mulai Diskon’ dan ‘Tanggal Diskon Berakhir’.
						</p>

						<p>
							<img alt="" src="view/image/tutorials/add_product/3.png" style="width: 500px; height: 300px;" /></p>
						<p>

						<p>
							<span style="color: rgb(255, 0, 0);"><strong>Langkah 4:</strong></span></p>
						<p>
							Pilih tab ‘Gambar’ untuk menaruh foto, kemudian ikuti langkah berikut: </br></br>
						1.	Klik ‘Pencarian’.</br></br> </p>
						<p><img alt="" src="view/image/tutorials/add_product/4.png" style="width: 450px; height: 280px;" /></p>
						2.	Klik ikon ‘Unggah’.</br></br>
						<p><img alt="" src="view/image/tutorials/add_product/5.png" style="width: 450px; height: 280px;" /></p>
						3.	Cari gambar dan pilihlah gambar yang akan diunggah.</br>
						4.	Klik ‘Buka’.</br></br>
						<p><img alt="" src="view/image/tutorials/add_product/6.png" style="width: 500px; height: 300px;" /></p>
						5.	Klik dua kali gambar yang baru saja diunggah. </br></br>
						<p><img alt="" src="view/image/tutorials/add_product/7.png" style="width: 500px; height: 300px;" /></p>
						6.	Klik ‘save’.</br></br>
						<p><img alt="" src="view/image/tutorials/add_product/8.png" style="width: 500px; height: 300px;" /></p>
						</p>
							
							
							
							
					

<?php echo $footer ?>