<?php echo $header ?>
<div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-file-text'></i>
                            <span>Cek Pembayaran</span>
                        </h1>
                    </div>
                </div>
            </div>

<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 1:</strong></span></p>
<p>
	Pilih tombol ‘Pembayaran’ pada tab ‘Penjualan’</p>
<p>
	<img alt="" src="view/image/tutorials/check_payment/1.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>
<p>
	<span style="color: rgb(255, 0, 0);"><strong>Langkah 2:</strong></span></p>
<p>
1.	Pilih ‘Tanggal Mulai’ untuk melihat tanggal awal pembayaran. </br>
2.	Pilih ‘Tanggal Selesai’ untuk melihat tanggal akhir pembayaran. </br>
3.	Pilih ‘Dikategorikan’ (berdasarkan nama pembeli)</br>
4.	Ubah ‘Status Pesanan’ menjadi ‘Semua status’</br>
5.	Atur ‘Status Pembayaran’ menjadi ‘Ya’ untuk melihat daftar pembeli yang sudah membayar.</br>
6.	Tekan ‘Filter’ untuk melihat pembayaran.

</p>
<p>
	<img alt="" src="view/image/tutorials/check_payment/2.png" style="width: 500px; height: 300px;" /></p>
<p>
	&nbsp;</p>


<?php echo $footer ?>