<?php echo $header; ?>

    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-gear '></i>
                            <span>Product Returns</span>
                        </h1>

                        <div class='pull-right'>
                            <div class='btn-group'>
                                <a onclick="$('#form').submit();" class="btn btn-white"><span><?php echo $button_save; ?></span></a>
                                <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white"><span><?php echo $button_cancel; ?></span></a>
                            </div>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                </div>
            </div>

            <!--Start Form-->
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='box bordered-box green-border' style='margin-bottom:0;'>
                        <div class='box-header blue-background '>
                            <div class='title'><div class='icon-gear'></div> Return</div>
                            <div class='actions'>

                            </div>
                        </div>

                        <div class='box-content'>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal">
                                <div class='tabbable' style='margin-top: 20px'>
                                    <ul class='nav nav-responsive nav-tabs'>
                                        <li class='active'>
                                            <a data-toggle='tab' href='#tab-general'>
                                                Return Details
                                            </a>
                                        </li>
                                        <li class=''>
                                            <a data-toggle='tab' href='#tab-product'>
                                                Product
                                            </a>
                                        </li>


                                    </ul>
                                    <div class='tab-content'>

                                        <div id="tab-general" class="tab-pane active">
                                            <table class="form table" >
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_order_id; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" name="order_id" value="<?php echo $order_id; ?>" readonly="readonly" />
                                                                <?php if ($error_order_id) { ?>
                                                                    <span class="error"><?php echo $error_order_id; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_date_ordered; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" name="date_ordered" value="<?php echo $date_ordered; ?>" readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr style="display:none;">
                                                    <td><?php echo $entry_customer; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="customer" value="<?php echo $customer; ?>" />
                                                                <input type="hidden" name="customer_id" value="<?php echo $customer_id; ?>" /></td>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="firstname" value="<?php echo $firstname; ?>" readonly="readonly" />
                                                                <?php if ($error_firstname) { ?>
                                                                    <span class="error"><?php echo $error_firstname; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                </tr>
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="lastname" value="<?php echo $lastname; ?>" readonly="readonly" />
                                                                <?php if ($error_lastname) { ?>
                                                                    <span class="error"><?php echo $error_lastname; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_email; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" name="email" value="<?php echo $email; ?>" size="32" readonly="readonly" />
                                                                <?php if ($error_email) { ?>
                                                                    <span class="error"><?php echo $error_email; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" name="telephone" value="<?php echo $telephone; ?>" size="25" readonly="readonly"/>
                                                                <?php if ($error_telephone) { ?>
                                                                    <span class="error"><?php echo $error_telephone; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>

                                            </table>
                                        </div>

                                        <div id="tab-product" class="tab-pane">
                                            <table class="form table">
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_product; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="product" value="<?php echo $product; ?>"  readonly="readonly" />
                                                                <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                                                <?php if ($error_product) { ?>
                                                                    <span class="error"><?php echo $error_product; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_model; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="model" value="<?php echo $model; ?>"  readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_quantity; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <input class="form-control" type="text" name="quantity" value="<?php echo $quantity; ?>"  readonly="readonly" />
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required">*</span> <?php echo $entry_reason; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <select name="return_reason_id" class="form-control"  readonly="readonly">
                                                                  <?php foreach ($return_reasons as $return_reason) { ?>
                                                                  <?php if ($return_reason['return_reason_id'] == $return_reason_id) { ?>
                                                                  <option value="<?php echo $return_reason['return_reason_id']; ?>" selected="selected"><?php echo $return_reason['name']; ?></option>
                                                                  <?php } else { ?>
                                                                  <option value="<?php echo $return_reason['return_reason_id']; ?>"><?php echo $return_reason['name']; ?></option>
                                                                  <?php } ?>
                                                                  <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_opened; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <select class="form-control" name="opened"  readonly="readonly">
                                                                  <?php if ($opened) { ?>
                                                                  <option value="1" selected="selected"><?php echo $text_opened; ?></option>
                                                                  <option value="0"><?php echo $text_unopened; ?></option>
                                                                  <?php } else { ?>
                                                                  <option value="1"><?php echo $text_opened; ?></option>
                                                                  <option value="0" selected="selected"><?php echo $text_unopened; ?></option>
                                                                  <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_comment; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-6">
                                                                <textarea class="form-control" name="comment" cols="40" rows="5"><?php echo $comment; ?></textarea>
                                                                <?php if ($error_comment) { ?>
                                                                    <span class="error"><?php echo $error_comment; ?></span>
                                                                <?php } ?>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_action; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-3">
                                                                <select class="form-control"  name="return_action_id">
                                                                  <option value="0"></option>
                                                                  <?php foreach ($return_actions as $return_action) { ?>
                                                                  <?php if ($return_action['return_action_id'] == $return_action_id) { ?>
                                                                  <option value="<?php echo $return_action['return_action_id']; ?>" selected="selected"> <?php echo $return_action['name']; ?></option>
                                                                  <?php } else { ?>
                                                                  <option value="<?php echo $return_action['return_action_id']; ?>"><?php echo $return_action['name']; ?></option>
                                                                  <?php } ?>
                                                                  <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_return_status; ?></td>
                                                    <td>
                                                        <div class="form-group">
                                                            <div class="col-md-3">
                                                                <select class="form-control"  name="return_status_id">
                                                                  <?php foreach ($return_statuses as $return_status) { ?>
                                                                  <?php if ($return_status['return_status_id'] == $return_status_id) { ?>
                                                                  <option value="<?php echo $return_status['return_status_id']; ?>" selected="selected"><?php echo $return_status['name']; ?></option>
                                                                  <?php } else { ?>
                                                                  <option value="<?php echo $return_status['return_status_id']; ?>"><?php echo $return_status['name']; ?></option>
                                                                  <?php } ?>
                                                                  <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>



                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
<script type="text/javascript"><!--
$('input[name=\'product\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id,
						model: item.model
					}
				}));
			}
		});
	},
	select: function(event, ui) {
		$('input[name=\'product_id\']').attr('value', ui.item.value);
		$('input[name=\'product\']').attr('value', ui.item.label);
		$('input[name=\'model\']').attr('value', ui.item.model);

		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script>
<script type="text/javascript"><!--
$('.htabs a').tabs();
//--></script>
<?php echo $footer; ?>