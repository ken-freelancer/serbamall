<?php if ($error_warning) { ?>
<div class="alert alert-warning alert-dismissable"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="alert alert-success alert-dismissable"><?php echo $success; ?></div>
<?php } ?>
<table class="list table">
  <thead>
    <tr>
      <th class="left">Tanggal Ditambahkan</th>
      <th class="left">Komentar</th>
      <th class="left">Melacak</th>
      <th class="left"><?php echo $column_status; ?></th>
      <th class="left">Pelanggan Yang Diinformasikan</th>
    </tr>
  </thead>
  <tbody>
    <?php if ($histories) { ?>
    <?php foreach ($histories as $history) { ?>
    <tr>
      <td class="left"><?php echo $history['date_added']; ?></td>
      <td class="left"><?php echo $history['comment']; ?></td>
      <td class="left"><?php echo $history['tracking']; ?></td>
      <td class="left"><?php echo $history['status']; ?></td>
      <td class="left"><?php echo $history['notify']; ?></td>
    </tr>
    <?php } ?>
    <?php } else { ?>
    <tr>
      <td class="center" colspan="4"><?php echo $text_no_results; ?></td>
    </tr>
    <?php } ?>
  </tbody>
</table>
<div class="pagination"><?php echo $pagination; ?></div>
