<?php echo $header; ?>

<div id="content-wrapper" class="row">
    <div class="col-xs-12">

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header  page-header-with-buttons'>
                    <h1 class='pull-left'>
                        <i class='icon-table'></i>
                        <span>Pengembalian Produk</span>
                    </h1>

                    <div class='pull-right'>
                        <div class='btn-group'>
                            <?php /*
                            <a href="<?php echo $insert; ?>" class="btn btn-primary">Add</a>
                            <!--<a onclick="$('form').submit();" class="btn btn-danger">Delete</a>-->
                            */
                            ?>
                        </div>
                    </div>
                </div>

                <?php if ($error_warning) { ?>
                    <div class='alert alert-warning alert-dismissable'>
                        <a class='close' data-dismiss='alert' href='#'>&times;</a>
                        <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                    </div>
                <?php } ?>

                <?php if ($success) { ?>
                    <div class='alert alert-success alert-dismissable'>
                        <a class='close' data-dismiss='alert' href='#'>&times;</a>
                        <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                    </div>
                <?php } ?>
            </div>
        </div>

        <!--Start List-->
        <div class='row'>
            <div class='col-sm-12'>
                <div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
                        <div class='title'>Daftar Pengembalian</div>
                        <div class='actions'>

                        </div>
                    </div>

                    <div class='box-content box-no-padding'>
                        <div class='responsive-table'>
                            <div class='scrollable-area'>
                                <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                                    <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
                                        <thead>
                                        <tr>
                                            <th width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></th>
                                            <th class="right"><?php if ($sort == 'r.return_id') { ?>
                                                    <a href="<?php echo $sort_return_id; ?>" class="<?php echo strtolower($order); ?>">ID Pengembalian</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_return_id; ?>">ID Pengembalian</a>
                                                <?php } ?></th>
                                            <th class="right"><?php if ($sort == 'r.order_id') { ?>
                                                    <a href="<?php echo $sort_order_id; ?>" class="<?php echo strtolower($order); ?>">ID Pesanan</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_order_id; ?>">ID Pesanan</a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'customer') { ?>
                                                    <a href="<?php echo $sort_customer; ?>" class="<?php echo strtolower($order); ?>">Konsumen</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_customer; ?>">Konsumen</a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'r.product') { ?>
                                                    <a href="<?php echo $sort_product; ?>" class="<?php echo strtolower($order); ?>">Produk</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_product; ?>">Produk</a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'r.model') { ?>
                                                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'status') { ?>
                                                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'r.date_added') { ?>
                                                    <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>">Tanggal Ditambahkan</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_date_added; ?>">Tanggal Ditambahkan</a>
                                                <?php } ?></th>
                                            <th class="left"><?php if ($sort == 'r.date_modified') { ?>
                                                    <a href="<?php echo $sort_date_modified; ?>" class="<?php echo strtolower($order); ?>">Tanggal Diubah</a>
                                                <?php } else { ?>
                                                    <a href="<?php echo $sort_date_modified; ?>">Tanggal Diubah</a>
                                                <?php } ?></th>
                                            <th class="right">Tindakan</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr class="filter">
                                            <td></td>
                                            <td align="right"><input class="form-control"  type="text" name="filter_return_id" value="<?php echo $filter_return_id; ?>" size="4" style="text-align: right;" /></td>
                                            <td align="right"><input class="form-control"  type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" size="4" style="text-align: right;" /></td>
                                            <td><input type="text" class="form-control"  name="filter_customer" value="<?php echo $filter_customer; ?>" /></td>
                                            <td><input type="text" class="form-control"  name="filter_product" value="<?php echo $filter_product; ?>" /></td>
                                            <td><input type="text" class="form-control"  name="filter_model" value="<?php echo $filter_model; ?>" /></td>
                                            <td><select name="filter_return_status_id" class="form-control" >
                                                    <option value="*"></option>
                                                    <?php foreach ($return_statuses as $return_status) { ?>
                                                        <?php if ($return_status['return_status_id'] == $filter_return_status_id) { ?>
                                                            <option value="<?php echo $return_status['return_status_id']; ?>" selected="selected"><?php echo $return_status['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $return_status['return_status_id']; ?>"><?php echo $return_status['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select></td>
                                            <td><input type="text" class="form-control"  name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
                                            <td><input type="text" class="form-control"  name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" size="12" class="date" /></td>
                                            <td align="right"><a onclick="filter();" class="btn btn-warning btn-sm"><?php echo $button_filter; ?></a></td>

                                        </tr>
                                        <?php if ($returns) { ?>
                                            <?php foreach ($returns as $return) { ?>
                                                <tr>
                                                    <td style="text-align: center;"><?php if ($return['selected']) { ?>
                                                            <input type="checkbox" name="selected[]" value="<?php echo $return['return_id']; ?>" checked="checked" />
                                                        <?php } else { ?>
                                                            <input type="checkbox" name="selected[]" value="<?php echo $return['return_id']; ?>" />
                                                        <?php } ?></td>
                                                    <td class="right"><?php echo $return['return_id']; ?></td>
                                                    <td class="right"><?php echo $return['order_id']; ?></td>
                                                    <td class="left"><?php echo $return['customer']; ?></td>
                                                    <td class="left"><?php echo $return['product']; ?></td>
                                                    <td class="left"><?php echo $return['model']; ?></td>
                                                    <td class="left"><?php echo $return['status']; ?></td>
                                                    <td class="left"><?php echo $return['date_added']; ?></td>
                                                    <td class="left"><?php echo $return['date_modified']; ?></td>
                                                    <td class="right"><?php foreach ($return['action'] as $action) { ?>
                                                            [ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
                                                        <?php } ?></td>
                                                </tr>
                                            <?php } ?>
                                        <?php } else { ?>
                                            <tr>
                                                <td class="center" colspan="10" style="text-align:center;font-weight: bold;">Tidak Ada Hasil</td>
                                            </tr>
                                        <?php } ?>
                                        </tbody>
                                    </table>
                                </form>
                                <div class="pagination"><?php echo $pagination; ?></div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

    </div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=sale/return2ven&token=<?php echo $token; ?>';
	
	var filter_return_id = $('input[name=\'filter_return_id\']').attr('value');
	
	if (filter_return_id) {
		url += '&filter_return_id=' + encodeURIComponent(filter_return_id);
	}
	
	var filter_order_id = $('input[name=\'filter_order_id\']').attr('value');
	
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}	
		
	var filter_customer = $('input[name=\'filter_customer\']').attr('value');
	
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	
	var filter_product = $('input[name=\'filter_product\']').attr('value');
	
	if (filter_product) {
		url += '&filter_product=' + encodeURIComponent(filter_product);
	}

	var filter_model = $('input[name=\'filter_model\']').attr('value');
	
	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}
		
	var filter_return_status_id = $('select[name=\'filter_return_status_id\']').attr('value');
	
	if (filter_return_status_id != '*') {
		url += '&filter_return_status_id=' + encodeURIComponent(filter_return_status_id);
	}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}

	var filter_date_modified = $('input[name=\'filter_date_modified\']').attr('value');
	
	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}
			
	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

$('input[name=\'filter_customer\']').catcomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						category: item.customer_group,
						label: item.name,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_customer\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
//--></script> 
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<?php echo $footer; ?> 