<?php echo $header; ?>

<div id="content-wrapper" class="row">
    <div class="col-xs-12">

        <div class='row'>
            <div class='col-sm-12'>
                <div class='page-header  page-header-with-buttons'>
                    <h1 class='pull-left'>
                        <i class='icon-inbox'></i>
                        <span>Product Return</span>
                    </h1>

                    <div class='pull-right'>
                        <div class='btn-group'>
                            <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white"><?php echo $button_cancel; ?></a>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <!--Start form-->
        <div class='row'>
            <div class='col-sm-12'>
                <div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
                        <div class='title'><div class='icon-edit'></div> Product Return Details</div>
                        <div class='actions'>

                        </div>
                    </div>

                    <div class='box-content'>
                        <div class='tabbable' style='margin-top: 20px'>
                            <ul class='nav nav-responsive nav-tabs'>
                                <li class='active'>
                                    <a data-toggle='tab' href="#tab-return"><?php echo $tab_return; ?></a>
                                </li>
                                <li>
                                    <a data-toggle='tab' href="#tab-product"><?php echo $tab_product; ?></a>
                                </li>
                                <li>
                                    <a data-toggle='tab' href="#tab-history"><?php echo $tab_history; ?></a>
                                </li>

                            </ul>

                            <div class='tab-content'>

                                <div id="tab-return" class="tab-pane active">
                                    <table class="form table">
                                        <tr>
                                            <td><?php echo $text_return_id; ?></td>
                                            <td><?php echo $return_id; ?></td>
                                        </tr>
                                        <?php if ($order) { ?>
                                            <tr>
                                                <td><?php echo $text_order_id; ?></td>
                                                <td><a href="<?php echo $order; ?>"><?php echo $order_id; ?></a></td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td><?php echo $text_order_id; ?></td>
                                                <td><?php echo $order_id; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><?php echo $text_date_ordered; ?></td>
                                            <td><?php echo $date_ordered; ?></td>
                                        </tr>
                                        <?php if ($customer) { ?>
                                            <tr>
                                                <td><?php echo $text_customer; ?></td>
                                                <td><a href="<?php echo $customer; ?>"><?php echo $firstname; ?> <?php echo $lastname; ?></a></td>
                                            </tr>
                                        <?php } else { ?>
                                            <tr>
                                                <td><?php echo $text_customer; ?></td>
                                                <td><?php echo $firstname; ?> <?php echo $lastname; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><?php echo $text_email; ?></td>
                                            <td><?php echo $email; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_telephone; ?></td>
                                            <td><?php echo $telephone; ?></td>
                                        </tr>
                                        <?php if ($return_status) { ?>
                                            <tr>
                                                <td><?php echo $text_return_status; ?></td>
                                                <td id="return-status"><?php echo $return_status; ?></td>
                                            </tr>
                                        <?php } ?>
                                        <tr>
                                            <td><?php echo $text_date_added; ?></td>
                                            <td><?php echo $date_added; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_date_modified; ?></td>
                                            <td><?php echo $date_modified; ?></td>
                                        </tr>
                                    </table>

                                </div>

                                <div id="tab-product" class="tab-pane">

                                    <table class="form table">
                                        <tr>
                                            <td><?php echo $text_product; ?></td>
                                            <td><?php echo $product; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_model; ?></td>
                                            <td><?php echo $model; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_quantity; ?></td>
                                            <td><?php echo $quantity; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_return_reason; ?></td>
                                            <td><?php echo $return_reason; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_opened; ?></td>
                                            <td><?php echo $opened; ?></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $text_return_action; ?></td>
                                            <td><select name="return_action_id">
                                                    <option value="0"></option>
                                                    <?php foreach ($return_actions as $return_action) { ?>
                                                        <?php if ($return_action['return_action_id'] == $return_action_id) { ?>
                                                            <option value="<?php echo $return_action['return_action_id']; ?>" selected="selected"><?php echo $return_action['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $return_action['return_action_id']; ?>"><?php echo $return_action['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select></td>
                                        </tr>
                                        <?php if ($comment) { ?>
                                            <tr>
                                                <td><?php echo $text_comment; ?></td>
                                                <td><?php echo $comment; ?></td>
                                            </tr>
                                        <?php } ?>
                                    </table>
                                </div>


                                <div id="tab-history" class="tab-pane">
                                    <div id="history"></div>
                                    <table class="form table">
                                        <tr>
                                            <td><?php echo $entry_return_status; ?></td>
                                            <td><select name="return_status_id">
                                                    <?php foreach ($return_statuses as $return_status) { ?>
                                                        <?php if ($return_status['return_status_id'] == $return_status_id) { ?>
                                                            <option value="<?php echo $return_status['return_status_id']; ?>" selected="selected"><?php echo $return_status['name']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $return_status['return_status_id']; ?>"><?php echo $return_status['name']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $entry_notify; ?></td>
                                            <td><input type="checkbox" name="notify" value="1" /></td>
                                        </tr>
                                        <tr>
                                            <td><?php echo $entry_comment; ?></td>
                                            <td><textarea name="comment" cols="40" rows="8" style="width: 99%"></textarea>
                                                <div style="margin-top: 10px; text-align: right;"><a onclick="history();" id="button-history" class="btn"><?php echo $button_add_history; ?></a></div></td>
                                        </tr>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<script type="text/javascript"><!--
$('select[name=\'return_action_id\']').bind('change', function() {
	$.ajax({
		url: 'index.php?route=sale/return2ven/action&token=<?php echo $token; ?>&return_id=<?php echo $return_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'return_action_id=' + this.value,
		beforeSend: function() {
			$('.success, .warning, .attention').remove();
			
			$('.box').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		success: function(json) {
			$('.success, .warning, .attention').remove();
			
			if (json['error']) {
				$('.box').before('<div id="div_warning" class="alert alert-warning alert-dismissable" style="display: none;">' + json['error'] + '</div>');
				
				$('#div_warning').fadeIn('slow');
			}
			
			if (json['success']) {
				$('.box').before('<div id="div_success" class="alert alert-success alert-dismissable" style="display: none;">' + json['success'] + '</div>');
				
				$('#div_success').fadeIn('slow');
			}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
});

$('#history .pagination a').live('click', function() {
	$('#history').load(this.href);
	
	return false;
});			

$('#history').load('index.php?route=sale/return2ven/history&token=<?php echo $token; ?>&return_id=<?php echo $return_id; ?>');

function history() {
	$.ajax({
		url: 'index.php?route=sale/return2ven/history&token=<?php echo $token; ?>&return_id=<?php echo $return_id; ?>',
		type: 'post',
		dataType: 'html',
		data: 'return_status_id=' + encodeURIComponent($('select[name=\'return_status_id\']').val()) + '&notify=' + encodeURIComponent($('input[name=\'notify\']').attr('checked') ? 1 : 0) + '&append=' + encodeURIComponent($('input[name=\'append\']').attr('checked') ? 1 : 0) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-history').attr('disabled', true);
			$('#history').prepend('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-history').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#history').html(html);
			
			$('textarea[name=\'comment\']').val(''); 
			
			$('#return-status').html($('select[name=\'return_status_id\'] option:selected').text());
		}
	});
}
//--></script> 
<script type="text/javascript"><!--
$('.vtabs a').tabs();
//--></script> 
<?php echo $footer; ?>