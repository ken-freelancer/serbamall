<?php echo $header; ?>

<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-inbox'></i>
					  <span>Pesanan</span>
					</h1>
					
					<div class='pull-right'>
						
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
				<?php if ($success) { ?>
					<div class='alert alert-success alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($success) ? $success : '' ).'<br>'; ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<!--Start List-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
                      <div class='title'>Daftar Pesanan</div>
                      <div class='actions'> 
					  
                      </div>
                    </div>
					
                    <div class='box-content box-no-padding'>
						<div class='responsive-table'>
							<div class='scrollable-area'>
								<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
									<table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
									<thead>
									  <tr>
										<th>Identitas Pesanan</th>
										<th>Konsumen</th>
										<th>Status</th>
										<th>Total</th>
										<th>Tanggal Ditambahkan</th>
										<th>Tanggal Diubah</th>
										<th>Tindakan</th>
									  </tr>
									</thead>
									<tbody>
										<tr class="filter">
											<td align="right"><input class="form-control"  type="text" name="filter_order_id" value="<?php echo $filter_order_id; ?>" size="4" style="text-align: right;" /></td>
											<td><input class="form-control" type="text" name="filter_customer" value="<?php echo $filter_customer; ?>" /></td>
											<td>
												<select class="form-control" name="filter_order_status_id">
													<option value="*"></option>
													<?php if ($filter_order_status_id == '0') { ?>
													<option value="0" selected="selected"><?php echo $text_missing; ?></option>
													<?php } else { ?>
													<option value="0"><?php echo $text_missing; ?></option>
													<?php } ?>
													
													<?php foreach ($order_statuses as $order_status) { ?>
														<?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
														<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
														<?php } else { ?>
														<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
														<?php } ?>
													<?php } ?>
												</select>
											</td>
										  <td align="right"><input class="form-control" type="text" name="filter_total" value="<?php echo $filter_total; ?>" size="4" style="text-align: right;" hidden /></td>
										  <td><input type="text" class="form-control" name="filter_date_added" value="<?php echo $filter_date_added; ?>" size="12" class="date" /></td>
										  <td><input type="text" class="form-control" name="filter_date_modified" value="<?php echo $filter_date_modified; ?>" size="12" class="date" /></td>
										  <td><a class="btn btn-warning btn-sm" onclick="filter();" class="button"><?php echo $button_filter; ?></a></td>
										</tr>
										
										 <?php if ($orders) { ?>
											<?php foreach ($orders as $order) { ?>
											<tr>
												<td class="right"><?php echo $order['order_id']; ?></td>
												<td class="left"><?php echo $order['customer']; ?></td>
												<td class="left"><?php echo $order['status']; ?></td>
												<td class="right"><?php echo $order['total']; ?></td>
												<td class="left"><?php echo $order['date_added']; ?></td>
												<td class="left"><?php echo $order['date_modified']; ?></td>
												<td class="right"><?php foreach ($order['action'] as $action) { ?>
												<a class="btn-primary btn-sm" href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a>
												<?php } ?></td>
											</tr>
											<?php } ?>
										<?php } else { ?>
											<tr>
											  <td class="center" colspan="8">Tidak Ada Hasil</td>
											</tr>
										<?php } ?>
										
									</tbody>
									</table>
								</form>
                                <div class="pagination"><?php echo $pagination; ?></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>	
</div>		
		
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=sale/vendor_order&token=<?php echo $token; ?>';
	
	var filter_order_id = $('input[name=\'filter_order_id\']').attr('value');
	
	if (filter_order_id) {
		url += '&filter_order_id=' + encodeURIComponent(filter_order_id);
	}
	
	var filter_customer = $('input[name=\'filter_customer\']').attr('value');
	
	if (filter_customer) {
		url += '&filter_customer=' + encodeURIComponent(filter_customer);
	}
	
	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').attr('value');
	
	if (filter_order_status_id != '*') {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}	

	var filter_total = $('input[name=\'filter_total\']').attr('value');

	if (filter_total) {
		url += '&filter_total=' + encodeURIComponent(filter_total);
	}	
	
	var filter_date_added = $('input[name=\'filter_date_added\']').attr('value');
	
	if (filter_date_added) {
		url += '&filter_date_added=' + encodeURIComponent(filter_date_added);
	}
	
	var filter_date_modified = $('input[name=\'filter_date_modified\']').attr('value');
	
	if (filter_date_modified) {
		url += '&filter_date_modified=' + encodeURIComponent(filter_date_modified);
	}
				
	location = url;
}
//--></script>  
<script type="text/javascript"><!--
$(document).ready(function() {
	$('.date').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';
		
		$.each(items, function(index, item) {
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');
				
				currentCategory = item.category;
			}
			
			self._renderItem(ul, item);
		});
	}
});

$('input[name=\'filter_customer\']').catcomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=sale/customer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						category: item.customer_group,
						label: item.name,
						value: item.customer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_customer\']').val(ui.item.label);
						
		return false;
	}
});
//--></script> 
<?php echo $footer; ?>