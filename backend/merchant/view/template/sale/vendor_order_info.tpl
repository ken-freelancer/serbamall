<?php echo $header; ?>
<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-inbox'></i>
					  <span>Pesanan</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>					
							<a onclick="window.open('<?php echo $invoice; ?>');" class="btn btn-white">Print Invoice Pembayaran</a>
							<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white">Batal</a>
						</div>
					</div>
				</div>
				
			</div>
		</div>

		<!--Start form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'><div class='icon-edit'></div> Order Details</div>
						<div class='actions'> 
							
						</div>
                    </div>
					
					<div class='box-content'>
						<div class='tabbable' style='margin-top: 20px'>
							<ul class='nav nav-responsive nav-tabs'>
							
								<?php if ($this->config->get('sales_order_detail')) { ?>
								<li class='active'>									
									<a data-toggle='tab' href="#tab-order">Rincian Pesanan</a>
								</li>
								<?php } ?>
								
								<?php if ($this->config->get('sales_payment_detail')) { ?>
								<li>
									<a data-toggle='tab' href="#tab-payment"><?php echo $tab_payment; ?></a>
								</li>
								<?php } ?>
								
								<?php if ($this->config->get('sales_order_history')) { ?>
								<li>
									<a data-toggle='tab' href="#tab-history">Penanganan Pesanan</a>
								</li>
								<?php } ?>
								
							</ul>
							
							<div class='tab-content'>
								
								<?php if ($this->config->get('sales_order_detail')) { ?>
									<div id="tab-order" class="tab-pane active">
										<table class="form table">
											<tr>
												<td>Identitas Pesanan:</td>
												<td>#<?php echo $order_id; ?></td>
											</tr>
											<tr> 
												<td>Nomor Invoice Pembayaran:</td>
												<td>
													<?php if ($invoice_no) { ?>
														<?php echo $invoice_no; ?>
													<?php } else { ?>
														<span id="invoice"><a class="btn-primary btn-sm" id="invoice-generate"><?php echo $text_generate; ?></a> </span>
													<?php } ?>
												</td>
											</tr>
											<tr>
												<td>Nama Toko:</td>
												<td><?php echo $store_name; ?></td>
											</tr>
											<tr>
												<td>Link Url Toko:</td>
												<td><a onclick="window.open('<?php echo $store_url; ?>');"><u><?php echo $store_url; ?></u></a></td>
											</tr>
											<?php if ($customer) { ?>
											<tr>
												<td>Pelanggan:</td>
												<td><a href="<?php echo $customer; ?>"><?php echo $firstname; ?> <?php echo $lastname; ?></a></td>
											</tr>
											<?php } else { ?>
											<tr>
												<td>Pelanggan:</td>
												<td><?php echo $firstname; ?> <?php echo $lastname; ?></td>
											</tr>
											<?php } ?>
										  
											<?php if ($customer_group) { ?>
											<tr>
												<td>Grup Pelanggan:</td>
												<td><?php echo $customer_group; ?></td>
											</tr>
											<?php } ?>
										  
											<tr>
												<td>Alamat IP:</td>
												<td><?php echo $ip; ?></td>
											</tr>
											<tr>
												<td><?php echo $text_email; ?></td>
												<td><?php echo $email; ?></td>
											</tr>
											<tr>
												<td>Telepon:</td>
												<td><?php echo $telephone; ?></td>
											</tr>
											<?php if ($fax) { ?>
											<tr>
												<td><?php echo $text_fax; ?></td>
												<td><?php echo $fax; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td><?php echo $text_total; ?></td>
												<td>
													<?php echo $totals; ?>
													<?php if ($credit && $customer) { ?>
														<?php if (!$credit_total) { ?>
														<span id="credit"><b>[</b> <a id="credit-add"><?php echo $text_credit_add; ?></a> <b>]</b></span>
														<?php } else { ?>
															<span id="credit"><b>[</b> <a id="credit-remove"><?php echo $text_credit_remove; ?></a> <b>]</b></span>
														<?php } ?>
													<?php } ?>
												</td>
											</tr>
										  
										  <?php if ($order_status) { ?>
											<tr>
												<td>Status Pesanan</td>
												<td id="order-status"><?php echo $order_status; ?></td>
											</tr>
										  <?php } ?>
										  
										  <?php if ($comment) { ?>
											<tr>
												<td>Komentar:</td>
												<td><?php echo $comment; ?></td>
											</tr>
										  <?php } ?>
										  
										  <?php if ($affiliate) { ?>
											<tr>
												<td><?php echo $text_affiliate; ?></td>
												<td><a href="<?php echo $affiliate; ?>"><?php echo $affiliate_firstname; ?> <?php echo $affiliate_lastname; ?></a></td>
											</tr>											
											<tr>
												<td><?php echo $text_commission; ?></td>
												<td><?php echo $commission; ?>
													<?php if (!$commission_total) { ?>
														<span id="commission"><b>[</b> <a id="commission-add"><?php echo $text_commission_add; ?></a> <b>]</b></span>
													<?php } else { ?>
														<span id="commission"><b>[</b> <a id="commission-remove"><?php echo $text_commission_remove; ?></a> <b>]</b></span>
													<?php } ?>
												</td>
											</tr>
										  <?php } ?>
										  
											<tr>
												<td>Tanggal Ditambahkan</td>
												<td><?php echo $date_added; ?></td>
											</tr>
											<tr>
												<td>Tanggal Diubah</td>
												<td><?php echo $date_modified; ?></td>
											</tr>
										</table>
										
										<hr class='hr-drouble'>
										
										<div class="box">
											<div class="box-header blue-background">
												<div class="title">Pesan barang</div>
												<div class="actions">
													
												</div>
											</div>
											<div class="box-content box-no-padding">
												<table id="product" class="list table">
												<thead>
													<tr>
													  <th class="left">Produk</th>
													  <th class="left"><?php echo $column_model; ?></th>
													  <th class="right">Jumlah</th>
													  <th class="right">Harga Satuan</th>
													  <th class="right"><?php echo $column_total; ?></th>
													</tr>
												</thead>
												<?php foreach ($products as $product) { ?>
												<tbody id="product-row<?php echo $product['order_product_id']; ?>">
													<tr>
														<td class="left">
															<?php if ($product['product_id']) { ?>
																<a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a>
															<?php } else { ?>
																<?php echo $product['name']; ?>
															<?php } ?>
															
															<?php foreach ($product['option'] as $option) { ?>
																<br />
																<?php if ($option['type'] != 'file') { ?>
																&nbsp;<small> - <?php echo $option['name']; ?>: <?php echo $option['value']; ?></small>
																<?php } else { ?>
																&nbsp;<small> - <?php echo $option['name']; ?>: <a href="<?php echo $option['href']; ?>"><?php echo $option['value']; ?></a></small>
																<?php } ?>
															<?php } ?>
														</td>
													  <td class="left"><?php echo $product['model']; ?></td>
													  <td class="right"><?php echo $product['quantity']; ?></td>
													  <td class="right"><?php echo $product['price']; ?></td>
													  <td class="right"><?php echo $product['total']; ?></td>
													</tr>
												</tbody>
												<?php } ?>
												<tbody id="totals">
													<tr>
														<td colspan="4" class="right"><?php echo $text_subtotal; ?> :</td>
														<td class="right"><?php echo $subtotal; ?></td>
													</tr>
													<?php if ($shipping_weight) { ?>
													<tr>
														<td colspan="4" class="right"><?php echo $shipping_weight; ?> :</td>
														<td class="right"><?php echo $shipping_cost; ?></td>
													</tr>
													<?php } ?>
													<?php if ($coupon_amount) { ?>
													<tr>
														<td colspan="4" class="right"><?php echo $coupon_title; ?> :</td>
														<td class="right">-<?php echo $coupon_amount; ?></td>
													</tr>
													<?php } ?>
													<?php if ($tax) { ?>
													<tr>
														<td colspan="4" class="right">Pajak:</td>
														<td class="right"><?php echo $tax; ?></td>
													</tr>
													<?php } ?>
													<tr>
														<td colspan="4" class="right"><?php echo $text_total; ?></td>
														<td class="right"><?php echo $totals; ?></td>
													</tr>
												</tbody>
												</table>
											</div>
										</div>
									</div>
								<?php } ?>
								
								<?php if ($this->config->get('sales_payment_detail')) { ?>
									<div id="tab-payment" class="tab-pane">
										
										<table class="form table">
											<tr>
												<td><?php echo $text_firstname; ?></td>
												<td><?php echo $payment_firstname; ?></td>
											</tr>
											<tr>
												<td><?php echo $text_lastname; ?></td>
												<td><?php echo $payment_lastname; ?></td>
											</tr>
											<?php if ($payment_company) { ?>
											<tr>
												<td><?php echo $text_company; ?></td>
												<td><?php echo $payment_company; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td><?php echo $text_address_1; ?></td>
												<td><?php echo $payment_address_1; ?></td>
											</tr>
											<?php if ($payment_address_2) { ?>
											<tr>
												<td><?php echo $text_address_2; ?></td>
												<td><?php echo $payment_address_2; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td><?php echo $text_city; ?></td>
												<td><?php echo $payment_city; ?></td>
											</tr>
											<?php if ($payment_postcode) { ?>
											<tr>
												<td><?php echo $text_postcode; ?></td>
												<td><?php echo $payment_postcode; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td><?php echo $text_zone; ?></td>
												<td><?php echo $payment_zone; ?></td>
											</tr>
											<?php if ($payment_zone_code) { ?>
											<tr>
												<td><?php echo $text_zone_code; ?></td>
												<td><?php echo $payment_zone_code; ?></td>
											</tr>
											<?php } ?>
											<tr>
												<td><?php echo $text_country; ?></td>
												<td><?php echo $payment_country; ?></td>
											</tr>
											<tr>
												<td><?php echo $text_payment_method; ?></td>
												<td><?php echo $payment_method; ?></td>
											</tr>
										</table>
										
									</div>
								<?php } ?>
								
								
								
								<?php if ($this->config->get('sales_order_history')) { ?>
									<div id="tab-history" class="tab-pane">
											<div id="history"></div>
											<table class="form table">
												<?php if ($this->config->get('sales_order_history_update')) { ?>
												<tr>
													<td>Status Pesanan</td>
													<?php if (!$this->config->get('sales_order_status_availability')) { ?>
														<td>
														<select name="order_status_id">
															<?php foreach ($order_statuses as $order_status) {
                                                                if($order_status['order_status_id'] == '3'){ ?>
                                                                <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                                                <?php }else{ ?>
																<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
															<?php } }?>
														</select> 
														</td>
													<?php } else { ?>
													<td>
														<select name="order_status_id">
															<?php foreach ($order_statuses as $order_status) { ?>
																<?php if (in_array($order_status['order_status_id'], $this->config->get('sales_order_status_availability'))) {  if($order_status['order_status_id'] == '3'){ ?>
                                                                    <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                                                <?php }else{ ?>
                                                                    <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                                                <?php } }?>
															<?php } ?>
														</select><img id="info_icon" style="margin-left:5px;" title="Pilih status yang anda inginkan:

- Sedang diproses, jika anda sudah mengetahui barang pesanan dan sedang mempersiapkan barang tersebut untuk dikirim
- Sudah terkirim, jika anda sudah mengirimkan barang pesanan anda
- Batal, jika anda membatalkan barang pesanan dari pelanggan.
- Ditunda, jika anda mengalami masalah saat mempersiapkan barang pesanan dan membutuhkan waktu untuk mempersiapkannya.
" height="18px" width="18px" src="view/image/information.png"/> 
													</td>
													<?php } ?>
												</tr>
												<!-- Vincent - 8/12/2015 -->
												<tr style="display:none;">
													<td><?php echo $entry_notify; ?></td>
													<td><input type="checkbox" name="notify" value="1" checked="checked"/></td>
												</tr>
                                                    <tr>
                                                        <td>Nomor Pelacakan: </td>
                                                        <td><input type="text" name="tracking" id="tracking" value="" class="form-control" /></td>
                                                    </tr>
												<tr>
													<td>Komentar</td>
													<td><textarea name="comment" cols="40" rows="8" style="width: 99%"></textarea>
														<div style="margin-top: 10px; text-align: right;">
															<a onclick="history();" id="button-history" class="btn">Tambah History</a>
														</div>
													</td>
												</tr>
												<?php } ?>
											</table>
									</div>
								<?php } ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>		
</div>	

<style type="text/css">
#info_icon:hover{
	cursor: pointer;
}
</style>

<script type="text/javascript"><!--
$('#invoice-generate').live('click', function() {
	$.ajax({
		url: 'index.php?route=sale/vendor_order/createinvoiceno&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'json',
		beforeSend: function() {
			$('#invoice').after('<img src="view/image/loading.gif" class="loading" style="padding-left: 5px;" />');	
		},
		complete: function() {
			$('.loading').remove();
		},
		success: function(json) {
			$('.success, .warning').remove();
						
			if (json['error']) {
				$('#tab-order').prepend('<div class="warning" style="display: none;">' + json['error'] + '</div>');
				
				$('.warning').fadeIn('slow');
			}
			
			if (json.invoice_no) {
				$('#invoice').fadeOut('slow', function() {
					$('#invoice').html(json['invoice_no']);
					
					$('#invoice').fadeIn('slow');
				});
			}
		}
	});
});

$('#history .pagination a').live('click', function() {
	$('#history').load(this.href);
	
	return false;
});			

$('#history').load('index.php?route=sale/vendor_order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>');

function history() {
	$.ajax({
		type: 'POST',
		url: 'index.php?route=sale/vendor_order/history&token=<?php echo $token; ?>&order_id=<?php echo $order_id; ?>',
		dataType: 'html',
		data: 'order_status_id=' + encodeURIComponent($('select[name=\'order_status_id\']').val()) + '&notify=' + encodeURIComponent($('input[name=\'notify\']').attr('checked') ? 1 : 0) + '&append=' + encodeURIComponent($('input[name=\'append\']').attr('checked') ? 1 : 0) + '&tracking=' + encodeURIComponent($('input[name=\'tracking\']').val()) + '&comment=' + encodeURIComponent($('textarea[name=\'comment\']').val()),
		beforeSend: function() {
            var_order_status = encodeURIComponent($('select[name=\'order_status_id\']').val())
            if(var_order_status == 3)
            {
                if($('#tracking').val() == '') {
                    alert('Tracking No. cannot be empty if status selected Shipped');
                    return false;
                }
            }
			$('.success, .warning').remove();
			$('#button-history').attr('disabled', true);
			$('#history').prepend('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-history').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(html) {
			$('#history').html(html);
			
			$('input[name=\'tracking\']').val('');
			$('textarea[name=\'comment\']').val('');

			$('#order-status').html($('select[name=\'order_status_id\'] option:selected').text());
		}
	});
}
//--></script> 
<script type="text/javascript"><!--
$('.vtabs a').tabs();
//--></script> 
<?php echo $footer; ?>