<?php echo $header; ?>
<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-inbox'></i>
					  <span>Penjualan</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>					
							
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--Start form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'>Laporan Penjualan</div>
						<div class='actions'> 
					  
						</div>
                    </div>
					
                    <div class='box-content'>
						<div class='responsive-table'>
							<form method="post" enctype="multipart/form-data" id="form">
								<table class="form table">
								<tr>
								<?php foreach ($store_revenue AS $income) { ?>
									<?php if (!$this->user->getVP()) { ?>
										<td><span title="<?php echo $title_gross_revenue . $income['revenue_shipping']; ?>" style="padding-right:25px;background:transparent url(view/image/revenue.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<font color="#FDD017">[ </font><b><?php echo $text_gross_incomes; ?> :</b> <?php echo $income['gross_amount']; ?><font color="#FDD017"> ]</font></td>
										<td><span style="padding-right:25px;background:transparent url(view/image/commission.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<font color="#4AA02C">[ </font><b><?php echo $text_commission; ?> :</b> <?php echo $income['commission']; ?><font color="#4AA02C"> ]</font></td>
										<td><span style="padding-right:25px;background:transparent url(view/image/vendor.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<font color="#FF0000">[ </font><b><?php echo $text_vendor_earning; ?> :</b> <?php echo $income['vendor_amount']; ?></td>
										<!--
										<td><span style="padding-right:25px;padding-top:4px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/shipping_fee.png) no-repeat scroll right center;">&nbsp;</span><font color="#FDD017">&nbsp;[ </font><b><?php echo $text_shipping; ?> :</b> <?php echo $income['shipping_charged']; ?><font color="#FDD017"> ] </font></td>
										<td><span style="padding-right:25px;padding-top:4px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/coupon.png) no-repeat scroll right center;">&nbsp;</span><font color="#FDD017">&nbsp;[ </font><b><?php echo $text_coupon; ?> :</b> <?php echo '-' . $income['coupon_amount']; ?><font color="#FDD017"> ] </font></td>
										-->
										<td><span style="padding-right:25px;background:transparent url(view/image/amount_to_pay.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<font color="#FDD017">&nbsp;[ </font> <b><?php echo $text_amount_pay_vendor; ?> :</b> <?php echo $income['amount_pay_vendor']; ?><font color="#FF0000"> ]</font></td>
										<?php if ($filter_paid_status != 1 && $filter_vendor_group !=0 && !empty($orders)) { ?>
											<td style="width:60px"><div class="buttons"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&no_shipping=1&business=<?php echo $income['paypal_email']; ?>&item_name=<?php echo $income['company']; ?>&amount=<?php echo $income['paypal_amount']; ?>&currency_code=<?php echo $this->config->get('config_currency'); ?>" target="_blank" class="button"><span><?php echo $button_Paypal; ?></span></a></div></td>
											<td style="width:120px"><div class="buttons"><a onclick=" $('#form').submit();" name="submit_add_payment" id="submit_add_payment" class="button"><span><?php echo $button_addPayment; ?></span></a></div></td>
											<input type="hidden" name="paypal_standard" id="paypal_standard" value="<?php echo $addPayment . '&payment_option=paypal_standard'; ?>" />
											<input type="hidden" name="pay_cheque" id="pay_cheque" value="<?php echo $addPayment . '&payment_option=pay_cheque&chequeno='; ?>" />
											<input type="hidden" name="other_payment_method" id="other_payment_method" value="<?php echo $addPayment . '&payment_option=other_payment_method&opm='; ?>" />
										<?php } else { ?>
											<td><div></div></td>
											<td><div></div></td>
										<?php } ?>
									<?php } else { ?>
										<td><span style="padding-right:25px;background:transparent url(view/image/vendor.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<b>Saldo Penjualan</b>   <?php echo $income['vendor_amount']; ?></td>
										<!--
										<td><span style="padding-right:25px;padding-top:4px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/shipping_fee.png) no-repeat scroll right center;">&nbsp;</span><b><?php echo $text_shipping; ?> :</b> <?php echo $income['shipping_charged']; ?></td>
										<td><span style="padding-right:25px;padding-top:4px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/coupon.png) no-repeat scroll right center;">&nbsp;</span><b><?php echo $text_coupon; ?> :</b> <?php echo '-' . $income['coupon_amount']; ?></td>
										-->
										<td><span style="padding-right:25px;background:transparent url(view/image/amount_to_pay.png) no-repeat scroll right center;">&nbsp;</span>&nbsp;<b>Total Pendapatan :</b> <?php echo $income['amount_pay_vendor']; ?></td>
										<td></td>
										<td></td>
										<td></td>
										<td></td>
									<?php } ?>
								<?php } ?>
								</tr>
								<tr>
									<td>
										Tanggal Mulai
										<div class='datepicker input-group' >
											<input class='form-control' id="date-start" data-format='yyyy-MM-dd' placeholder='Tanggal Mulai' type='text' name="filter_date_start" value="<?php echo (isset($filter_date_start) ? $filter_date_start : ''); ?>" />
											<span class='input-group-addon'>
												<span data-date-icon='icon-calendar' data-time-icon='icon-time'></span>
											</span>
										</div>
										<!--<input class="form-control" type="text" name="filter_date_start" value="<?php echo $filter_date_start; ?>" id="date-start" size="12" />-->
									</td>
									<td>
										Tanggal Selesai
										<div class='datepicker input-group' >
											<input class='form-control' id="date-end" data-format='yyyy-MM-dd' placeholder='Tanggal Selesai' type='text' name="filter_date_end" value="<?php echo (isset($filter_date_end) ? $filter_date_end : ''); ?>" />
											<span class='input-group-addon'>
												<span data-date-icon='icon-calendar' data-time-icon='icon-time'></span>
											</span>
										</div>
										<!--<input class="form-control" type="text" name="filter_date_end" value="<?php echo $filter_date_end; ?>" id="date-end" size="12" />-->
									</td>
									<td>
										Dikategorikan
										<select class="form-control" name="filter_vendor_group">
											<?php if (!$this->user->getVP()) { ?>
												<option value="0"><?php echo $text_all_vendors; ?></option>
											<?php } ?>
											<?php foreach ($vendors_name as $vendors_name) { ?>
												<?php if ($vendors_name['vendor_id'] == $filter_vendor_group) { ?>
													<option value="<?php echo $vendors_name['vendor_id']; ?>" selected="selected"><?php echo $vendors_name['name']; ?></option>
												<?php } else { ?>
													<option value="<?php echo $vendors_name['vendor_id']; ?>"><?php echo $vendors_name['name']; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</td>
									<td>
										Status Pesanan
										<select class="form-control" name="filter_order_status_id">
											<option value="0"><?php echo $text_all_status; ?></option>
											<?php foreach ($order_statuses as $order_status) { ?>
												<?php if ($order_status['order_status_id'] == $filter_order_status_id) { ?>
													<option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
												<?php } else { ?>
													<option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
												<?php } ?>
											<?php } ?>
										</select>
									</td>
									<td>
										Status Pembayaran
										<select class="form-control" name="filter_paid_status">
										<?php if ($filter_paid_status) { ?>
											<option value="0">Tidak</option>
											<option value="1" selected="selected">Ya</option>
										<?php } else { ?>
											<option value="0" selected="selected">Tidak</option>
											<option value="1">Ya</option>
										<?php } ?>
										</select>
									</td>
									<td></td>
									<td></td>
									<td style="text-align: right;">
										<br>
										<a onclick="filter();" class="btn btn-warning btn-sm"><?php echo $button_filter; ?></a>
									</td>
								</tr>
								</table>
							</form>
						</div>
						
						<div class="responsive-table">
							<table class="list table sf-table">
							<thead>
							  <tr>
								<th class="right">Identitas Pesanan</th>
								<th class="left">Nama Produk</th>
								<th class="left">Tanggal Terjual</th>
								<th class="left">Status Pesanan</th>
								<th class="left">Tanggal Selesai</th>
								<th class="right">Harga satuan<?php echo ' (' . $this->config->get('config_currency') . ')'; ?></th>
								<th class="right">Jumlah</th>
								<th class="right"><?php echo $column_total; ?><?php echo ' (' . $this->config->get('config_currency') . ')'; ?></th>
								<!--<th class="right"><?php echo $column_commission; ?><?php echo ' (' . $this->config->get('config_currency') . ')'; ?></th>-->
								<th class="right">Keuntungan Bersih<?php echo ' (' . $this->config->get('config_currency') . ')'; ?></th>
								<th class="left">Pembayaran dari SerbaMall</th>
							  </tr>
							</thead>
							<tbody>
								  <?php if ($orders) { ?>
								  <?php foreach ($orders as $order) { ?>
								  <tr>
									<td class="right"><?php echo $order['order_id']; ?></td>
									<td class="left"><?php echo $order['product_name']; ?></td>
									<td class="left"><?php echo $order['date']; ?></td>
									<td class="left">
									<?php foreach ($order_statuses as $order_status) { ?>
										<?php if ($order_status['order_status_id'] == $order['order_status']) { ?>
											<?php echo $order_status['name']; ?>
										<?php } ?>
									<?php } ?></td>
									<td><?php echo $order['completed_date']; ?></td>
									<td class="right"><?php echo $order['price']; ?></td>
									<td class="right"><?php echo $order['quantity']; ?></td>
									<td class="right"><?php echo $order['total']; ?><br /><?php echo $order['title']; ?></td>
									<!--<td class="right"><?php echo $order['commission']; ?></td>-->
									<td class="right"><?php echo $order['amount']; ?></td>
									<td class="left">
									<?php if ($order['paid_status']) { ?>
										<span style="padding-right:20px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/success.png) no-repeat scroll right center;">&nbsp;</span>
									<?php } else { ?>
										<?php if (!$this->user->getVP()) { ?>
										<div class="buttons"><a href="https://www.paypal.com/cgi-bin/webscr?cmd=_xclick&no_shipping=1&business=<?php echo $order['paypal_email']; ?>&item_name=<?php echo $order['pp_product_name']; ?>&amount=<?php echo $order['paypal_amount']; ?>&invoice=<?php echo $order['order_id']; ?>&currency_code=<?php echo $this->config->get('config_currency'); ?>" target="_blank" class="button"><span><?php echo $button_Paypal; ?></span></a><span style="cursor:pointer;padding-right:20px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/add.png) no-repeat scroll right center;" onclick="$('#form').attr('action', '<?php echo 'index.php?route=report/vendor_transaction/addPaymentRecord&token=' . $token . '&oid=' . $order['order_id'] . '&pid=' . $order['product_id'] . '&opid=' . $order['order_product_id']; ?>'); $('#form').submit();">&nbsp;</span></div>	 		 
										<?php } else { ?>
										<span style="padding-right:20px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/not_paid.png) no-repeat scroll right center;">&nbsp;</span>
										<?php } ?>
									<?php } ?></td>
								  </tr>
								  <?php } ?>
								  <?php } else { ?>
								  <tr>
									<td class="center" colspan="10">Tidak Ada Hasil</td>
								  </tr>
								  <?php } ?>
							</tbody>
							</table>
							<div class="pagination"><?php echo $pagination; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'>
							<?php if (!$this->user->getVP()) { ?>
								<?php echo "Top 10 Sales History To Vendors" ?>
							<?php } else { ?>
								10 Pembayaran Terakhir dari Toko
							<?php } ?>
						</div>
						<div class='actions'> 
					  
						</div>
                    </div>
					
                    <div class='box-content'>
						<div class='responsive-table'>
							<table class="table list" id="history">
							<thead>
							  <tr>
								<?php if (!$this->user->getVP()) { echo '<td></td>'; } ?>
								<!--<th class="left" width="150"><?php echo $column_vendor_name; ?></th>-->
								<th class="left" width="275">Order Produk (ID Order - Nama Produk)</th>
								<!--<th class="left" width="120"><?php echo $column_payment_type; ?></th>-->
                                  <th class="left" width="200">Berkas Laporan</th>
                                  <th class="right" width="130">Jumlah Pembayaran (<?php echo $this->config->get('config_currency'); ?>)</th>
								<th class="left" width="100">Tanggal Pembayaran</th>
							  </tr>
							</thead>
							<tbody>
							  <?php if ($histories) { ?>
							  <?php foreach ($histories as $payment_history) { ?>
								<tbody id="history_<?php echo $payment_history['payment_id']; ?>">
								  <tr>
									<?php /*if (!$this->user->getVP()) { ?>
									<td class="left" style="width:3px;"><span style="cursor:pointer;padding-right:20px;display:inline-block;-moz-background-origin:padding;-moz-background-inline-policy:continuous;-moz-background-clip:border;color:#000000;background:transparent url(view/image/delete.png) no-repeat scroll right center;" onclick="removeHistory('<?php echo $payment_history['payment_id']; ?>');">&nbsp;</span></td>
									<?php } ?>
									<td class="left"><?php echo $payment_history['name']; ?></td>
                                    */ ?>
									<td class="left" >
									<?php foreach ($payment_history['details'] AS $orders) { ?>
									[<?php echo $orders['order_id']; ?> - <?php echo $orders['product_name']; ?>]
									<?php } ?>
									</td>
									<!--<td class="left"><?php echo $payment_history['payment_type']; ?></td>-->
                                    <td class="left">
										<?php //echo $payment_history['payment_remark']; ?>
										<?php //echo "<img src='".$payment_history['payment_image']."' />"; ?>
										<a href="#" target="_blank" style="text-decoration: underline; color: #49bf67;">View</a>
									</td>
									<td class="right">-<?php echo $payment_history['amount']; ?></td>
									<td class="left"><?php echo $payment_history['date']; ?></td>
								  </tr>
								</tbody>
							  <?php } ?>
							  <?php } else { ?>
							  <tr>
								<td class="center" colspan="<?php if (!$this->user->getVP()) { echo '7'; } else { echo '6'; } ?>">Tidak Ada Hasil</td>
							  </tr>
							  <?php } ?>
							</tbody>
						  </table>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>	


<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=report/vendor_transaction&token=<?php echo $token; ?>';
	
	var filter_date_start = $('input[name=\'filter_date_start\']').attr('value');
	
	if (filter_date_start) {
		url += '&filter_date_start=' + encodeURIComponent(filter_date_start);
	}

	var filter_date_end = $('input[name=\'filter_date_end\']').attr('value');
	
	if (filter_date_end) {
		url += '&filter_date_end=' + encodeURIComponent(filter_date_end);
	}
		
	var filter_vendor_group = $('select[name=\'filter_vendor_group\']').attr('value');
	
	if (filter_vendor_group) {
		url += '&filter_vendor_group=' + encodeURIComponent(filter_vendor_group);
	}
	
	var filter_paid_status = $('select[name=\'filter_paid_status\']').attr('value');
	
	if (filter_paid_status) {
		url += '&filter_paid_status=' + encodeURIComponent(filter_paid_status);
	}
	
	var filter_order_status_id = $('select[name=\'filter_order_status_id\']').attr('value');
	
	if (filter_order_status_id) {
		url += '&filter_order_status_id=' + encodeURIComponent(filter_order_status_id);
	}	

	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$('#submit_add_payment').click(function() {
    var $dialog = $('<div></div>')
    .load('view/template/report/choose_payment_type.html')
    .dialog({
        autoOpen: false,
        title: 'Select Payment Methods',
        buttons: {
            "Add Payment": function() {
				var pay_type = $("input[name=RadioGroup]:checked").val();
				var paypal_standard = $("input[name=paypal_standard]").val();
				var pay_cheque = $("input[name=pay_cheque]").val() + $("input[name=cheque_no]").val();
				var other_payment_method = $("input[name=other_payment_method]").val() + $("input[name=other_payment]").val();
				
				switch(pay_type) {
					case 'paypal_direct':
					$('#form').attr('action', paypal_standard); $('#form').submit();
					break;
					
					case 'cheque':
					$('#form').attr('action', pay_cheque); $('#form').submit();
					break;
					
					case 'other':
					$('#form').attr('action', other_payment_method); $('#form').submit();
					break;
				}
	
            },
            "Cancel": function() {
                $(this).dialog("close");
            }
        }
    });
    $dialog.dialog('open');
});
//--></script>
<script type="text/javascript"><!--
function removeHistory(id) {
	$.ajax({
		url: 'index.php?route=report/vendor_transaction/removeHistory&token=<?php echo $token; ?>&payment_id=' + id,
		dataType: 'json',
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#history').before('<div class="attention"><img src="view/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		
		complete: function() {
			$('.attention').remove();
		},
		
		success: function(data) {
			$('#history_' + id).remove();
			$('#history').before('<div class="success">' + data.success + '</div>');
		}
			
	});
}
//--></script>
<script type="text/javascript"><!--
$(document).ready(function() {
	//$('#date-start').datepicker({dateFormat: 'yy-mm-dd'});
	
	//$('#date-end').datepicker({dateFormat: 'yy-mm-dd'});
});
//--></script> 
<?php echo $footer; ?>