<?php echo $header; ?>
<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-bar-chart'></i>
					  <span>Statistik Produk Dilihat</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>					
							<a onclick="location = '<?php echo $reset; ?>';" class="btn btn-warning">Ulang</a>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<!--Start form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'>Produk Dilihat</div>
						<div class='actions'> 
					  
						</div>
                    </div>
					
                    <div class='box-content'>
						<div class='responsive-table'>
							<table class="list table">
							<thead>
							  <tr>
								<th class="left">Nama Produk</th>
								<th class="left"><?php echo $column_model; ?></th>
								<th class="right">Dilihat</th>
								<th class="right">Perseratus</th>
							  </tr>
							</thead>
							<tbody>
							  <?php if ($products) { ?>
							  <?php foreach ($products as $product) { ?>
							  <tr>
								<td class="left"><?php echo $product['name']; ?></td>
								<td class="left"><?php echo $product['model']; ?></td>
								<td class="right"><?php echo $product['viewed']; ?></td>
								<td class="right"><?php echo $product['percent']; ?></td>
							  </tr>
							  <?php } ?>
							  <?php } else { ?>
							  <tr>
								<td class="center" colspan="4"><?php echo $text_no_results; ?></td>
							  </tr>
							  <?php } ?>
							</tbody>
						  </table>
						  <div class="pagination"><?php echo $pagination; ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>

<?php echo $footer; ?>