<?php echo $header; ?>
<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-cogs'></i>
					  <span>Voucher</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>					
							<a onclick="$('#form').submit();" id="a_Submit" class="btn btn-white"><?php echo $button_save; ?></a>
							<a href="<?php echo $cancel; ?>" class="btn btn-white"><?php echo $button_cancel; ?></a>
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
			</div>
		</div>
		
		<!--Start Form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header green-background '>
						<div class='title'><div class='icon-edit'></div> Voucher Detail</div>
						<div class='actions'> 
							
						</div>
                    </div>
					
					<div class='box-content'>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal validate-form">
							<div class='' style='margin-top: 20px'>
								<table class="form data-table table table-bordered table-striped">
									<tr>
										<td><?php echo $txt_voucher_no; ?></td>
										<td><?php echo $voucher_info['voucher_no']; ?></td>
									</tr>
									<tr>
										<td><?php echo $txt_security_code; ?></td>
										<td><?php echo $voucher_info['security_code']; ?></td>
									</tr>
									<tr>
										<td><?php echo $txt_customer_name; ?></td>
										<td><a href="<?php echo $cust_link ?>"><?php echo $voucher_info['cust_fn']; ?> <?php echo $voucher_info['cust_ln']; ?></a></td>
									</tr>
									<tr>
										<td><?php echo $txt_vendor; ?></td>
										<td><a href="<?php echo $vendor_link ?>"><?php echo $voucher_info['vendor_name']; ?></a></td>
									</tr>
									<tr>
										<td><?php echo $txt_product; ?></td>
										<td><a href="<?php echo $product_link ?>"><?php echo $voucher_info['name']; ?></a></td>
									</tr>
									<tr>
										<td><?php echo $txt_created_at; ?></td>
										<td><?php echo $voucher_info['created_at']; ?></td>
									</tr>
									<tr>
										<td><?php echo $txt_updated_at; ?></td>
										<td><?php if( $voucher_info['updated_at'] == null){ echo "N/A"; }else{ echo $voucher_info['updated_at'] . "by " . $voucher_info['username'] ; } ?></td>
									</tr>
									<tr>
										<td style="width: 200px;"><?php echo $entry_status; ?></td>
										<td>
											<select name="status" class="form-control" style="width: 150px;">
												<?php if ($voucher_info['pv_status'] == 0) { ?>
												<option value="0" selected="selected"><?php echo $txt_status_unused; ?></option>
												<option value="1"><?php echo $txt_status_used; ?></option>
												<option value="2"><?php echo $txt_status_void; ?></option>
												<?php } elseif ($voucher_info['pv_status'] == 1) { ?>
												<option value="0"><?php echo $txt_status_unused; ?></option>
												<option value="1" selected="selected"><?php echo $txt_status_used; ?></option>
												<option value="2"><?php echo $txt_status_void; ?></option>
												<?php } else { ?>
												<option value="0"><?php echo $txt_status_unused; ?></option>
												<option value="1"><?php echo $txt_status_used; ?></option>
												<option value="2" selected="selected"><?php echo $txt_status_void; ?></option>
												<?php } ?>
											</select>
										</td>
									</tr>
								</table>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>	
</div>				
<?php echo $footer; ?>