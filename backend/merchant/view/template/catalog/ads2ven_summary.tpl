<?php echo $header; ?>

    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-table'></i>
                            <span>Advertising</span>
                        </h1>

                        <div class='pull-right'>
                            <ul class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        Advertising
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Summary</li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                    <?php if ($success) { ?>
                        <div class='alert alert-success alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
            <div class="col-sm-12">
                <div class="box">
                    <div class="box-header">
                        <div class="title">Quick links</div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-sm-3">
                                <div class="box-quick-link blue-background">
                                    <a>
                                        <div class="header">
                                            <div class="icon-comments"></div>
                                        </div>
                                        <div class="content">Comments</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box-quick-link green-background">
                                    <a>
                                        <div class="header">
                                            <div class="icon-star"></div>
                                        </div>
                                        <div class="content">Veeeery long title of this quick link</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box-quick-link orange-background">
                                    <a>
                                        <div class="header">
                                            <div class="icon-magic"></div>
                                        </div>
                                        <div class="content">Magic</div>
                                    </a>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <div class="box-quick-link red-background">
                                    <a>
                                        <div class="header">
                                            <div class="icon-inbox"></div>
                                        </div>
                                        <div class="content">Orders</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>

            <div class='row'>
                <div class='col-sm-12'>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="well comment">
                                For more information, send email to <a href="mailto:advertising@serbamall.com" style="text-decoration:underline;font-weight:bold; color:#ec1719;">advertising@serbamall.com</a>
                            </div>
                        </div>
                    </div>

                    <div class="row pricing-tables">
                        <div class="col-sm-6 col-md-3">
                            <div class="pricing-table">
                                <div class="header">MY BALANCE</div>
                                <div class="price blue-background">
                                    <span>$20</span>
                                </div>
                                <ul class="list-unstyled features">
                                    <li>
                                        <strong>10</strong>
                                        Banner Ads
                                    </li>
                                    <li>
                                        <strong>2</strong>
                                        Product Ads
                                    </li>
                                    <li>
                                        <strong>20</strong>
                                        Keyword Ads
                                    </li>
                                </ul>
                                <div class="footer">
                                    <a class="btn btn-primary" href="#"><i class="icon-signin"></i>
                                        Top Up
                                    </a>

                                </div>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-3">
                            <div class="box">
                                <div class="box-header">
                                    <div class="title">
                                        <i class="icon-group"></i>
                                        Banner Ads <br />
                                        <small>(Last 30 days)</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-error">0</h3>
                                            <small>Purchased Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-warning">0</h3>
                                            <small>Live Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-primary">0</h3>
                                            <small>Expired Ads</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="box">
                                <div class="box-header">
                                    <div class="title">
                                        <i class="icon-group"></i>
                                        Product Ads <br />
                                        <small>(Last 30 days)</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-error">0</h3>
                                            <small>Purchased Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-warning">0</h3>
                                            <small>Live Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-primary">0</h3>
                                            <small>Expired Ads</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-6 col-md-3">
                            <div class="box">
                                <div class="box-header">
                                    <div class="title">
                                        <i class="icon-group"></i>
                                        Keyword Ads <br />
                                        <small>(Last 30 days)</small>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-error">0</h3>
                                            <small>Purchased Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-warning">0</h3>
                                            <small>Live Ads</small>
                                        </div>
                                        <div class="box-content box-statistic">
                                            <h3 class="title text-primary">0</h3>
                                            <small>Expired Ads</small>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-sm-12">
                        <div class="box">
                            <div class="box-header">
                                <div class="title">
                                    Banner Ads
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <address>
                                            Purchase Billboard Banner in Popular page in serbaMall to advertise your product!
                                        </address>
                                    </div>
                                    <div class="col-sm-6">
                                        <blockquote>
                                            <a href="<?= $url_banner_info ?>" style="left: 650px;"> click to see more detail</a>
                                        </blockquote>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-header">
                                <div class="title">
                                    Product Ads
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <address>
                                            Purchase Product Ads and advertise your product in most popular page in serbaMall.
                                        </address>
                                    </div>
                                    <div class="col-sm-6">
                                        <blockquote>
                                            <a href="<?= $url_product_info ?>" style="left: 650px;"> click to see more detail</a>
                                        </blockquote>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box">
                            <div class="box-header">
                                <div class="title">
                                    Keyword Ads
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <address>
                                            Purchase Keyword Ads and advertise your product in most popular page in elevenia.
                                        </address>
                                    </div>
                                    <div class="col-sm-6">
                                        <blockquote>
                                            <a href="<?= $url_keyword_info ?>" style="left: 650px;"> click to see more detail</a>
                                        </blockquote>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
<?php echo $footer; ?>