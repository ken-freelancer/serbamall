<?php echo $header; ?>

<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-gear '></i>
					  <span>Pengaturan Pengiriman</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>
							<a onclick="$('#form').submit();" class="btn btn-primary"><span>Simpan</span></a>
							<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white">Batal</span></a>
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
				<?php if ($success) { ?>
					<div class='alert alert-success alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($success) ? $success : '' ).'<br>'; ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<!--Start Form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'><div class='icon-gear'></div>Pengiriman Servis</div>
						<div class='actions'> 

						</div>
                    </div>
					
					<div class='box-content'>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal">
							<div class='tabbable' style='margin-top: 20px'>
								<ul class='nav nav-responsive nav-tabs'>
									<li class='active'>
										<a data-toggle='tab' href='#tab-shipping-service'>
											Pengiriman Servis
										</a>
									</li>
                                    <?php //Multiple Shipping ?>
                                    <li class=''>
                                        <a data-toggle='tab' href='#tab-utm-shipping'>
                                            Pengaturan Pengiriman 
                                        </a>
                                    </li>
                                    <?php //.Multiple Shipping ?>
								</ul>
								<div class='tab-content'>
									
									<div id="tab-shipping-service" class="tab-pane active" >
                                        <table border="0" style="width:100%; text-align:center;">
                                                <tbody id="ultm-shipping-row<?php echo $ultm_shipping_row; ?>">
                                               <?php 
                                               $count= "0";

                                               if ($couriers){

                                               $courier = array_chunk($couriers,6);
                                               		foreach($courier as $row){
                                               			echo "<tr>";
                                               			foreach ($row as $entry) {
                                               				echo "
                                               				<td style='padding-top:20px;'>
                                               				<img width='120px' height='100px' src='../image/" . $entry['courier_image'] . "' /> </br></br>
                                               				<span>" . $entry['courier_name'] . "</span>
                                               				</td>";
                                               			}
                                               			echo "</tr>";
                                               		}
                                               }
                                               ?>
                                           <!-- <tr>
                                           	<?php if ($couriers) {
                                           		
                                           		foreach ($couriers as $courier) {
                                           			
                                               			echo "<td>" . $courier['courier_name'] . "</td>";
                                           		}
                                           	}
                                           	?>
                                           </tr> -->
                                                </tbody>
                                        </table>
                                    </div>

									<?php //Multiple Shipping ?>
                                    <div id="tab-utm-shipping-setting" class="tab-pane" >
                                        <table class="form table">
                                            <tr>
                                                <td><?php echo $entry_measurement; ?></td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-6"><select class="form-control" name="measurement_id">
                                                        <?php if (($measurement_id==0) || (empty($measurement_id)) ) { ?>
                                                            <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php /* Ken - Disable the option if ($measurement_id==1) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1" selected="selected"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php if ($measurement_id==2) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2" selected="selected"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php if ($measurement_id==3) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3" selected="selected"><?php echo $text_tapv_text; ?></option>
                                                        <?php }*/ ?>
                                                    </select>
                                                            </div></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="tab-utm-shipping" class="tab-pane" >
                                        <table id="ultm-shipping" class="list form table">
                                            <thead>
                                            <tr>
                                                <td class="left">Kurir Pengiriman</td>
                                                <td class="right">Harga Pengiriman</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                                <tbody id="ultm-shipping-row<?php echo $ultm_shipping_row; ?>">
                                                <tr>
                                                    <td class="left">Self Delivery (Same Provision) - Type -1 for not available<br />Sample 10:10000,20:60000,30:120000</td>
                                                    <td class="right"><input type="text" class="form-control" name="vendor_ultimate_shipping[0][shipping_rate]" value="<?php echo isset($vendor_ultimate_shippings[0]['shipping_rate'])?$vendor_ultimate_shippings[0]['shipping_rate']:'-1'; ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Self Delivery (Other from Same Provision - Type -1 for not available<br />Sample 10:30000,20:120000,30:320000</td>
                                                    <td class="right"><input type="text" class="form-control" name="vendor_ultimate_shipping[1][shipping_rate]" value="<?php echo isset($vendor_ultimate_shippings[1]['shipping_rate'])?$vendor_ultimate_shippings[1]['shipping_rate']:'-1'; ?>" /></td>
                                                </tr>
                                                </tbody>
                                        </table>
                                    </div>
                                    <?php //.Multiple Shipping ?>
									
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>


<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor_profile/zone&token=<?php echo $token; ?>&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><br><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="vendor_gallery[' + image_row + '][image]" value="" id="image' + image_row + '" /><br><br><a class="btn" onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">Browses File</a> <a class="btn" onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');">Clear Image</a></div></td>';
	html += '    <td class="right"><input class="form-control" type="text" name="vendor_gallery[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove(); checkStatus();" class="btn btn-sm btn-warning" style="margin-left: 20px;">Remove</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
	
	if(image_row >= 3)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}
	
}

function checkStatus(){
	
	var stt_image_row = $('#tab-gallery tbody').length;
	console.log(stt_image_row);
	if(stt_image_row >= 5)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}else{
		$('#a_LimitAddButton').css('display', 'inline-block');
	}
	
}
//--></script>

<?php //Multiple Shipping ?>
    <script type="text/javascript"><!--
        var ultm_shipping_row = <?php echo $ultm_shipping_row; ?>;

        function add_ultm_Shipping() {
            html  = '<tbody id="ultm-shipping-row' + ultm_shipping_row + '">';
            html += '  <tr>';
            html += '  <td class="text-left"><select class="form-control" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][courier_id]">';
            <?php foreach ($couriers as $courier) { ?>
            html += '      <option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';
            html += '    <input type="hidden" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][vutm_shipping_id]" value="" />';
            html += '    <td class="right"><input class="form-control" type="text" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][shipping_rate]" value="" /></td>';
            html += '    <td class="right" style="display:none;"><select name="vendor_ultimate_shipping[' + ultm_shipping_row + '][geo_zone_id]">';
            <?php foreach ($geo_zones as $geo_zone) { ?>
            html += '      <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';

            html += '    <td class="left"><a onclick="$(\'#ultm-shipping-row' + ultm_shipping_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
            html += '  </tr>';
            html += '</tbody>';

            $('#ultm-shipping tfoot').before(html);
            $('#ultm-shipping-row' + ultm_shipping_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

            ultm_shipping_row++;
        }
        //--></script>
<?php //.Multiple Shipping ?>
<?php echo $footer; ?>