<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
      <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a href="<?php echo $cancel; ?>" class="button"><?php echo $button_cancel; ?></a></div>
    </div>
    <div class="content">

      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
        <div id="tab-general">
            <table class="form">
              <tr>
                <td><?php echo $txt_voucher_no; ?></td>
                <td><?php echo $voucher_info['voucher_no']; ?></td>
              </tr>
              <tr>
                <td><?php echo $txt_security_code; ?></td>
                <td><?php echo $voucher_info['security_code']; ?></td>
              </tr>
              <tr>
                <td><?php echo $txt_customer_name; ?></td>
                <td><a href="<?php echo $cust_link ?>"><?php echo $voucher_info['cust_fn']; ?> <?php echo $voucher_info['cust_ln']; ?></a></td>
              </tr>
              <tr>
                <td><?php echo $txt_vendor; ?></td>
                <td><a href="<?php echo $vendor_link ?>"><?php echo $voucher_info['vendor_name']; ?></a></td>
              </tr>
              <tr>
                <td><?php echo $txt_product; ?></td>
                <td><a href="<?php echo $product_link ?>"><?php echo $voucher_info['name']; ?></a></td>
              </tr>
              <tr>
                <td><?php echo $txt_created_at; ?></td>
                <td><?php echo $voucher_info['created_at']; ?></td>
              </tr>
              <tr>
                <td><?php echo $txt_updated_at; ?></td>
                <td><?php if( $voucher_info['updated_at'] == null){ echo "N/A"; }else{ echo $voucher_info['updated_at'] . "by " . $voucher_info['username'] ; } ?></td>
              </tr>
                <tr>
                    <td><?php echo $entry_status; ?></td>
                    <td>
                        <select name="status">
                            <?php if ($voucher_info['pv_status'] == 0) { ?>
                            <option value="0" selected="selected"><?php echo $txt_status_unused; ?></option>
                            <option value="1"><?php echo $txt_status_used; ?></option>
                            <option value="2"><?php echo $txt_status_void; ?></option>
                            <?php } elseif ($voucher_info['pv_status'] == 1) { ?>
                            <option value="0"><?php echo $txt_status_unused; ?></option>
                            <option value="1" selected="selected"><?php echo $txt_status_used; ?></option>
                            <option value="2"><?php echo $txt_status_void; ?></option>
                            <?php } else { ?>
                            <option value="0"><?php echo $txt_status_unused; ?></option>
                            <option value="1"><?php echo $txt_status_used; ?></option>
                            <option value="2" selected="selected"><?php echo $txt_status_void; ?></option>
                            <?php } ?>
                        </select>
                    </td>
              </tr>
            </table>
          </div>
      </form>
    </div>
  </div>
</div>

			
<?php echo $footer; ?>