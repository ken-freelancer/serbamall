<?php echo $header; ?>

    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-table'></i>
                            <span>Advertising - Keyword Advertisement</span>
                        </h1>

                        <div class='pull-right'>
                            <ul class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        Advertising
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Keyword Advertisement</li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                    <?php if ($success) { ?>
                        <div class='alert alert-success alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="title">Quick links</div>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="box-quick-link blue-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-comments"></div>
                                            </div>
                                            <div class="content">Comments</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link green-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-star"></div>
                                            </div>
                                            <div class="content">Veeeery long title of this quick link</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link orange-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-magic"></div>
                                            </div>
                                            <div class="content">Magic</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link red-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-inbox"></div>
                                            </div>
                                            <div class="content">Orders</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='row'>
                <div class='col-sm-12'>

                    <div class="box">
                        <div class="box-header">
                            <div class="title">
                                Keyword Ads
                            </div>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h4>
                                        Purchase Keyword Ads and advertise your product in most popular page in serbaMall.
                                    </h4>
                                </div>
                                <div class="col-sm-12">
                                    <input class="btn btn-danger btn-block btn-lg" value="Buy Now" type="button" onclick="window.location.href='<?= $url_buynow; ?>';">
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="col-sm-12">
                                        Image Guide
                                        <div class="box bordered-box purple-border" style="margin-bottom:0;">
                                            <div class="box-header purple-background">
                                                <div class="title">01. Top 100 Ads</div>
                                                <div class="actions">
                                                    <!--<a class="btn box-remove btn-xs btn-link" href="#"><i class="icon-remove"></i>
                                                    </a>

                                                    <a class="btn box-collapse btn-xs btn-link" href="#"><i></i>
                                                    </a>-->
                                                </div>
                                            </div>
                                            <div class="box-content box-no-padding">
                                                <table class="table table-striped" style="margin-bottom:0;">
                                                    <thead>
                                                    <tr>
                                                        <th>
                                                            Displaying Location
                                                        </th>
                                                        <th>
                                                            Availability Slot
                                                        </th>
                                                        <th>
                                                            Period
                                                        </th>
                                                        <th>
                                                            Price
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <tr>
                                                        <td>Top 100 - ALL </td>
                                                        <td>4 Slot</td>
                                                        <td>
                                                            Minimum 1 Day <br> Maximum  7 Day
                                                        </td>
                                                        <td>
                                                            Rp 50.000 per day
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Top 100 - META </td>
                                                        <td>4 Slot</td>
                                                        <td>
                                                            Minimum 1 Day <br> Maximum  7 Day
                                                        </td>
                                                        <td>
                                                            Rp 30.000 per day
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--.row-->

            </div>

        </div>
    </div>

<?php echo $footer; ?>