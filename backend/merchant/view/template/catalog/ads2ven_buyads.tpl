<?php echo $header; ?>

<link href="https://cdn.datatables.net/1.10.10/css/jquery.dataTables.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="https://cdn.datatables.net/select/1.1.0/css/select.dataTables.min.css" media="all" rel="stylesheet" type="text/css" />
<script src="https://cdn.datatables.net/1.10.10/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdn.datatables.net/select/1.1.0/js/dataTables.select.min.js" type="text/javascript"></script>




    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-table'></i>
                            <span>Buy Advertisement</span>
                        </h1>

                        <div class='pull-right'>
                            <ul class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        Advertising
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Buy Ads</li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                    <?php if ($success) { ?>
                        <div class='alert alert-success alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="title">Quick links</div>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="box-quick-link blue-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-comments"></div>
                                            </div>
                                            <div class="content">Comments</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link green-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-star"></div>
                                            </div>
                                            <div class="content">Veeeery long title of this quick link</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link orange-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-magic"></div>
                                            </div>
                                            <div class="content">Magic</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link red-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-inbox"></div>
                                            </div>
                                            <div class="content">Orders</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12 box">
                    <div class="box-header green-background">
                        <div class="title">Select Advertisement</div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="tabbable tabs-left">
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#lA">
                                                Product Ads
                                            </a>
                                        </li>
                                        <li class="">
                                            <a data-toggle="tab" href="#lB">
                                                Keyword Ads
                                            </a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div class="tab-pane active" id="lA">
                                           <div class="row">
                                               <div class="col-sm-3">
                                                   <div class="radio">

                                                       <label>
                                                           <input id="top_100real" checked="checked" type="radio" value="product_ads_type_top100" name="product_ads_type_real[]" style="z-index:99">
                                                           <input id="top_100" type="radio" value="product_ads_type_top100" name="product_ads_type[]" data-toggle="tab"  href="#top100_panel">
                                                           Top 100 Ads
                                                       </label>
                                                   </div>
                                                   <div class="radio">
                                                       <label>
                                                           <input id="dealreal" type="radio" value="product_ads_type_deal" name="product_ads_type_real[]" style="z-index:99">
                                                           <input id="deal" type="radio" value="product_ads_type_deal"  name="product_ads_type[]" data-toggle="tab"  href="#deal_panel">
                                                           Deals Ads
                                                       </label>
                                                   </div>

                                                   <script>
                                                       $( "#top_100" ).on( "click", function( event ) {
                                                           $('#top_100real').attr('checked', 'checked');
                                                       });
                                                       $( "#top_100real" ).on( "click", function( event ) {
                                                           $('#top_100').trigger( "click" );
                                                       });
                                                       $( "#deal" ).on( "click", function( event ) {
                                                           $('#dealreal').attr('checked', 'checked');
                                                       });
                                                       $( "#dealreal" ).on( "click", function( event ) {
                                                           $('#deal').trigger( "click" );
                                                       });
                                                   </script>

                                               </div>
                                               <div class="tab-content">
                                                   <div class="col-sm-6 tab-pane active" id="top100_panel">
                                                       <div class="col-sm-6">
                                                           <h5>Top 100 Ads</h5>
                                                       Image 1
                                                       </div>
                                                       <div class="col-sm-6">
                                                           <h5>Display Location :</h5>
                                                           <p>Top of the serbaMall Top100 page (All, 8 Meta Category)</p>

                                                           <h5>Display Method :</h5>
                                                           <p>Fixed Display.</p>

                                                           <h5>Price :</h5>
                                                           <p>Rp 30.000 ~ Rp 50.000 per Day.</p>
                                                       </div>
                                                   </div>

                                                   <div class="col-sm-6 tab-pane" id="deal_panel">
                                                       <div class="col-sm-6">
                                                           <h5>Deal Ads</h5>
                                                       Image 2
                                                       </div>
                                                       <div class="col-sm-6">
                                                           <h5>Display Location :</h5>
                                                           <p>Bottom of the Daily, Shocking Deal page.</p>

                                                           <h5>Display Method :</h5>
                                                           <p>Fixed Display.</p>

                                                           <h5>Price :</h5>
                                                           <p>Rp 30.000 per Day.</p>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                        </div>
                                        <div class="tab-pane" id="lB">
                                            <div class="row">
                                                <div class="col-sm-3">
                                                    <div class="radio">

                                                        <label>
                                                            <input id="kw_listing_adsreal" checked="checked" type="radio" value="kw_ads_type_listing" name="kw_ads_type_real[]" style="z-index:99">
                                                            <input id="kw_listing_ads" type="radio" value="kw_ads_type_listing" name="kw_ads_type[]" data-toggle="tab"  href="#kw_listing_panel">
                                                            Search Listing Ads
                                                        </label>
                                                    </div>
                                                    <div class="radio">
                                                        <label>
                                                            <input id="kw_window_adsreal" type="radio" value="kw_ads_type_window" name="kw_ads_type_real[]" style="z-index:99">
                                                            <input id="kw_window_ads" type="radio" value="kw_ads_type_window"  name="kw_ads_type[]" data-toggle="tab"  href="#kw_window_panel">
                                                            Search Window Ads
                                                        </label>
                                                    </div>

                                                    <script>
                                                        $( "#kw_listing_ads" ).on( "click", function( event ) {
                                                            $('#kw_listing_adsreal').attr('checked', 'checked');
                                                        });
                                                        $( "#kw_listing_adsreal" ).on( "click", function( event ) {
                                                            $('#kw_listing_ads').trigger( "click" );
                                                        });
                                                        $( "#kw_listing_ads" ).on( "click", function( event ) {
                                                            $('#kw_listing_adsreal').attr('checked', 'checked');
                                                        });
                                                        $( "#kw_listing_adsreal" ).on( "click", function( event ) {
                                                            $('#kw_listing_ads').trigger( "click" );
                                                        });
                                                    </script>

                                                </div>
                                                <div class="tab-content">
                                                    <div class="col-sm-6 tab-pane active" id="top100_panel">
                                                        <div class="col-sm-6">
                                                            <h5>Top 100 Ads</h5>
                                                            Image 1
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Display Location :</h5>
                                                            <p>Top of the serbaMall Top100 page (All, 8 Meta Category)</p>

                                                            <h5>Display Method :</h5>
                                                            <p>Fixed Display.</p>

                                                            <h5>Price :</h5>
                                                            <p>Rp 30.000 ~ Rp 50.000 per Day.</p>
                                                        </div>
                                                    </div>

                                                    <div class="col-sm-6 tab-pane" id="deal_panel">
                                                        <div class="col-sm-6">
                                                            <h5>Deal Ads</h5>
                                                            Image 2
                                                        </div>
                                                        <div class="col-sm-6">
                                                            <h5>Display Location :</h5>
                                                            <p>Bottom of the Daily, Shocking Deal page.</p>

                                                            <h5>Display Method :</h5>
                                                            <p>Fixed Display.</p>

                                                            <h5>Price :</h5>
                                                            <p>Rp 30.000 per Day.</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

            <!--<div class="row">
                <div class="col-sm-12 box">
                    <div class="box-header green-background">
                        <div class="title">Select Product to Advertise</div>
                    </div>
                    <div class="box-content">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="col-sm-3">
                                    <label class="control-label col-sm-12 col-xs-12">Search Criteria</label>

                                </div>
                                <div class="col-sm-9">
                                    <div class="col-xs-6 col-md-4">
                                        <select class="form-control">
                                            <option value="">All</option>
                                            <option value="">Product Name</option>
                                            <option value="">Product SKU</option>
                                        </select>
                                    </div>
                                    <div class="col-xs-6 col-md-6">
                                        <input class="form-control" placeholder="" type="text">
                                    </div>

                                </div>

                            </div>

                        </div>
                        <div class="row">
                            <div class="col-sm-12" style="text-align:center;margin-top:10px;">
                                <button class="btn btn-primary btn-lg" style="margin-bottom:5px">
                                    Search
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->

            <div class="row">
                <div class="col-sm-12">
                    <div class="box bordered-box red-border" style="margin-bottom:0;">
                        <div class="box-header red-background">
                            <div class="title">My Product</div>
                            <div class="actions">

                            </div>
                        </div>
                        <div class="box-content box-no-padding">
                            <div class="responsive-table">
                                <div class="scrollable-area">
                                    <table id="vendor_product" class="table table-hover table-striped display" cellspacing="0" width="100%" style="margin-bottom:0;">
                                        <thead>
                                        <tr>
                                            <th></th>
                                            <th>
                                                Name
                                            </th>
                                            <th>
                                                E-mail
                                            </th>
                                            <th>
                                                Status
                                            </th>
                                            <th></th>
                                            <th></th>
                                            <th></th>
                                        </tr>
                                        </thead>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>
<script>
$(document).ready(function() {
    $('#vendor_product').DataTable( {
        "processing": true,
        "serverSide": true,
        "ajax": "<?= HTTPS_SERVER ?>server_side/server_processing.php",
        columnDefs: [ {
            orderable: false,
            className: 'select-checkbox',
            targets:   0
        } ],
        select: {
            style:    'os',
            selector: 'td:first-child'
        },
        order: [[ 1, 'asc' ]]
    } );
} );
</script>
<?php echo $footer; ?>

