<?php echo $header; ?>


    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-gear '></i>
                            <span>Tinjauan</span>
                        </h1>

                        <div class='pull-right'>
                            <div class='btn-group'>
                                <a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white"><span>Kembali</span></a>
                            </div>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                </div>
            </div>

            <!--Start Form-->
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='box bordered-box green-border' style='margin-bottom:0;'>
                        <div class='box-header blue-background '>
                            <div class='title'><div class='icon-gear'></div>Tinjauan</div>
                            <div class='actions'>

                            </div>
                        </div>

                        <div class='box-content'>
                            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal">
                                <div class='tabbable' style='margin-top: 20px'>

                                    <div class='tab-content'>

                                        <div id="tab-general" class="tab-pane active">
                                            <table class="form table" >
                                                <tr>
                                                    <td><span class="required">*</span> Penulis: </td>
                                                    <td><input readonly="readonly" type="text" name="author" value="<?php echo $author; ?>" />
                                                        <?php if ($error_author) { ?>
                                                            <span class="error"><?php echo $error_author; ?></span>
                                                        <?php } ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Produk :</td>
                                                    <td><input readonly="readonly" type="text" name="product" value="<?php echo $product; ?>" />
                                                        [<a target="_blank" href="<?php echo $product_link; ?>">Tampilkan Produk</a>]
                                                        <input type="hidden" name="product_id" value="<?php echo $product_id; ?>" />
                                                        <?php if ($error_product) { ?>
                                                            <span class="error"><?php echo $error_product; ?></span>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td><span class="required">*</span> Teks: </td>
                                                    <td><textarea readonly="readonly" name="text" cols="60" rows="8"><?php echo $text; ?></textarea>
                                                        <?php if ($error_text) { ?>
                                                            <span class="error"><?php echo $error_text; ?></span>
                                                        <?php } ?></td>
                                                </tr>
                                                <tr>
                                                    <td>Penilaian</td>
                                                    <td><b class="rating">Buruk</b>&nbsp;
                                                        <?php if ($rating == 1) { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="1" checked />
                                                        <?php } else { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="1" />
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($rating == 2) { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="2" checked />
                                                        <?php } else { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="2" />
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($rating == 3) { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="3" checked />
                                                        <?php } else { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="3" />
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($rating == 4) { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="4" checked />
                                                        <?php } else { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="4" />
                                                        <?php } ?>
                                                        &nbsp;
                                                        <?php if ($rating == 5) { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="5" checked />
                                                        <?php } else { ?>
                                                            <input disabled="disabled" type="radio" name="rating" value="5" />
                                                        <?php } ?>
                                                        &nbsp; <b class="rating">Baik</b>
                                                        <?php if ($error_rating) { ?>
                                                            <span class="error"><?php echo $error_rating; ?></span>
                                                        <?php } ?></td>
                                                </tr>
                                                <tr>
                                                    <td><?php echo $entry_status; ?></td>
                                                    <td><select disabled="disabled" name="status">
                                                            <?php if ($status) { ?>
                                                                <option value="1" selected="selected">Aktifkan</option>
                                                                <option value="0">Non Aktifkan</option>
                                                            <?php } else { ?>
                                                                <option value="1">Aktifkan</option>
                                                                <option value="0" selected="selected">Non Aktifkan</option>
                                                            <?php } ?>
                                                        </select></td>
                                                </tr>
                                            </table>
                                        </div>

                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

<?php echo $footer; ?>