<?php echo $header; ?>

<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-table'></i>
					  <span>Produk</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>
							<a href="<?php echo $insert; ?>" class="btn btn-primary">Tambah Produk</a>
							<a onclick="$('form').submit();" class="btn btn-danger">Hapus Produk</a>
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
				<?php if ($success) { ?>
				 <div class="modal fade" id="myModal" role="dialog">
				    <div class="modal-dialog">
				    
				      <!-- Modal content-->
				      <div class="modal-content">
				        <div class="modal-header">
				          <button type="button" class="close" data-dismiss="modal">&times;</button>
				          <h4 class="modal-title">Berhasil.</h4>
				        </div>
				        <div class="modal-body">
				          <p>Anda telah menambahkan / memodifikasi produk. Produk baru ini segera tersedia di Serbamall dalam waktu 15 menit.</p>
				        </div>
				        <div class="modal-footer">
				          <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
				        </div>
				      </div>
				      
				    </div>
				  </div>

				  <script>
					$(document).ready(function(){
					        $("#myModal").modal();
					});
					</script>

					<div class='alert alert-success alert-dismissable' style="display:none;">
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($success) ? $success : '' ).'<br>'; ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<!--Start List-->
		<div class='row'>
			<div class='col-sm-12'>
				<!--Vincent 24/11/2015 - Changed "green" class to "blue" class  -->
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
                      <div class='title'>Daftar Produk</div>
                      <div class='actions'> 
					  
                      </div>
                    </div>
					
                    <div class='box-content box-no-padding'>
						<div class='responsive-table'>
							<div class='scrollable-area'>
								<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
									<table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
									<thead>
                                      <tr>
                                          <th width="1" style="text-align: center;"><input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" /></th>
                                          <th class="center">Gambar</th>
                                          <th class="left"><?php if ($sort == 'pd.name') { ?>
                                                  <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>">Nama Produk</a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_name; ?>">Nama Produk</a>
                                              <?php } ?></th>
                                          <th class="left"><?php if ($sort == 'p.model') { ?>
                                                  <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>">Model</a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_model; ?>">Model</a>
                                              <?php } ?></th>
                                          <!--code start-->
                                          <th class="left"><?php if ($sort == 'p.sku') { ?>
                                                  <a href="<?php echo $sort_sku; ?>" class="<?php echo strtolower($order); ?>">SKU</a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_sku; ?>">SKU</a>
                                              <?php } ?></th>
                                          <!--code end-->
                                          <th class="left"><?php if ($sort == 'p.price') { ?>
                                                  <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>">Harga</a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_price; ?>">Harga</a>
                                              <?php } ?></th>
                                          <th class="right"><?php if ($sort == 'p.quantity') { ?>
                                                  <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>">Jumlah</a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_quantity; ?>">Jumlah</a>
                                              <?php } ?></th>
                                          <th class="left"><?php if ($sort == 'p.status') { ?>
                                                  <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                              <?php } else { ?>
                                                  <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                              <?php } ?></th>
                                          <th class="right">Tindakan</th>
                                      </tr>
									</thead>
									<tbody>
                                    <tr class="filter">
                                        <td></td>
                                        <td></td>
                                        <td><input type="text" class="form-control" name="filter_name" value="<?php echo $filter_name; ?>" /></td>
                                        <td><input type="text" class="form-control" name="filter_model" value="<?php echo $filter_model; ?>" /></td>
                                        <!--code end-->
                                        <td><input type="text" class="form-control" name="filter_sku" value="<?php echo $filter_sku; ?>" /></td>
                                        <!--code end-->
                                        <td align="left"><input class="form-control" type="text" name="filter_price" value="<?php echo $filter_price; ?>" size="8"/></td>
                                        <td align="right"><input class="form-control" type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" style="width:100%" /></td>
                                        <td><select name="filter_status" class="form-control">
                                                <option value="*"></option>
                                                <?php if ($filter_status) { ?>
                                                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                                <?php } else { ?>
                                                    <option value="1"><?php echo $text_enabled; ?></option>
                                                <?php } ?>
                                                <?php if (!is_null($filter_status) && !$filter_status) { ?>
                                                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                                <?php } else { ?>
                                                    <option value="0"><?php echo $text_disabled; ?></option>
                                                <?php } ?>
                                            </select></td>
                                        <td align="right"><a onclick="filter();" class="btn btn-warning btn-sm"><?php echo $button_filter; ?></a></td>
                                    </tr>
                                    <?php if ( count($products) ) { ?>
										<?php foreach ($products as $product) { ?>
											<tr>
												<td style="text-align: center;"><?php if ($product['selected']) { ?>
													<input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
													<?php } else { ?>
													<input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
													<?php } ?>
												</td>
												<td class="center"><img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" style="padding: 1px; border: 1px solid #DDDDDD;" /></td>
												<td class="left"><?php echo $product['name']; ?></td>
												<td class="left"><?php echo $product['model']; ?></td>
												<!--code start-->
												<td class="left"><?php echo $product['sku']; ?></td>
												<!--code end-->
												<td class="left">
													<?php if ($product['special']) { ?>
														<span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
														<span style="color: #b00;"><?php echo $product['special']; ?></span>
													<?php } else { ?>
														<?php echo $product['price']; ?>
													<?php } ?>
												</td>
											  <td class="right">
												<?php if ($product['quantity'] <= 0) { ?>
													<span style="color: #FF0000;"><?php echo $product['quantity']; ?></span>
												<?php } elseif ($product['quantity'] <= 5) { ?>
													<span style="color: #FFA500;"><?php echo $product['quantity']; ?></span>
												<?php } else { ?>
													<span style="color: #008000;"><?php echo $product['quantity']; ?></span>
												<?php } ?></td>
											  <td class="left"><?php echo $product['status']; ?></td>
											  <td class="right"><?php foreach ($product['action'] as $action) { ?>
												<a class="btn-primary btn-sm" href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a>
                                                      <a class="btn-info btn-sm" target="_blank" href="<?php echo $action['href_view']; ?>">Tampilan</a>
												<?php } ?>
											</td>
										  
										  </tr>
										<?php } ?>
                                    <?php }else{ ?>
                                        <tr>
                                            <td colspan="10" style="text-align:center;font-weight: bold;">No Result Found!</td>
                                        </tr>
                                    <?php } ?>

                                    </tbody>
									</table>
								</form>
                                <div class="pagination"><?php echo $pagination; ?></div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</div>
<?php if ( count($products) <= 0 && ($empty_result)) { ?>
<script type="text/javascript"><!--
    setTimeout('location = \'<?php echo $continue; ?>\';', 2500);
//--></script>
<?php } ?>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=catalog/pro2ven&token=<?php echo $token; ?>';
	
	var filter_name = $('input[name=\'filter_name\']').attr('value');
	
	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}
	
	var filter_model = $('input[name=\'filter_model\']').attr('value');
	
	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}
	
	<!--code start-->
	var filter_sku = $('input[name=\'filter_sku\']').attr('value');
	
	if (filter_sku) {
		url += '&filter_sku=' + encodeURIComponent(filter_sku);
	}
	<!--code end-->
	
	var filter_price = $('input[name=\'filter_price\']').attr('value');
	
	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}
	
	var filter_quantity = $('input[name=\'filter_quantity\']').attr('value');
	
	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}	

	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	delay: 0,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_name\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});

$('input[name=\'filter_model\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.model,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_model\']').val(ui.item.label);
						
		return false;
	},
	focus: function(event, ui) {
      	return false;
   	}
});
<!--code start-->
$('input[name=\'filter_sku\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/autocomplete&token=<?php echo $token; ?>&filter_sku=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.sku,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'filter_sku\']').val(ui.item.label);
						
		return false;
	}
});
<!--code end-->
//--></script> 
<?php echo $footer; ?>