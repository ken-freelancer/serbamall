<?php echo $header; ?>
    <script id="script_select2">
        $(document).ready(function () {
            $.getJSON('<?php echo $json_tag; ?>', function(opts){
                $(".select2-tags").select2({
                    tags: opts,
                    tokenSeparators: [",", " "],
                    placeholder: "Ketik label Anda di sini..",
                    maximumSelectionSize: 5,
                });
            });

            $("#manufacturer_id").select2({
                placeholder: "Pilih merek Anda di sini... ",
                maximumSelectionSize: 1
            });

        });

    </script>
<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-table'></i>
					  <span>Produk</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>					
							<a onclick="$('#form').submit();" id="a_Submit" class="btn btn-primary">Simpan</a>
							<a href="<?php echo $cancel; ?>" class="btn btn-white">Batal</a>
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
			</div>
		</div>
		
		
		<!--Start Form-->
		<div class='row'>
			<div class='col-sm-12'>
				<!--Vincent 24/11/2015 - Changed "green" class to "blue" class  -->
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'><div class='icon-edit'></div> Produk</div>
						<div class='actions'> 
							
						</div>
                    </div>
					
					<div class='box-content'>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal validate-form">
							<div class='tabbable' style='margin-top: 20px'>
								<ul class='nav nav-responsive nav-tabs'>
									<li class='active'>
										<a data-toggle='tab' href='#retab1'>
											Umum
										</a>
									</li>
									<li class=''>
										<a data-toggle='tab' href='#retab2'>
											Harga Produk
										</a>
									</li>
									<li class=''>
										<a data-toggle='tab' href='#retab4'>
											Gambar
										</a>
									</li>
								</ul>
								<div class='tab-content'>
									
									<div id="retab1" class="tab-pane active">
										
										
										<?php foreach ($languages as $language) { ?>
											<div class="form-group">											
												<label class='col-md-2 control-label' for=''>Nama*</label>
												<div class='col-md-7'>
													<input class="form-control" type="text" id="product_name" data-rule-required='true' name="product_description[<?php echo $language['language_id']; ?>][name]" size="100" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>" />
												</div>
											</div>
											
											<div class="form-group">											
												<label class='col-md-2 control-label' for='description<?php echo $language['language_id']; ?>'>Deskripsi</label>
												<div class='col-md-7'>
													<textarea class='form-control' name="product_description[<?php echo $language['language_id']; ?>][description]" id="description<?php echo $language['language_id']; ?>"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
												</div>
											</div>



										<?php } ?>


										<div class="form-group">											
											<label class='col-md-2 control-label' for='inputText1'>Status*</label>
											<div class='col-md-2'>
												<select class="form-control" name="status" data-rule-required='true'>
													<?php if ($this->config->get('vendor_product_approval')) { ?>
													<?php if ($status != 5) { ?> 	
													  <?php if ($status) { ?>
													  <option value="1" selected="selected">Aktif</option>
													  <option value="0">Tidak Aktif</option>
													  <?php } else { ?>
													  <option value="1">Aktif</option>
													  <option value="0" selected="selected">Tidak Aktif</option>
													  <?php } ?>
													<?php } else { ?>
													  <option value="5" selected="selected"><?php echo $txt_pending_approval; ?></option>
													<?php } ?>
													<?php } else { ?>
													 <?php if ($status) { ?>
													  <option value="1" selected="selected">Aktif</option>
													  <option value="0">Tidak Aktif</option>
													  <?php } else { ?>
													  <option value="1">Aktif</option>
													  <option value="0" selected="selected">Tidak Aktif</option>
													  <?php } ?>
													<?php } ?>
												</select>
											</div>
											<img id="info_icon" style="margin-top:0.6%" title="Pilih 'Aktif' jika produk anda ditampilkan dan dijual di SerbaMall. 
											Pilih 'Tidak Aktif' jika produk anda tidak ingin ditampilkan dan tidak ingin dijual di SerbaMall karena alasan tertentu." height="18px" width="18px" src="view/image/information.png"/>
										</div>

										<div class="form-group">											
											<label class='col-md-2 control-label' for='inputText1'>Merek</label>
											<div class='col-md-3'>
												<select name="manufacturer_id" id="manufacturer_id" class="form-control">
													<option value="0"> -- Sila Pilih Merek -- </option>
													<option value="1"> - Tidak Ada Di Dalam - </option>
													<?php foreach($manufacturer_list as $manufacturer_t){ ?>
														<option value="<?php echo $manufacturer_t['manufacturer_id']; ?>" <?php echo (isset($manufacturer) && $manufacturer_id == $manufacturer_t['manufacturer_id'] ? 'selected' : ''); ?>><?php echo $manufacturer_t['name']; ?></option> 
													<?php } ?>
												</select>
												<?php
												/*
												<input class='form-control' data-rule-required='true' type="text" name="manufacturer" value="<?php echo $manufacturer ?>" />
												<input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
												*/
												?>
											</div>
											<img id="info_icon" style="margin-top:0.6%" title="Anda dapat memilih nama merk produk anda di sini. Jika anda tidak menemukannya, anda dapat mengklik ‘Tidak Ada Di Dalam’, kemudian anda dapat mengetik nama merk yang anda inginkan di bagian ‘Merek Menyarankan’  " height="18px" width="18px" src="view/image/information.png"/> 
										</div>
										
										<div class="form-group" id="brand_suggest" style="display:none;	">											
											<label class='col-md-2 control-label' for='inputText1'>Merek Menyarankan</label>
											<div class='col-md-3'>
												<input class='form-control' type="text" name="manufacturer_suggest" value="<?php echo $manufacturer_suggest; ?>" />
												<span style="font-size:12px; font-style:italic">*Sarankan merek anda disini apabila tidak menemukan dalam list merek kami. </span>
											</div>
										</div>
										
										<div class="form-group">											
											<label class='col-md-2 control-label' for='inputText1'>Model</label>
											<div class='col-md-2'>
												<input class='form-control' data-rule-required='false' type="text" name="model" value="<?php echo $model; ?>" />
												<?php if ($error_model) { ?>
												<span class="error"><?php echo $error_model; ?></span>
												<?php } ?>
											</div>
										</div>

										<!-- Vincent - 17122015 - Categories select2-->
										<div class="form-group" id="categories_breadcrumb">
											<label class='col-md-2 control-label' for='inputText1'>Kategori </br> (Terpilih)</label>
											<div class='col-md-8'> 
											<div class="product-category-breadcrumb" >
											
													<ul class="breadcrumb" style="padding-left: 0; margin-bottom: 0;">
														<?php foreach ($product_categories as $k=>$product_category) { ?>
														- <li class="text-success"><?php echo $product_category['name']; ?></li></br>
															
														<?php } ?>
													</ul>
												</div>
												<a id="changeCategoriesBut" class="btn btn-primary">Edit Kategori</a>
												<a id="cancel_changeCategoriesBut" class="btn btn-primary">Batal</a>
											</div>
											</div>
										
										<div class="form-group" id="product_categories_dropdownlist">
											<label class='col-md-2 control-label' for='inputText1'>Kategori</label>
											<div class='col-md-8' > 
											<TABLE class="form" id="category_table" border="0" style="width:650px;" cellpadding="0">
												<tr>
										          <td>Kategori Utama:</br>
										          		<select id="first-layer-cat" name="first_layer_cat" style="margin-top:10px; height:30px; width:200px;" data-rule-required='false'>
										              <option value="">-- Sila Pilih --</option>
										              <?php foreach ($parent_categories as $cat) {  ?> 
										              
										              <option value="<?php echo $cat['category_id'];?>" ><?php echo $cat['name']; ?></option>
										              
										              <?php } ?>
										            </select>
										            <img id="info_icon" style="margin-left:1%" title="Pilih nama kategori yang paling mendekati kategori produk anda di bagian kategori utama hingga sub kategori ketiga. Jika anda tidak menemukan kategori produk yang sesuai di seluruh sub kategori yang ada, mohon pilih ‘Tidak ada di atas’" height="18px" width="18px" src="view/image/information.png"/>
										          </td>
										      </tr>
										      <tr>
										          <td style="padding-top:10px;">Lapisan pertama sub kategori:</br>
										          <select id="second-layer-cat" name="second_layer_cat" style="margin-top:10px; height:30px; width:200px;">
										          </select>
										      	  </td>
										          </tr>
										          <tr>
										          <td style="padding-top:10px;">Lapisan kedua sub kategori:</br>
										          <select id="third-layer-cat" name="third_layer_cat" style="margin-top:10px; height:30px; width:200px;">
										              
										            </select></td>
										         </tr>
										         <tr>
										          <td style="padding-top:10px;">Lapisan ketiga sub kategori:</br>
										          <select id="fouth-layer-cat" name="fouth_layer_cat" style="margin-top:10px; height:30px; width:200px;">
										              
										            </select></td>
										          </tr>
											</TABLE>
										</div>
									</div>


                                            <div class="form-group">
                                                <label class='col-md-2 control-label' for=''>Label</label>
                                                <div class='col-md-7'>
                                                    <input type="hidden" id="tags" class="form-control select2-tags" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>" name="product_description[<?php echo $language['language_id']; ?>][tag]"/>
                                                </div>
                                                <img id="info_icon" style="margin-top:0.6%" title="Kata kunci yang berkaitan dengan produk anda, mempermudah pembeli untuk mencari nama produk anda di SerbaMall" height="18px" width="18px" src="view/image/information.png"/> 
                                            </div>
                                        </br>
                                        	
										<?php /* 20160107 - Vincent 
										<div class="form-group" style="display:none;">											
											<label class='col-md-2 control-label' for='inputText1'>SKU</label>
											<div class='col-md-2'>
												<input class='form-control' type="text" name="sku" value="<?php echo $sku; ?>" />
											</div>
										</div>
										*/?>

										<div id="tab-option">
												<div class='box-header blue-background'>
													<div class='title'>Informasi Pengiriman</div>
													<div class='actions'>
														
													</div>
												</div> </br>

                                        <div class="form-group" >
                                            <label class='col-md-2 control-label' for='inputText1'>Pengiriman yang Diperlukan</label>
                                            <div class='col-md-2'>
                                                <select class="form-control" name="shipping">
                                                    <?php if ($shipping) { ?>
                                                        <option value="1" selected="selected">Ya</option>
                                                        <option value="0">Tidak</option>
                                                    <?php } else { ?>
                                                        <option value="1">Ya</option>
                                                        <option value="0" selected="selected">Tidak</option>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                            <img id="info_icon" style="margin-top:0.6%" title="Pilih 'Ya' jika produk akan dikirim. Pilih 'Tidak' jika produk tidak  dikirim." height="18px" width="18px" src="view/image/information.png"/>
                                        </div>
										
										<div class="form-group" style="">
											<label class='col-md-2 control-label' for='inputText1'>Dimensi</label>
											<div class='col-md-5'>
												
												<div class='col-md-4' style="padding-left: 0;">
													Panjang (CM)<br>
													<input class='form-control' type="text" name="length" value="<?php echo round($length, 2); ?>" size="4" />
												</div>
												
												<div class='col-md-4'>
													Lebar (CM)<br>
													<input class='form-control' type="text" name="width" value="<?php echo round($width, 2); ?>" size="4" />
												</div>
												
												<div class='col-md-4'>
													<TABLE width="100px">
														<tr>
													<td colspan="2">Tinggi (CM)</td>
													</tr>
													<tr>
														<td><input class='form-control' type="text" name="height" value="<?php echo round($height, 2); ?>" size="4" /></td>
														<td width="30px"><img id="info_icon" style="margin-top:2%; float:right" title="Ketik dimensi ukuran produk untuk mendapatkan rincian biaya pengiriman" height="18px" width="18px" src="view/image/information.png"/></td>
													</tr>
													
													</TABLE>
												</div>
											</div>
										</div>
									</br>
										<!--Vincent 24/11/2015 -->
										<?php /* 
                                        <div class="form-group" style="">
                                            <label class='col-md-2 control-label' for='inputText1'>Length Class</label>
                                            <div class='col-md-2'>
                                                <select class="form-control" name="length_class_id" data-rule-required='true'>
                                                        <?php foreach ($length_classes as $length_class) { ?>
                                                            <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                                                                <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                                                            <?php } else { ?>
                                                                <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                                                            <?php } ?>
                                                        <?php } ?>
                                                    </select>
                                            </div>
                                        </div>
										< */?>
                                        <div class="form-group" style="">
                                            <label class='col-md-2 control-label' for='inputText1'>Berat (KG)</label>
                                            <div class='col-md-2'>
                                                <input type="text" name="weight" class='form-control' value="<?php echo $weight; ?>" />
                                            </div>
                                            <img id="info_icon" style="margin-top:0.6%" title="Ketik berat produk untuk mendapatkan rincian biaya pengiriman." height="18px" width="18px" src="view/image/information.png"/>
                                        </div>
                                    </div>
										<!--Vincent 24/11/2015 -->
										<?php /*
                                        <div class="form-group" style="">
                                            <label class='col-md-2 control-label' for='inputText1'><?php echo $entry_weight_class; ?></label>
                                            <div class='col-md-2'>
                                                <select class="form-control" name="weight_class_id" data-rule-required='true'>
                                                    <?php foreach ($weight_classes as $weight_class) { ?>
                                                        <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                                                            <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                                                        <?php } else { ?>
                                                            <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                                                        <?php } ?>
                                                    <?php } ?>
                                                </select>
                                            </div>
                                        </div>
										 */ ?>


										 <?php /* 
                                        <div class="form-group" style="display:none;">
											<label class='col-md-2 control-label' for='inputText1'>Category*</label>
											<div class='col-md-6'>
													<a id="a_SelectCategory" class="btn" data-toggle='modal' role='button' href="#modal-category">Select Category</a>
													<div class='modal fade' id='modal-category' tabindex='-1'>
														<div class='modal-dialog'>
															<div class='modal-content'>
																<div class='modal-header'>
																	<button aria-hidden='true' class='close' data-dismiss='modal' type='button'>×</button>
																	<h4 class='modal-title' id='myModalLabel'>Select Product Category</h4>
																</div>
																<div class='modal-body'>
																	<div class="row">
																		<div style="padding-right: 0;" class="col-md-6">
																			<div class='box'>
																				<div class='box-content'>
																					<div class='clearfix' id='slider'>
																						<div class="" id="divMainCategory">
																							<div class="box-header box-header-small blue-background">
																								<div class="title">Main Category</div>
																								<div class="actions"></div>
																							</div>
																							<div class='slider-content scrollable' data-scrollable-height='250' data-scrollable-start='top'>
																								<ul class="list-unstyled">
																									<?php
																									if(isset($parent_categories)){
																									foreach($parent_categories as $cat){ ?>
																									<li><a class="title" href="#" data-category-id='<?php echo $cat['category_id']; ?>'><?php echo $cat['name']; ?></a></li>
																									<?php } } ?>
																								</ul>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>
																		<div style="padding-left: 0;" class="col-md-6">
																			<div class='box'>
																				<div class='box-content' style=" border-left: 0;">
																					<div class='clearfix' id='slider'>
																						<div class="" id="divSubCategory">
																							<div class="box-header box-header-small blue-background">
																								<div class="title">Sub Category</div>
																								<div class="actions"></div>
																							</div>
																							<div class='slider-content scrollable' data-scrollable-height='250' data-scrollable-start='top'>
																								<ul class="list-unstyled">
																								</ul>
																							</div>
																						</div>
																					</div>
																				</div>
																			</div>
																		</div>

																	</div>
																	<div class="row">
																		<div class="col-md-12">
																			<a href="#" id="a_CompleteSelectCategory" class="btn btn-block btn-lg btn-success fade">Select</a>
																		</div>
																	</div>
																</div>
																<div class='modal-footer'>
																	
																</div>
															</div>
														</div>
													</div>
													
												<div class="product-category-breadcrumb">
													<ul class="breadcrumb" style="padding-left: 0; margin-bottom: 0;">
														<?php foreach ($product_categories as $k=>$product_category) { ?>
														<li class="text-success"><?php echo $product_category['name']; ?></li>
															<?php if($k == 0){ ?>
															<li class="separator">
																<i class="icon-angle-right"></i>
															</li>
															<?php } ?>
														<?php } ?>
													</ul>
												</div>
												<div class="product-category-ids">
												<?php if(count($product_categories) > 0){ ?>
													<?php foreach ($product_categories as $product_category) { ?>
														<input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
													<?php } ?>
												<?php }else{ ?>
													<input type="hidden" data-rule-required='false' name="product_category_check" value="" />
												<?php } ?>
												</div>
											</div>
										</div>
										*/?>
										
									<!-- *Vincent - 17122015 - Categories select2-->
										
										<!-- Vincent - 8/12/2015 -->
										<?php /*
										<div style="display:none;" class="form-group">
											<label class='col-md-2 control-label' for='inputText1'>Specification*</label>
											<div class='col-md-7'>
												 <div class='box-content box-no-padding' style="border: 0; box-shadow: none;">
													<div class='responsive-table'>
														<div id='div_AttributesTable'>
															<table id="attribute" class="list table">
																<?php $attribute_row = 0; ?>
																<?php foreach ($product_attributes as $product_attribute) { ?>
																<tbody id="attribute-row<?php echo $attribute_row; ?>">
																  <tr>
																	<td class="left">
																		<input class="form-control attribute-label-control" type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" />
																		<input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" />
																	</td>
																	<td class="left"><?php foreach ($languages as $language) { ?>
																	  <input class="form-control" data-rule-required='true' name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" value="<?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?>" />
																	  <!--<img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" align="top" /><br />-->
																	  <?php } ?></td>
																	<td class="left"></td> <!--<a onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" class="btn btn-white"><?php echo $button_remove; ?></a>-->
																  </tr>
																</tbody>
																<?php $attribute_row++; ?>
																<?php } ?>
																<tfoot>
																  <tr>
																	<td colspan="2"></td>
																	<td class="left"><!--<a onclick="addAttribute();" class="btn"><?php echo $button_add_attribute; ?></a>--></td>
																  </tr>
																</tfoot>
															</table>
														</div>
													</div>
												</div>
											</div>
											
										</div>
										*/?>
										<br><br>
										<!-- Product Option -->
										<div id="tab-option">
												<div class='box-header blue-background'>
													<div class='title'>Pilihan Produk</div>
													<div class='actions'>
														
													</div>
												</div>
											
											<div class="box-content" style="min-height: 100px;">
												<div class='tabbable tabs-left'>													
													<ul id="vtab-option" class='nav nav-tabs' style="min-height: 60px;">
														<?php $option_row = 0; ?>
														<?php foreach ($product_options as $product_option) { ?>
															<li class='<?php echo ($option_row == 0 ? 'active' : ''); ?>'>
																<a data-toggle='tab' href="#tab-option-<?php echo $option_row; ?>" id="option-<?php echo $option_row; ?>">
																	<?php echo $product_option['name']; ?>
																</a>
																<?php $option_row++; ?>
															</li>
														<?php } ?>
													</ul>
												  
													<div class='tab-content' id="tab-option-content">
														<?php $option_row = 0; ?>
														<?php $option_value_row = 0; ?>
														<?php foreach ($product_options as $product_option) { ?>
															<div class='vtabs-content tab-pane <?php echo ($option_row == 0 ? 'active' : ''); ?>' id='tab-option-<?php echo $option_row; ?>'>
																<input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
																<input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
																<input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
																<input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
																<div class="responsive-table">
																	<div class="">
																		<table class="form table">
																			<tr>
																				<td style="vertical-align: middle;"><?php echo $entry_required; ?></td>
																				<td style="width: 95%;">
																					<div class=" col-md-3">
																						<select name="product_option[<?php echo $option_row; ?>][required]" class="form-control col-md-2">
																							<?php if ($product_option['required']) { ?>
																							<option value="1" selected="selected"><?php echo $text_yes; ?></option>
																							<option value="0"><?php echo $text_no; ?></option>
																							<?php } else { ?>
																							<option value="1"><?php echo $text_yes; ?></option>
																							<option value="0" selected="selected"><?php echo $text_no; ?></option>
																							<?php } ?>
																						</select>
																					</div>
																				  </td>
																			</tr>
																		  <?php if ($product_option['type'] == 'text') { ?>
																			  <tr>
																				<td><?php echo $entry_option_value; ?></td>
																				<td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" /></td>
																			  </tr>
																		  <?php } ?>
																		  <?php if ($product_option['type'] == 'textarea') { ?>
																			  <tr>
																				<td><?php echo $entry_option_value; ?></td>
																				<td><textarea name="product_option[<?php echo $option_row; ?>][option_value]" cols="40" rows="5"><?php echo $product_option['option_value']; ?></textarea></td>
																			  </tr>
																		  <?php } ?>
																		  <?php if ($product_option['type'] == 'file') { ?>
																			  <tr style="display: none;">
																				<td><?php echo $entry_option_value; ?></td>
																				<td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" /></td>
																			  </tr>
																		  <?php } ?>
																		  <?php if ($product_option['type'] == 'date') { ?>
																			  <tr>
																				<td><?php echo $entry_option_value; ?></td>
																				<td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="date" /></td>
																			  </tr>
																		  <?php } ?>
																		  <?php if ($product_option['type'] == 'datetime') { ?>
																		  <tr>
																			<td><?php echo $entry_option_value; ?></td>
																			<td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="datetime" /></td>
																		  </tr>
																		  <?php } ?>
																		  <?php if ($product_option['type'] == 'time') { ?>
																		  <tr>
																			<td><?php echo $entry_option_value; ?></td>
																			<td><input type="text" name="product_option[<?php echo $option_row; ?>][option_value]" value="<?php echo $product_option['option_value']; ?>" class="time" /></td>
																		  </tr>
																		  <?php } ?>
																		</table>
																	</div>
																</div>
																
																
																<?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
																
																<div class="responsive-table">
																	<div class="">
																		<table id="option-value<?php echo $option_row; ?>" class="list table">
																		  <thead>
																			<tr>
																			  <th class="left" style="border-bottom: 0;"><?php echo $entry_option_value; ?></th>
																			  <th class="right" style="border-bottom: 0;"><?php echo $entry_quantity; ?></th>
																			  <?php /*<th class="right"><?php echo $entry_price; ?></th>*/ ?>
																			  <th style="border-bottom: 0;"></th>
																			</tr>
																		  </thead>
																		  <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
																		  <tbody id="option-value-row<?php echo $option_value_row; ?>">
																			<tr>
																				<td class="left">
																					<select class="form-control" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]">
																					  <?php if (isset($option_values[$product_option['option_id']])) { ?>
																					  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
																					  <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
																					  <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
																					  <?php } else { ?>
																					  <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
																					  <?php } ?>
																					  <?php } ?>
																					  <?php } ?>
																					</select>
																					<input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" />
																				</td>
																				<td class="right">
																					<div class="form-group">
																						<div class="col-md-12">
																							<input data-rule-required='true' class="form-control" type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" size="3" />
																						</div>
																					</div>
																				</td>																				
																				<td class="left"><a onclick="$('#option-value-row<?php echo $option_value_row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>
																			</tr>
																		  </tbody>
																		  <?php $option_value_row++; ?>
																		  <?php } ?>
																		  <tfoot>
																			<tr>
																			  <td colspan="6"></td>
																			  <td class="left"><a onclick="addOptionValue('<?php echo $option_row; ?>');" class="btn a_addOptionValue"><?php echo $button_add_option_value; ?></a></td>
																			</tr>
																		  </tfoot>
																		</table>
																	</div>
																</div>
																<select id="option-values<?php echo $option_row; ?>" style="display: none;">
																  <?php if (isset($option_values[$product_option['option_id']])) { ?>
																  <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
																  <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
																  <?php } ?>
																  <?php } ?>
																</select>
																<?php } ?>
															</div>
															<?php $option_row++; ?>
														<?php } ?>
													</div>
												</div>
											</div>
										</div>
										
									</div>
									
									<div id="retab2" class="tab-pane">
										
										<div class="form-group">											
											<label class='col-md-2 control-label' for='inputText1'>Produk Tersedia*</label>
											<div class='col-md-1'>
												<input class="form-control" data-rule-required='true'  type="text" name="quantity" value="<?php echo $quantity; ?>" size="2" />
											</div>
										</div>
										
										<div class="form-group">											
											<label class='col-md-2 control-label' for='inputText1'>Harga Satuan*</label>
											<div class='col-md-2'>
												<input class="form-control" data-rule-required='true'  type="text" name="price" value="<?php echo $price; ?>"   />
											</div>
										</div>
										
										<div class="form-group">
											 <?php $discount_row = 0; ?>
											 <?php $special_row = 0; ?>
											 <?php $shipping_row = 0; ?>
											<label class='col-md-2 control-label' for='inputText1'>Harga Diskon</label>
											<div class='col-md-7'>
												<table id="special" class='table' style='margin-bottom:0;'>
												<thead class="">
												  <tr>
													<!--<td class="left"><?php echo $entry_customer_group; ?></td>-->
													<!--<td class="right"><?php echo $entry_quantity; ?></td>-->
													<!--<td class="right"><?php echo $entry_priority; ?></td>-->
													<th class="left">Harga</th>
													<th class="left">Tanggal Mulai Diskon</th>
													<th class="left">Tanggal Diskon Berakhir</th>
												  </tr>
												</thead>
												
												<?php 
												if(isset($product_specials[0])){
													$product_special = $product_specials[0];
												}
												?>
												<tbody id="special-row0">  
													<tr>
														<td class="left col-md-2">
															<input class="form-control" type="text" name="product_special[0][price]" value="<?php echo (isset($product_special['price']) ? $product_special['price'] : ''); ?>" />
														</td>
														<td class="left">
															<div>
																<div class='datepicker input-group' >
																	<input class='form-control' data-format='yyyy-MM-dd' placeholder='Start Date' type='text' name="product_special[0][date_start]" value="<?php echo (isset($product_special['date_start']) ? $product_special['date_start'] : ''); ?>" />
																	<span class='input-group-addon'>
																		<span data-date-icon='icon-calendar' data-time-icon='icon-time'></span>
																	</span>
																</div>
															</div>
														</td>
														<td class="left">
															<div>
																<div class='datepicker input-group'>
																	<input class='form-control' data-format='yyyy-MM-dd' placeholder='End Date' type='text' name="product_special[0][date_end]" value="<?php echo (isset($product_special['date_end']) ? $product_special['date_end'] : '');  ?>" /> 
																	<span class='input-group-addon'>
																		<span data-date-icon='icon-calendar' data-time-icon='icon-time'></span>
																	</span>
																</div>
															</div>
														</td>
													</tr>
												</tbody>
												</table>
												<input type="hidden" name="product_special[0][customer_group_id]" value="1" />
											</div>
										</div>
										
									</div>
									
									
									<div id="retab4" class="tab-pane">
										
										<div class="form-group">
											<div class='col-md-10'>
												<div class='box-header blue-background'>
													<div class='title'>Gambar</div>
													<div class='actions'>
													
													</div>
												</div>
												
												
												<div class='box-content'>
													
													<p>
														<strong>Gambar produk utama</strong>
													</p>
													<div class="image">
														<img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br /><br />
														<input type="hidden" name="image" value="<?php echo $image; ?>" id="image" />
														<a class="btn" onclick="image_upload('image', 'thumb');">Pencarian</a>																	
														<a class="btn" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Hapus</a>
													</div>
													
													<hr class="hr-normal">
													
													<p>
														<strong>Galeri Produk</strong>
													</p>
													<div class='responsive-table'>
														<div class=''>
															<table id="images" class="table">
															<thead>
															  <tr>
																<th class="left">Gambar</th>
																<th class="right">Sortir Pesanan</th>
																<th></th>
															  </tr>
															</thead>
															<?php $image_row = 0; ?>
															<?php foreach ($product_images as $product_image) { ?>
															<tbody id="image-row<?php echo $image_row; ?>">
															  <tr>
																<td class="left"><div class="image"><img src="<?php echo $product_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
																	<input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="image<?php echo $image_row; ?>" />
																	<br />
																	<a class="btn" onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>
																	
																	<a class="btn" onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
																<td class="right"><input class="form-control" type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" size="2" /></td>
																<td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>
															  </tr>
															</tbody>
															<?php $image_row++; ?>
															<?php } ?>
															<tfoot>
															  <tr>
																<td colspan="2"></td>
																<td class="left"><a onclick="addImage();" class="btn"><?php echo $button_add_image; ?></a></td>
															  </tr>
															</tfoot>
														</table>
														</div>
													</div>
												</div>
											
												
											</div>
										</div>
										
										<br><br><br>
										
										<div class="form-group" style="display:none;">
											
											<div class='col-md-10'>
												<div class='box-header blue-background'>
													<div class='title'>YouTube Videos</div>
													<div class='actions'>
														
													</div>
												</div>
											
												<div class='box-content box-no-padding'>
													<div class='responsive-table'>
														<div class=''>
															<table id="video" class="table">
															<thead>
															<tr>
																<th><?php echo $entry_video; ?></th>
																<th><?php echo $entry_video_preview; ?></th>
																<th><?php echo $entry_sort_order; ?></th>
																<th></th>
															  </tr>
															</thead>
															<?php $video_row = 0; ?>
															<?php foreach ($product_videos as $product_video) { ?>
															<tbody id="video-row<?php echo $video_row; ?>">
															  <tr>
																<td class="left"><textarea class="form-control" name="product_video[<?php echo $video_row; ?>][video]" cols="50" rows="1"><?php echo $product_video['video']; ?></textarea></td>
																<td class="left">
																	<object width="200" height="150">
																	  <param name="movie" value="https://www.youtube.com/v/<?php print $product_video['video'];?>?version=3"></param>
																	  <param name="allowFullScreen" value="true"></param>
																	  <param name="allowScriptAccess" value="always"></param>
																	  <embed src="https://www.youtube.com/v/<?php print $product_video['video'];?>?version=3" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="200" height="150"></embed>
																	</object>
																</td>
																<td class="right"><input class="form-control" type="text" name="product_video[<?php echo $video_row; ?>][sort_order]" value="<?php echo $product_video['sort_order']; ?>" size="2" /></td>
																<td class="left"><a onclick="$('#video-row<?php echo $video_row; ?>').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>
															  </tr>
															</tbody>
															<?php $video_row++; ?>
															<?php } ?>
															<tfoot>
															  <tr>
																<td colspan="3"></td>
																<td class="left"><a onclick="addVideo();" class="btn"><?php echo $button_add_video; ?></a></td>
															  </tr>
															</tfoot>
															</table>
														</div>
													</div>
												</div>
											</div>
										</div>
										
									</div>
									
									
								</div>
							</div>
							
							
							<input type="hidden" name="product_store[]" value="0" />
							<input type="hidden" name="tax_class_id" value="0" />
							<input type="hidden" name="minimum" value="1" />
							<input type="hidden" name="subtract" value="1" />
							<input type="hidden" name="stock_status_id" value="5" />
							<input type="hidden" name="keyword" value="" />
							<input type="hidden" name="length_class_id" value="1" />
							
							<input type="hidden" name="upc" value="" />
							<input type="hidden" name="ean" value="" />
							<input type="hidden" name="jan" value="" />
							<input type="hidden" name="isbn" value="" />
							<input type="hidden" name="mpn" value="" />
							<input type="hidden" name="location" value="" />
							<input type="hidden" name="ori_country" value="" />
							<input type="hidden" name="product_cost" value=""  />
							<input type="hidden" name="product_url" value=""  />
							<input type="hidden" name="shipping_method" value=""  />
							<input type="hidden" name="prefered_shipping" value=""  />
							<input type="hidden" name="shipping_cost" value=""  />
							<input type="hidden" name="vtotal" value=""  />
							<input type="hidden" name="meta_keyword" value=""  />
							<input type="hidden" name="meta_description" value=""  />
							<input type="hidden" name="tag" value=""  />
							<input type="hidden" name="wholesale" value=""  />
							
							<input type="hidden" name="price_prefix" value=""  />
							<input type="hidden" name="points" value=""  />
							<input type="hidden" name="points_prefix" value=""  />
							<input type="hidden" name="weight_prefix" value=""  />
							<input type="hidden" name="priority" value=""  />
							
							
							
							<?php /*Vendor details*/ ?>
							<?php foreach ($vendors as $vend) { ?>
								<?php if ($vendor) { ?>
									<?php if ($vend['vendor_id'] == $vendor) { ?>
										<input id="vendor" type="hidden" name="vendor" value="<?php echo $vend['vendor_id']; ?>" onchange="getVendors();" />
									<?php } ?>
								<?php } else { ?>									
									<?php if ($vend['vendor_id'] == $default_vendor) { ?>								
										<input id="vendor" type="hidden" name="vendor" value="<?php echo $vend['vendor_id']; ?>" onchange="getVendors();" />
									<?php } ?>
								<?php } ?>
							<?php } ?>
							
						</form>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>	
</div>

<style type="text/css">
#info_icon:hover{
	cursor: pointer;
}
</style>
<script type="text/javascript" src="view/javascript/ckeditor/ckeditor.js"></script> 
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('description<?php echo $language['language_id']; ?>', {
	filebrowserBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashBrowseUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserImageUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>',
	filebrowserFlashUploadUrl: 'index.php?route=common/filemanager&token=<?php echo $token; ?>'
});
<?php } ?>
//--></script> 
<script type="text/javascript">
$(document).ready(function(){
	<?php if(count($product_description) <= 0){ ?>
		$('.a_addOptionValue').trigger('click');
	<?php } ?>

	if ($("#product_name").val() == "") {
		document.getElementById("categories_breadcrumb").style.display="none";
		document.getElementById("cancel_changeCategoriesBut").style.display="none";
	}else{
		document.getElementById("product_categories_dropdownlist").style.display="none";
		document.getElementById("cancel_changeCategoriesBut").style.display="none";

	}

	//$('#manufacturer_id').append('<option value="1" selected>Tidak ada diatas</option>');
});


$('#changeCategoriesBut').click(function(){
	
	document.getElementById("product_categories_dropdownlist").style.display="inline";
	document.getElementById("cancel_changeCategoriesBut").style.display="inline";
		document.getElementById("changeCategoriesBut").style.display="none";
});

$('#cancel_changeCategoriesBut').click(function(){
	
	document.getElementById("product_categories_dropdownlist").style.display="none";
	document.getElementById("cancel_changeCategoriesBut").style.display="none";
		document.getElementById("changeCategoriesBut").style.display="inline";
});

$('#manufacturer_id').on("select2-selecting", function(e) { 
	if (e.val == "1"){
		document.getElementById("brand_suggest").style.display="inline";	
	}else{
		document.getElementById("brand_suggest").style.display="none";
	}
	});

$('#a_Submit').click(function(){
	
	if($("#form").valid())
	{ 
		return true;
	}else{
		return false;
	}
});


// Get the first layer categories from select2 and pass to second layer categories select2
$('#first-layer-cat').on('change',(function(e){
	e.preventDefault();
	//ajax call for secondary category
if ( $( "#first-layer-cat" ).val() =="130" || $( "#first-layer-cat" ).val() =="141" || $( "#first-layer-cat" ).val() =="145" || $( "#first-layer-cat" ).val() =="150" || $( "#first-layer-cat" ).val() =="57") {
	$.ajax({
		url: 'index.php?route=catalog/cat2ven/getCategory&token=<?php echo $token; ?>&filter_parent_id=' + $( "#first-layer-cat" ).val() ,
		dataType: 'json',
		success: function(json) {
			$('#second-layer-cat').empty();
			$('#third-layer-cat').empty();
			$('#fouth-layer-cat').empty();
			Object.keys( json ).forEach(function( key ) {
				$('#second-layer-cat').append($('<option>', { 
        value: json[key].category_id,
        text : json[key].name 
    		}));
		});

			$('#second-layer-cat').append($('<option>', { 
        value: "none",
        text : "-- Tidak ada di atas --"
    		}));
		}
	});
	}else{
		$('#second-layer-cat').empty();
			$('#third-layer-cat').empty();
			$('#fouth-layer-cat').empty();
	}
	
}));

// Get the second layer categories from select2 and pass to third layer categories select2
$('#second-layer-cat').on('change',(function(e){ 

	if ($( "#second-layer-cat" ).val() == "none") {
		$('#third-layer-cat').empty();
		$('#fouth-layer-cat').empty();
	}else{
	e.preventDefault();
	//ajax call for secondary category
	if ($( "#second-layer-cat" ).val() != "none") {
	$.ajax({
		url: 'index.php?route=catalog/cat2ven/getCategory&token=<?php echo $token; ?>&filter_parent_id=' + $( "#second-layer-cat" ).val(),
		dataType: 'json',
		success: function(json) {
			$('#third-layer-cat').empty();
			Object.keys( json ).forEach(function( key ) {
				$('#third-layer-cat').append($('<option>', { 
        value: json[key].category_id,
        text : json[key].name 
    }));
				
			});
			$('#third-layer-cat').append($('<option>', { 
        value: "none",
        text : "-- Tidak ada di atas --"
    		}));
			
		}
	});
}
}
}));

// Get the third layer categories from select2 and pass to fouth layer categories select2
$('#third-layer-cat').on('change',(function(e){

	if ($( "#third-layer-cat" ).val() == "none") {
		$('#fouth-layer-cat').empty();
	}else{
	e.preventDefault();
	//ajax call for secondary category

	$.ajax({
		url: 'index.php?route=catalog/cat2ven/getCategory&token=<?php echo $token; ?>&filter_parent_id=' + $( "#third-layer-cat" ).val(),
		dataType: 'json',
		success: function(json) {
			$('#fouth-layer-cat').empty();
			Object.keys( json ).forEach(function( key ) {
				$('#fouth-layer-cat').append($('<option>', { 
        value: json[key].category_id,
        text : json[key].name 
    }));
				
			});
			$('#fouth-layer-cat').append($('<option>', { 
        value: "none",
        text : "-- Tidak ada di atas --"
    		}));
			
		}
	});
}
}));

$('#divMainCategory ul li a').click(function(e){
	e.preventDefault();
	
	//set active
	$(this).parents('ul.list-unstyled').find('a').removeClass('active');
	$(this).addClass('active');
	//ajax call for secondary category
	$.ajax({
		url: 'index.php?route=catalog/cat2ven/getCategory&token=<?php echo $token; ?>&filter_parent_id=' +  encodeURIComponent($(this).attr('data-category-id')),
		dataType: 'json',
		success: function(json) {
			//append list into #divSubCategory
			var $_LIST = "";
			Object.keys( json ).forEach(function( key ) {
				$_LIST += "<li><a class='title' href='#' data-category-id='"+json[key].category_id+"'>"+ json[key].name +"</a></li>";
			});

			$('#divSubCategory ul').html('').append($_LIST);
			
		}
	});
	
	//return false;
});

$('#divSubCategory ul').on("click", "a", function(e){
	e.preventDefault();
	//set active
	$(this).parents('ul.list-unstyled').find('a').removeClass('active');
	$(this).addClass('active');
	
	//show Select Button
	$('#a_CompleteSelectCategory').removeClass('fade');
});

$('#a_CompleteSelectCategory').click(function(e){
	e.preventDefault();
	
	//set $("input[name='product_category_check']").val() 
	$("input[name='product_category_check']").val('1');
	
	//set category in hidden field
	$('.product-category-ids').html('').append('<input type="hidden" name="product_category[]" value="'+$("#divMainCategory ul li a.active").attr('data-category-id')+'" />');
	$('.product-category-ids').append('<input type="hidden" name="product_category[]" value="'+$("#divSubCategory ul li a.active").attr('data-category-id')+'" />');
	
	//show in category breadcrumb
	$('.product-category-breadcrumb ul').html('').append('<li class="text-success">'+$("#divMainCategory ul li a.active").text()+'</li> ');
	$('.product-category-breadcrumb ul').append('<li class="separator"><i class="icon-angle-right"></i></li> ');
	$('.product-category-breadcrumb ul').append('<li class="text-success">'+$("#divSubCategory ul li a.active").text()+'</li>');
	
	load_filters();
	
	$('.modal-dialog .close').trigger('click');
});

$.widget('custom.catcomplete', $.ui.autocomplete, {
	_renderMenu: function(ul, items) {
		var self = this, currentCategory = '';

		$.each(items, function(index, item) {
			//console.log(item);
			if (item.category != currentCategory) {
				ul.append('<li class="ui-autocomplete-category">' + item.category + '</li>');

				currentCategory = item.category;
			}

			self._renderItemData(ul, item);
			//console.log(ul);
		});
		//console.log(self);
	}
});

// Manufacturer
$('input[name=\'manufacturer\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/manufacturerAutocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.manufacturer_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('input[name=\'manufacturer\']').attr('value', ui.item.label);
		$('input[name=\'manufacturer_id\']').attr('value', ui.item.value);
	
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});

// Category
$('input[name=\'category\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/cat2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.category_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('#product-category' + ui.item.value).remove();		
		$('#product-category').append('<div id="product-category' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_category[]" value="' + ui.item.value + '" /></div>');
        //Attributes to Categories
        load_filters();
        
		$('#product-category div:odd').attr('class', 'odd');
		$('#product-category div:even').attr('class', 'even');
				
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});

$('#product-category div img').live('click', function() {
	$(this).parent().remove();

    //Attributes to Categories
    load_filters();
	
	$('#product-category div:odd').attr('class', 'odd');
	$('#product-category div:even').attr('class', 'even');	
});


// Related
$('input[name=\'related\']').autocomplete({
	delay: 500,
	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/pro2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {		
				response($.map(json, function(item) {
					return {
						label: item.name,
						value: item.product_id
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		$('#product-related' + ui.item.value).remove();
		
		$('#product-related').append('<div id="product-related' + ui.item.value + '">' + ui.item.label + '<img src="view/image/delete.png" alt="" /><input type="hidden" name="product_related[]" value="' + ui.item.value + '" /></div>');

		$('#product-related div:odd').attr('class', 'odd');
		$('#product-related div:even').attr('class', 'even');
				
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});

$('#product-related div img').live('click', function() {
	$(this).parent().remove();
	
	$('#product-related div:odd').attr('class', 'odd');
	$('#product-related div:even').attr('class', 'even');	
});
</script> 
<script type="text/javascript">
    <?php $attribute_row = 0; //Ken - define for catch error (TEMP FIX) ?>
var attribute_row = <?php echo $attribute_row; ?>;

function addAttribute() {	
	html  = '<tbody id="attribute-row' + attribute_row + '">';
    html += '  <tr>';
	html += '    <td class="left"><input type="text" class="form-control attribute-label-control" name="product_attribute[' + attribute_row + '][name]" value="" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
	html += '    <td class="left">';
	<?php foreach ($languages as $language) { ?>
	html += '<input name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]"  />';
    <?php } ?>
	html += '    </td>';
	html += '    <td class="left"><a onclick="$(\'#attribute-row' + attribute_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
    html += '  </tr>';	
    html += '</tbody>';
	
	$('#attribute tfoot').before(html);
	
	attributeautocomplete(attribute_row);
	
	attribute_row++;
}

function attributeautocomplete(attribute_row) {
	$('input[name=\'product_attribute[' + attribute_row + '][name]\']').catcomplete({
		delay: 500,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/attr2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
				dataType: 'json',
				success: function(json) {	
					response($.map(json, function(item) {
						return {
							category: item.attribute_group,
							label: item.name,
							value: item.attribute_id
						}
					}));
				}
			});
		}, 
		select: function(event, ui) {
			$('input[name=\'product_attribute[' + attribute_row + '][name]\']').attr('value', ui.item.label);
			$('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').attr('value', ui.item.value);			
			
			return false;
		},
		focus: function(event, ui) {
      		return false;
   		}
	});
}

$('#attribute tbody').each(function(index, element) {
	attributeautocomplete(index);
});
</script> 
<script type="text/javascript"><!--

var option_row = <?php echo $option_row; ?>;

$('input[name=\'option\']').catcomplete({

    minLength: 0, delay: 200,

	source: function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/opt2ven/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request.term),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					console.log(item);
					return {
						category: item.category,
						label: item.name,
						value: item.option_id,
						type: item.type,
						option_value: item.option_value
					}
				}));
			}
		});
	}, 
	select: function(event, ui) {
		html  = '<div id="tab-option-' + option_row + '" class="vtabs-content tab-pane active">';
		html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + ui.item.label + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + ui.item.value + '" />';
		html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + ui.item.type + '" />';
		html += '	<div class="responsive-table">';
		html += '	<div class="">';
		html += '	<table class="form table">';
		html += '	  <tr>';
		html += '		<td style="vertical-align: middle;"><?php echo $entry_required; ?></td>';
		html += '       <td style="width: 95%;"><div class=" col-md-3"><select class="form-control" name="product_option[' + option_row + '][required]">';
		html += '	      <option value="1"><?php echo $text_yes; ?></option>';
		html += '	      <option value="0"><?php echo $text_no; ?></option>';
		html += '	    </select></div></td>';
		html += '     </tr>';
		
		if (ui.item.type == 'text') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" class="form-control" name="product_option[' + option_row + '][option_value]" value="" /></td>';
			html += '     </tr>';
		}
		
		if (ui.item.type == 'textarea') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><textarea class="form-control" name="product_option[' + option_row + '][option_value]" cols="40" rows="5"></textarea></td>';
			html += '     </tr>';						
		}
		 
		if (ui.item.type == 'file') {
			html += '     <tr style="display: none;">';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" class="form-control" name="product_option[' + option_row + '][option_value]" value="" /></td>';
			html += '     </tr>';			
		}
						
		if (ui.item.type == 'date') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="date form-control" /></td>';
			html += '     </tr>';			
		}
		
		if (ui.item.type == 'datetime') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="datetime form-control" /></td>';
			html += '     </tr>';			
		}
		
		if (ui.item.type == 'time') {
			html += '     <tr>';
			html += '       <td><?php echo $entry_option_value; ?></td>';
			html += '       <td><input type="text" name="product_option[' + option_row + '][option_value]" value="" class="time form-control" /></td>';
			html += '     </tr>';			
		}
		
		html += '  </table>';
		html += '  </div>';
		html += '  </div>';
			
		if (ui.item.type == 'select' || ui.item.type == 'radio' || ui.item.type == 'checkbox' || ui.item.type == 'image') {
			html += '	<div class="responsive-table">';
			html += '	<div class="">';
			html += '  <table id="option-value' + option_row + '" class="list table">';
			html += '  	 <thead>'; 
			html += '      <tr>';
			html += '        <th class="left"><?php echo $entry_option_value; ?></th>';
			html += '        <th class="right"><?php echo $entry_quantity; ?></th>';
			//html += '        <th class="left"><?php echo $entry_subtract; ?></th>';
			//html += '        <th class="right"><?php echo $entry_price; ?></th>';
			//html += '        <th class="right"><?php echo $entry_option_points; ?></th>';
			//html += '        <th class="right"><?php echo $entry_weight; ?></th>';
			html += '        <th></th>';
			html += '      </tr>';
			html += '  	 </thead>';
			html += '    <tfoot>';
			html += '      <tr>';
			html += '        <td colspan="6"></td>';
			html += '        <td class="left"><a onclick="addOptionValue(' + option_row + ');" class="btn"><?php echo $button_add_option_value; ?></a></td>';
			html += '      </tr>';
			html += '    </tfoot>';
			html += '  </table>';
			html += '  </div>';
			html += '  </div>';
            html += '  <select id="option-values' + option_row + '" class="form-control" style="display: none;">';
			
            for (i = 0; i < ui.item.option_value.length; i++) {
				html += '  <option value="' + ui.item.option_value[i]['option_value_id'] + '">' + ui.item.option_value[i]['name'] + '</option>';
            }

            html += '  </select>';			
			html += '</div>';	
		}
		
		$('.vtabs-content.tab-pane.active').removeClass('active');
		$('#vtab-option li.active').removeClass('active');
		
		$('#tab-option-content').append(html);
		
		$('#option-add').parent('li').before('<li class="active"><a data-toggle="tab" href="#tab-option-' + option_row + '" id="option-' + option_row + '" data-toggle="tab">' + ui.item.label + '&nbsp;<img src="view/image/delete.png" alt="" onclick="$(\'#option-' + option_row + '\').remove(); $(\'#tab-option-' + option_row + '\').remove(); $(\'#vtab-option a:first\').trigger(\'click\'); return false;" /></a></li>');
		
		//$('#vtab-option a').tabs();
		
		//$('#option-' + option_row).trigger('click');
		
		//$('.date').datepicker({dateFormat: 'yy-mm-dd'});
		/*$('.datetime').datetimepicker({
			dateFormat: 'yy-mm-dd',
			timeFormat: 'h:m'
		});	
		*/	
		/*$('.time').timepicker({timeFormat: 'h:m'});	*/
				
		option_row++;
		
		return false;
	},
	focus: function(event, ui) {
      return false;
   }
});


//--></script> 
<script type="text/javascript"><!--		
var option_value_row = <?php echo $option_value_row; ?>;

function addOptionValue(option_row) {	
	html  = '<tbody id="option-value-row' + option_value_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><select class="form-control"  name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]">';
	html += $('#option-values' + option_row).html();
	html += '    </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';
	html += '    <td class="right"><div class="form-group"><div class="col-md-12"><input data-rule-required="true" type="text" class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" size="3" /></div></div></td>'; 
	
	/*
	html += '    <td class="left"><select  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]">';
	html += '      <option value="1"><?php echo $text_yes; ?></option>';
	html += '      <option value="0"><?php echo $text_no; ?></option>';
	html += '    </select></td>';
	html += '    <td class="right"><select  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text"  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" size="5" /></td>';
	*/
	/*
	html += '    <td class="right"><select  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text"  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" size="5" /></td>';	
	*/
	/*
	html += '    <td class="right"><select  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]">';
	html += '      <option value="+">+</option>';
	html += '      <option value="-">-</option>';
	html += '    </select>';
	html += '    <input type="text"  class="form-control" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" size="5" /></td>';
	*/
	html += '    <td class="left"><a onclick="$(\'#option-value-row' + option_value_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#option-value' + option_row + ' tfoot').before(html);

	option_value_row++;
}
//--></script>

<script type="text/javascript"><!--
var special_row = <?php echo $special_row; ?>;

function addSpecial() {
	html  = '<tbody id="special-row' + special_row + '">';
	html += '  <tr>'; 
    html += '    <td class="left"><select name="product_special[' + special_row + '][customer_group_id]">';
    <?php foreach ($customer_groups as $customer_group) { ?>
    html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
    <?php } ?>
    html += '    </select></td>';		
    html += '    <td class="right"><input type="text" name="product_special[' + special_row + '][priority]" value="" size="2" /></td>';
	html += '    <td class="right"><input type="text" name="product_special[' + special_row + '][price]" value="" /></td>';
    html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_start]" value="" class="date" /></td>';
	html += '    <td class="left"><input type="text" name="product_special[' + special_row + '][date_end]" value="" class="date" /></td>';
	html += '    <td class="left"><a onclick="$(\'#special-row' + special_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
    html += '</tbody>';
	
	$('#special tfoot').before(html);
 
	$('#special-row' + special_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});
	
	special_row++;
}
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 110%;" frameborder="no" scrolling="no"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
	
	$('.ui-dialog :button').blur();
};
//--></script> 
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a class="btn" onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');"><?php echo $text_browse; ?></a><a class="btn" onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');"><?php echo $text_clear; ?></a></div></td>';
	html += '    <td class="right"><input class="form-control" type="text" name="product_image[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
}
//--></script> 
<script type="text/javascript">

    var video_row = <?php echo $video_row; ?>;

    function addVideo() {
      html  = '<tbody id="video-row' + video_row + '">';
        html += '  <tr>';
		html += '    <td><textarea class="form-control" name="product_video[' + video_row + '][video]" cols="50" rows="1" /><input type="hidden" name="product_video[' + video_row + '][video_id]" value="" /></td>';
		html += '    <td class="left"></td>';
        html += '    <td class="right"><input class="form-control" type="text" name="product_video[' + video_row + '][sort_order]" value="" size="3" /></td>';
		html += '    <td class="left"><a onclick="$(\'#video-row' + video_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
        html += '  </tr>';  
        html += '</tbody>';
      
      $('#video tfoot').before(html);
      
      
      video_row++;
    }
</script>

<script type="text/javascript"><!--
//$('#tabs a').tabs(); 
//$('#languages a').tabs(); 
//$('#vtab-option a').tabs();
//--></script> 

    <!-- Attributes to Categories -->
<?php if ($this->config->get('categoryattribute_status')) { ?>
    <script type="text/javascript"><!--
        $('input[name=\'product_category[]\']').change(function() {
            load_filters();
        });

        function load_filters() {
            <?php if (isset($this->request->get['product_id'])) {?>
            var product_id = '<?php echo $this->request->get['product_id'];?>';
            <?php } else {?>
            var product_id = 0;
            <?php } ?>

            var selected;
            $('input[name=\'product_category[]\']').each(function() {
                if ($(this).val()) {
                    if (!selected){
                        selected=$(this).val();
                    }else {
                        selected+=","+$(this).val();
                    }
                }
            });

            $.ajax({
                url: 'index.php?route=catalog/pro2ven/getAttributes&token=<?php echo $token; ?>&category_id=' + selected+'&product_id='+product_id,
                dataType: 'json',
                success: function(data) {
                    //$('#tab-attribute').html(data);
					$('#div_AttributesTable').html(data);
                }
            });
        }
        load_filters();
        //--></script>
<?php } ?>
    <script type="text/javascript"><!--
        $('input[name=\'option\']').click(function(){
            $(this).catcomplete('search');
        });
        //--></script>
    <?php if(isset($function) && $function == 'insert'): ?>
    <script type="text/javascript"><!--
        window.onload = function () {
            $('#option-value-row0').remove();
            $('#option-value-row1').remove();
        };
        //--></script>
    <?php endif; ?>
<?php echo $footer; ?>