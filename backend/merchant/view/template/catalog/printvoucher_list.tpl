<?php echo $header; ?>

<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-cogs'></i>
					  <span>Vouchers</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>
							<a href="<?php echo $insert; ?>" class="btn btn-white">Add Product</a>
							<a onclick="$('form').submit();" class="btn btn-white">Delete Product</a>						
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
				<?php if ($success) { ?>
					<div class='alert alert-success alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($success) ? $success : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
			</div>
		</div>
		
		<!--Start List-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box green-border' style='margin-bottom:0;'>
                    <div class='box-header green-background '>
                      <div class='title'>Voucher List</div>
                      <div class='actions'> 
					  
                      </div>
                    </div>
					
                    <div class='box-content box-no-padding'>
						<div class='responsive-table'>
							<div class='scrollable-area'>
								<form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
									<table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
									<thead>
										<tr>
											<th class="left"><?php if ($sort == 'pv.voucher_no') { ?>
												<a href="<?php echo $sort_voucher_no; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_voucher_no; ?></a>
												<?php } else { ?>
												<a href="<?php echo $sort_voucher_no; ?>"><?php echo $column_voucher_no; ?></a>
												<?php } ?>
											</th>

											<th class="left"><?php if ($sort == 'vds.vendor_name') { ?>
												<a href="<?php echo $sort_vendor; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_vendor; ?></a>
												<?php } else { ?>
												<a href="<?php echo $sort_vendor; ?>"><?php echo $column_vendor; ?></a>
												<?php } ?>
											</th>
										
											<th class="left"><?php if ($sort == 'pd.name') { ?>
												<a href="<?php echo $sort_product; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_product; ?></a>
												<?php } else { ?>
												<a href="<?php echo $sort_product; ?>"><?php echo $column_product; ?></a>
												<?php } ?>
											</th>

											<th class="right"><?php if ($sort == 'pv.created_at') { ?>
												<a href="<?php echo $sort_created_at; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_created_at; ?></a>
												<?php } else { ?>
												<a href="<?php echo $sort_created_at; ?>"><?php echo $column_created_at; ?></a>
												<?php } ?>
											</th>

											<th class="left"><?php if ($sort == 'pv.status') { ?>
												<a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
												<?php } else { ?>
												<a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
												<?php } ?>
											</th>
											
											<th class="right"><?php echo $column_action; ?></th>
										</tr>
									</thead>
									<tbody>
										<tr class="filter">
											<td><input class="form-control" type="text" name="filter_voucher_no" value="<?php echo $filter_voucher_no; ?>" /></td>
											<td><input class="form-control" type="text" name="filter_vendor" value="<?php echo $filter_vendor; ?>" /></td>
											<td><input class="form-control" type="text" name="filter_product" value="<?php echo $filter_product; ?>" /></td>
											<td class="right"><input class="form-control" type="text" name="filter_created_at" value="<?php echo $filter_created_at; ?>" /></td>
											<td>
												<select class="form-control" name="filter_status">
													<option value="*"></option>
													<?php if ($filter_status == 0 && $filter_status != NULL) { ?>
														<option value="0" selected="selected"><?php echo $txt_status_unused; ?></option>
													<?php } else { ?>
														<option value="0"><?php echo $txt_status_unused; ?></option>
													<?php } ?>
													<?php if ($filter_status == 1) { ?>
														<option value="1" selected="selected"><?php echo $txt_status_used; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $txt_status_used; ?></option>
													<?php } ?>
													<?php if ($filter_status == 9) { ?>
														<option value="9" selected="selected"><?php echo $txt_status_void; ?></option>
													<?php } else { ?>
														<option value="9"><?php echo $txt_status_void; ?></option>
													<?php } ?>
												</select>
											</td>
											<td align="right"><a onclick="filter();" class="btn-primary btn-sm"><?php echo $button_filter; ?></a></td>
										</tr>
										<?php if ($vouchers) { ?>
										<?php foreach ($vouchers as $voucher) { ?>
										<tr>										 
											<td class="left"><?php echo $voucher['voucher_no']; ?></td>
											<td class="left"><?php echo $voucher['vendor_name']; ?></td>										
											<td class="left"><?php echo $voucher['name']; ?></td>
											<td class="right"><?php echo $voucher['created_at']; ?></td>
											<td class="left"><?php echo $voucher['status']; ?></td>
											<td class="right">
												<?php foreach ($voucher['action'] as $action) { ?>
													[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
												<?php } ?>
											</td>
										</tr>
										<?php } ?>
										<?php } else { ?>
										<tr>
										  
										<td class="center" colspan="10"><?php echo $text_no_results; ?></td>
										

										<?php } ?>
									</tbody>
									</table>
								</form>
								<div class="pagination"><?php echo $pagination; ?></div>
							</div>
						</div>
					</div>
				</div>	
			</div>
		</div>
	</div>
</div>
<script type="text/javascript"><!--
function filter() {
	url = 'index.php?route=catalog/printvoucher&token=<?php echo $token; ?>';
	
	var filter_voucher_no = $('input[name=\'filter_voucher_no\']').attr('value');
	
	if (filter_voucher_no) {
		url += '&filter_voucher_no=' + encodeURIComponent(filter_voucher_no);
	}
	
	var filter_vendor = $('input[name=\'filter_vendor\']').attr('value');
	
	if (filter_vendor) {
		url += '&filter_vendor=' + encodeURIComponent(filter_vendor);
	}

			
	var filter_product = $('input[name=\'filter_product\']').attr('value');
	
	if (filter_product) {
		url += '&filter_product=' + encodeURIComponent(filter_product);
	}
	
	var filter_created_at = $('input[name=\'filter_created_at\']').attr('value');
	
	if (filter_created_at) {
		url += '&filter_created_at=' + encodeURIComponent(filter_created_at);
	}
	
	var filter_status = $('select[name=\'filter_status\']').attr('value');
	
	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}	

	location = url;
}
//--></script> 
<script type="text/javascript"><!--
$('#form input').keydown(function(e) {
	if (e.keyCode == 13) {
		filter();
	}
});
//--></script> 
<?php echo $footer; ?>