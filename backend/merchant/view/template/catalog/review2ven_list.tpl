<?php echo $header; ?>

    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-table'></i>
                            <span>Tinjauan</span>
                        </h1>

                        <div class='pull-right'>
                            <div class='btn-group'>
                            </div>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                    <?php if ($success) { ?>
                        <div class='alert alert-success alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <!--Start List-->
            <div class='row'>
                <div class='col-sm-12'>
                    <div class='box bordered-box green-border' style='margin-bottom:0;'>
                        <div class='box-header blue-background '>
                            <div class='title'>Daftar Tinjauan</div>
                            <div class='actions'>

                            </div>
                        </div>

                        <div class='box-content box-no-padding'>
                            <div class='responsive-table'>
                                <div class='scrollable-area'>
                                    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form">
                                        <table class='data-table table table-bordered table-striped' style='margin-bottom:0;'>
                                            <thead>
                                            <tr>
                                                <th class="left"><?php if ($sort == 'pd.name') { ?>
                                                        <a href="<?php echo $sort_product; ?>" class="<?php echo strtolower($order); ?>">Produk</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo $sort_product; ?>">Produk</a>
                                                    <?php } ?></th>
                                                <th class="left"><?php if ($sort == 'r.author') { ?>
                                                        <a href="<?php echo $sort_author; ?>" class="<?php echo strtolower($order); ?>">Penulis</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo $sort_author; ?>">Penulis</a>
                                                    <?php } ?></th>
                                                <th class="right"><?php if ($sort == 'r.rating') { ?>
                                                        <a href="<?php echo $sort_rating; ?>" class="<?php echo strtolower($order); ?>">Penilaian</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo $sort_rating; ?>">Penilaian</a>
                                                    <?php } ?></th>
                                                <th class="left"><?php if ($sort == 'r.status') { ?>
                                                        <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                                                    <?php } ?></th>
                                                <th class="left"><?php if ($sort == 'r.date_added') { ?>
                                                        <a href="<?php echo $sort_date_added; ?>" class="<?php echo strtolower($order); ?>">Tanggal Ditambahkan</a>
                                                    <?php } else { ?>
                                                        <a href="<?php echo $sort_date_added; ?>">Tanggal Ditambahkan</a>
                                                    <?php } ?></th>
                                                <th class="right">Tindakan</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <?php if ( count($reviews) ) { ?>
                                                <?php foreach ($reviews as $review) { ?>
                                                    <tr>
                                                        <td class="left"><?php echo $review['name']; ?></td>
                                                        <td class="left"><?php echo $review['author']; ?></td>
                                                        <td class="right"><?php echo $review['rating']; ?></td>
                                                        <td class="left"><?php echo $review['status']; ?></td>
                                                        <td class="left"><?php echo $review['date_added']; ?></td>
                                                        <td class="right"><?php foreach ($review['action'] as $action) { ?>
                                                                [ <a href="<?php echo $action['href']; ?>">View</a> ]
                                                            <?php } ?></td>
                                                    </tr>
                                                <?php } ?>
                                            <?php }else{ ?>
                                                <tr>
                                                    <td colspan="10" style="text-align:center;font-weight: bold;">No Result Found!</td>
                                                </tr>
                                            <?php } ?>

                                            </tbody>
                                        </table>
                                    </form>
                                    <div class="pagination"><?php echo $pagination; ?></div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>
<?php echo $footer; ?>