<?php echo $header; ?>

<div id="content-wrapper" class="row">
	<div class="col-xs-12">
		
		<div class='row'>
			<div class='col-sm-12'>
				<div class='page-header  page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-gear '></i>
					  <span>Pengaturan</span>
					</h1>
					
					<div class='pull-right'>
						<div class='btn-group'>
							<a onclick="$('#form').submit();" class="btn btn-primary"><span>Simpan</span></a>
							<a onclick="location = '<?php echo $cancel; ?>';" class="btn btn-white">Batal</span></a>
						</div>
					</div>
				</div>
				
				<?php if ($error_warning) { ?>
					<div class='alert alert-warning alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
					</div>
				<?php } ?>
				
				<?php if ($success) { ?>
					<div class='alert alert-success alert-dismissable'>
						<a class='close' data-dismiss='alert' href='#'>&times;</a>
						<?php echo (isset($success) ? $success : '' ).'<br>'; ?>
					</div>
				<?php } ?>
			</div>
		</div>
		
		<!--Start Form-->
		<div class='row'>
			<div class='col-sm-12'>
				<div class='box bordered-box blue-border' style='margin-bottom:0;'>
                    <div class='box-header blue-background '>
						<div class='title'><div class='icon-gear'></div>Profil</div>
						<div class='actions'> 

						</div>
                    </div>
					
					<div class='box-content'>
                        <div class='alert alert-warning alert-dismissable'>
                        Informasi penjual harap perbaharui dari website <a target="_blank" href="<?php echo HTTPS_SERBAPAY; ?>member/profile/edit/update">serbaPay Website</a>.
                        </div>
						<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form form-horizontal">
							<div class='tabbable' style='margin-top: 20px'>
								<ul class='nav nav-responsive nav-tabs'>
									<li class='active'>
										<a data-toggle='tab' href='#tab-general'>
											Umum
										</a>
									</li>
									<li class=''>
										<a data-toggle='tab' href='#tab-address'>
											Alamat
										</a>
									</li>
                                    <?php /* Multiple Shipping 
                                    <li class=''>
                                        <a data-toggle='tab' href='#tab-utm-shipping'>
                                            Pengaturan Pengiriman 
                                        </a>
                                    </li>
                                    */?>
									<li class='' style="display:none;">
										<a data-toggle='tab' href='#tab-gallery'>
											Gambar
										</a>
									</li>
									<li class='' style="display:none;">
										<a data-toggle='tab' href='#tab-finance'>
											Finance
										</a>
									</li>
									<?php if ($this->config->get('multi_store_activated')) { ?>
										<li class=''>
											<a data-toggle='tab' href='#tab-payment'>
												Pembayaran
											</a>
										</li>
									<?php } ?>
		
		
								</ul>
								<div class='tab-content'>
									
									<div id="tab-general" class="tab-pane active">
										<table class="form table" >
										<tr style="display:none;">  
											<td><?php echo $entry_company_id; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="company_id" value="<?php echo $company_id; ?>"  />
													</div>
												</div>
											</td>
										</tr>
										<tr style="display:none;">
											<td><?php echo $entry_tax_id; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="tax_id" value="<?php echo $tax_id; ?>" size="25" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Nama Toko:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" type="text" name="company" value="<?php echo $company; ?>" readonly="readonly" />
														<?php if ($error_vendor_company) { ?>
															<span class="error"><?php echo $error_vendor_company; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Nama Lengkap:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" type="text" name="firstname" value="<?php echo $firstname; ?>" readonly="readonly" />
                                                        <input class="form-control" type="hidden" name="lastname" value="<?php echo $lastname; ?>" />

                                                        <?php if ($error_vendor_firstname) { ?>
															<span class="error"><?php echo $error_vendor_firstname; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Telepon:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="telephone" value="<?php echo $telephone; ?>" size="25" readonly="readonly" />
														<?php if ($error_vendor_telephone) { ?>
														<span class="error"><?php echo $error_vendor_telephone; ?></span>
														<?php } ?>
													</div>
												</div>												
										  </td>
										</tr>
										<tr>
											<td><?php echo $entry_email; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="email" value="<?php echo $email; ?>" size="32"  readonly="readonly"/>
														<?php if ($error_vendor_email) { ?>
														<span class="error"><?php echo $error_vendor_email; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td><?php echo $entry_fax; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="fax" value="<?php echo $fax; ?>" size="25" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Deskripsi:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<textarea class="form-control" name="vendor_description" cols="68" rows="3" ><?php echo $vendor_description; ?></textarea>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Website Penjual:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" name="store_url" value="<?php echo $store_url; ?>" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Gambar:</td>
											<td>
												<div class="form-group">
													<div class="col-md-4">
														<div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br><br>
															<input type="hidden" name="vendor_image" value="<?php echo $vendor_image; ?>" id="image" />
															<a class="btn" onclick="image_upload('image', 'thumb');">Cari Berkas</a>													
															<a class="btn" onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');">Hapus Gambar</a>
														</div>
													</div>
												</div>
											</td>
										</tr>
										<!--vendor banner-->
										<tr>
											<td>Banner:<br><span class="help">Company Banner(990x150)</span></td>
											<td>
												<div class="form-group">
													<div class="col-md-7">
														<div class="image"><img style="max-width: 100%;" src="<?php echo $banner_thumb; ?>" alt="" id="banner_thumb" /><br><br>
														<input type="hidden" name="vendor_banner" value="<?php echo $vendor_banner; ?>" id="banner_image" />
														<a class="btn" onclick="image_upload('banner_image', 'banner_thumb');">Cari Berkas</a>											  
														<a class="btn" onclick="$('#banner_thumb').attr('src', '<?php echo $banner_no_image; ?>'); $('#image').attr('value', '');">Hapus Gambar</a></div>
													</div>
												</div>
											</td>
										</tr>
										</table>
									</div>
									
									<div id="tab-address" class="tab-pane">
										<table class="form table">
										<tr>
											<td>Alamat 1: </td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" type="text" name="address_1" value="<?php echo $address_1; ?>" readonly="readonly" />
                                                        <input class="form-control" type="hidden" name="address_2" value="<?php echo $address_2; ?>" />
														<?php if ($error_vendor_address_1) { ?>
														<span class="error"><?php echo $error_vendor_address_1; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Kota:</td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<input class="form-control" type="text" name="city" value="<?php echo $city; ?>" readonly="readonly" />
														<?php if ($error_vendor_city) { ?>
														<span class="error"><?php echo $error_vendor_city; ?></span>
														<?php } ?>
													</div>
												</div>	
											</td>
										</tr>
										<tr>
											<td>Kode Pos: </td>
											<td>
												<div class="form-group">
													<div class="col-md-3">
														<input class="form-control" type="text" name="postcode" value="<?php echo $postcode; ?>" readonly="readonly" />
														<?php if ($error_vendor_postcode) { ?>
														<span class="error"><?php echo $error_vendor_postcode; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr style="display:none;">
											<td><?php echo $entry_country; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-3">
														<select class="form-control" name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor_profile/zone&token=<?php echo $token; ?>&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
															<option value=""><?php echo $text_select; ?></option>
															<?php foreach ($countries as $country) { ?>
																<?php if ($country['country_id'] == $country_id) { ?>
																	<option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
																<?php } else { ?>
																	<option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
																<?php } ?>
															<?php } ?>
														</select>
														<?php if ($error_vendor_country) { ?>
														<span class="error"><?php echo $error_vendor_country; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td>Wilayah: </td>
											<td>
												<div class="form-group">
													<div class="col-md-6">
														<select class="form-control" name="zone_id" readonly="readonly">
														</select>
														<?php if ($error_vendor_zone) { ?>
														<span class="error"><?php echo $error_vendor_zone; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
									   </table>
									</div>
									<!-- Vincent - 8/12/2015 -->
									<?php /*
									<div id="tab-gallery" class="tab-pane" style="display:none;">
										<table id="images" class="list">
										<thead>
										  <tr>
											<th class="left">Image:</td>
											<th class="right">Sort Order:</td>
											<th></th>
										  </tr>
										</thead>			
										<?php $image_row = 0; ?>
										<?php foreach ($vendor_gallery as $vendor_image) { ?>
										<?php if($image_row > 3){ continue; } ?>
										<tbody id="image-row<?php echo $image_row; ?>">
										  <tr>
											<td class="left">
												<br>
												<div class="image"><img src="<?php echo $vendor_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" /><br><br>
												<input type="hidden" name="vendor_gallery[<?php echo $image_row; ?>][image]" value="<?php echo $vendor_image['image']; ?>" id="image<?php echo $image_row; ?>" />												
												<a class="btn" onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>
												<a class="btn" onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
												</div>
											</td>
											<td class="right">
												<input class="form-control" type="text" name="vendor_gallery[<?php echo $image_row; ?>][sort_order]" value="<?php echo $vendor_image['sort_order']; ?>" size="2" />
											</td>
											<td class="right">
												<a onclick="$('#image-row<?php echo $image_row; ?>').remove(); checkStatus();" class="btn btn-sm btn-warning" style="margin-left: 20px;">Remove</a>
											</td>
										  </tr>
										</tbody>
										<?php $image_row++; ?>
										<?php } ?>
										<tfoot>
										  <tr>
											<?php if($image_row < 5){ ?>											
											<td colspan="3" class="left">
												<br>
												<a onclick="addImage();" id="a_LimitAddButton" class="btn btn-primary btn-block">Add Image</a>
												<br><br>
											</td>
											<?php } ?>
										  </tr>
										</tfoot>
									  </table>
									</div>
									*/?>

                                    <?php //Multiple Shipping ?>
                                    <div id="tab-utm-shipping-setting" class="tab-pane" style="display:none;">
                                        <table class="form table">
                                            <tr>
                                                <td><?php echo $entry_measurement; ?></td>
                                                <td>
                                                    <div class="form-group">
                                                        <div class="col-md-6"><select class="form-control" name="measurement_id">
                                                        <?php if (($measurement_id==0) || (empty($measurement_id)) ) { ?>
                                                            <option value="0" selected="selected"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php /* Ken - Disable the option if ($measurement_id==1) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1" selected="selected"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php if ($measurement_id==2) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2" selected="selected"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3"><?php echo $text_tapv_text; ?></option>
                                                        <?php } ?>
                                                        <?php if ($measurement_id==3) { ?>
                                                            <option value="0"><?php echo $text_none; ?></option>
                                                            <option value="1"><?php echo $text_nws_text; ?></option>
                                                            <option value="2"><?php echo $text_wbsp_text; ?></option>
                                                            <option value="3" selected="selected"><?php echo $text_tapv_text; ?></option>
                                                        <?php }*/ ?>
                                                    </select>
                                                            </div></div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="tab-utm-shipping" class="tab-pane" >
                                        <table id="ultm-shipping" class="list form table">
                                            <thead>
                                            <tr>
                                                <td class="left">Kurir Pengiriman</td>
                                                <td class="right">Harga Pengiriman</td>
                                                <td></td>
                                            </tr>
                                            </thead>
                                                <tbody id="ultm-shipping-row<?php echo $ultm_shipping_row; ?>">
                                                <tr>
                                                    <td class="left">Self Delivery (Same Provision) - Type -1 for not available<br />Sample 10:10000,20:60000,30:120000</td>
                                                    <td class="right"><input type="text" class="form-control" name="vendor_ultimate_shipping[0][shipping_rate]" value="<?php echo isset($vendor_ultimate_shippings[0]['shipping_rate'])?$vendor_ultimate_shippings[0]['shipping_rate']:'-1'; ?>" /></td>
                                                </tr>
                                                <tr>
                                                    <td class="left">Self Delivery (Other from Same Provision - Type -1 for not available<br />Sample 10:30000,20:120000,30:320000</td>
                                                    <td class="right"><input type="text" class="form-control" name="vendor_ultimate_shipping[1][shipping_rate]" value="<?php echo isset($vendor_ultimate_shippings[1]['shipping_rate'])?$vendor_ultimate_shippings[1]['shipping_rate']:'-1'; ?>" /></td>
                                                </tr>
                                                </tbody>
                                        </table>
                                    </div>
                                    <?php //.Multiple Shipping ?>
									<div id="tab-finance" class="tab-pane">
										<table class="form table">		
										<tr>
											<td style="width: 200px;">
												<span class="required">*</span><?php echo $entry_paypal_email; ?>
											</td>
											<td>
												<div class="form-group">
													<div class="col-md-5">
														<input class="form-control" name="paypal_email" value="<?php echo $paypal_email; ?>" size="25" />
														<?php if ($error_vendor_paypal_email) { ?>
															<span class="error"><?php echo $error_vendor_paypal_email; ?></span>
														<?php } ?>
													</div>
												</div>
											</td>
										</tr>
										<tr>
										  <td>Bank Account No.<?php //echo $entry_iban; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-5">
														<input class="form-control" name="iban" value="<?php echo $iban; ?>" size="25" />
													</div>
												</div>
											</td>
										</tr>
										<tr>
											<td><?php echo $entry_bank_name; ?></td>
											<td>
												<div class="form-group">
													<div class="col-md-5">
														<input class="form-control" name="bank_name" value="<?php echo $bank_name; ?>" size="25" />
													</div>
												</div>
											</td>
										</tr>
										<?php /* 
										<tr style="display: none;">
										  <td><?php echo $entry_bank_addr; ?></td>
										  <td><textarea name="bank_address" cols="68" rows="5" ><?php echo $bank_address; ?></textarea></td>
										</tr>
										<tr style="display: none;">
										  <td><?php echo $entry_swift_bic; ?></td>
										  <td><input name="swift_bic" value="<?php echo $swift_bic; ?>" size="25" /></td>
										</tr>
										*/ ?>
										</table>
									</div>
									
									<?php if ($this->config->get('multi_store_activated')) { ?>
									<div id="tab-payment" class="tab-pane">
										<table class="form">
										<tr>
											<td><?php echo $entry_accept_paypal; ?></td>
											<td>
												<select class="form-control" name="accept_paypal">
													<?php if ($accept_paypal) { ?>
														<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
														<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $text_enabled; ?></option>
														<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
										<tr>
											<td><?php echo $entry_accept_cheques; ?></td>
											<td>
												<select class="form-control" name="accept_cheques">
													<?php if ($accept_cheques) { ?>
														<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
														<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $text_enabled; ?></option>
														<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
										<tr>
											<td><?php echo $entry_accept_bank_transfer; ?></td>
											<td>
												<select class="form-control" name="accept_bank_transfer">
													<?php if ($accept_bank_transfer) { ?>
														<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
														<option value="0"><?php echo $text_disabled; ?></option>
													<?php } else { ?>
														<option value="1"><?php echo $text_enabled; ?></option>
														<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
													<?php } ?>
												</select>
											</td>
										</tr>
										</table>
									</div>
									<?php } else { ?>
										<input type="hidden" name="accept_paypal" value="0" /> 
										<input type="hidden" name="accept_cheques" value="0" />
										<input type="hidden" name="accept_bank_transfer" value="0" />
									<?php } ?>
									
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		
	</div>
</div>


<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor_profile/zone&token=<?php echo $token; ?>&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><br><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="vendor_gallery[' + image_row + '][image]" value="" id="image' + image_row + '" /><br><br><a class="btn" onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">Browses File</a> <a class="btn" onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');">Clear Image</a></div></td>';
	html += '    <td class="right"><input class="form-control" type="text" name="vendor_gallery[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove(); checkStatus();" class="btn btn-sm btn-warning" style="margin-left: 20px;">Remove</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
	
	if(image_row >= 3)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}
	
}

function checkStatus(){
	
	var stt_image_row = $('#tab-gallery tbody').length;
	console.log(stt_image_row);
	if(stt_image_row >= 5)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}else{
		$('#a_LimitAddButton').css('display', 'inline-block');
	}
	
}
//--></script>

<?php //Multiple Shipping ?>
    <script type="text/javascript"><!--
        var ultm_shipping_row = <?php echo $ultm_shipping_row; ?>;

        function add_ultm_Shipping() {
            html  = '<tbody id="ultm-shipping-row' + ultm_shipping_row + '">';
            html += '  <tr>';
            html += '  <td class="text-left"><select class="form-control" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][courier_id]">';
            <?php foreach ($couriers as $courier) { ?>
            html += '      <option value="<?php echo $courier['courier_id']; ?>"><?php echo $courier['courier_name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';
            html += '    <input type="hidden" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][vutm_shipping_id]" value="" />';
            html += '    <td class="right"><input class="form-control" type="text" name="vendor_ultimate_shipping[' + ultm_shipping_row + '][shipping_rate]" value="" /></td>';
            html += '    <td class="right" style="display:none;"><select name="vendor_ultimate_shipping[' + ultm_shipping_row + '][geo_zone_id]">';
            <?php foreach ($geo_zones as $geo_zone) { ?>
            html += '      <option value="<?php echo $geo_zone['geo_zone_id']; ?>"><?php echo $geo_zone['name']; ?></option>';
            <?php } ?>
            html += '  </select></td>';

            html += '    <td class="left"><a onclick="$(\'#ultm-shipping-row' + ultm_shipping_row + '\').remove();" class="btn btn-danger"><?php echo $button_remove; ?></a></td>';
            html += '  </tr>';
            html += '</tbody>';

            $('#ultm-shipping tfoot').before(html);
            $('#ultm-shipping-row' + ultm_shipping_row + ' .date').datepicker({dateFormat: 'yy-mm-dd'});

            ultm_shipping_row++;
        }
        //--></script>
<?php //.Multiple Shipping ?>
<?php echo $footer; ?>