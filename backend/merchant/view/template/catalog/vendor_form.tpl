<?php echo $header; ?>
<div id="content">
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><img src="view/image/product.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><span><?php echo $button_save; ?></span></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><span><?php echo $button_cancel; ?></span></a></div>
  </div>
  <div class="content">
	<div id="tabs" class="htabs">
		<a href="#tab-general"><?php echo $tab_general; ?></a>
		<a href="#tab-address"><?php echo $tab_address; ?></a>
		<a href="#tab-gallery">Gallery</a>
		<a href="#tab-finance"><?php echo $tab_finance; ?></a>
		<a href="#tab-commission"><?php echo $tab_commission; ?></a>
		<?php if ($this->config->get('multi_store_activated')) { ?>
			<a href="#tab-payment"><?php echo $tab_payment; ?></a>
		<?php } ?>
	</div>
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <div id="tab-general">
	  <table class="form">
		<tr>
          <td><span class="required">*</span> <?php echo $entry_user_account; ?></td>
          <td><select name="user_id">
		  <option value=""><?php echo $text_select; ?></option>
		  <?php foreach ($user_accounts as $user_account) { ?>
			 <?php if ($user_account['user_id'] == $user_id) { ?>
			   <option value="<?php echo $user_account['user_id']; ?>" selected="selected"><?php echo $user_account['username']; ?></option>
			 <?php } else { ?>
			   <option value="<?php echo $user_account['user_id']; ?>"><?php echo $user_account['username']; ?></option>
			 <?php } ?>
          <?php } ?>
		  </select></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_vendor_name; ?></td>
          <td><input name="vendor_name" value="<?php echo $vendor_name; ?>" size="25" />
            <?php if ($error_vendor_name) { ?>
            <span class="error"><?php echo $error_vendor_name; ?></span>
            <?php } ?></td>
        </tr>
		<tr>
          <td><?php echo $entry_company; ?></td>
          <td><input name="company" value="<?php echo $company; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_company_id; ?></td>
          <td><input name="company_id" value="<?php echo $company_id; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_tax_id; ?></td>
          <td><input name="tax_id" value="<?php echo $tax_id; ?>" size="25" /></td>
        </tr>
	    <tr>
          <td><span class="required">*</span> <?php echo $entry_firstname; ?></td>
          <td><input type="text" name="firstname" value="<?php echo $firstname; ?>" />
            <?php if ($error_vendor_firstname) { ?>
            <span class="error"><?php echo $error_vendor_firstname; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_lastname; ?></td>
          <td><input type="text" name="lastname" value="<?php echo $lastname; ?>" />
            <?php if ($error_vendor_lastname) { ?>
            <span class="error"><?php echo $error_vendor_lastname; ?></span>
            <?php } ?></td>
        </tr>
		<tr>
          <td><span class="required">*</span> <?php echo $entry_telephone; ?></td>
          <td><input name="telephone" value="<?php echo $telephone; ?>" size="25" />
		  <?php if ($error_vendor_telephone) { ?>
            <span class="error"><?php echo $error_vendor_telephone; ?></span>
            <?php } ?>
		  </td>
        </tr>
		<tr>
          <td><span class="required">*</span> <?php echo $entry_email; ?></td>
          <td><input name="email" value="<?php echo $email; ?>" size="25" />
		   <?php if ($error_vendor_email) { ?>
            <span class="error"><?php echo $error_vendor_email; ?></span>
           <?php } ?>
		  </td>
        </tr>
		<tr>
          <td><?php echo $entry_fax; ?></td>
          <td><input name="fax" value="<?php echo $fax; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_description; ?></td>
          <td><textarea name="vendor_description" cols="68" rows="3" ><?php echo $vendor_description; ?></textarea></td>
        </tr>
		<tr>
          <td><?php echo $entry_store_url; ?></td>
          <td><textarea name="store_url" cols="68" rows="3" ><?php echo $store_url; ?></textarea></td>
        </tr>
        <tr>
		  <td><?php echo $entry_image; ?></td>
          <td><div class="image"><img src="<?php echo $thumb; ?>" alt="" id="thumb" /><br />
              <input type="hidden" name="vendor_image" value="<?php echo $vendor_image; ?>" id="image" />
              <a onclick="image_upload('image', 'thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb').attr('src', '<?php echo $no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
        </tr>
		<!--vendor banner-->
		<tr>
		  <td>Image:<br><span class="help">Company Banner(990x150)</span></td>
          <td><div class="image"><img src="<?php echo $banner_thumb; ?>" alt="" id="banner_thumb" /><br />
              <input type="hidden" name="vendor_banner" value="<?php echo $vendor_banner; ?>" id="banner_image" />
              <a onclick="image_upload('banner_image', 'banner_thumb');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#banner_thumb').attr('src', '<?php echo $banner_no_image; ?>'); $('#image').attr('value', '');"><?php echo $text_clear; ?></a></div></td>
        </tr>		
		<tr>
          <td><?php echo $entry_sort_order; ?></td>
          <td><input name="sort_order" value="<?php echo $sort_order; ?>" size="1" />
        </tr>
		</table>
		</div>
	   <div id="tab-address">
	   <table class="form">
	    <tr>
          <td><span class="required">*</span> <?php echo $entry_address_1; ?></td>
          <td><input type="text" name="address_1" value="<?php echo $address_1; ?>" />
            <?php if ($error_vendor_address_1) { ?>
            <span class="error"><?php echo $error_vendor_address_1; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><?php echo $entry_address_2; ?></td>
          <td><input type="text" name="address_2" value="<?php echo $address_2; ?>" /></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_city; ?></td>
          <td><input type="text" name="city" value="<?php echo $city; ?>" />
            <?php if ($error_vendor_city) { ?>
            <span class="error"><?php echo $error_vendor_city; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_postcode; ?></td>
          <td><input type="text" name="postcode" value="<?php echo $postcode; ?>" />
            <?php if ($error_vendor_postcode) { ?>
            <span class="error"><?php echo $error_vendor_postcode; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_country; ?></td>
          <td><select name="country_id" onchange="$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor/zone&token=<?php echo $token; ?>&country_id=' + this.value + '&zone_id=<?php echo $zone_id; ?>');">
              <option value=""><?php echo $text_select; ?></option>
              <?php foreach ($countries as $country) { ?>
              <?php if ($country['country_id'] == $country_id) { ?>
              <option value="<?php echo $country['country_id']; ?>" selected="selected"><?php echo $country['name']; ?></option>
			  <?php } else { ?>
              <option value="<?php echo $country['country_id']; ?>"><?php echo $country['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
            <?php if ($error_vendor_country) { ?>
            <span class="error"><?php echo $error_vendor_country; ?></span>
            <?php } ?></td>
        </tr>
        <tr>
          <td><span class="required">*</span> <?php echo $entry_zone; ?></td>
          <td><select name="zone_id">
            </select>
            <?php if ($error_vendor_zone) { ?>
            <span class="error"><?php echo $error_vendor_zone; ?></span>
            <?php } ?></td>
        </tr>
		</table>
		</div>
		
		<!-- vendor gallery -->
	   <div id="tab-gallery">
          <table id="images" class="list">
            <thead>
              <tr>
                <td class="left">Image:</td>
                <td class="right">Sort Order:</td>
                <td></td>
              </tr>
            </thead>			
            <?php $image_row = 0; ?>
            <?php foreach ($vendor_gallery as $vendor_image) { ?>
			<?php if($image_row > 6){ continue; } ?>
            <tbody id="image-row<?php echo $image_row; ?>">
              <tr>
                <td class="left">
					<div class="image"><img src="<?php echo $vendor_image['thumb']; ?>" alt="" id="thumb<?php echo $image_row; ?>" />
                    <input type="hidden" name="vendor_gallery[<?php echo $image_row; ?>][image]" value="<?php echo $vendor_image['image']; ?>" id="image<?php echo $image_row; ?>" />
                    <br />
                    <a onclick="image_upload('image<?php echo $image_row; ?>', 'thumb<?php echo $image_row; ?>');"><?php echo $text_browse; ?></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$('#thumb<?php echo $image_row; ?>').attr('src', '<?php echo $no_image; ?>'); $('#image<?php echo $image_row; ?>').attr('value', '');"><?php echo $text_clear; ?></a>
					</div>
				</td>
                <td class="right"><input type="text" name="vendor_gallery[<?php echo $image_row; ?>][sort_order]" value="<?php echo $vendor_image['sort_order']; ?>" size="2" /></td>
                <td class="left"><a onclick="$('#image-row<?php echo $image_row; ?>').remove(); checkStatus();" class="button">Remove</a></td>
              </tr>
            </tbody>
            <?php $image_row++; ?>
            <?php } ?>
            <tfoot>
              <tr>
				<?php if($image_row < 6){ ?>
                <td colspan="2"></td>
                <td class="left"><a onclick="addImage();" id="a_LimitAddButton" class="button">Add Image</a></td>
				<?php } ?>
              </tr>
            </tfoot>
          </table>
        </div>
		
		<div id="tab-finance">
		<table class="form">		
		<tr>
          <td><?php echo $entry_paypal_email; ?></td>
          <td><input name="paypal_email" value="<?php echo $paypal_email; ?>" size="25" />
		  <?php if ($error_vendor_paypal_email) { ?>
            <span class="error"><?php echo $error_vendor_paypal_email; ?></span>
          <?php } ?>
		  </td>
        </tr>
		<tr>
          <td>Bank Account No.<?php //echo $entry_iban; ?></td>
          <td><input name="iban" value="<?php echo $iban; ?>" size="25" /></td>
        </tr>
		<tr>
          <td><?php echo $entry_bank_name; ?></td>
          <td><input name="bank_name" value="<?php echo $bank_name; ?>" size="25" /></td>
        </tr>
		<?php /* 
		<tr style="display: none;">
          <td><?php echo $entry_bank_addr; ?></td>
          <td><textarea name="bank_address" cols="68" rows="5" ><?php echo $bank_address; ?></textarea></td>
        </tr>
		<tr style="display: none;">
          <td><?php echo $entry_swift_bic; ?></td>
          <td><input name="swift_bic" value="<?php echo $swift_bic; ?>" size="25" /></td>
        </tr>
		*/ ?>
		</table>
		</div>
		
	   <div id="tab-commission">
	   <table class="form">
		<tr>
          <td><span class="required">*</span> <?php echo $entry_commission; ?></td>
          <td><select name="commission">
		  <option value=""><?php echo $text_select; ?></option>
		  <?php foreach ($commissions as $mycom) { ?>
			<?php if ($commission == $mycom['commission_id']) { ?>
			<option value="<?php echo $mycom['commission_id']; ?>" selected="selected">
			<?php if ($mycom['commission_type'] == '0') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $mycom['commission'] . '%) - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '1') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $mycom['commission'] . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '2') { ?>
				<?php $data = explode(':',$mycom['commission']); ?>
				<?php echo $mycom['commission_name'] . ' (' . $data[0] . '% + ' . $data[1] . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '3') { ?>
				<?php $data = explode(':',$mycom['commission']); ?>
				<?php echo $mycom['commission_name'] . ' (' . $data[0] . ' + ' . $data[1] . '%) - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '4') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $this->currency->format($mycom['commission'], $this->config->get('config_currency')) . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '5') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $this->currency->format($mycom['commission'], $this->config->get('config_currency')) . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } ?>
			</option> 
			<?php } else { ?>
			<option value="<?php echo $mycom['commission_id']; ?>">
			<?php if ($mycom['commission_type'] == '0') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $mycom['commission'] . '%) - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '1') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $mycom['commission'] . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '2') { ?>
				<?php $data = explode(':',$mycom['commission']); ?>
				<?php echo $mycom['commission_name'] . ' (' . $data[0] . '% + ' . $data[1] . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '3') { ?>
				<?php $data = explode(':',$mycom['commission']); ?>
				<?php echo $mycom['commission_name'] . ' (' . $data[0] . ' + ' . $data[1] . '%) - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '4') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $this->currency->format($mycom['commission'], $this->config->get('config_currency')) . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } elseif ($mycom['commission_type'] == '5') { ?>
				<?php echo $mycom['commission_name'] . ' (' . $this->currency->format($mycom['commission'], $this->config->get('config_currency')) . ') - ' . $mycom['product_limit'] . $text_products; ?>
			<?php } ?>
			</option> 
			<?php } ?>
          <?php } ?>
		  </select></td>
        </tr>	
		<tr>
          <td><span class="required">*</span> <?php echo $entry_limit; ?></td>
          <td><select name="product_limit">
		  <?php foreach ($prolimits as $prolimit) { ?>
			 <?php if ($prolimit['product_limit_id'] == $product_limit) { ?>
			   <option value="<?php echo $prolimit['product_limit_id']; ?>" selected="selected"><?php echo $prolimit['package_name'] . ' (' . $prolimit['product_limit'] . ')' ?></option>
			 <?php } else { ?>
			   <option value="<?php echo $prolimit['product_limit_id']; ?>"><?php echo $prolimit['package_name'] . ' (' . $prolimit['product_limit'] . ')' ?></option>
			 <?php } ?>
          <?php } ?>
		  </select></td>
        </tr>
		</table>
			<?php 
			$com_id = 0;
			foreach ($commissions as $mycom) {
				if ($commission == $mycom['commission_id']) { 
					$com_id = $mycom['commission_id'];
					break;
				}				
			}
			?>
			<div class="div_package_info">
				<table border="1" cellpadding="5" cellspacing="0">
					<tbody>
						<?php 
							foreach ($commissionsoption as $key=>$mycom_opt) 
							{
								if($key == $com_id)
								{
									echo '<tr><td>Package: </td><td>'.$mycom_opt['name'].'</td></tr>';
								}
								
								foreach($mycom_opt['option'] as $mycom_opt_selected)
								{
									echo '<tr><td>'.$mycom_opt_selected['label_text'].': </td><td>'.$mycom_opt_selected['label_value'].'</td></tr>';
								}
								
								if($key == $com_id)
								{
									break;
								}
							}
						?>
					</tbody>
				</table>
				<?php //echo '<pre>'; print_r($commissionsoption); echo '</pre>';  ?>
			</div>
           
		</div>
	  <?php if ($this->config->get('multi_store_activated')) { ?> 	
	  <div id="tab-payment">
	   <table class="form">
	    <tr>
          <td><?php echo $entry_accept_paypal; ?></td>
          <td><select name="accept_paypal">
                  <?php if ($accept_paypal) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
		<tr>
          <td><?php echo $entry_accept_cheques; ?></td>
          <td><select name="accept_cheques">
                  <?php if ($accept_cheques) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
		<tr>
          <td><?php echo $entry_accept_bank_transfer; ?></td>
          <td><select name="accept_bank_transfer">
                  <?php if ($accept_bank_transfer) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select></td>
        </tr>
       </table>
	  </div>
	  <?php } else { ?>
		  <input type="hidden" name="accept_paypal" value="0" /> 
		  <input type="hidden" name="accept_cheques" value="0" />
		  <input type="hidden" name="accept_bank_transfer" value="0" />
	  <?php } ?>
    </form>
  </div>
</div>
<script type="text/javascript"><!--
$('select[name=\'zone_id\']').load('index.php?route=catalog/vendor/zone&token=<?php echo $token; ?>&country_id=<?php echo $country_id; ?>&zone_id=<?php echo $zone_id; ?>');
//--></script> 
<script type="text/javascript"><!--
function image_upload(field, thumb) {
	$('#dialog').remove();
	
	$('#content').prepend('<div id="dialog" style="padding: 3px 0px 0px 0px;"><iframe src="index.php?route=common/filemanager&token=<?php echo $token; ?>&field=' + encodeURIComponent(field) + '" style="padding:0; margin: 0; display: block; width: 100%; height: 100%;" frameborder="no" scrolling="auto"></iframe></div>');
	
	$('#dialog').dialog({
		title: '<?php echo $text_image_manager; ?>',
		close: function (event, ui) {
			if ($('#' + field).attr('value')) {
				$.ajax({
					url: 'index.php?route=common/filemanager/image&token=<?php echo $token; ?>&image=' + encodeURIComponent($('#' + field).attr('value')),
					dataType: 'text',
					success: function(text) {
						$('#' + thumb).replaceWith('<img src="' + text + '" alt="" id="' + thumb + '" />');
					}
				});
			}
		},	
		bgiframe: false,
		width: 800,
		height: 400,
		resizable: false,
		modal: false
	});
};
//--></script>
<script type="text/javascript"><!--
$('#tabs a').tabs(); 
//--></script> 
<script type="text/javascript"><!--
var image_row = <?php echo $image_row; ?>;

function addImage() {
    html  = '<tbody id="image-row' + image_row + '">';
	html += '  <tr>';
	html += '    <td class="left"><div class="image"><img src="<?php echo $no_image; ?>" alt="" id="thumb' + image_row + '" /><input type="hidden" name="vendor_gallery[' + image_row + '][image]" value="" id="image' + image_row + '" /><br /><a onclick="image_upload(\'image' + image_row + '\', \'thumb' + image_row + '\');">Browse</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a onclick="$(\'#thumb' + image_row + '\').attr(\'src\', \'<?php echo $no_image; ?>\'); $(\'#image' + image_row + '\').attr(\'value\', \'\');">Clear</a></div></td>';
	html += '    <td class="right"><input type="text" name="vendor_gallery[' + image_row + '][sort_order]" value="" size="2" /></td>';
	html += '    <td class="left"><a onclick="$(\'#image-row' + image_row  + '\').remove(); checkStatus();" class="button">Remove</a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#images tfoot').before(html);
	
	image_row++;
	
	if(image_row >= 6)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}
}

function checkStatus(){
	
	var stt_image_row = $('#tab-gallery tbody').length;
	console.log(stt_image_row);
	if(stt_image_row >= 6)
	{
		$('#a_LimitAddButton').css('display', 'none');
	}else{
		$('#a_LimitAddButton').css('display', 'inline-block');
	}
	
}
//--></script> 
<?php echo $footer; ?>