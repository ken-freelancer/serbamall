<?php echo $header; ?>

    <div id="content-wrapper" class="row">
        <div class="col-xs-12">

            <div class='row'>
                <div class='col-sm-12'>
                    <div class='page-header  page-header-with-buttons'>
                        <h1 class='pull-left'>
                            <i class='icon-table'></i>
                            <span>Advertising - Banner Advertisement</span>
                        </h1>

                        <div class='pull-right'>
                            <ul class="breadcrumb">
                                <li>
                                    <a href="index.html">
                                        Advertising
                                    </a>
                                </li>
                                <li class="separator">
                                    <i class="icon-angle-right"></i>
                                </li>
                                <li class="active">Banner Advertisement</li>
                            </ul>
                        </div>
                    </div>

                    <?php if ($error_warning) { ?>
                        <div class='alert alert-warning alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($error_warning) ? $error_warning : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>

                    <?php if ($success) { ?>
                        <div class='alert alert-success alert-dismissable'>
                            <a class='close' data-dismiss='alert' href='#'>&times;</a>
                            <?php echo (isset($success) ? $success : '' ).'<br>'; ?>
                        </div>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-header">
                            <div class="title">Quick links</div>
                        </div>
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-3">
                                    <div class="box-quick-link blue-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-comments"></div>
                                            </div>
                                            <div class="content">Comments</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link green-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-star"></div>
                                            </div>
                                            <div class="content">Veeeery long title of this quick link</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link orange-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-magic"></div>
                                            </div>
                                            <div class="content">Magic</div>
                                        </a>
                                    </div>
                                </div>
                                <div class="col-sm-3">
                                    <div class="box-quick-link red-background">
                                        <a>
                                            <div class="header">
                                                <div class="icon-inbox"></div>
                                            </div>
                                            <div class="content">Orders</div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class='row'>
                <div class='col-sm-12'>

                        <div class="box">
                            <div class="box-header">
                                <div class="title">
                                    Banner Ads
                                </div>
                            </div>
                            <div class="box-content">
                                <div class="row">
                                    <div class="col-sm-12">
                                        <h4>
                                            Purchase Billboard Banner in Popular page in serbaMall to advertise your product!
                                        </h4>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <span class="title titlebox">
                                            How to Purchase
                                        </span>
                                        <ol style="margin-top:15px;"">
                                            <li>Download PO Form</li>
                                            <li>Fill in PO Form, send it to <a href="mailto:advertising@serbamall.com">advertising@serbamall.com</a></li>
                                            <li>Advertising team will contact to Seller.</li>
                                        </ol>
                                    </div>
                                </div>
                            </div>
                        </div>

                </div>
            </div>

            <div class="row">
                <div class="col-sm-12">
                    <div class="box">
                        <div class="box-content">
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="guide2_con">
                                        <div class="detail_img_view">
                                            <div>
                                                Ads Guide Image
                                                <!--<img src="/images/thumb1.jpg" alt="Main Page" border="0" usemap="#Map_preview"></div>
                                            <map name="Map_preview" id="Map_preview">
                                                <map name="Map_preview" id="Map_preview"><area shape="rect" coords="204,83,516,279" href="javascript:scrollMove(0);" alt="Main Visual"><area shape="rect" coords="517,314,644,496" href="javascript:scrollMove(1);" alt="Hot Promo"><area shape="rect" coords="204,83,516,279" href="javascript:scrollMove(0);" alt="Main Visual"><area shape="rect" coords="517,314,644,496" href="javascript:scrollMove(1);" alt="Hot Promo"><area shape="rect" coords="199,1016,645,1118" href="javascript:scrollMove(2);" alt="Category Promo"></map>

                                            </map>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--.row-->

        </div>

    </div>
    </div>

<?php echo $footer; ?>