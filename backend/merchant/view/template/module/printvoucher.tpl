<?php
/**
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License, Version 3
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 *
 * @category   OpenCart
 * @package    Print Voucher
 * @copyright  Copyright (c) 2015 Ken Teo
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 */
 ?>

<?php echo $header; ?>

<div id="content">
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><img src="view/image/module.png" alt="" /> <?php echo $heading_title; ?></h1>
    <div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>
  </div>
  <div class="content">
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
      <table class="form">
        <tr>
          <td><?php echo $entry_status; ?></td>
          <td>
            <select name="printvoucher_status">
                <?php if ($printvoucher_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
            </select>
          </td>
        </tr>
        <tr>
          <td><?php echo $entry_listing_per_page; ?></td>
          <td><input type="text" name="pv_listing_per_page" value="<?php if ($pv_listing_per_page) { echo $pv_listing_per_page; } else { echo '0'; } ?>" size="8"> 
      </td>
    </tr> 
      </table>

    </form>
  </div>
  <div style="text-align:center;margin:20px">
    <?php echo $text_copyright; ?>
  </div>
</div>

<?php echo $footer; ?>
