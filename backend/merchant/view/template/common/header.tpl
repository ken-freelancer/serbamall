<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
<meta charset="UTF-8" />
<title><?php echo $title; ?></title>
<base href="<?php echo $base; ?>" />
<?php /*
<?php if ($description) { ?>
<meta name="description" content="<?php echo $description; ?>" />
<?php } ?>
<?php if ($keywords) { ?>
<meta name="keywords" content="<?php echo $keywords; ?>" />
<?php } ?>
<?php foreach ($links as $link) { ?>
<link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
<?php } ?>
<link rel="stylesheet" type="text/css" href="view/stylesheet/stylesheet.css" />
<?php foreach ($styles as $style) { ?>
<link rel="<?php echo $style['rel']; ?>" type="text/css" href="<?php echo $style['href']; ?>" media="<?php echo $style['media']; ?>" />
<?php } ?>
<script type="text/javascript" src="view/javascript/jquery/jquery-1.7.1.min.js"></script>
<script type="text/javascript" src="view/javascript/jquery/ui/jquery-ui-1.8.16.custom.min.js"></script>
<link type="text/css" href="view/javascript/jquery/ui/themes/ui-lightness/jquery-ui-1.8.16.custom.css" rel="stylesheet" />
<script type="text/javascript" src="view/javascript/jquery/tabs.js"></script>
<script type="text/javascript" src="view/javascript/jquery/superfish/js/superfish.js"></script>
*/?>


<link href="view/yobiclick/stylesheets/plugins/tabdrop/tabdrop.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/jgrowl/jquery.jgrowl.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/select2/select2.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.min.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/bootstrap_switch/bootstrap-switch.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/common/bootstrap-wysihtml5.css" media="all" rel="stylesheet" type="text/css" />
<link href="view/yobiclick/stylesheets/plugins/slider_nav/slidernav.css" media="screen" rel="stylesheet" type="text/css" />

<!-- / bootstrap [required] -->
<link href="view/yobiclick/stylesheets/bootstrap/bootstrap.css" media="all" rel="stylesheet" type="text/css" />
<!-- / theme file [required] -->
<link href="view/yobiclick/stylesheets/light-theme.css" media="all" id="color-settings-body-color" rel="stylesheet" type="text/css" />
<!-- / coloring file [optional] (if you are going to use custom contrast color) -->
<link href="view/yobiclick/stylesheets/theme-colors.css" media="all" rel="stylesheet" type="text/css" />
<!-- / custom file -->
<link href="view/yobiclick/stylesheets/custom.css" media="all" rel="stylesheet" type="text/css" />
<!--[if lt IE 9]>
  <script src="view/yobiclick/javascripts/ie/html5shiv.js" type="text/javascript"></script>
  <script src="view/yobiclick/javascripts/ie/respond.min.js" type="text/javascript"></script>
<![endif]-->

<!-- / jquery [required] -->
<script src="view/yobiclick/javascripts/jquery/jquery.min.js" type="text/javascript"></script>
<!-- / jquery mobile (for touch events) -->
<script src="view/yobiclick/javascripts/jquery/jquery.mobile.custom.min.js" type="text/javascript"></script>
<!-- / jquery migrate (for compatibility with new jquery) [required] -->
<script src="view/yobiclick/javascripts/jquery/jquery-migrate.min.js" type="text/javascript"></script>
<!-- / bootstrap [required] -->
<script src="view/yobiclick/javascripts/bootstrap/bootstrap.js" type="text/javascript"></script>
<!-- / jquery ui -->
<script src="view/yobiclick/javascripts/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.js" type="text/javascript"></script>
<link href="view/yobiclick/javascripts/jquery/jquery-ui-1.11.4.custom/jquery-ui.min.css" media="all" rel="stylesheet" type="text/css" />
<!-- / modernizr -->
<script src="view/yobiclick/javascripts/plugins/modernizr/modernizr.min.js" type="text/javascript"></script>
<!-- / retina -->
<script src="view/yobiclick/javascripts/plugins/retina/retina.js" type="text/javascript"></script>
    <!-- / select2 -->
    <script src="view/yobiclick/javascripts/plugins/select2/select2.js" type="text/javascript"></script>

    <script type="text/javascript">
        //-----------------------------------------
        // Confirm Actions (delete, uninstall)
        //-----------------------------------------
        $(document).ready(function(){
            // Confirm Delete
            $('#form').submit(function(){
                if ($(this).attr('action').indexOf('delete',1) != -1) {
                    if (!confirm('<?php echo $text_confirm; ?>')) {
                        return false;
                    }
                }
            });
            // Confirm Uninstall
            $('a').click(function(){
                if ($(this).attr('href') != null && $(this).attr('href').indexOf('uninstall', 1) != -1) {
                    if (!confirm('<?php echo $text_confirm; ?>')) {
                        return false;
                    }
                }
            });
        });
    </script>
</head>
<body class='contrast-blue <?php if (!$logged) { echo 'login'; } ?> contrast-background'>
<?php if($logged){ ?>
<header>
  <nav class='navbar navbar-default'>
	<a class='navbar-brand' href='index.html' style="">
	  <img class="logo" alt="logo" style="width:120px" src="view/yobiclick/images/store_logo.png" />
	</a>
	<a class='toggle-nav btn pull-left' href='#'>
	  <i class='icon-reorder'></i>
	</a>
	<ul class='nav'>	  
	  <li class='dropdown dark user-menu'>
		<a class='dropdown-toggle' data-toggle='dropdown' href='#'>
		  <span class='user-name'><?php echo $logged; ?></span>
		  <b class='caret'></b>
		</a>
		<ul class='dropdown-menu'>
		  <li>
			<a href='<?php echo $update_vendor_profile; ?>'>
			  <i class='icon-user'></i>
			  Profil
			</a>
		  </li>
		  <li class='divider'></li>
		  <li>
			<a href='<?php echo $logout; ?>'>
			  <i class='icon-signout'></i>
			  Keluar
			</a>
		  </li>
		</ul>
	  </li>
	</ul>
  </nav>
</header>

 <div id='wrapper'>
	<div id='main-nav-bg'></div>
	<nav id='main-nav'>
		<div class='navigation'>
			<ul class='nav nav-stacked'>
				<li class=''>
				  <a href='<?php echo $home; ?>'>
					<i class='icon-dashboard'></i>
					<span>Papan Dashboard</span>
				  </a>
				</li>
				
				<li class=''>
					<a class="dropdown-collapse" href="<?php echo $pro2ven; ?>"><i class='icon-table'></i>
						<span>Produk</span>
						<i class='icon-angle-down angle-down'></i>
					</a>

					<ul class='nav nav-stacked'>
						<li class=''>
							<a href='<?php echo $pro2ven; ?>'>
								<i class='icon-caret-right'></i>
								<span>Daftar Produk</span>
							</a>
						</li>
						<li class=''>
							<a href='<?php echo $this->url->link('catalog/pro2ven/insert', 'token=' . $this->session->data['token'], 'SSL'); ?>'>
							<i class='icon-caret-right'></i>
							<span>Tambah Produk</span>
						</a>
						</li>
					</ul>
				</li>
				
				<li>
					<a class='dropdown-collapse ' href='<?php echo $ven_sale_order; ?>'>
						<i class='icon-inbox'></i>
						<span>Penjualan & Pemesanan</span>
						<i class='icon-angle-down angle-down'></i>
					</a>
					
					<ul class='nav nav-stacked'>
						<li class=''>
							<a href='<?php echo $ven_sale_order; ?>'>
								<i class='icon-caret-right'></i>
								<span>Pesanan</span>
							</a>
						</li>
						<li class=''>
							<a href='<?php echo $ven_transaction; ?>'>
								<i class='icon-caret-right'></i>
								<span>Penjualan</span>
							</a>
						</li>
					</ul>
				</li>
				
				<li class=''>
					<a class='dropdown-collapse ' href='<?php echo $vendor_product_viewed; ?>'>
						<i class='icon-bar-chart'></i>
						<span>Laporan Produk</span>
						<i class='icon-angle-down angle-down'></i>
					</a>
					
					<ul class='nav nav-stacked'>
						<li class=''>
							<a href='<?php echo $vendor_product_viewed; ?>'>
								<i class='icon-caret-right'></i>
								<span>Produk yang Dilihat</span>
							</a>
						</li>
						<li class=''>
							<a href='<?php echo $vendor_product_purchased; ?>'>
								<i class='icon-caret-right'></i>
								<span>Produk yang Dibeli</span>
							</a>
						</li>
					</ul>
				</li>

                <li>
                    <a href='<?php echo $ads_link; ?>'>
                        <i class='icon-eye-open'></i>
                        <span>Advertising</span>
                    </a>
                </li>

                <li>
                    <a href='<?php echo $review_link; ?>'>
                        <i class='icon-leaf'></i>
                        <span>Tinjauan</span>
                    </a>
                </li>
                <li>
                    <a href='<?php echo $return_link; ?>'>
                        <i class='icon-random'></i>
                        <span>Pengembalian</span>
                    </a>
                </li>
                <li>
					<a href='<?php echo $shipping_link; ?>'>
						<i class='icon-truck'></i>
						<span>Pengiriman</span>
					</a>
				</li>
				<li>
					<a href='<?php echo $update_vendor_profile; ?>'>
						<i class='icon-gear'></i>
						<span>Profil & Pengiriman</span>
					</a>
				</li>
				<!--Vincent-26/11/2015-->
			    <li>
			     	<a class="dropdown-collapse">
			     		<i class='icon-file'></i>
			      		<span>Tutorial</span>
			      		<i class='icon-angle-down angle-down'></i>
			     	</a>

			     <ul class='nav nav-stacked'>
			      	<li class=''>
			       		<a href='<?php echo $add_product; ?>'>
			        	<i class='icon-caret-right'></i>
			        <span>Tambah Produk</span>
			       </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $edit_product; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Edit Produk</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $remove_product; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Hapus Produk</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $check_order; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Cek Dan Ubah Pesanan</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $check_payment; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Cek Pembayaran</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $product_view; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Produk yang Dilihat</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $product_purchase; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Produk yang Dibeli</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $product_review; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Cek Produk Tinjauan</span>
			      </a>
			      </li>
			      <li class=''>
			       <a href='<?php echo $profile_setting; ?>'>
			       <i class='icon-caret-right'></i>
			       <span>Pengaturan Profil</span>
			      </a>
			      </li>
			     </ul>
			    </li>
		</div>
	</nav>
</div>

<section id="content">
	<div class="container">

<?php } ?>
