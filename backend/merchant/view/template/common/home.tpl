<?php echo $header; ?>

		<div id="content-wrapper" class="row">
			<div class="col-xs-12">
				
				<div class='page-header page-header-with-buttons'>
					<h1 class='pull-left'>
					  <i class='icon-dashboard'></i>
					  <span>Papan Dashboard</span>
					</h1>
				</div>
				
				<?php if ($error_install || $error_image || $error_image_cache || $error_cache || $error_download || $error_logs) { ?>
				<div class='alert alert-warning alert-dismissable'>
					<a class='close' data-dismiss='alert' href='#'>&times;</a>
					<?php echo (isset($error_install) ? $error_install : '' ).'<br>'; ?>
					<?php echo (isset($error_image) ? $error_image : '' ).'<br>'; ?>
					<?php echo (isset($error_image_cache) ? $error_image_cache : '' ).'<br>'; ?>
					<?php echo (isset($error_cache) ? $error_cache : '' ).'<br>'; ?>
					<?php echo (isset($error_download) ? $error_download : '' ).'<br>'; ?>
					<?php echo (isset($error_logs) ? $error_logs : '' ).'<br>'; ?>
				</div>
				<?php } ?>

				<div class='row box box-transparent'>
					<div class='col-xs-4 col-sm-2'>
						<div class='box-quick-link red-background'>
						<a href='<?php echo $this->url->link('catalog/pro2ven/insert', 'token=' . $this->session->data['token'], 'SSL'); ?>'>
						  <div class='header'>
							<div class='icon-table'></div>
						  </div>
						  <div class='content'>Tambah Produk</div>
						</a>
						</div>
					</div>
					<div class='col-xs-4 col-sm-2'>
						<div class='box-quick-link orange-background'>
						<a href='<?php echo $this->url->link('sale/vendor_order', 'token=' . $this->session->data['token'], 'SSL'); ?>'>
						  <div class='header'>
							<div class='icon-inbox'></div>
						  </div>
						  <div class='content'>Tampilkan Pesanan</div>
						</a>
						</div>
					</div>
				</div>
				
				<div class='row'>
					
					<div class="col-sm-12 col-md-6">
						<div class="box">
							<div class="box-header">
								<div class="title">
									<div class="icon-inbox"></div>
									Pesanan
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="box-content box-statistic">
										<h3 class="title text-error"><?php echo $total_sale; ?></h3>
										<small>Total Penjualan</small>
										<div class="text-error icon-inbox align-right"></div>
									</div>
									<div class="box-content box-statistic">
										<h3 class="title text-warning"><?php echo $total_sale_year; ?></h3>
										<small>Penjualan Tahunan</small>
										<div class="text-warning icon-check align-right"></div>
									</div>
									<div class="box-content box-statistic">
										<h3 class="title text-primary"><?php echo $total_product; ?></h3>
										<small>Total Produk</small>
										<div class="text-primary icon-ok  align-right"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-12 col-md-6">
						<div class="box">
							<div class="box-header">
								<div class="title">
									<div class="icon-inbox"></div>
									
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="box-content box-statistic">
										<h3 class="title text-info"><?php echo $total_order; ?></h3>
										<small>Total Pemesanan</small>
										<div class="text-info icon-time align-right"></div>									
									</div>								
									<div class="box-content box-statistic">
										<h3 class="title text-success"><?php echo $order_completed; ?></h3>
										<small>Barang Terkirim</small>
										<div class="text-success icon-flag align-right"></div>
									</div>
									<div class="box-content box-statistic">
										<h3 class="title text-muted"><?php echo $order_canceled; ?></h3>
										<small>Pesanan Batal</small>
										<div class="text-muted icon-remove align-right"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
				<div class='row'>
					<div class="col-sm-12 col-md-6">
						<div class='row recent-activity'>
							<div class='col-sm-12'>
								<div class="box">
									<div class="box-header">
										<div class="title">
											<i class='icon-refresh'></i>
											Pesanan Baru-Baru ini
										</div>
									</div>
									<div class='box-content box-no-padding'>
														  
										<ul class='list-unstyled users list-hover list-striped'>
											<li class="li_label">
												<div class='div_order_id pull-left'>
													ID
												</div>
												<div class='div_customer_name pull-left'>
													Pelanggan
												</div>
												<div class='div_status pull-left'>
													Status
												</div>
												<div class='div_total pull-left'>
													Total
												</div>
												<div class='date pull-left'>
													Tanggal
												</div>
												<div class='pull-right'>
													
												</div>
											</li>
										
											<?php if ($orders) { ?>
											  <?php foreach ($orders as $order) { ?>
												<li>
													<div class='div_order_id pull-left'>
														<?php echo $order['order_id']; ?>
													</div>
													<div class='div_customer_name pull-left text-contrast'>
														<?php echo $order['customer']; ?>
													</div>
													<div class='div_status pull-left'>
														<?php echo $order['status']; ?>
													</div>
													<div class='div_total pull-left'>
														<?php echo $order['total']; ?>
													</div>
													<small class='date pull-left text-muted'>
														<?php echo $order['date_added']; ?>
													</small>
													<div class='pull-right'>
														<?php foreach ($order['action'] as $action) { ?>
															[ <a href="<?php echo $action['href']; ?>"><?php echo $action['text']; ?></a> ]
														<?php } ?>
													</div>
												</li>
											  <?php } ?>
											<?php } else { ?>
												<li>
													Tidak Ada Hasil
												</li>
											<?php } ?>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					
					<div class="col-sm-12 col-md-6">
						<div class="box">
							<div class="box-header">
								<div class="title">
									<div class="icon-bar-chart"></div>
									Statistik
								</div>
    
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="statistic">
										<div class="range">Pilih Jangkauan:
										  <select id="range">
											<option value="day">Hari Ini</option>
											<option value="week">Minggu Ini</option>
											<option value="month">Bulan Ini</option>
											<option value="year">Tahun Ini</option>
										  </select>
										</div>
										<div id="chart-sale" style="width: 100%; height: 260px;"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
		
		
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.js"></script> 
<script type="text/javascript" src="view/javascript/jquery/flot/jquery.flot.resize.min.js"></script>
<script type="text/javascript"><!--
$('#range').on('change', function(e) {
	e.preventDefault();
	
	$(this).parent().parent().find('li').removeClass('active');
	
	$(this).parent().addClass('active');
	
	$.ajax({
		type: 'get',
		url: 'index.php?route=common/home/chart&token=<?php echo $token; ?>&range=' + $(this).val(),
		dataType: 'json',
		success: function(json) {
                        if (typeof json['order'] == 'undefined') { return false; }
			var option = {	
				shadowSize: 0,
				colors: ['#eac442', '#b3d7f8'],
				bars: { 
					show: true,
					fill: true,
					lineWidth: 1
				},
				grid: {
					backgroundColor: '#FFFFFF',
					hoverable: true
				},
				points: {
					show: false
				},
				xaxis: {
					show: true,
            		ticks: json['xaxis']
				}
			}
			
			$.plot('#chart-sale', [json['order'], json['customer']], option);	
					
			$('#chart-sale').bind('plothover', function(event, pos, item) {
				$('.tooltip').remove();
			  
				if (item) {
					$('<div id="tooltip" class="tooltip top in"><div class="tooltip-arrow"></div><div class="tooltip-inner">' + item.datapoint[1].toFixed(2) + '</div></div>').prependTo('body');
					
					$('#tooltip').css({
						position: 'absolute',
						left: item.pageX - ($('#tooltip').outerWidth() / 2),
						top: item.pageY - $('#tooltip').outerHeight(),
						pointer: 'cusror'
					}).fadeIn('slow');	
					
					$('#chart-sale').css('cursor', 'pointer');		
			  	} else {
					$('#chart-sale').css('cursor', 'auto');
				}
			});
		},
        error: function(xhr, ajaxOptions, thrownError) {
           alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
});

$('#range').trigger('change');
//--></script>
<?php echo $footer; ?>