<?php if($logged){ ?>		
		<footer id='footer'>
            <div class='footer-wrapper'>
              <div class='row'>
                <div class='col-sm-6 text'>
                  Copyright © <?php echo date('Y');?> PT Serba Mall Indonesia
                </div>
                <div class='col-sm-6 buttons'>
                  
                </div>
              </div>
            </div>
        </footer>
	</div>
</section>
<?php } ?>
<!-- / theme file [required] -->
<script src="view/yobiclick/javascripts/theme.js" type="text/javascript"></script>
<!-- / demo file [not required!] -->
<script src="view/yobiclick/javascripts/demo.js" type="text/javascript"></script>
<!-- / START - page related files and scripts [optional] -->
<script src="view/yobiclick/javascripts/plugins/validate/jquery.validate.min.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/validate/additional-methods.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/fileinput/bootstrap-fileinput.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/bootstrap_daterangepicker/bootstrap-daterangepicker.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/common/moment.min.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/bootstrap_datetimepicker/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="view/yobiclick/javascripts/plugins/slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<!-- / END - page related files and scripts [optional] -->
<script src="view/yobiclick/javascripts/default.js" type="text/javascript"></script>

</body></html>