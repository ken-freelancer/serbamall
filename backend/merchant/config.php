<?php
if ( file_exists( dirname( __FILE__ ) . '/config-local.php' ) ) {
    include( dirname( __FILE__ ) . '/config-local.php' );
}
else
{
    // GET project basepath for different development environment
    $basepath = str_replace('merchant', '', dirname(realpath(__FILE__)));
    // HTTP
    define('HTTP_SERVER', 'http://localhost/c2c/merchant/');
    define('HTTP_CATALOG', 'http://localhost/c2c/');


    // HTTPS
    define('HTTPS_SERVER', 'http://localhost/c2c/merchant/');
    define('HTTPS_CATALOG', 'http://localhost/c2c/');

    // SERBAPAY
    define('HTTPS_SERBAPAY', 'http://serbapay.app/');
    define('HTTPS_SERBAPAY_SHIPPED_CURL', HTTPS_SERBAPAY.'checkoutbox/order-status');

    //IMAGE PATH
    //THiS IS FOR LOAD THE IMAGE PATH IF IMAGE PATH SEPARATED
    define('IMAGE_PATH_SPLIT', true);
    define('CDN_ENABLE', true);
    define('HTTP_CDN_IMAGE', 'http://cdn1.serbamall.com/');
    define('HTTPS_CDN_IMAGE', 'https://cdn1.serbamall.com/');
    define('HTTP_IMAGE', 'http://image.serbamall.com/');
    define('HTTPS_IMAGE', 'https://image.serbamall.com/');


    define('REDIS_ENABLE', getenv('REDIS_ENABLE')?getenv('REDIS_ENABLE'):0);
    define('REDIS_HOST', getenv('REDIS_HOST')?getenv('REDIS_HOST'):'192.168.10.10');
    define('REDIS_PORT', getenv('REDIS_PORT')?getenv('REDIS_PORT'):'6379');
    define('REDIS_PORTOCAL', getenv('REDIS_PORTOCAL')?getenv('REDIS_PORTOCAL'):'tcp');

    //cache cron job
    define('CACHE_CRON_ENABLE', getenv('CACHE_CRON_ENABLE')?getenv('CACHE_CRON_ENABLE'):0);

    // DIR
    define('DIR_APPLICATION', $basepath . '/merchant/');
    define('DIR_SYSTEM', $basepath . '/system/');
    define('DIR_DATABASE', $basepath . '/system/database/');
    define('DIR_LANGUAGE', $basepath . '/merchant/language/');
    define('DIR_TEMPLATE', $basepath . '/merchant/view/template/');
    define('DIR_CONFIG', $basepath . '/system/config/');
    define('DIR_IMAGE', $basepath . '/image/');
    define('DIR_CACHE', $basepath . '/system/cache/');
    define('DIR_DOWNLOAD', $basepath . '/download/');
    define('DIR_LOGS', $basepath . '/system/logs/');
    define('DIR_CATALOG', $basepath . '/catalog/');

    // DB
    define('DB_DRIVER', 'mysqli');
    define('DB_HOSTNAME', 'localhost');
    define('DB_USERNAME', 'root');
    define('DB_PASSWORD', 'root');
    define('DB_DATABASE', 'c2c');
    define('DB_PREFIX', 'oc_');

}

//REQUIRED REDIS
require $basepath.'vendor/predis/predis/autoload.php';