<?php
// Heading
$_['heading_title']          = 'Print Voucher List'; 

// Text  
$_['text_success']           = 'Success: You have modified vouchers!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Default';
$_['text_image_manager']     = 'Image Manager';
$_['text_browse']            = 'Browse';
$_['text_clear']             = 'Clear';
$_['text_option']            = 'Option';
$_['text_option_value']      = 'Option Value';
$_['text_percent']           = 'Percentage';
$_['text_amount']            = 'Fixed Amount';

// Column
$_['column_voucher_no']      = 'Voucher No.';
$_['column_vendor']          = 'Vendor Name';
$_['column_product']         = 'Product Name';
$_['column_created_at']      = 'Date Added';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Entry
$_['entry_voucher_no']       = 'Voucher No.:';
$_['entry_security_code'] 	 = 'Security Code:';
$_['entry_ref_no'] 			 = 'REF No.:';
$_['entry_created_date']     = 'Created Date:';
$_['entry_used_date']        = 'Used Date:';
$_['entry_customer']         = 'Customer:';
$_['entry_updated_by']       = 'Updated By:';
$_['entry_product']          = 'Product Name:';
$_['entry_vendor']           = 'Vender Name:';
$_['entry_status']           = 'Status';

$_['txt_status_unused']    	= 'Un-Used';
$_['txt_status_used']   	= 'Used';
$_['txt_status_void']   	= 'Void';


$_['txt_voucher_no']      = 'Voucher No.';
$_['txt_security_code']   = 'Security Code';
$_['txt_vendor']          = 'Vendor Name';
$_['txt_product']         = 'Product Name';
$_['txt_created_at']      = 'Date Added';
$_['txt_updated_at']      = 'Date Updated';
$_['txt_status']          = 'Status';
$_['txt_action']          = 'Action';
$_['txt_customer_name']          = 'Customer Name';

$_['entry_recurring']        = 'Recurring billing:';
$_['entry_recurring_price']  = 'Recurring price:';
$_['entry_recurring_freq']   = 'Recurring frequency:';
$_['entry_recurring_cycle']  = 'Recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_recurring_length'] = 'Recurring length:<span class="help">0 = until cancelled</span>';
$_['entry_trial']            = 'Trial period:';
$_['entry_trial_price']      = 'Trial recurring price:';
$_['entry_trial_freq']       = 'Trial recurring frequency:';
$_['entry_trial_cycle']      = 'Trial recurring cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_trial_length']     = 'Trial recurring length:';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_status']           = 'Invalid Status';
$_['error_model']            = 'Product Model must be greater than 3 and less than 64 characters!';
?>