<?php
/**
 * 
 * LICENSE
 *
 * This source file is subject to the GNU General Public License, Version 3
 * It is also available through the world-wide-web at this URL:
 * http://www.gnu.org/copyleft/gpl.html
 * If you did not receive a copy of the license and are unable to
 * obtain it through the world-wide-web, please send an email
 *
 * @category   OpenCart
 * @package    Print Voucher
 * @copyright  Copyright (c) 2015 Ken Teo
 * @license    http://www.gnu.org/copyleft/gpl.html     GNU General Public License, Version 3
 */

// Heading
$_['heading_title']       = 'Print Voucher Settings';
$_['heading_style_title'] = '<span style="color:red;font-weight:bold;">Print Voucher</span>';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Print Voucher!';
$_['text_copyright']      = 'Pixelstail <a href="http://www.pixelstail.com" target="_blank">Pixelstail</a>';

// Entry
$_['entry_status']        = 'Status';
$_['entry_listing_per_page']   = 'Listing Per Page:';

// Button
$_['button_cancel']      = 'Back to Module';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Print Voucher!';
