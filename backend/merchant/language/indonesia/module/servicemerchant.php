<?php
// Heading
$_['heading_title']       = 'Service Merchant';

// Text
$_['text_module']         = 'Modules';
$_['text_success']        = 'Success: You have modified module Service Merchant!';
$_['text_content_top']    = 'Content Top';
$_['text_content_bottom'] = 'Content Bottom';
$_['text_column_left']    = 'Column Left';
$_['text_column_right']   = 'Column Right';
$_['text_yes']            = 'Yes';
$_['text_no']             = 'No';

// Entry
$_['entry_servicemerchant']        = 'Service Merchant Name:<br /><span class="help">Choose specific service Merchants to apply. Select no service Merchants to show all service Merchants logo.</span>';
$_['entry_servicemerchants_image'] = 'Information Image (W x H):<br /><span class="help">Image size for Service Merchant image at Service Merchant section</span>';
$_['entry_product_image'] = 'Product Image (W x H):<br /><span class="help">Image size for Service Merchant image at product page</span>';
$_['entry_infomation']    = 'Enable Service Merchant Service: ';
$_['entry_review']    	  = 'Service Merchant Reviews: <br /><span class="help"><b>Yes : </b>Display service Merchant rating<br/><b>No : </b> Hide service Merchant rating</span>';
$_['entry_limit']         = 'Display Limit (Carousel): <span class="help">Partition the logos at <br/>carousel mode';
$_['entry_logos']         = 'Limit Logos: <span class="help">Carousel - Limit the available logos<br/>Vertical - Display available logos</span>';
$_['entry_scroll']        = 'Scroll (Carousel): <span class="help">Logos display at <br/>single click</span>';
$_['entry_mode']          = 'Display Mode: <span class="help"><b>Yes : </b>Combo Box Display<br/> <b>No : </b>Logo Display</span>';
$_['entry_image']         = 'Image (W x H):';
$_['entry_layout']        = 'Layout:';
$_['entry_position']      = 'Position:';
$_['entry_status']        = 'Status:';
$_['entry_sort_order']    = 'Sort Order:';

// Error
$_['error_permission']    = 'Warning: You do not have permission to modify module Service Merchant Logo!';
$_['error_image']         = 'Image width &amp; height dimensions required!';
?>