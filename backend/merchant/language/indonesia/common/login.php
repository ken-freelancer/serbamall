<?php
// header
$_['heading_title']  = 'Merchant';

// Text
$_['text_heading']   = 'Merchant';
$_['text_login']     = 'Please enter your login details.';
$_['text_forgotten'] = 'Forgotten Password';

// Entry
$_['entry_username'] = 'Username:';
$_['entry_password'] = 'Password:';

// Button
$_['button_login']   = 'Login';

// Error
$_['error_login']    = 'No match for Username and/or Password.';
			
			$_['error_expired']  = 'Account Expired. Please contact System Administrator.';
			
$_['error_token']    = 'Invalid token session. Please login again.';
?>
