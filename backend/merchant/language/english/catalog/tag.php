<?php
// Heading
$_['heading_title']     = 'Tags';

// Text
$_['text_success']      = 'Success: You have modified tags!';

// Column
$_['column_group']      = 'Tags Group';
$_['column_sort_order'] = 'Sort Order';
$_['column_action']     = 'Action';

// Entry
$_['entry_group']       = 'Tag Group Name:';
$_['entry_name']        = 'Tag Name:';
$_['entry_sort_order']  = 'Sort Order:';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify filters!';
$_['error_group']       = 'Tag Group Name must be between 1 and 64 characters!';
$_['error_name']        = 'Tag Name must be between 1 and 64 characters!';
?>