<?php
// Heading
$_['heading_title']      = 'Package';

// Text
$_['text_success']       = 'Success: You have modified Package!';
$_['text_fixed_rate']    = 'Fixed Rate';
$_['text_percentage']    = 'Percentage';
$_['text_pf']    		 = 'Percentage + Fixed Rate';
$_['text_fp']    		 = 'Fixed + Percentage Rate';
$_['text_month']    	 = 'Month';
$_['text_year']    		 = 'Year';
$_['text_subs_fee']    	 = 'Subscription Amount : ';

// Column
$_['column_name']			= 'Package Name';
$_['column_type']  			= 'Package Type';
$_['column_commission'] 	= 'Package Rate';
$_['column_total_vendors'] 	= 'Total Vendors';
$_['column_sort_order'] 	= 'Sort Order';
$_['column_action']     	= 'Action';

// Entry
$_['entry_name']		= 'Package Name:';
$_['entry_type']		= 'Package Type:';
$_['entry_duration']	= 'Subscription Duration:<br/><span class="help">This field apply to Month/Year subscription only</span>';
$_['entry_subscription']= 'Subscription Amount:<br/><span class="help">Subscription amount require to pay to store admin during sign up</span>';
$_['entry_commission']	= 'Package Rate:<br/><span class="help"><b>Percentage (P)%:</b> 1.00-100.00 <br/> <b>Fixed (F):</b> 1.00 <br/> <b> (P)% + (F) :</b> 10.50:1</span>';
$_['entry_limit']  		= 'Product Limit:';
$_['entry_sort_order']  = 'Sort Order:';

// Error
$_['error_permission']  			= 'Warning: You do not have permission to modify Package!';
$_['error_delete']     				= 'Warning: This Package cannot be deleted as it is currently assigned to %s vendors!';
$_['error_required_data']  			= 'Warning: Please fill in all required data!';
$_['error_commission_name']  		= 'Type name must be between 3 and 64 characters!';
$_['error_commission']   			= 'Package must be numbers!';

?>
