<?php

#run daily request
require_once('merchant/config.php');

#send request to cron JOB
$curl_handle = curl_init();
curl_setopt( $curl_handle, CURLOPT_URL, HTTPS_CATALOG.'index.php?route=common/home/cron&cron=Cron' );
curl_setopt( $curl_handle, CURLOPT_HEADER, 0);
curl_setopt( $curl_handle, CURLOPT_CONNECTTIMEOUT, 180 ); // 2 second timeout
curl_exec( $curl_handle ); // Execute the request
curl_close( $curl_handle );
echo "DONE!";
exit;