-- Create syntax for 'oc_tag'

CREATE TABLE `oc_tag` (
  `tag_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(255) NOT NULL DEFAULT '',
  `language_id` int(11) NOT NULL,
  `date_added` datetime NOT NULL,
  `tag_user_created` int(11) DEFAULT NULL,
  PRIMARY KEY (`tag_id`),
  UNIQUE KEY `tag_name` (`tag_name`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=utf8;
