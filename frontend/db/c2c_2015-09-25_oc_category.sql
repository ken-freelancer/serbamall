-- phpMyAdmin SQL Dump
-- version 4.2.9.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 25, 2015 at 11:02 AM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `c2c`
--

-- --------------------------------------------------------

--
-- Table structure for table `oc_category`
--

CREATE TABLE IF NOT EXISTS `oc_category` (
`category_id` int(11) NOT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `top` tinyint(1) NOT NULL,
  `column` int(3) NOT NULL,
  `sort_order` int(3) NOT NULL DEFAULT '0',
  `status` tinyint(1) NOT NULL,
  `date_added` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=MyISAM AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category`
--

INSERT INTO `oc_category` (`category_id`, `image`, `parent_id`, `top`, `column`, `sort_order`, `status`, `date_added`, `date_modified`) VALUES
(245, '', 150, 0, 1, 0, 1, '2015-09-25 03:07:03', '2015-09-25 03:07:03'),
(57, '', 0, 1, 1, 3, 1, '2011-04-26 08:53:16', '2015-04-04 12:41:20'),
(130, '', 0, 0, 1, 0, 1, '2015-08-08 23:24:33', '2015-09-23 11:28:39'),
(131, '', 130, 0, 1, 0, 1, '2015-08-08 23:26:13', '2015-09-23 11:31:25'),
(132, '', 130, 0, 1, 0, 1, '2015-08-08 23:27:22', '2015-09-23 11:31:13'),
(137, '', 0, 0, 1, 0, 1, '2015-08-10 01:24:32', '2015-08-10 01:24:32'),
(138, '', 0, 0, 1, 0, 1, '2015-08-10 14:11:36', '2015-08-10 14:11:36'),
(139, '', 130, 0, 1, 0, 1, '2015-08-10 15:05:14', '2015-08-10 15:05:14'),
(140, '', 130, 0, 1, 0, 1, '2015-08-10 15:05:43', '2015-08-10 15:05:43'),
(141, '', 0, 0, 1, 0, 1, '2015-08-17 14:26:10', '2015-08-17 14:26:10'),
(142, '', 141, 0, 1, 0, 1, '2015-08-17 14:26:34', '2015-08-17 14:26:34'),
(143, '', 141, 0, 1, 0, 1, '2015-08-17 14:27:05', '2015-08-17 14:27:05'),
(144, '', 141, 0, 1, 0, 1, '2015-08-17 14:27:24', '2015-08-17 14:27:24'),
(145, '', 0, 0, 1, 0, 1, '2015-08-25 15:42:46', '2015-08-25 15:42:46'),
(146, '', 145, 0, 1, 0, 1, '2015-08-25 15:43:10', '2015-08-25 15:43:10'),
(147, '', 145, 0, 1, 0, 1, '2015-08-25 15:43:26', '2015-08-25 15:44:14'),
(148, '', 145, 0, 1, 0, 1, '2015-08-25 15:43:55', '2015-08-25 15:43:55'),
(149, '', 145, 0, 1, 0, 1, '2015-08-25 15:44:37', '2015-08-25 15:44:37'),
(150, '', 0, 0, 1, 0, 1, '2015-08-26 11:22:07', '2015-08-26 11:22:07'),
(151, '', 150, 0, 1, 0, 1, '2015-08-26 11:23:57', '2015-08-26 11:23:57'),
(152, '', 150, 0, 1, 0, 1, '2015-08-26 11:26:43', '2015-08-26 11:26:43'),
(154, '', 150, 0, 1, 0, 1, '2015-08-26 11:27:14', '2015-08-26 11:27:14'),
(155, '', 141, 0, 1, 0, 1, '2015-08-26 15:44:22', '2015-08-26 15:44:22'),
(156, '', 141, 0, 1, 0, 1, '2015-08-26 15:48:44', '2015-08-26 15:48:44'),
(157, '', 141, 0, 1, 0, 1, '2015-08-26 15:49:34', '2015-08-26 15:49:34');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_description`
--

CREATE TABLE IF NOT EXISTS `oc_category_description` (
  `category_id` int(11) NOT NULL,
  `language_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `meta_description` varchar(255) NOT NULL,
  `meta_keyword` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_description`
--

INSERT INTO `oc_category_description` (`category_id`, `language_id`, `name`, `description`, `meta_description`, `meta_keyword`) VALUES
(245, 1, 'Movies &amp; Series', '', '', ''),
(150, 5, 'Entertainment', '', '', ''),
(151, 5, 'Games &amp; Toys', '', '', ''),
(152, 5, 'Music &amp; Songs', '', '', ''),
(154, 5, 'Books', '', '', ''),
(155, 5, 'Home &amp; Dining', '', '', ''),
(156, 5, 'Sports &amp; Fitness', '', '', ''),
(157, 5, 'Travel &amp; Luggage', '', '', ''),
(149, 5, 'IT Accessory', '', '', ''),
(148, 5, 'Photography', '', '', ''),
(147, 5, 'PC &amp; Laptop', '', '', ''),
(57, 1, 'Others', '', '', ''),
(146, 5, 'Mobile &amp; Tablet', '', '', ''),
(144, 5, 'Health &amp; Beauty', '', '', ''),
(143, 5, 'Appliances', '', '', ''),
(142, 5, 'Home &amp; Living', '', '', ''),
(141, 5, 'Home &amp; Lifestyles', '', '', ''),
(140, 5, 'Watches &amp; Jewerly', '', '', ''),
(139, 5, 'Baby &amp; Kids', '', '', ''),
(138, 5, 'Best Offer', '', '', ''),
(137, 5, 'Latest Added', '', '', ''),
(132, 5, 'For Her', '', '', ''),
(131, 5, 'For Him', '', '', ''),
(130, 5, 'Clothing &amp; Accessories', '', '', ''),
(57, 5, 'Others', '', '', ''),
(245, 5, 'Movies &amp; Series', '', '', ''),
(130, 1, 'Clothing &amp; Accessories', '', '', ''),
(131, 1, 'For Him', '', '', ''),
(132, 1, 'For Her', '', '', ''),
(137, 1, 'Latest Added', '', '', ''),
(138, 1, 'Best Offer', '', '', ''),
(139, 1, 'Baby &amp; Kids', '', '', ''),
(140, 1, 'Watches &amp; Jewerly', '', '', ''),
(141, 1, 'Home &amp; Lifestyles', '', '', ''),
(142, 1, 'Home &amp; Living', '', '', ''),
(143, 1, 'Appliances', '', '', ''),
(144, 1, 'Health &amp; Beauty', '', '', ''),
(145, 1, 'IT &amp; Gadgets', '', '', ''),
(146, 1, 'Mobile &amp; Tablet', '', '', ''),
(147, 1, 'PC &amp; Laptop', '', '', ''),
(148, 1, 'Photography', '', '', ''),
(149, 1, 'IT Accessory', '', '', ''),
(150, 1, 'Entertainment', '', '', ''),
(151, 1, 'Games &amp; Toys', '', '', ''),
(152, 1, 'Music &amp; Songs', '', '', ''),
(145, 5, 'IT &amp; Gadgets', '', '', ''),
(154, 1, 'Books', '', '', ''),
(155, 1, 'Home &amp; Dining', '', '', ''),
(156, 1, 'Sports &amp; Fitness', '', '', ''),
(157, 1, 'Travel &amp; Luggage', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_path`
--

CREATE TABLE IF NOT EXISTS `oc_category_path` (
  `category_id` int(11) NOT NULL,
  `path_id` int(11) NOT NULL,
  `level` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `oc_category_path`
--

INSERT INTO `oc_category_path` (`category_id`, `path_id`, `level`) VALUES
(57, 57, 0),
(245, 245, 1),
(245, 150, 0),
(130, 130, 0),
(131, 130, 0),
(131, 131, 1),
(132, 130, 0),
(132, 132, 1),
(137, 137, 0),
(138, 138, 0),
(139, 130, 0),
(139, 139, 1),
(140, 130, 0),
(140, 140, 1),
(141, 141, 0),
(142, 141, 0),
(142, 142, 1),
(143, 141, 0),
(143, 143, 1),
(144, 141, 0),
(144, 144, 1),
(145, 145, 0),
(146, 145, 0),
(146, 146, 1),
(147, 147, 1),
(148, 145, 0),
(148, 148, 1),
(147, 145, 0),
(149, 145, 0),
(149, 149, 1),
(150, 150, 0),
(151, 150, 0),
(151, 151, 1),
(152, 150, 0),
(152, 152, 1),
(154, 150, 0),
(154, 154, 1),
(155, 141, 0),
(155, 155, 1),
(156, 141, 0),
(156, 156, 1),
(157, 141, 0),
(157, 157, 1);

-- --------------------------------------------------------

--
-- Table structure for table `oc_category_to_layout`
--

CREATE TABLE IF NOT EXISTS `oc_category_to_layout` (
  `category_id` int(11) NOT NULL,
  `store_id` int(11) NOT NULL,
  `layout_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `oc_category`
--
ALTER TABLE `oc_category`
 ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `oc_category_description`
--
ALTER TABLE `oc_category_description`
 ADD PRIMARY KEY (`category_id`,`language_id`), ADD KEY `name` (`name`);

--
-- Indexes for table `oc_category_path`
--
ALTER TABLE `oc_category_path`
 ADD PRIMARY KEY (`category_id`,`path_id`);

--
-- Indexes for table `oc_category_to_layout`
--
ALTER TABLE `oc_category_to_layout`
 ADD PRIMARY KEY (`category_id`,`store_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `oc_category`
--
ALTER TABLE `oc_category`
MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=246;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
