<?php
require_once(DIR_SYSTEM . 'journal2/lib/Mobile_Detect.php');

class Journal2Cache {

    /* set cache duration in seconds (3600 means one hour) */
    const EXPIRE = 3600;

    // set last update timestamp, Ken use for get redis timestamp
    public static $redis_timestamp;
    public static $redis_cache_update = false;

    public static $path;
    public static $dir = 'journal-cache/';
    public static $mobile_detect;
    public static $device = 'desktop';
    public static $page_cache_file = null;

    private $skin_id = '1';
    private $store_id = '1';
    private $language_id = '1';
    private $currency_id = '1';
    private $price = '';
    private $logged_in = '0';
    private $developer_mode = '1';
    private $hostname = '';

    private $request;

    //redis
    private static $redis;

    public function __construct($registry) {
        $config = $registry->get('config');
        $currency = $registry->get('currency');
        $this->request = $registry->get('request');

        $this->store_id = $config->get('config_store_id');
        $this->language_id = $config->get('config_language_id');
        $this->currency_id = $currency->getCode();

        if (self::$mobile_detect->isMobile()) {
            if (self::$mobile_detect->isTablet()) {
                self::$device = 'tablet';
            } else {
                self::$device = 'mobile';
            }
        }

        $this->logged_in = $registry->get('customer')->isLogged() ? 1 : 0;
        $this->price = ($config->get('config_customer_price') && $registry->get('customer')->isLogged()) || !$config->get('config_customer_price') ? 'price' : 'noprice';

        $this->hostname = md5(Journal2Utils::getHostName());
        if(REDIS_ENABLE)
        {
            Predis\Autoloader::register();
            self::$redis = new Predis\Client([
                'scheme' => REDIS_PORTOCAL,
                'host'   => REDIS_HOST,
                'port'   => REDIS_PORT,
            ]);
        }
    }

    public function getLanguageId() {
        return $this->language_id;
    }

    public function getJournalAssetsFileName($extension) {
        return "journal-skin-{$this->skin_id}-" . self::$device ."-{$this->language_id}-" . JOURNAL_VERSION . ".{$extension}";
    }

    public function setDeveloperMode($mode) {
        $this->developer_mode = self::canCache() ? $mode : false;
    }

    public function getDeveloperMode() {
        if (self::canCache()) {
            if ($this->developer_mode) {
                self::createCacheFiles();
            }
            return $this->developer_mode;
        }
        return true;
    }

    public function setSkinId($skin_id) {
        $this->skin_id = $skin_id;
    }

    public function get($property) {
        if ($this->developer_mode) {
            return null;
        }
        $file_name = $this->getFileName($property);

        if(REDIS_ENABLE)
        {
            try {
                return self::$redis->get($file_name);
            }
            catch (Exception $exception) {
                return null;
            }
        }
        else
        {
            if (file_exists($file_name))
            {
                return file_get_contents($file_name);
            }
        }
        return null;
    }

    public function set($property, $value) {
        if (!$this->developer_mode && self::canCache()) {

            if(REDIS_ENABLE)
            {
                $timestamp = '';
                if(CACHE_CRON_ENABLE)
                {
                    //get lastest timestamp
                    $timestamp = self::$redis->get('latest_timestamp');
                    if(self::$redis_cache_update)
                    {
                        $timestamp = $this->getRedisTimestamp();
                    }


                }
                //change redis prefix to c2c/
                $file_name = 'c2c/' . $timestamp . "_j2_{$property}_sk{$this->skin_id}_s{$this->store_id}_l{$this->language_id}_c{$this->currency_id}_u{$this->logged_in}_" . self::$device . "_{$this->price}_" . JOURNAL_VERSION . "_{$this->hostname}.cache.html";

                try {
                    //no expired date, but we will not use it
                    //self::$redis->set($file_name, $value);
                    //every 15 minutes will generate a new cache
                    self::$redis->setex($file_name, self::EXPIRE, $value);
                }
                catch (Exception $exception) {
                    echo 'nocache';
                }
            }
            else
            {
                $file_name = self::getCachePath() . (time() + self::EXPIRE) . "_j2_{$property}_sk{$this->skin_id}_s{$this->store_id}_l{$this->language_id}_c{$this->currency_id}_u{$this->logged_in}_" . self::$device . "_{$this->price}_" . JOURNAL_VERSION . "_{$this->hostname}.cache.html";
                file_put_contents($file_name, $value, LOCK_EX);
            }
        }
        return $value;
    }

    private function getFileName($property) {

        if(REDIS_ENABLE)
        {
            $file_name = 'c2c/' . "_j2_{$property}_sk{$this->skin_id}_s{$this->store_id}_l{$this->language_id}_c{$this->currency_id}_u{$this->logged_in}_" . self::$device ."_{$this->price}_" . JOURNAL_VERSION . "_{$this->hostname}.cache.html";

            if(CACHE_CRON_ENABLE)
            {
                //get lastest timestamp
                $timestamp = self::$redis->get('latest_timestamp');
                if(self::$redis_cache_update)
                {
                    $timestamp = self::$redis_timestamp;
                }
                $file_name = 'c2c/' . $timestamp . "_j2_{$property}_sk{$this->skin_id}_s{$this->store_id}_l{$this->language_id}_c{$this->currency_id}_u{$this->logged_in}_" . self::$device ."_{$this->price}_" . JOURNAL_VERSION . "_{$this->hostname}.cache.html";
            }
            return $file_name;
            /*try {
                //get the cache key pattern
                $pattern = self::$redis->keys($file_name);
                if(count($pattern))
                {
                    foreach($pattern as $key)
                    {
                        $time = (int) substr(str_replace(self::getCachePath(), '', $key), 0, 10);
                        if (time() > $time)
                        {
                            //echo 'null';echo "<br />";
                            return null;
                        }
                        //echo $key;echo "<br />";

                        return $key;
                    }
                }
                return null;

            }
            catch (Exception $exception) {
                return null;
            }*/
        }
        else
        {
            $file_name = self::getCachePath() . "*_j2_{$property}_sk{$this->skin_id}_s{$this->store_id}_l{$this->language_id}_c{$this->currency_id}_u{$this->logged_in}_" . self::$device ."_{$this->price}_" . JOURNAL_VERSION . "_{$this->hostname}.cache.html";
            $files = glob($file_name);

            if (!$files)
            {
                return null;
            }

            $time = (int) substr(str_replace(self::getCachePath(), '', $files[0]), 0, 10);

            if (time() > $time)
            {
                foreach ($files as $file)
                {
                    if (file_exists($file))
                    {
                        unlink($file);
                    }
                }

                return null;
            }
            return $files[0];
        }
    }

    public static function getCachePath() {
        return self::$path . DIRECTORY_SEPARATOR . self::$dir;
    }

    public static function getCacheDir() {
        return self::$dir;
    }

    public static function canCache() {
        if(REDIS_ENABLE)
        {
            try {
                // explicitly call Predis\Client::connect()
                self::$redis->connect();
                return true;
            }
            catch (Predis\CommunicationException $exception) {
                // do stuff to handle the fact that you weren't able to connect to the server
                // here I'm using exit() just as a quick example...
                return false;
            }

        }
        else
        {
            $cache_path = self::getCachePath();

            /* if folder exists and it's writable */
            if (file_exists($cache_path) && is_writable($cache_path))
            {
                return true;
            }

            /* try to create folder */
            if (is_writable(self::$path) && @mkdir($cache_path))
            {
                return true;
            }
        }
        /* can't use cache */
        return false;
    }

    public static function createCacheFiles() {
        $path = self::getCachePath();
        if (!file_exists($path . '.htaccess')) {
            file_put_contents($path . '.htaccess', file_get_contents(DIR_SYSTEM . 'journal2/data/cache.htaccess'));
        }
        if (!file_exists($path . 'empty.css')) {
            file_put_contents($path . 'empty.css', '');
        }
        if (!file_exists($path . 'empty.js')) {
            file_put_contents($path . 'empty.js', '');
        }
    }

    public static function deleteCache($pattern = '*') {
        $path = self::getCachePath();
        if(REDIS_ENABLE)
        {
            $path = 'c2c/';
            try
            {
                self::$redis->eval("for i, name in ipairs(redis.call('KEYS', '" . $path . $pattern . "')) do redis.call('DEL', name); end", 0);
            }
            catch (Exception $e) {
                //Log($e->getMessage());

            }
        }
        else
        {
            if (!$path) return;
            $files = glob($path . $pattern);
            if ($files)
            {
                foreach ($files as $file)
                {
                    if (file_exists($file))
                    {
                        unlink($file);
                    }
                }
            }
        }
    }

    public static function deleteModuleCache($pattern) {
        if(REDIS_ENABLE)
        {
            self::deleteCache("_j2_{$pattern}*.cache.html");
            self::deleteSettingsCache();
        }
        else
        {
            self::deleteCache("*_j2_{$pattern}*.cache.html");
            self::deleteSettingsCache();
        }
    }

    public static function deleteSettingsCache() {
        self::deleteCache('journal-*.css');
        self::deleteCache('journal-*.js');
        self::deleteCache('_*.css');
        self::deleteCache('_*.js');
    }

    public function getRouteCacheKey() {
        $result = array();
        if (isset($this->request->get['path'])) {
            $result[] = 'c' . $this->request->get['path'];
        }
        if (isset($this->request->get['product_id'])) {
            $result[] = 'p' . $this->request->get['product_id'];
        }
        return implode('_', $result);
    }

    public static function getCurrentUrl() {
        $url = isset($_SERVER['HTTPS']) && (($_SERVER['HTTPS'] == 'on') || ($_SERVER['HTTPS'] == '1')) ? "https://" : "http://";
        $url .= $_SERVER["HTTP_HOST"];
        $url .= $_SERVER["REQUEST_URI"];
        return $url;
    }

    private static function canPageCache() {
        if (!self::canCache()) {
            return false;
        }
        if (isset($_SESSION['customer_id']) || isset($_SESSION['user_id'])) {
            return false;
        }
        $session_keys = array('cart', 'wishlist', 'compare');
        foreach ($session_keys as $session_key) {
            if (isset($_SESSION[$session_key]) && is_array($_SESSION[$session_key]) && count($_SESSION[$session_key]) > 0) {
                return false;
            }
        }
        return true;
    }

    public static function enablePageCache($language = 'en', $currency = 'usd') {
        if (!session_id()) {
            session_start();
        }
        if (self::canPageCache()) {
            self::$page_cache_file = Journal2Cache::$path . '/' . Journal2Cache::$dir . implode('_', array(
                md5(self::getCurrentUrl()),
                $language,
                $currency,
                self::$device
            ));

            if(REDIS_ENABLE)
            {
                try
                {
                    if(self::$redis->exists(self::$page_cache_file))
                        echo self::$redis->get(self::$page_cache_file);
                }
                catch (Exception $e) {
                    //echo($e->getMessage());
                }
            }
            else
            {
                if (file_exists(self::$page_cache_file))
                {
                    echo file_get_contents(self::$page_cache_file);
                    exit();
                }
            }
        }
    }

    public static function setRedisTimestamp($timestamp = null)
    {
        self::$redis_timestamp = $timestamp;
        /*if(!is_null($timestamp))
        else
            self::$redis_timestamp = time();*/
    }

    public static function getRedisTimestamp()
    {
        return self::$redis_timestamp;
    }

    public static function setRedisCacheUpdate($value = null)
    {
        self::$redis_cache_update = $value;
    }

    public static function getRedisCacheUpdate()
    {
        return self::$redis_cache_update;
    }

}

Journal2Cache::$path = realpath(DIR_SYSTEM . '../');
Journal2Cache::$mobile_detect = new Mobile_Detect();
