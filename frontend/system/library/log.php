<?php

use Aws\S3\S3Client;

class Log {
	private $filename;
	
	public function __construct($filename) {
		$this->filename = $filename;
	}

    public function write($message){

        if(LOG_ON_S3)
        {
            $client = S3Client::factory(array(
                'credentials' => array(
                    'key'    => S3_KEY,
                    'secret' => S3_SECRET,
                ),
                'region'      => S3_REGION,
            ));

            try
            {
                //get the log file
                $result = $client->getObject(array(
                    'Bucket' => S3_BUCKET,
                    'Key'    => $this->filename
                ));
                $file_message = $result['Body'];

                $result = $client->putObject(array(
                    'Bucket' => S3_BUCKET,
                    'Key'    => $this->filename,
                    'Body'   => $file_message . "\n" . $message
                ));

            } catch (Exception $e)
            {
                //echo 'Oops, something went wrong '.$e->getMessage();
                //if file not exist create a new file
                $result = $client->putObject(array(
                    'Bucket' => S3_BUCKET,
                    'Key'    => $this->filename,
                    'Body'   => $message
                ));
            }
        }else{
            $file = DIR_LOGS . $this->filename;

            $handle = fopen($file, 'a+');

            fwrite($handle, date('Y-m-d G:i:s') . ' - ' . print_r($message, true)  . "\n");

            fclose($handle);
        }
    }
}