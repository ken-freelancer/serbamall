<?php

use Aws\S3\S3Client;

/**
 * Debugging and Logging manager
 */
class Serbapay_Logger
{
	function __construct()
	{
		// if debug mode is set to true, then check for the writable log file
		if ( Serbapay_Auth::$config["debug_mode"] ){
            //if only not s3 can check
            if(!LOG_ON_S3)
            {
                if (!file_exists(Serbapay_Auth::$config["debug_file"]))
                {
                    throw new Exception("'debug_mode' is set to 'true', but the file " . Serbapay_Auth::$config['debug_file'] . " in 'debug_file' does not exit.", 1);
                }

                if (!is_writable(Serbapay_Auth::$config["debug_file"]))
                {
                    throw new Exception("'debug_mode' is set to 'true', but the given log file path 'debug_file' is not a writable file.", 1);
                }
            }
		} 
	}

	public static function debug( $message, $object = NULL )
	{
		if( Serbapay_Auth::$config["debug_mode"] ){
			$datetime = new DateTime();
			$datetime =  $datetime->format(DATE_ATOM);

            if(LOG_ON_S3)
            {
                $client = S3Client::factory(array(
                    'credentials' => array(
                        'key'    => S3_KEY,
                        'secret' => S3_SECRET,
                    ),
                    'region'    => S3_REGION,
                ));

                try
                {
                    //get the log file
                    $result = $client->getObject(array(
                        'Bucket' => S3_BUCKET,
                        'Key'    => 'serbapayauth.txt'
                    ));
                    $file_message = $result['Body'];

                    $result = $client->putObject(array(
                        'Bucket' => S3_BUCKET,
                        'Key'    => 'serbapayauth.txt',
                        'Body'   => $file_message."\n"."DEBUG -- " . $_SERVER['REMOTE_ADDR'] . " -- " . $datetime . " -- " . $message . " -- " . print_r($object, true) . "\n",
                    ));

                }catch (Exception $e) {
                    //echo 'Oops, something went wrong '.$e->getMessage();
                    //if file not exist create a new file
                    $result = $client->putObject(array(
                        'Bucket' => S3_BUCKET,
                        'Key'    => 'serbapayauth.txt',
                        'Body'   => "DEBUG -- " . $_SERVER['REMOTE_ADDR'] . " -- " . $datetime . " -- " . $message . " -- " . print_r($object, true) . "\n",
                    ));
                }
            }
            else
            {
                file_put_contents(
                    Serbapay_Auth::$config["debug_file"],
                    "DEBUG -- " . $_SERVER['REMOTE_ADDR'] . " -- " . $datetime . " -- " . $message . " -- " . print_r($object, true) . "\n",
                    FILE_APPEND
                );
            }
		}
	}

	public static function info( $message )
	{ 
		if( Serbapay_Auth::$config["debug_mode"] ){
			$datetime = new DateTime();
			$datetime =  $datetime->format(DATE_ATOM);

			file_put_contents( 
				Serbapay_Auth::$config["debug_file"], 
				"INFO -- " . $_SERVER['REMOTE_ADDR'] . " -- " . $datetime . " -- " . $message . "\n", 
				FILE_APPEND
			);
		}
	}

	public static function error($message, $object = NULL)
	{ 
		if( Serbapay_Auth::$config["debug_mode"] ){
			$datetime = new DateTime();
			$datetime =  $datetime->format(DATE_ATOM);

			file_put_contents( 
				Serbapay_Auth::$config["debug_file"], 
				"ERROR -- " . $_SERVER['REMOTE_ADDR'] . " -- " . $datetime . " -- " . $message . " -- " . print_r($object, true) . "\n", 
				FILE_APPEND
			);
		}
	}
}
