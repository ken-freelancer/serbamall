<?php

class Serbapay_Auth 
{
	public static $version = "1.0.0";

	public static $config  = array();

	public static $store   = NULL;

	public static $error   = NULL;

	public static $logger  = NULL;

	// --------------------------------------------------------------------

	/**
	* Try to start a new session of none then initialize Serbapay_Auth
	* 
	* Serbapay_Auth constructor will require either a valid config array or
	* a path for a configuration file as parameter. To know more please 
	* refer to the Configuration section:
	*/
	function __construct( $config )
	{ 
		Serbapay_Auth::initialize( $config ); 
	}

	// --------------------------------------------------------------------

	/**
	* Try to initialize Serbapay_Auth with given $config hash or file
	*/
	public static function initialize( $config )
	{
		if( ! is_array( $config ) && ! file_exists( $config ) ){
			throw new Exception( "Hybriauth config does not exist on the given path.", 1 );
		}

		if( ! is_array( $config ) ){
			$config = include $config;
		}

		// build some need'd paths
		$config["path_base"]        = realpath( dirname( __FILE__ ) )  . "/"; 
		$config["path_libraries"]   = $config["path_base"] . "thirdparty/";
		$config["path_resources"]   = $config["path_base"] . "resources/";
		$config["path_providers"]   = $config["path_base"] . "Providers/";

		// reset debug mode
		if( ! isset( $config["debug_mode"] ) ){
			$config["debug_mode"] = false;
			$config["debug_file"] = null;
		}

		# load serbapayauth required files, a autoload is on the way...
		require_once $config["path_base"] . "Error.php";
		require_once $config["path_base"] . "Logger.php";

		require_once $config["path_base"] . "Storage.php";

		require_once $config["path_base"] . "Provider_Adapter.php";

		require_once $config["path_base"] . "Provider_Model.php";
		require_once $config["path_base"] . "Provider_Model_OpenID.php";
		require_once $config["path_base"] . "Provider_Model_OAuth1.php";
		require_once $config["path_base"] . "Provider_Model_OAuth2.php";

		require_once $config["path_base"] . "User.php";
		require_once $config["path_base"] . "User_Profile.php";
		require_once $config["path_base"] . "User_Contact.php";
		require_once $config["path_base"] . "User_Activity.php";

		// hash given config
		Serbapay_Auth::$config = $config;

		// instace of log mng
		Serbapay_Auth::$logger = new Serbapay_Logger();

		// instace of errors mng
		Serbapay_Auth::$error = new Serbapay_Error();

		// start session storage mng
		Serbapay_Auth::$store = new Serbapay_Storage();

		Serbapay_Logger::info( "Enter Serbapay_Auth::initialize()"); 
		Serbapay_Logger::info( "Serbapay_Auth::initialize(). PHP version: " . PHP_VERSION ); 
		Serbapay_Logger::info( "Serbapay_Auth::initialize(). Serbapay_Auth version: " . Serbapay_Auth::$version ); 
		Serbapay_Logger::info( "Serbapay_Auth::initialize(). Serbapay_Auth called from: " . Serbapay_Auth::getCurrentUrl() ); 

		// PHP Curl extension [http://www.php.net/manual/en/intro.curl.php]
		if ( ! function_exists('curl_init') ) {
			Serbapay_Logger::error('Serbapayauth Library needs the CURL PHP extension.');
			throw new Exception('Serbapayauth Library needs the CURL PHP extension.');
		}

		// PHP JSON extension [http://php.net/manual/en/book.json.php]
		if ( ! function_exists('json_decode') ) {
			Serbapay_Logger::error('Serbapayauth Library needs the JSON PHP extension.');
			throw new Exception('Serbapayauth Library needs the JSON PHP extension.');
		} 

		// session.name
		if( session_name() != "PHPSESSID" ){
			Serbapay_Logger::info('PHP session.name diff from default PHPSESSID. http://php.net/manual/en/session.configuration.php#ini.session.name.');
		}

		// safe_mode is on
		if( ini_get('safe_mode') ){
			Serbapay_Logger::info('PHP safe_mode is on. http://php.net/safe-mode.');
		}

		// open basedir is on
		if( ini_get('open_basedir') ){
			Serbapay_Logger::info('PHP open_basedir is on. http://php.net/open-basedir.');
		}

		Serbapay_Logger::debug( "Serbapay_Auth initialize. dump used config: ", serialize( $config ) );
		Serbapay_Logger::debug( "Serbapay_Auth initialize. dump current session: ", Serbapay_Auth::storage()->getSessionData() ); 
		Serbapay_Logger::info( "Serbapay_Auth initialize: check if any error is stored on the endpoint..." );

		if( Serbapay_Error::hasError() ){ 
			$m = Serbapay_Error::getErrorMessage();
			$c = Serbapay_Error::getErrorCode();
			$p = Serbapay_Error::getErrorPrevious();

			Serbapay_Logger::error( "Serbapay_Auth initialize: A stored Error found, Throw an new Exception and delete it from the store: Error#$c, '$m'" );

			Serbapay_Error::clearError();

			// try to provide the previous if any
			// Exception::getPrevious (PHP 5 >= 5.3.0) http://php.net/manual/en/exception.getprevious.php
			if ( version_compare( PHP_VERSION, '5.3.0', '>=' ) && ($p instanceof Exception) ) { 
				throw new Exception( $m, $c, $p );
			}
			else{
				throw new Exception( $m, $c );
			}
		}

		Serbapay_Logger::info( "Serbapay_Auth initialize: no error found. initialization succeed." );

		// Endof initialize 
	}

	// --------------------------------------------------------------------

	/**
	* Serbapay storage system accessor
	*
	* Users sessions are stored using SerbapayAuth storage system ( SerbapayAuth 2.0 handle PHP Session only) and can be acessed directly by
	* Serbapay_Auth::storage()->get($key) to retrieves the data for the given key, or calling
	* Serbapay_Auth::storage()->set($key, $value) to store the key => $value set.
	*/
	public static function storage()
	{
		return Serbapay_Auth::$store;
	}

	// --------------------------------------------------------------------

	/**
	* Get serbapayauth session data. 
	*/
	function getSessionData()
	{ 
		return Serbapay_Auth::storage()->getSessionData();
	}

	// --------------------------------------------------------------------

	/**
	* restore serbapayauth session data. 
	*/
	function restoreSessionData( $sessiondata = NULL )
	{ 
		Serbapay_Auth::storage()->restoreSessionData( $sessiondata );
	}

	// --------------------------------------------------------------------

	/**
	* Try to authenticate the user with a given provider. 
	*
	* If the user is already connected we just return and instance of provider adapter,
	* ELSE, try to authenticate and authorize the user with the provider. 
	*
	* $params is generally an array with required info in order for this provider and SerbapayAuth to work,
	*  like :
	*          hauth_return_to: URL to call back after authentication is done
	*        openid_identifier: The OpenID identity provider identifier
	*           google_service: can be "Users" for Google user accounts service or "Apps" for Google hosted Apps
	*/
	public static function authenticate( $providerId, $params = NULL )
	{
		Serbapay_Logger::info( "Enter Serbapay_Auth::authenticate( $providerId )" );

		// if user not connected to $providerId then try setup a new adapter and start the login process for this provider
		if( ! Serbapay_Auth::storage()->get( "hauth_session.$providerId.is_logged_in" ) ){ 
			Serbapay_Logger::info( "Serbapay_Auth::authenticate( $providerId ), User not connected to the provider. Try to authenticate.." );

			$provider_adapter = Serbapay_Auth::setup( $providerId, $params );

			$provider_adapter->login();
		}

		// else, then return the adapter instance for the given provider
		else{
			Serbapay_Logger::info( "Serbapay_Auth::authenticate( $providerId ), User is already connected to this provider. Return the adapter instance." );

			return Serbapay_Auth::getAdapter( $providerId );
		}
	}

	// --------------------------------------------------------------------

	/**
	* Return the adapter instance for an authenticated provider
	*/ 
	public static function getAdapter( $providerId = NULL )
	{
		Serbapay_Logger::info( "Enter Serbapay_Auth::getAdapter( $providerId )" );

		return Serbapay_Auth::setup( $providerId );
	}

	// --------------------------------------------------------------------

	/**
	* Setup an adapter for a given provider
	*/ 
	public static function setup( $providerId, $params = NULL )
	{
		Serbapay_Logger::debug( "Enter Serbapay_Auth::setup( $providerId )", $params );

		if( ! $params ){ 
			$params = Serbapay_Auth::storage()->get( "hauth_session.$providerId.id_provider_params" );
			
			Serbapay_Logger::debug( "Serbapay_Auth::setup( $providerId ), no params given. Trying to get the sotred for this provider.", $params );
		}

		if( ! $params ){ 
			$params = ARRAY();
			
			Serbapay_Logger::info( "Serbapay_Auth::setup( $providerId ), no stored params found for this provider. Initialize a new one for new session" );
		}

		if( ! isset( $params["hauth_return_to"] ) ){
			$params["hauth_return_to"] = Serbapay_Auth::getCurrentUrl(); 
		}

		Serbapay_Logger::debug( "Serbapay_Auth::setup( $providerId ). SerbapayAuth Callback URL set to: ", $params["hauth_return_to"] );

		# instantiate a new IDProvider Adapter
		$provider   = new Serbapay_Provider_Adapter();

		$provider->factory( $providerId, $params );

		return $provider;
	} 

	// --------------------------------------------------------------------

	/**
	* Check if the current user is connected to a given provider
	*/
	public static function isConnectedWith( $providerId )
	{
		return (bool) Serbapay_Auth::storage()->get( "hauth_session.{$providerId}.is_logged_in" );
	}

	// --------------------------------------------------------------------

	/**
	* Return array listing all authenticated providers
	*/ 
	public static function getConnectedProviders()
	{
		$idps = array();

		foreach( Serbapay_Auth::$config["providers"] as $idpid => $params ){
			if( Serbapay_Auth::isConnectedWith( $idpid ) ){
				$idps[] = $idpid;
			}
		}

		return $idps;
	}

	// --------------------------------------------------------------------

	/**
	* Return array listing all enabled providers as well as a flag if you are connected.
	*/ 
	public static function getProviders()
	{
		$idps = array();

		foreach( Serbapay_Auth::$config["providers"] as $idpid => $params ){
			if($params['enabled']) {
				$idps[$idpid] = array( 'connected' => false );

				if( Serbapay_Auth::isConnectedWith( $idpid ) ){
					$idps[$idpid]['connected'] = true;
				}
			}
		}

		return $idps;
	}

	// --------------------------------------------------------------------

	/**
	* A generic function to logout all connected provider at once 
	*/ 
	public static function logoutAllProviders()
	{
		$idps = Serbapay_Auth::getConnectedProviders();

		foreach( $idps as $idp ){
			$adapter = Serbapay_Auth::getAdapter( $idp );

			$adapter->logout();
		}
	}

	// --------------------------------------------------------------------

	/**
	* Utility function, redirect to a given URL with php header or using javascript location.href
	*/
	public static function redirect( $url, $mode = "PHP" )
	{
		Serbapay_Logger::info( "Enter Serbapay_Auth::redirect( $url, $mode )" );

		if( $mode == "PHP" ){
			header( "Location: $url" ) ;
		}
		elseif( $mode == "JS" ){
			echo '<html>';
			echo '<head>';
			echo '<script type="text/javascript">';
			echo 'function redirect(){ window.top.location.href="' . $url . '"; }';
			echo '</script>';
			echo '</head>';
			echo '<body onload="redirect()">';
			echo 'Redirecting, please wait...';
			echo '</body>';
			echo '</html>'; 
		}

		die();
	}

	// --------------------------------------------------------------------

	/**
	* Utility function, return the current url. TRUE to get $_SERVER['REQUEST_URI'], FALSE for $_SERVER['PHP_SELF']
	*/
	public static function getCurrentUrl( $request_uri = true ) 
	{
		if(
			isset( $_SERVER['HTTPS'] ) && ( $_SERVER['HTTPS'] == 'on' || $_SERVER['HTTPS'] == 1 )
		|| 	isset( $_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == 'https'
		){
			$protocol = 'https://';
		}
		else {
			$protocol = 'http://';
		}

		$url = $protocol . $_SERVER['HTTP_HOST'];

		// use port if non default
		if( isset( $_SERVER['SERVER_PORT'] ) && strpos( $url, ':'.$_SERVER['SERVER_PORT'] ) === FALSE ) {
			$url .= ($protocol === 'http://' && $_SERVER['SERVER_PORT'] != 80 && !isset( $_SERVER['HTTP_X_FORWARDED_PROTO']))
				|| ($protocol === 'https://' && $_SERVER['SERVER_PORT'] != 443 && !isset( $_SERVER['HTTP_X_FORWARDED_PROTO']))
				? ':' . $_SERVER['SERVER_PORT'] 
				: '';
		}

		if( $request_uri ){
			$url .= $_SERVER['REQUEST_URI'];
		}
		else{
			$url .= $_SERVER['PHP_SELF'];
		}

		// return current url
		return $url;
	}
}
