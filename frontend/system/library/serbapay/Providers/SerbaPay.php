<?php

use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;


class Serbapay_Providers_Serbapay extends Serbapay_Provider_Model
{
	public $scope = "";

	/**
	* IDp wrappers initializer 
	*/
	function initialize() 
	{
		Serbapay_Logger::debug( "Call initialize" );


		if ( ! $this->config["keys"]["id"] || ! $this->config["keys"]["secret"] ){
			throw new Exception( "Your application id and secret are required in order to connect to {$this->providerId}.", 4 );
		}

		if ( ! class_exists('SerbaPayApiException', false) ) {
			require_once Serbapay_Auth::$config["path_libraries"] . "SerbaPay/serbapayapi.php";
		}
		

		$this->api = new SerbaPayAPI( array( 'appId' => $this->config["keys"]["id"], 'secret' => $this->config["keys"]["secret"] ) );
		Serbapay_Logger::debug( "Initialize SerbaPayAPI" );
        Serbapay_Logger::debug( "Check access token" );
		if ( $this->token("access_token") ) {
			$this->api->setAccessToken( $this->token("access_token") );
					$access_token = $this->api->getAccessToken();

			if( $access_token ){
				$this->token("access_token", $access_token );
				$this->api->setAccessToken( $access_token );
			}

			$this->api->setAccessToken( $this->token("access_token") );
		}
        Serbapay_Logger::debug( "Check userJWT session" );
		if(isset($_SESSION['userJWT']))
			$this->api->getUser($_SESSION['userJWT']);
	}

	/**
	* begin login step
	* 
	* simply call Facebook::require_login(). 
	*/
	function loginBegin()
	{
		Serbapay_Logger::debug( "libarary/serbapay/Provider/SerbaPay.php loginBegin" );
        $lang = Serbapay_Auth::$config['lang'];
        Serbapay_Logger::debug( "language: " .$lang);
		$parameters = array("scope" => $this->scope, "redirect_uri" => $this->endpoint, "display" => "popup", "language" => $lang);
		$optionals  = array("scope", "redirect_uri", "display");

		foreach ($optionals as $parameter){
			if( isset( $this->config[$parameter] ) && ! empty( $this->config[$parameter] ) ){
				$parameters[$parameter] = $this->config[$parameter];
			}
		}
		// get the login url 
		$url = $this->api->getLoginUrl( $parameters );

		// redirect to SerbaPay
		Serbapay_Auth::redirect( $url );
	}

	/**
	* finish login step 
	*/
	function loginFinish()
	{

		$jwt = $_REQUEST['token'];
		$key = $this->api->getAppSecret();

		if (! isset( $_REQUEST['token'] ) ){
			throw new Exception( "Token is required. Please try again", 5 );
		}

		try
		{
			$decoded = JWT::decode($jwt, $key, array('HS256'));
		}
		catch(ExpiredException $e)
		{
			echo 'Token Expired! Please login again. Thanks!';
			//redirect back

			Serbapay_Auth::redirect( "index.php?route=account/login" );
		}
		catch(DomainException $e) {
			echo 'Wrong Domain!';
		}

		// in case we get error_reason=user_denied&error=access_denied
		if ( isset( $_REQUEST['error'] ) && $_REQUEST['error'] == "access_denied" ){ 
			throw new Exception( "Authentication failed! The user denied your request.", 5 );
		}

		// try to get the UID of the connected user from fb, should be > 0 
		if ( ! $this->api->getUser($decoded) ){
			throw new Exception( "Authentication failed! {$this->providerId} returned an invalid user id.", 5 );
		}

		// set user as logged in
		$this->setUserConnected();

		// store facebook access token 
		$this->token( "access_token", $this->api->getAccessToken() );

		// store jwt access token
		$this->token( "login_token", $jwt );
		
	}

	/**
	* logout
	*/
	function logout()
	{ 
		$this->api->destroySession();

		parent::logout();
	}

	/**
	* load the user profile from the IDp api client
	*/
	function getUserProfile()
	{
        Serbapay_Logger::info( "Start getUserProfile()" );
        //deault is get detail from IDp API client, eg. OAUTH, in this case SerbaPay did not provide any api to call.
        //We need to get from the hash number and decode the JWT again

        $sessionData = $_SESSION["HA::STORE"];
        $jwt = $sessionData["hauth_session.serbapay.token.login_token"];
        $key = $this->api->getAppSecret();

        $tks = explode('.', $jwt);

        list($headb64, $bodyb64, $cryptob64) = $tks;

        if (null === $payload = JWT::jsonDecode(JWT::urlsafeB64Decode($bodyb64))) {
            echo '<script type="text/javascript"> alert("Invalid Session! Please try again."); </script>';
            echo '<script type="text/javascript"> window.opener.location.reload(); window.close(); </script>';
            return;
        }
        if ( !isset($_SESSION['userJWT']) ) {
            $_SESSION['userJWT'] = $payload;
        }

		# store the user profile.
		$this->user->profile->identifier    = (isset($_SESSION['userJWT']->user_key))?$_SESSION['userJWT']->user_key:"";
		$this->user->profile->displayName   = (isset($_SESSION['userJWT']->full_name))?$_SESSION['userJWT']->full_name:"";
		$this->user->profile->full_name     = (isset($_SESSION['userJWT']->full_name))?$_SESSION['userJWT']->full_name:"";
		$this->user->profile->lastName      = (isset($_SESSION['userJWT']->last_name))?$_SESSION['userJWT']->last_name:"";
		$this->user->profile->photoURL      = "";
		$this->user->profile->profileURL    = "";
		$this->user->profile->webSiteURL    = "";
		$this->user->profile->gender        = (isset($_SESSION['userJWT']->gender))?$_SESSION['userJWT']->gender:"";
		$this->user->profile->description   = "";
		$this->user->profile->email         = (isset($_SESSION['userJWT']->email))?$_SESSION['userJWT']->email:"";
		$this->user->profile->emailVerified = (isset($_SESSION['userJWT']->email))?$_SESSION['userJWT']->email:"";
		$this->user->profile->region        = "Jakarta Raya"; // set Jakarta as default
		$this->user->profile->country       = "Indonesia"; // set Indonesia as default

		if( isset($_SESSION['userJWT']->birthday) ) {
			list($birthday_year, $birthday_month, $birthday_day) = explode( "-", $_SESSION['userJWT']->birthday );

			$this->user->profile->birthDay   = (int) $birthday_day;
			$this->user->profile->birthMonth = (int) $birthday_month;
			$this->user->profile->birthYear  = (int) $birthday_year;
		}
        Serbapay_Logger::info( "User Identifier: " . $this->user->profile->identifier );
        Serbapay_Logger::info( "End getUserProfile() and next return" );

		return $this->user->profile;
 	}

	/**
	* load the user contacts
	*/
	function getUserContacts()
	{
		try{ 
			$response = $this->api->api('/me/friends'); 
		}
		catch( FacebookApiException $e ){
			throw new Exception( "User contacts request failed! {$this->providerId} returned an error: $e" );
		} 
 
		if( ! $response || ! count( $response["data"] ) ){
			return ARRAY();
		}

		$contacts = ARRAY();
 
		foreach( $response["data"] as $item ){
			$uc = new Serbapay_User_Contact();

			$uc->identifier  = (array_key_exists("id",$item))?$item["id"]:"";
			$uc->displayName = (array_key_exists("name",$item))?$item["name"]:"";
			$uc->profileURL  = "https://www.facebook.com/profile.php?id=" . $uc->identifier;
			$uc->photoURL    = "https://graph.facebook.com/" . $uc->identifier . "/picture?width=150&height=150";

			$contacts[] = $uc;
		}

		return $contacts;
 	}

	/**
	* update user status
	*/
	function setUserStatus( $status )
	{
		$parameters = array();

		if( is_array( $status ) ){
			$parameters = $status;
		}
		else{
			$parameters["message"] = $status; 
		}

		try{ 
			$response = $this->api->api( "/me/feed", "post", $parameters );
		}
		catch( FacebookApiException $e ){
			throw new Exception( "Update user status failed! {$this->providerId} returned an error: $e" );
		}
 	}

	/**
	* load the user latest activity  
	*    - timeline : all the stream
	*    - me       : the user activity only  
	*/
	function getUserActivity( $stream )
	{
		try{
			if( $stream == "me" ){
				$response = $this->api->api( '/me/feed' ); 
			}
			else{
				$response = $this->api->api('/me/home'); 
			}
		}
		catch( FacebookApiException $e ){
			throw new Exception( "User activity stream request failed! {$this->providerId} returned an error: $e" );
		} 

		if( ! $response || ! count(  $response['data'] ) ){
			return ARRAY();
		}

		$activities = ARRAY();

		foreach( $response['data'] as $item ){
			if( $stream == "me" && $item["from"]["id"] != $this->api->getUser() ){
				continue;
			}

			$ua = new Serbapay_User_Activity();

			$ua->id                 = (array_key_exists("id",$item))?$item["id"]:"";
			$ua->date               = (array_key_exists("created_time",$item))?strtotime($item["created_time"]):"";

			if( $item["type"] == "video" ){
				$ua->text           = (array_key_exists("link",$item))?$item["link"]:"";
			}

			if( $item["type"] == "link" ){
				$ua->text           = (array_key_exists("link",$item))?$item["link"]:"";
			}

			if( empty( $ua->text ) && isset( $item["story"] ) ){
				$ua->text           = (array_key_exists("link",$item))?$item["link"]:"";
			}

			if( empty( $ua->text ) && isset( $item["message"] ) ){
				$ua->text           = (array_key_exists("message",$item))?$item["message"]:"";
			}

			if( ! empty( $ua->text ) ){
				$ua->user->identifier   = (array_key_exists("id",$item["from"]))?$item["from"]["id"]:"";
				$ua->user->displayName  = (array_key_exists("name",$item["from"]))?$item["from"]["name"]:"";
				$ua->user->profileURL   = "https://www.facebook.com/profile.php?id=" . $ua->user->identifier;
				$ua->user->photoURL     = "https://graph.facebook.com/" . $ua->user->identifier . "/picture?type=square";

				$activities[] = $ua;
			}
		}

		return $activities;
 	}
}
