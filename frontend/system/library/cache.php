<?php
class Cache {
	private $expire = 3600;
    private static $redis = '';

    public function __construct()
    {
        Predis\Autoloader::register();
        self::$redis = new Predis\Client([
            'scheme' => REDIS_PORTOCAL,
            'host'   => REDIS_HOST,
            'port'   => REDIS_PORT,
        ]);
    }

    public function initRedis()
    {
        Predis\Autoloader::register();
        self::$redis = new Predis\Client([
            'scheme' => REDIS_PORTOCAL,
            'host'   => REDIS_HOST,
            'port'   => REDIS_PORT,
        ]);
    }

	public function get($key) {
        if(REDIS_ENABLE)
        {
            //$this->initRedis();
            try {
                $file_name = 'cache.'.preg_replace('/[^A-Z0-9\._-]/i', '', $key);

                $cache = self::$redis->get($file_name);

                return unserialize($cache);
            }
            catch (Exception $exception) {
                return null;
            }
        }
        else
        {

            $files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

            if ($files)
            {
                $cache = file_get_contents($files[0]);

                $data = unserialize($cache);

                foreach ($files as $file)
                {
                    $time = substr(strrchr($file, '.'), 1);

                    if ($time < time())
                    {
                        if (file_exists($file))
                        {
                            unlink($file);
                        }
                    }
                }

                return $data;
            }
        }
	}

	public function set($key, $value) {
        if(REDIS_ENABLE)
        {

            $file_name = 'cache.' .  preg_replace('/[^A-Z0-9\._-]/i', '', $key);

            try {
                self::$redis->set($file_name, serialize($value));
                //self::$redis->setex($file_name, $this->expire,  serialize($value));
            }
            catch (Exception $exception) {
                echo $exception->getMessage();
            }
        }
        else
        {
            $this->delete($key);

            $file = DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

            $handle = fopen($file, 'w');

            fwrite($handle, serialize($value));

            fclose($handle);
        }
	}

	public function delete($key) {
        if(REDIS_ENABLE)
        {
            //$this->initRedis();
            $file_name = 'cache.' .  preg_replace('/[^A-Z0-9\._-]/i', '', $key);

            try {
                self::$redis->del($file_name);
            }
            catch (Exception $exception) {
                return 'nocache';
            }

        }
        else
        {
            $files = glob(DIR_CACHE . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

            if ($files)
            {
                foreach ($files as $file)
                {
                    if (file_exists($file))
                    {
                        unlink($file);
                    }
                }
            }
        }
	}
}
?>