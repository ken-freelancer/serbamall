<?php
// GET project basepath for different development environment
$basepath   = dirname(realpath(__FILE__));

//composer autoload
require $basepath . '/vendor/autoload.php';

//load .env if .env file exist
$filePath = rtrim(__DIR__, DIRECTORY_SEPARATOR).DIRECTORY_SEPARATOR . '.env';
if(is_file($filePath) && is_readable($filePath))
{
    $dotenv = new Dotenv\Dotenv(__DIR__);
    $dotenv->load();
}

// ALLOW multi domain
/*$protocal   = ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "https" : "http");
$host       = "://".$_SERVER['HTTP_HOST'];
$dirPath    = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
//work in anyhost, in localhost if do not have ssh support put both http
define('HTTP_SERVER', 'http'.$host.$dirPath);
define('HTTPS_SERVER', 'http'.$host.$dirPath);*/

// HTTP
define('HTTP_SERVER', getenv('HTTP_SERVER')?getenv('HTTP_SERVER'):'http://serbamall.dev/');

// HTTPS
define('HTTPS_SERVER', getenv('HTTPS_SERVER')?getenv('HTTPS_SERVER'):'http://serbamall.dev/');

// SERBAPAY LOGIN
define('HTTPS_SERBAPAY', getenv('HTTPS_SERBAPAY')?getenv('HTTPS_SERBAPAY'):'https://www.serbapay.com/');
define('HTTPS_SERBAPAY_SHIPPED_CURL', HTTPS_SERBAPAY.'checkoutbox/order-status');

define('JNE_API_URL', 'http://api.jne.co.id:8889/tracing/serbamall/price/');
define('JNE_API_KEY', getenv('JNE_API_KEY'));
define('JNE_API_USERNAME', getenv('JNE_API_USERNAME'));

//CDN
define('CDN_ENABLE', getenv('CDN_ENABLE')?getenv('CDN_ENABLE'):0);
define('HTTP_CDN_IMAGE', getenv('HTTP_CDN_IMAGE')?getenv('HTTP_CDN_IMAGE'):'http://cdn1.serbamall.com/');
define('HTTPS_CDN_IMAGE', getenv('HTTPS_CDN_IMAGE')?getenv('HTTPS_CDN_IMAGE'):'https://cdn1.serbamall.com/');

//S3
define('LOG_ON_S3', getenv('LOG_ON_S3')?getenv('LOG_ON_S3'):0);
define('S3_MINIFIER_ENABLE', getenv('S3_MINIFIER_ENABLE')?getenv('S3_MINIFIER_ENABLE'):0);
define('S3_KEY', getenv('S3_KEY'));
define('S3_SECRET', getenv('S3_SECRET'));
define('S3_REGION', getenv('S3_REGION'));
define('S3_BUCKET', getenv('S3_BUCKET'));
define('S3_REGION_URL', getenv('S3_REGION_URL')?getenv('S3_REGION_URL'):'https://serbamall.s3-ap-southeast-1.amazonaws.com/');

define('HTTP_S3_CDN_REGION_URL', getenv('HTTP_S3_CDN_REGION_URL')?getenv('HTTP_S3_CDN_REGION_URL'):'http://cdn2.serbamall.com/');
define('HTTPS_S3_CDN_REGION_URL', getenv('HTTPS_S3_CDN_REGION_URL')?getenv('HTTPS_S3_CDN_REGION_URL'):'https://serbamall.s3-ap-southeast-1.amazonaws.com/');


//REDIS
define('REDIS_ENABLE', getenv('REDIS_ENABLE')?getenv('REDIS_ENABLE'):0);
define('REDIS_HOST', getenv('REDIS_HOST')?getenv('REDIS_HOST'):'192.168.10.10');
define('REDIS_PORT', getenv('REDIS_PORT')?getenv('REDIS_PORT'):'6379');
define('REDIS_PORTOCAL', getenv('REDIS_PORTOCAL')?getenv('REDIS_PORTOCAL'):'tcp');


//cache cron job
define('CACHE_CRON_ENABLE', getenv('CACHE_CRON_ENABLE')?getenv('CACHE_CRON_ENABLE'):0);

// DIR
define('DIR_APPLICATION', $basepath.'/catalog/');
define('DIR_SYSTEM', $basepath.'/system/');
define('DIR_DATABASE', $basepath.'/system/database/');
define('DIR_LANGUAGE', $basepath.'/catalog/language/');
define('DIR_TEMPLATE', $basepath.'/catalog/view/theme/');
define('DIR_CONFIG', $basepath.'/system/config/');
define('DIR_IMAGE', $basepath.'/image/');
define('DIR_CACHE', $basepath.'/system/cache/');
define('DIR_DOWNLOAD', $basepath.'/download/');
define('DIR_LOGS', $basepath.'/system/logs/');

// DB
define('DB_DRIVER', getenv('DB_DRIVER')?getenv('DB_DRIVER'):'mysqli');
define('DB_HOSTNAME', getenv('DB_HOSTNAME')?getenv('DB_HOSTNAME'):'localhost');
define('DB_USERNAME', getenv('DB_USERNAME')?getenv('DB_USERNAME'):'root');
define('DB_PASSWORD', getenv('DB_PASSWORD')?getenv('DB_PASSWORD'):'');
define('DB_DATABASE', getenv('DB_DATABASE')?getenv('DB_DATABASE'):'c2c');
define('DB_PREFIX', getenv('DB_PREFIX')?getenv('DB_PREFIX'):'oc_');