<?php
//require composer
require './vendor/autoload.php';
if (file_exists('config.php')) {
    require_once('config.php');
}
require_once( "system/library/serbapay/Auth.php" );
require_once( "system/library/serbapay/Endpoint.php" ); 

Serbapay_Endpoint::process();
