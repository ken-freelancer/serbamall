<?php
class ControllerCommonHome extends Controller {
	public function index() {
		$this->document->setTitle($this->config->get('config_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));

		$this->data['heading_title'] = $this->config->get('config_title');
		
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/common/home.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/common/home.tpl';
		} else {
			$this->template = 'default/template/common/home.tpl';
		}
		
		$this->children = array(
			'common/column_left',
			'common/column_right',
			'common/content_top',
			'common/content_bottom',
			'common/footer',
			'common/header'
		);
										
		$this->response->setOutput($this->render());
	}

    /**
     * This function is use call by cron job
     * It is to update the cache
     * IT is same as index function
     */
    public function cron() {

        $this->index();
        //after all cache has update then update the latest_timestamp
        //add to redis timestamp
        if($this->journal2->cache->getRedisCacheUpdate())
        {
            Predis\Autoloader::register();
            $redis = new Predis\Client([
                'scheme' => REDIS_PORTOCAL,
                'host'   => REDIS_HOST,
                'port'   => REDIS_PORT,
            ]);
            try
            {
                $redis->set('latest_timestamp', $this->journal2->cache->getRedisTimestamp());
            }
            catch (Exception $exception) {
                echo 'error to write cache timestamp';
            }
        }
    }
}
?>