<?php
class ControllerPaymentSerbapay extends Controller {
	protected function index() {
		$this->language->load('payment/serbapay');
		
		$this->data['button_confirm'] = $this->language->get('button_confirm');
		$this->data['button_back'] = $this->language->get('button_back');

    	$this->data['action'] = HTTPS_SERBAPAY . 'checkoutbox';
		

		$support_currency = $this->config->get('entry_currency');
		
		$this->load->model('checkout/order');

		$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);


		// Lets start define serbapay parameters here
		$this->data['products'] = array();

		$productString = '';
		$basket = '';

		foreach ($this->cart->getProducts() as $product)
        {
            $option_data = array();
            $option_data_str = array();

            if ($product['option'])
            {
                foreach ($product['option'] as $option)
                {
                    $option_data[] = array(
                        'name'  => $option['name'],
                        'value' => $option['option_value']
                    );
                    $option_data_str[] = $option['name'] . ': ' . $option['value'];
                }
            }

			if ($option_data_str) {
				$productString .= '<item-name>' . $product['name'] . ' ' . implode('; ', $option_data_str) . '</item-name>';
				$productString .= '<item-description>' . $product['name'] . ' ' . implode('; ', $option_data_str) . '</item-description>';
			} else {
				$productString .= '<item-name>' . $product['name'] . '</item-name>';
				$productString .= '<item-description>' . $product['name'] . '</item-description>';
			}


			$this->data['products'][] = array(
				'id'       => $product['product_id'],
				'name'     => $product['name'],
				'model'    => $product['model'],
				'price'    => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value'], false),
				'quantity' => $product['quantity'],
				'option'   => $option_data,
				'weight'   => $product['weight']
			);
            /**
             * Item_code, Vendor_key, Item_name, Item_description, unit_price, quantity, shipping, shipping_company##second_item_code,second_vendor_key,second_item_name,second_item_description….
             * shipping must be defined, leave it as 0 if the item is free shipping fee
             */
            $product_shipping_rate = 0;
            $product_shipping_company = 'SELF';

            //get post courier ID for that vendor
            $courier_id = $_SESSION['ken_customer_selected_product_vendor_shipping_arr'][$product['vendor_id']];

            if($courier_id >= 9111 && $courier_id <= 9113)
            {
                $product_shipping_company = 'JNE';
            }

            /*echo "<pre>";
            print_r($_SESSION['ken_product_vendor_shipping_arr'][$product['vendor_id']][$product['product_id']][$courier_id]['rate']);
            echo "</pre>";*/
            if(isset($_SESSION['ken_product_vendor_shipping_arr'][$product['vendor_id']][$product['product_id']][$courier_id]['rate']))
            {
                $product_shipping_rate = $_SESSION['ken_product_vendor_shipping_arr'][$product['vendor_id']][$product['product_id']][$courier_id]['rate'];
            }
            /*mvd_jne
            if(isset($_SESSION['cart_jne_product_vendor_shipping_arr'][$product['vendor_id']][$product['product_id']]))
            {
                $jne_shipping = $_SESSION['cart_jne_product_vendor_shipping_arr'][$product['vendor_id']][$product['product_id']]['rate'];
            }*/
			$basket .= $product['product_id'] . ',' .
                        $product['vendor_serbapay_id'] . ',' .
                        $product['name'] . ',' .
                        $productString . ',' .
                        $product['price'] . ',' .
                        $product['quantity'] . ',' .
                        $product_shipping_rate . ',' .
                        $product_shipping_company .
                        '##';
		}
        //unset session
        unset($_SESSION['ken_product_vendor_shipping_arr']);
        unset($_SESSION['cart_jne_product_vendor_shipping_arr']);
        //remove the last 2 hash
		$basket = substr($basket,0,-2);

		// Let's Generate Digital Signature

		$serbaypayHash ='';

		$merchant_id = $this->config->get('serbapay_vendor');
		$merchant_secret = $this->config->get('serbapay_password');

		$tmpAmount = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value'], FALSE);
		$orderAmount = number_format($tmpAmount, 2, ".", "");


		//order id
		$invoice = $this->session->data['order_id'];



		//get shipping detail
		$shipping_amount = 0;
		$total_data = array();
		$total = 0;
		$taxes = $this->cart->getTaxes();

		$this->load->model('setting/extension');

		$sort_order = array();

		$results = $this->model_setting_extension->getExtensions('total');

		foreach ($results as $key => $value) {
			$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
		}

		array_multisort($sort_order, SORT_ASC, $results);

		foreach ($results as $result) {
			if ($this->config->get($result['code'] . '_status')) {
				$this->load->model('total/' . $result['code']);

				$this->{'model_total_' . $result['code']}->getTotal($total_data, $total, $taxes);
			}
		}

		$sort_order = array();

		foreach ($total_data as $key => $value) {
			$sort_order[$key] = $value['sort_order'];
		}

		array_multisort($sort_order, SORT_ASC, $total_data);
		foreach($total_data as $total)
		{
			if($total['code'] == 'shipping')
				$shipping_amount = $total['value'];
		}
		//.get shipping detail
		//Debug Value
		/*echo "Merchant ID: " . $merchant_id;
		echo "<br />";
		echo "Invoice:" . $invoice;
		echo "<br />";
		echo "Order Amount:" . $orderAmount;
		echo "<br />";
		echo "Currency:" . $order_info['currency_code'];
		echo "<br />";
		echo "Merchant Secret: " . $merchant_secret;*/

		$hash_str = $merchant_id . $invoice . $orderAmount . $order_info['currency_code'];
        $hash = self::encrypt($hash_str, $merchant_secret);
		// Signature generating done !
		
		// Assign values for form post

		$this->data['merchant_id'] = $this->config->get('serbapay_vendor');
		$this->data['invoice'] = $invoice;
		$this->data['amount'] = $orderAmount;
		$this->data['shipping'] = $shipping_amount;
		$this->data['currency'] = $order_info['currency_code'];
		$this->data['basket'] = $basket;

		$this->data['username'] = $order_info['payment_firstname'] . ' ' . $order_info['payment_lastname'];
		$this->data['useremail'] = $order_info['email'];
		$this->data['usercontact'] = $order_info['telephone'];

		$this->data['remark'] = sprintf($this->language->get('text_description'), date($this->language->get('date_format_short')), $this->session->data['order_id']);
		$this->data['lang'] = $this->language->get('code');
		$this->data['hash'] = $hash;
		$this->data['ResponseURL'] = $this->url->link('payment/serbapay/callback', '', 'SSL');
		$this->data['BackendURL'] = $this->url->link('payment/serbapay/backendcallback', '', 'SSL');
		
		
		$this->data['back'] = $this->url->link('checkout/payment', '', 'SSL');
		
		$this->id       = 'payment';

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/serbapay.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/payment/serbapay.tpl';
		} else {
			$this->template = 'default/template/payment/serbapay.tpl';
		}	
		
		$this->render();
	}
	
	public function callback() 
	{
		$this->language->load('payment/serbapay');
	
		$this->data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

		if (!isset($this->request->server['HTTPS']) || ($this->request->server['HTTPS'] != 'on')) {
			$this->data['base'] = HTTP_SERVER;
		} else {
			$this->data['base'] = HTTPS_SERVER;
		}
	
		$this->data['charset'] = $this->language->get('charset');
		$this->data['language'] = $this->language->get('code');
		$this->data['direction'] = $this->language->get('direction');
	
		$this->data['heading_title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));
		
		$this->data['text_response'] = $this->language->get('text_response');
		$this->data['text_success'] = $this->language->get('text_success');
		$this->data['text_success_wait'] = sprintf($this->language->get('text_success_wait'), $this->url->link('checkout/success', '', 'SSL'));
		$this->data['text_failure'] = $this->language->get('text_failure');
		$this->data['text_failure_wait'] = sprintf($this->language->get('text_failure_wait'), $this->url->link('checkout/cart'));


		//it means cancel transaction with serbapay
		if(!isset($_POST['st'])){
			$this->data['continue'] = $this->url->link('checkout/cart');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/serbapay_cancel.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/serbapay_cancel.tpl';
			} else {
				$this->template = 'default/template/payment/serbapay_cancel.tpl';
			}

			$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
			return;
		}
		//.it means cancel transaction with serbapay

		$expected_sign = $_POST['hash'];

		$merchant_id = $this->config->get('serbapay_vendor');
		$merchant_secret = $this->config->get('serbapay_password');

        //merchant_id + invoice_no + reference + amount
		$hash_str = $merchant_id . $_POST['inv_no'] . $_POST['ref'] .  $_POST['amt'];

		$check_sign = self::encrypt($hash_str, $merchant_secret);

        //st = 00 success
        //st = 01 failed
		if ($_POST['st']=="00" && $check_sign == $expected_sign)
		{
			$this->load->model('checkout/order');

            $order_id = $_POST['inv_no'];
            $comment = "serbaPay Reference ID : " . $_POST['ref'];

            $this->model_checkout_order->confirm($order_id, $this->config->get('serbapay_order_status_id'), $comment, TRUE);

            $this->data['continue'] = $this->url->link('checkout/success', '', 'SSL');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/serbapay_success.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/serbapay_success.tpl';
			} else {
				$this->template = 'default/template/payment/serbapay_success.tpl';
			}

			$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
		}	
		else
		{				
			$this->data['continue'] = $this->url->link('checkout/cart');

			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/payment/serbapay_failure.tpl')) {
				$this->template = $this->config->get('config_template') . '/template/payment/serbapay_failure.tpl';
			} else {
				$this->template = 'default/template/payment/serbapay_failure.tpl';
			}
			$this->response->setOutput($this->render(TRUE), $this->config->get('config_compression'));
        }
		
	}
	
	
	public function backendcallback()
	{
        //check all the require variable is pass back or not
        if( ! isset($_POST['hash']) && ! isset($_POST['inv_no']) && ! isset($_POST['ref']) && ! isset($_POST['ref']) && ! isset($_POST['st']) )
        {
            //return json format
            $json['status'] = '01';
            $json['msg'] = 'required fields not found';
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
        }
        //.check all the require variable is pass back or not

		$this->language->load('payment/serbapay');
	
		$this->data['title'] = sprintf($this->language->get('heading_title'), $this->config->get('config_name'));

		if (!isset($this->request->server['HTTPS']) || ($this->request->server['HTTPS'] != 'on')) {
			$this->data['base'] = HTTP_SERVER;
		} else {
			$this->data['base'] = HTTPS_SERVER;
		}

        $expected_sign = $_POST['hash'];
		$merchant_id = $this->config->get('serbapay_vendor');
		$merchant_secret = $this->config->get('serbapay_password');

        $hash_str = $merchant_id . $_POST['inv_no'] . $_POST['ref'] .  $_POST['amt'];

        $check_sign = self::encrypt($hash_str, $merchant_secret);

		if ($_POST['st']=="00" && $check_sign==$expected_sign)
		{
			$this->load->model('checkout/order');

            $order_id = $_POST['inv_no'];
            $comment = "serbaPay Reference ID : " . $_POST['ref'];

			$this->model_checkout_order->confirm($order_id, $this->config->get('serbapay_order_status_id'), $comment, TRUE);

            //return json format
            $json['status'] = '00';
            $json['msg'] = 'Success!';
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}	
		else
		{
            //return json format
            $json['status'] = '01';
            $json['msg'] = 'Update Failed!';
            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($json));
            return;
		}
	}

    /**
     *
     * in transaction request, the $data sequence is merchant_id + invoice_no + amount + currency
     * in transaction callback, the $data sequence is merchant_id + invoice_no + reference + amount
     * @param $data
     * @param $secret
     * @return string
     */
    private function encrypt($data, $secret) {
        $key = $secret;
        $data = md5($data);

        $blockSize = mcrypt_get_block_size(MCRYPT_3DES, MCRYPT_MODE_ECB);
        $len = strlen($data);
        $pad = $blockSize - ($len % $blockSize);
        $data .= str_repeat(chr($pad), $pad);

        $encData = mcrypt_encrypt(MCRYPT_3DES, $key, $data, MCRYPT_MODE_ECB);

        return base64_encode($encData);
    }
}