<?php
class ControllerModuleVendorPackage extends Controller {
	protected function index($setting) {
		
		static $module = 0;

		$this->language->load('module/vendorlogo'); 

      	$this->data['heading_title'] = $this->language->get('heading_title');
		$this->data['text_vendor'] = $this->language->get('text_vendor');
		$this->data['text_select'] = $this->language->get('text_select');
		$this->data['button_allvendors'] = $this->language->get('button_allvendors');
		
		$this->load->model('catalog/vendorlogo');
		
		$this->load->model('tool/image');
		
		$this->document->addScript('catalog/view/javascript/jquery/jquery.jcarousel.min.js');
		
		if (file_exists('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/vcarousel.css')) {
			$this->document->addStyle('catalog/view/theme/' . $this->config->get('config_template') . '/stylesheet/vcarousel.css');
		} else {
			$this->document->addStyle('catalog/view/theme/default/stylesheet/vcarousel.css');
		}

		$this->data['limit'] = $setting['limit'];
		$this->data['scroll'] = $setting['scroll'];
				
		if ($this->model_catalog_vendorlogo->getAllVendor() != $setting['logos']) {
			$this->data['AllVendors'] = false;
			$this->data['viewallvendor'] = $this->url->link('module/vendorlogo/getAllVendor');
		} else {
			$this->data['AllVendors'] = true;
		}
		
		$this->data['vendor_data'] = array();
		$getList = array();
		
		if ($this->config->get('vendors_selected')) {
			$vendors = array_slice($this->config->get('vendors_selected'), 0, (int)$setting['logos']);			
		} else {
			$getIDs = $this->model_catalog_vendorlogo->getDefaultVendorsID();
			if ($getIDs) {
				foreach ($getIDs as $getID) {
					$getList[] = $getID['vendor_id'];
				}
			}
			$vendors = array_slice($getList, 0, (int)$setting['logos']);		
		}

		if ($vendors) {
					
			foreach ($vendors as $vendor_id) {
				$result = $this->model_catalog_vendorlogo->getVendor($vendor_id);
				
				if ($result) {
					
					if ($result['vendor_image']) {
						$image = $this->model_tool_image->resize($result['vendor_image'], $setting['image_width'], $setting['image_height']);
					} else {
						$image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']);
					}
					
					if ($result['vendor_banner']) {
						$banner_image = $this->model_tool_image->resize($result['vendor_banner'], $setting['image_width'], $setting['image_height']);
					} else {
						$banner_image = $this->model_tool_image->resize('no_image.jpg', $setting['image_width'], $setting['image_height']);
					}
					
					$preBool = $this->model_catalog_vendorlogo->preCheck();					
					$rating = $this->model_catalog_vendorlogo->getVendorRating($vendor_id);
					$total_feedback = $this->model_catalog_vendorlogo->getTotalFeedback($vendor_id);

					if ($preBool) {
						$total_order = $this->model_catalog_vendorlogo->getTotalVendorOrders($vendor_id);
					} else {
						$total_order = $this->model_catalog_vendorlogo->getOldTotalVendorOrders($vendor_id);
					}			
						
					$this->data['vendor_data'][] = array(
						'vendor_id' 	=> (int)$result['vendor_id'],
						'thumb'   	 	=> $image,
						'banner_thumb'	=> $banner_image,
						'vendor_name'   => $result['vendor_name'],
						'rating'		=> $rating,
						'review'		=> $this->language->get('text_feedback') . ' (' . $total_feedback . ') | ' . $this->language->get('text_order') . ' (' . $total_order . ')',
						'href'    	 	=> $this->url->link('module/vendorlogo/getVendorProducts', 'vendor_id=' . (int)$result['vendor_id'])
					);
					
				}
			}
			
			$this->data['module'] = $module++;			
			
			if ($setting['position'] == 'content_bottom' || $setting['position'] == 'content_top') {
				if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/carousel2.tpl')) {
					$this->template = $this->config->get('config_template') . '/template/module/carousel2.tpl';
				} else {
					$this->template = 'default/template/module/carousel2.tpl';
				}			
			}
			
			if ($setting['position'] == 'column_left' || $setting['position'] == 'column_right') {
				if ($this->config->get('display_mode')) {
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/vendor_combo.tpl')) {
						$this->template = $this->config->get('config_template') . '/template/module/vendor_combo.tpl';
					} else {
						$this->template = 'default/template/module/vendor_combo.tpl';
					}
				} else {
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/vendorlogo.tpl')) {
						$this->template = $this->config->get('config_template') . '/template/module/vendorlogo.tpl';
					} else {
						$this->template = 'default/template/module/vendorlogo.tpl';
					}
				}
			}			
			
			$this->render();
			unset($this->data['vendor_data']);
		}
	}
	
	public function getPackageInfo() {
    	$this->load->language('module/vendorpackage');

    	$this->document->setTitle($this->language->get('heading_vendor_info')); 
		
		$this->data['heading_vendor_info'] = $this->language->get('heading_vendor_info');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_store_url'] = $this->language->get('text_store_url');
		$this->data['text_company'] = $this->language->get('text_company');
		$this->data['text_telephone'] = $this->language->get('text_telephone');
		$this->data['text_fax'] = $this->language->get('text_fax');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['text_firstname'] = $this->language->get('text_firstname');
		$this->data['text_lastname'] = $this->language->get('text_lastname');
		$this->data['text_address_1'] = $this->language->get('text_address_1');
		$this->data['text_address_2'] = $this->language->get('text_address_2');
		$this->data['text_city'] = $this->language->get('text_city');
		$this->data['text_postcode'] = $this->language->get('text_postcode');
		$this->data['text_zone'] = $this->language->get('text_zone');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['button_view_all_products'] = $this->language->get('button_view_all_products');
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_vendor_package'),
			'href'      => $this->url->link('module/vendorlogo/getAllVendor'),			
			'separator' => $this->language->get('text_separator')
		);
		
		$this->load->model('module/vendorpackage');
		
    	$result = $this->model_module_vendorpackage->getCommissions();
	
		if ($result) {

			foreach($result as $package)
			{
				$this->data['package'][$package['commission_id']] = $package;
				$this->data['package'][$package['commission_id']]['signup_href'] = $this->url->link('account/signup', 'package_id=' . (int)$package['commission_id']);
				$this->data['package'][$package['commission_id']]['option'] = $this->model_module_vendorpackage->getCommissionOption($package['commission_id']);

			}
		}

    	if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/vendorpackage.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/vendorpackage.tpl';
		} else {
			$this->template = 'default/template/module/vendorpackage.tpl';
		}
		
		$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
		);
			
		$this->response->setOutput($this->render());
  	}
	
	public function getAllVendor() {
    	$this->language->load('module/vendorlogo');
		$this->document->addScript('catalog/view/javascript/jquery/jquery.total-storage.min.js');
		
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_vendors'),
			'href'      => $this->url->link('module/vendorlogo/getAllVendor'),			
			'separator' => $this->language->get('text_separator')
		);
		
		$url = '';
		
		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}
			
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
		
      	$this->data['heading_all_vendors'] = $this->language->get('heading_all_vendors');
		$this->data['button_allvendors'] = $this->language->get('button_allvendors');
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_limit'] = $this->language->get('text_limit');
		$this->data['text_empty'] = $this->language->get('text_empty');
		$this->data['text_feedback'] = $this->language->get('text_feedback');	
		$this->data['text_order'] = $this->language->get('text_order');	
					
		$this->data['button_continue'] = $this->language->get('button_continue');
				
		$this->load->model('catalog/vendorlogo');
		$this->load->model('tool/image');

		$this->data['vendor_data'] = array();		
		$vendors = array();
		$vendor_total = 0;
		
		if ($this->config->get('vendors_selected')) {
			$vendors = $this->config->get('vendors_selected');		
		} else {
			$getIDs = $this->model_catalog_vendorlogo->getDefaultVendorsID();
			if ($getIDs) {
				foreach ($getIDs as $getID) {
					$vendors[] = $getID['vendor_id'];
				}
			}
		}
		
		$data = array(
			'vendors'	=> $vendors,
			'start'		=> ($page - 1) * $limit,
			'limit'     => $limit
		);
		
		$vendor_total = sizeof($vendors);
		
		if ($vendor_total) {
			$results = $this->model_catalog_vendorlogo->getTheVendors($data);
			
			foreach ($results as $result) {
				if ($result['vendor_image']) {
					$image = $this->model_tool_image->resize($result['vendor_image'], $this->config->get('vw_image'), $this->config->get('vh_image'));
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('vw_image'), $this->config->get('vh_image'));
				}

				if ($result['vendor_banner']) {
					$banner_image = $this->model_tool_image->resize($result['vendor_banner'], $this->config->get('vw_image'), $this->config->get('vh_image'));
				} else {
					$banner_image = $this->model_tool_image->resize('no_image.jpg', $this->config->get('vw_image'), $this->config->get('vh_image'));
				}				
				
				$preBool = $this->model_catalog_vendorlogo->preCheck();
				$rating = $this->model_catalog_vendorlogo->getVendorRating($result['vendor_id']);
				$total_feedback = $this->model_catalog_vendorlogo->getTotalFeedback($result['vendor_id']);
			
				if ($preBool) {
					$total_order = $this->model_catalog_vendorlogo->getTotalVendorOrders($result['vendor_id']);
				} else {
					$total_order = $this->model_catalog_vendorlogo->getOldTotalVendorOrders($result['vendor_id']);
				}
				
				$this->data['vendor_data'][] = array(
					'vendor_id' 	=> (int)$result['vendor_id'],
					'thumb'   	 	=> $image,
					'banner_thumb'	=> $banner_image,
					'name'   		=> $result['vendor_name'],
					'rating'		=> $rating,
					'description'	=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
					'review'		=> $this->language->get('text_feedback') . ' (' . $total_feedback . ') | ' . $this->language->get('text_order') . ' (' . $total_order . ')',
					'href'    	 	=> $this->url->link('module/vendorlogo/getVendorProducts', 'vendor_id=' . (int)$result['vendor_id'])
				);
			}
		}
		
		$url = '';
		
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
			
		$pagination = new Pagination();
		$pagination->total = $vendor_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/vendorlogo/getAllVendor', $url . '&page={page}');
		
		$this->data['pagination'] = $pagination->render();
		$this->data['continue'] = $this->url->link('common/home');
		
		$this->data['limit'] = $limit;
			
		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/vendors.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/vendors.tpl';
		} else {
			$this->template = 'default/template/module/vendors.tpl';
		}
		
		$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
		);
		
		$this->response->setOutput($this->render());
  	}
	
	public function getVendorProducts() {
    	
		$this->language->load('module/vendorlogo'); 
		
      	$this->data['heading_product_by_vendors'] = $this->language->get('heading_product_by_vendors');
		$this->data['text_empty'] = $this->language->get('text_empty');			
		$this->data['text_tax'] = $this->language->get('text_tax');
		$this->data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$this->data['text_display'] = $this->language->get('text_display');
		$this->data['text_list'] = $this->language->get('text_list');
		$this->data['text_grid'] = $this->language->get('text_grid');
		$this->data['text_sort'] = $this->language->get('text_sort');
		$this->data['text_limit'] = $this->language->get('text_limit');

		$this->data['heading_vendor_info'] = $this->language->get('heading_vendor_info');
		$this->data['text_store_name'] = $this->language->get('text_store_name');
		$this->data['text_store_url'] = $this->language->get('text_store_url');
		$this->data['text_company'] = $this->language->get('text_company');
		$this->data['text_telephone'] = $this->language->get('text_telephone');
		$this->data['text_fax'] = $this->language->get('text_fax');
		$this->data['text_email'] = $this->language->get('text_email');
		$this->data['text_name'] = $this->language->get('text_name');
		$this->data['text_location'] = $this->language->get('text_location');
		$this->data['text_address_1'] = $this->language->get('text_address_1');
		$this->data['text_address_2'] = $this->language->get('text_address_2');
		$this->data['text_city'] = $this->language->get('text_city');
		$this->data['text_postcode'] = $this->language->get('text_postcode');
		$this->data['text_zone'] = $this->language->get('text_zone');
		$this->data['text_country'] = $this->language->get('text_country');
		$this->data['text_description'] = $this->language->get('text_description');
		$this->data['text_vendor_rating'] = $this->language->get('text_vendor_rating');
							
		$this->data['button_cart'] = $this->language->get('button_cart');
		$this->data['button_wishlist'] = $this->language->get('button_wishlist');
		$this->data['button_compare'] = $this->language->get('button_compare');
		$this->data['button_continue'] = $this->language->get('button_continue');
		
		$this->data['compare'] = $this->url->link('product/compare');
		
		$this->load->model('catalog/vendorlogo'); 
		
		$this->load->model('tool/image');
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
	
		if (isset($this->request->get['vendor_id'])) {
			$vendor_id = (int)$this->request->get['vendor_id'];
		} else {
			$vendor_id = '0';
		}
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}	
							
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}
		
		$this->data['breadcrumbs'] = array();

		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),			
			'separator' => false
		);
		
		$this->data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_vendors'),
			'href'      => $this->url->link('module/vendorlogo/getAllVendor'),			
			'separator' => $this->language->get('text_separator')
		);
		
		$this->data['products'] = array();
		
		if (isset($this->request->get['vendor_id'])) {
			$vendor_id = (int)$this->request->get['vendor_id'];
		}

		$data = array(
			'sort'      => $sort,
			'order'     => $order,
			'vendor_id' => (int)$vendor_id,
			'start' => ($page - 1) * $limit,
			'limit' => $limit
		);
		
		$preBool = $this->model_catalog_vendorlogo->preCheck();
		
		$vendor_result = $this->model_catalog_vendorlogo->getVendor((int)$this->request->get['vendor_id']);
		$rating = $this->model_catalog_vendorlogo->getVendorRating((int)$this->request->get['vendor_id']);
		$total_feedback = $this->model_catalog_vendorlogo->getTotalFeedback((int)$this->request->get['vendor_id']);

		if ($preBool) {
			$total_order = $this->model_catalog_vendorlogo->getTotalVendorOrders((int)$this->request->get['vendor_id']);
		} else {
			$total_order = $this->model_catalog_vendorlogo->getOldTotalVendorOrders((int)$this->request->get['vendor_id']);
		}
		
		if ($vendor_result) {
			
			if ($vendor_result['vendor_image']) {
				$image = $this->model_tool_image->resize($vendor_result['vendor_image'], $this->config->get('vw_image'), $this->config->get('vh_image'));
			} else {
				$image = false;
			}
			
			if ($vendor_result['vendor_banner']) {
				$banner_image = $this->model_tool_image->resize($vendor_result['vendor_banner'], 990, 150);
			} else {
				$banner_image = false;
			}
			
			//vendor gallery
			$vendor_gallery = $this->model_catalog_vendorlogo->getVendorGallery($vendor_result['user_id']);
			
			$this->data['vendor_gallery'] = array();			

			foreach ($vendor_gallery as $gallery_image) {
				$this->data['vendor_gallery'][] = array(
					'thumb'	=> $this->model_tool_image->resize($gallery_image['image'], 130, 87),
					'image' => $this->model_tool_image->resize($gallery_image['image'], 500, 333)
				);
			}
			
			
			$this->load->model('localisation/country');
			$country = $this->model_localisation_country->getCountry($vendor_result['country_id']);

			$this->load->model('localisation/zone');
			$zone = $this->model_localisation_zone->getZone($vendor_result['zone_id']);
			
			$this->data['vendor_id']	=	(int)$vendor_result['vendor_id'];
			$this->data['rating']		=	$rating;
			$this->data['feedback']		=	$this->language->get('text_feedback') . ' (' . (isset($total_feedback) ? $total_feedback : 0) . ') | ' . $this->language->get('text_order') . ' (' . (isset($total_order) ? $total_order : 0)  . ')';
			$this->data['thumb']		=   $image;
			$this->data['banner_thumb']	=   $banner_image;
			$this->data['store_name']	= 	$vendor_result['vendor_name'];
			$this->data['store_url']	=	$vendor_result['store_url'];
			$this->data['company']   	=	$vendor_result['company'];
			$this->data['telephone']   	=	$vendor_result['telephone'];
			$this->data['fax']   		=	$vendor_result['fax'];
			$this->data['email']   		=	$vendor_result['email'];
			$this->data['name']   		=	$vendor_result['firstname'] . ' ' . $vendor_result['lastname'];
			$this->data['location']   	=	'<img src="image/flags/' . strtolower($country['iso_code_2']) . '.png" />' . ' ' . $country['name']  . ' (' . (isset($zone['name']) ? $zone['name'] : $this->language->get('text_none')) . ') (' . $vendor_result['city'] . ')';
			$this->data['address_1']   	=	$vendor_result['address_1'];
			$this->data['address_2']   	=	$vendor_result['address_2'];
			$this->data['city']   		=	$vendor_result['city'];
			$this->data['postcode']   	=	$vendor_result['postcode'];
			$this->data['zone']   		=	isset($zone['name']) ? $zone['name'] : $this->language->get('text_none');
			$this->data['country']   	=	$country['name'];
			$this->data['description']  =	isset($vendor_result['vendor_description']) ? $vendor_result['vendor_description'] : $this->language->get('text_none');
		}
		
		$results = $this->model_catalog_vendorlogo->getProducts($data);
		$product_total = $this->model_catalog_vendorlogo->getTotalProducts($data); 
		
		$this->data['products'] = array();
		
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], $this->config->get('config_image_product_width'), $this->config->get('config_image_product_height'));
			} else {
				$image = false;
			}
						
			if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$price = false;
			}
					
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
			} else {
				$special = false;
			}
			
			if ($this->config->get('config_tax')) {
				$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
			} else {
				$tax = false;
			}
			
			if ($this->config->get('config_review_status')) {
				$rating = $result['rating'];
			} else {
				$rating = false;
			}
			
			$this->data['products'][] = array(
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
				'name'        => $result['name'],
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
				'price'       => $price,
				'special'     => $special,
				'tax'         => $tax,
				'rating'      => $result['rating'],
				'reviews'     => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
			);
		}
		
		$url = '';
	
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
							
		$this->data['sorts'] = array();
		
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=p.sort_order&order=ASC&vendor_id=' . $vendor_id . $url)
		);
			
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => 'pd.name-ASC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=pd.name&order=ASC&vendor_id=' . $vendor_id . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => 'pd.name-DESC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=pd.name&order=DESC&vendor_id=' . $vendor_id . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => 'p.price-ASC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=p.price&order=ASC&vendor_id=' . $vendor_id . $url)
		); 

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => 'p.price-DESC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=p.price&order=DESC&vendor_id=' . $vendor_id . $url)
		); 
			
		if ($this->config->get('config_review_status')) {
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => 'rating-DESC',
				'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=rating&order=DESC&vendor_id=' . $vendor_id . $url)
			); 
				
			$this->data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => 'rating-ASC',
				'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=rating&order=ASC&vendor_id=' . $vendor_id . $url)
			);
		}
			
		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_model_asc'),
			'value' => 'p.model-ASC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=p.model&order=ASC&vendor_id=' . $vendor_id . $url)
		);

		$this->data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => 'p.model-DESC',
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', '&sort=p.model&order=DESC&vendor_id=' . $vendor_id . $url)
		);
			
		$url = '';

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['vendor_id'])) {
			$url .= '&vendor_id=' . (int)$this->request->get['vendor_id'];
		}
			
		$this->data['limits'] = array();
		
		$this->data['limits'][] = array(
			'text'  => $this->config->get('config_catalog_limit'),
			'value' => $this->config->get('config_catalog_limit'),
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', $url . '&limit=' . $this->config->get('config_catalog_limit'))
		);
						
		$this->data['limits'][] = array(
			'text'  => 25,
			'value' => 25,
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', $url . '&vendor_id=' . $vendor_id . '&limit=25')
		);
			
		$this->data['limits'][] = array(
			'text'  => 50,
			'value' => 50,
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', $url . '&vendor_id=' . $vendor_id . '&limit=50')
		);

		$this->data['limits'][] = array(
			'text'  => 75,
			'value' => 75,
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', $url . '&vendor_id=' . $vendor_id . '&limit=75')
		);
			
		$this->data['limits'][] = array(
			'text'  => 100,
			'value' => 100,
			'href'  => $this->url->link('module/vendorlogo/getVendorProducts', $url . '&vendor_id=' . $vendor_id . '&limit=100')
		);
						
		$url = '';
	
		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}	

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}
		
		if (isset($this->request->get['vendor_id'])) {
			$url .= '&vendor_id=' . (int)$this->request->get['vendor_id'];
		}
		
		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}
				
		$pagination = new Pagination();
		$pagination->total = $product_total;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->text = $this->language->get('text_pagination');
		$pagination->url = $this->url->link('module/vendorlogo/getVendorProducts', $url . '&page={page}');
		$this->data['pagination'] = $pagination->render();
		
		$this->data['sort'] = $sort;
		$this->data['order'] = $order;
		$this->data['limit'] = $limit;
		$this->data['continue'] = $this->url->link('common/home');

		if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/probyven.tpl')) {
			$this->template = $this->config->get('config_template') . '/template/module/probyven.tpl';
		} else {
			$this->template = 'default/template/module/probyven.tpl';
		}
		
		$this->children = array(
				'common/column_left',
				'common/column_right',
				'common/content_top',
				'common/content_bottom',
				'common/footer',
				'common/header'
		);
		
		$this->response->setOutput($this->render());
  	}
}
?>