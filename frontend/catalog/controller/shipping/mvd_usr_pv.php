<?php
class ControllerShippingMVDUSRPV extends Controller { 

	public function getRates() {
        $_SESSION['ken_customer_selected_product_vendor_shipping_arr'] = array();
        unset($_SESSION['ken_customer_selected_product_vendor_shipping_arr']);
 		$this->language->load('shipping/mvd_usr_pv');
		
		$info = array();
		$cost = 0;
		$cost_tax = 0;
		$titles = '';
		
		$OrderID_Query = $this->db->query("SELECT order_id FROM `" . DB_PREFIX . "order` ORDER BY order_id DESC LIMIT 1");

		if ($OrderID_Query->num_rows) {
			$order_id = $OrderID_Query->row['order_id']+1;
		} else {
			$order_id = false;
		}
		
		if ($order_id) {
			$this->db->query("DELETE FROM `" . DB_PREFIX . "order_shipping` WHERE order_id = '" . (int)$order_id . "'");
		}

		foreach ($this->request->post as $value) {

            /**
             *
            0: Rex See [ JNE ] -  	//name
            1: 10 					//cost 1=4
            2: 3 					//vendor_id
            3: 4 					//courier_id
            4: 10 					//cost
            5: 2.00kg 				//weight
            6: JNE 					//title
             */
            $info = explode(':', $value);

            //validate the amount with session
            /*$final_courier_per_vendor[$final_courier['courier_id']] = array(
                'vendor_id'			=> $uni_vendor['vendor'],
                'vendor_name'		=> $fc_vname,
                'name' 				=> $getCourierData['courier_name'],
                'description'		=> $getCourierData['description'],
                'courier_id' 		=> $final_courier['courier_id'],
                'thumb' 			=> $this->model_tool_image->resize($this->getCourierImage($final_courier['courier_id']),'88','30'),
                'shipping_weight'	=> $total_weight,
                'shipping_rate' 	=> $total_shipping,
                'tax_class_id' 		=> $this->config->get('mvd_usr_pv_tax_class_id'),
                'text_weight'		=> $this->weight->format($total_weight, $this->config->get('config_weight_class_id')),
                'text_rate'			=> $this->currency->format($this->tax->calculate($total_shipping, $this->config->get('mvd_usr_pv_tax_class_id'), $this->config->get('config_tax')))
            );*/
            if($_SESSION['ken_final_courier_per_vendor'][$info[3]]['shipping_rate'] == $info[4])
            {

                //from the post value will know user select which shipping method, and update
                $_SESSION['ken_customer_selected_product_vendor_shipping_arr'][$info[2]] = $info[3];

                if ($info)
                {
                    if ($this->config->get('mvd_usr_pv_tax_class_id'))
                    {
                        $shipping_tax = $this->tax->calculate($info[4], $this->config->get('mvd_usr_pv_tax_class_id'), $this->config->get('config_tax')) - $info[4];
                    } else
                    {
                        $shipping_tax = '0';
                    }

                    if ($titles)
                    {
                        $titles .= $info[0] . $this->currency->format($info[1] + $shipping_tax) . ' : <br/>';
                    } else
                    {
                        $titles = $info[0] . $this->currency->format($info[1] + $shipping_tax) . ' : <br/>';
                    }
                }

                $cost += $info[4];
                $cost_tax += $info[4] + $shipping_tax;
                $this->db->query("INSERT INTO " . DB_PREFIX . "order_shipping SET order_id = '" . (int) $order_id . "', vendor_id =  '" . (int) $info[2] . "', title = '" . $info[6] . "', tax =  '" . (float) $shipping_tax . "', cost =  '" . (float) $info[4] . "', weight = '" . (float) $info[5] . "'");
            }else
            {
                //user attempted to change the shipping cost
                //error log
                $log = new Log(date("Y-m-d") . '-shipping-hack.log');
                $log->write(date("Y-m-d H:s:i") . ' ' . $this->customer->getId());
                $log->write('[CLOSED]');
            }
		}

		if (($titles) && ($shipping_tax > '0')) {
			$titles .= $this->language->get('text_ship_amount_tax') . $this->currency->format($cost_tax) . ' ';
		} else {
			$titles .= $this->language->get('text_ship_amount') . $this->currency->format($cost) . ' ';
		}
		
		$this->session->data['shipping_methods']['mvd_usr_pv']['quote']['mvd_usr_pv']['title'] = $titles;
		$this->session->data['shipping_methods']['mvd_usr_pv']['quote']['mvd_usr_pv']['cost'] = $cost;
		$this->session->data['shipping_methods']['mvd_usr_pv']['quote']['mvd_usr_pv']['text'] = $this->currency->format($cost);

		if ($this->config->get('mvd_usr_pv_tax_class_id') && ($shipping_tax > '0')) {
			echo $this->language->get('text_title_2') . $this->currency->format($cost_tax);
		} else {
			echo $this->language->get('text_title_2') . $this->currency->format($cost);
		}
  	}
}