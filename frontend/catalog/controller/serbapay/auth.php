<?php
use \Firebase\JWT\JWT;
use \Firebase\JWT\ExpiredException;

class ControllerSerbapayAuth extends Controller {

    private $_config = array();
    private $_redirect;

    public function index() {

        $this->_prepare();

        // Check if Logged
        if ($this->customer->isLogged()) {
            //$this->redirect($this->_redirect);
            //After vincent implement iframe, now need to refresh the outside iframe.
            echo '<script type="text/javascript"> window.top.location.reload();  </script>';
            return;
        }

        // Check if module is not Enabled direct back the original page, means it is not working
        if (!$this->config->get('serbapay_auth_status')) {
            $this->redirect($this->_redirect);
        }

        // Dependencies
        $this->language->load('serbapay/auth');
        require_once(DIR_SYSTEM . 'library/serbapay/Auth.php');
        $this->load->model('serbapay/auth');

        // Load Config
        $this->_config['base_url']   = HTTPS_SERVER . 'serbapayauth.php';
        $this->_config['debug_file'] = DIR_SYSTEM . 'logs/serbapayauth.txt';
        $this->_config['debug_mode'] = (bool) $this->config->get('serbapay_auth_debug');
        $this->_config['lang']  = $this->language->get('code');

        $settings = $this->config->get('serbapay_auth');
        foreach ($settings as $config) {
            $this->_config['providers'][$config['provider']] = array('enabled' => (bool) $config['enabled'],
                                                                     'keys'    => array('id'     => $config['key'],
                                                                                        'key'    => $config['key'],
                                                                                        'secret' => $config['secret'],
                                                                                        'scope'  => $config['scope']));
        }


        // Receive request
        if (isset($this->request->get['provider'])) {
            $provider = $this->request->get['provider'];
        } else {

            // Save error to the System Log
            $this->log->write('Missing application provider.');

            // Set Message
            $this->session->data['error'] = sprintf("An error occurred, please <a href=\"%s\">notify</a> the administrator.",
                                                    $this->url->link('information/contact'));

            // Redirect to the Login Page
            //$this->redirect($this->_redirect);
			//Ken Custom Mod - iframe reload top parent
			echo '<script type="text/javascript"> window.top.location.reload();  </script>';
            return;
        }

        try {

            // Authentication Begin
            $auth = new Serbapay_Auth($this->_config);
            $adapter = $auth->authenticate($provider);
            $user_profile = $adapter->getUserProfile();

            // 1 - check if user already have authenticated using this provider before
            $customer_id = $this->model_serbapay_auth->findCustomerByIdentifier($provider, $user_profile->identifier);

            if ($customer_id) {
                //$this->log->write('If got $customer_id');
                // 1.1 Login
                $this->model_serbapay_auth->login($customer_id);

                // 1.2 Redirect to Refer Page
                //$this->redirect($this->_redirect);
				
				//Ken Custom Mod - iframe reload top parent
				echo '<script type="text/javascript"> window.top.location.reload();  </script>';
                return;
            }
            //$this->log->write('NOT MAKE IT. If got $customer_id');

            // 2 - else, here lets check if the user email we got from the provider already exists in our database ( for this example the email is UNIQUE for each user )
            // if authentication does not exist, but the email address returned  by the provider does exist in database,
            // then we tell the user that the email  is already in use
            // but, its up to you if you want to associate the authentication with the user having the address email in the database
            if ($user_profile->email){
                //$this->log->write('If got $customer email but not every user got email because using serbapay');
                $customer_id = $this->model_serbapay_auth->findCustomerByEmail($user_profile->email);

                if ($customer_id) {
                    //$this->session->data['success'] = sprintf($this->language->get('text_provider_email_already_exists'), $provider, $user_profile->email);
                    //$this->redirect($this->url->link('account/login'));
					
					//Ken Custom Mod
					echo '<script type="text/javascript"> window.top.location.reload();  </script>';
                    return;
                }
            }
            //$this->log->write('NOT MAKE IT. If got $customer email but not every user got email because using serbapay');

            // 3 - if authentication does not exist and email is not in use, then we create a new user
            $user_address = array();

            if (!empty($user_profile->address)) {
                $user_address[] = $user_profile->address;
            }

            if (!empty($user_profile->region)) {
                $user_address[] = $user_profile->region;
            }

            if (!empty($user_profile->country)) {
                $user_address[] = $user_profile->country;
            }

            // 3.1 - create new customer
            $customer_id = $this->model_serbapay_auth->addCustomer(
                array('email'      => $user_profile->email,
                      'firstname'  => $user_profile->full_name,
                      'lastname'   => '',
                      'telephone'  => $user_profile->phone,
                      'fax'        => false,
                      'newsletter' => true,
                      'company'    => false,
                      'address_1'  => ($user_address ? implode(', ', $user_address) : false),
                      'address_2'  => false,
                      'city'       => '1235',
                      'postcode'   => $user_profile->zip,
                      'country_id' => '100',
                      'zone_id'    => '1513',
                      'password'   => substr(rand().microtime(), 0, 6)));

            // 3.2 - create a new authentication for him/her
            $this->model_serbapay_auth->addAuthentication(
                array('customer_id' => (int) $customer_id,
                    'provider' => $provider,
                    'identifier' => $user_profile->identifier,
                    'web_site_url' => $user_profile->webSiteURL,
                    'profile_url' => $user_profile->profileURL,
                    'photo_url' => $user_profile->photoURL,
                    'display_name' => $user_profile->displayName,
                    'description' => $user_profile->description,
                    'first_name' => $user_profile->full_name,
                    'last_name' => '',
                    'gender' => $user_profile->gender,
                    'language' => $user_profile->language,
                    'age' => $user_profile->age,
                    'birth_day' => $user_profile->birthDay,
                    'birth_month' => $user_profile->birthMonth,
                    'birth_year' => $user_profile->birthYear,
                    'email' => $user_profile->email,
                    'email_verified' => $user_profile->emailVerified,
                    'phone' => $user_profile->phone,
                    'address' => $user_profile->address,
                    'country' => $user_profile->country,
                    'region' => $user_profile->region,
                    'city' => $user_profile->city,
                    'zip' => $user_profile->zip));

            // 3.3 - login
            $this->model_serbapay_auth->login($customer_id);

            // 3.4 - redirect to Refer Page
            //$this->redirect($this->_redirect);

            //Ken - Go to Account Information to fill in Email and Telephone field

						
			//Ken Custom Mod
			echo '<script type="text/javascript"> window.top.location.reload();  </script>';
            return;
			
       } catch (Exception $e) {

            // Error Descriptions
            switch ($e->getCode()){
                case 0 : $error = "Unspecified error."; break;
                case 1 : $error = "Hybriauth configuration error."; break;
                case 2 : $error = "Provider not properly configured."; break;
                case 3 : $error = "Unknown or disabled provider."; break;
                case 4 : $error = "Missing provider application credentials."; break;
                case 5 : $error = "Authentication failed. The user has canceled the authentication or the provider refused the connection."; break;
                case 6 : $error = "User profile request failed. Most likely the user is not connected to the provider and he should to authenticate again.";
                         $adapter->logout();
                         break;
                case 7 : $error = "User not connected to the provider.";
                         $adapter->logout();
                         break;
            }

            $error .= "\n\nSerbapayAuth Error: " . $e->getMessage();
            $error .= "\n\nTrace:\n " . $e->getTraceAsString();

            $this->log->write($error);
       }
    }

    public function cancel(){
        echo '<script type="text/javascript"> window.top.location.reload();  </script>';
        return;
    }
    
    
    private function _prepare() {

        // Some API returns encoded URL
        if (isset($this->request->get) && isset($_GET)) {

            // Prepare for OpenCart
            foreach ($this->request->get as $key => $value) {
                $this->request->get[str_replace('amp;', '', $key)] = $value;
            }

            // Prepare for Library
            foreach ($_GET as $key => $value) {
                $_GET[str_replace('amp;', '', $key)] = $value;
            }
        }

        // Base64 URL Decode
        if (isset($this->request->get['redirect'])) {
            $this->_redirect = base64_decode($this->request->get['redirect']);
        } else {
            $this->_redirect = $this->url->link('account/account');
        }
    }
    
}
