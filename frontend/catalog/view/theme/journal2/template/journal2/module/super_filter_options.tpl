<?php /*
<div class="box sf-option sf-<?php echo $option['display_mode']; ?> sf-option-<?php echo $option['option_id']; ?> sf-<?php echo $option['type']; ?>">
    <div class="box-heading">Shop by <?php echo $option['option_name']; ?></div>
    <div class="box-content">
        <ul class="<?php echo $this->journal2->settings->get('filter_show_box') ? '' : 'hide-checkbox'; ?>">
            <?php foreach ($option['values'] as $value) { ?>
            <?php if ($option['display_mode'] === 'list'): ?>
            <li><label><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><span class="sf-name"><?php echo $value['option_value_name']; ?></span> </label></li>
            <?php elseif ($option['display_mode'] === 'image'): ?>
            <li><label><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><img width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" src="<?php echo $value['image']; ?>" alt="<?php echo $value['option_value_name']; ?>" title="<?php echo $value['option_value_name']; ?>"/></label></li>
            <?php else: ?>
            <li><label><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><img width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" src="<?php echo $value['image']; ?>" alt="<?php echo $value['option_value_name']; ?>"/><span class="sf-name"><?php echo $value['option_value_name']; ?></span> </label></li>
            <?php endif; ?>
            <?php } ?>
        </ul>
    </div>
</div>
*/ ?>
<div class="box-multi-dropdown box sf-option sf-<?php echo $option['display_mode']; ?> sf-option-<?php echo $option['option_id']; ?> sf-<?php echo $option['type']; ?>">
	<div class="box-heading">Shop by <?php echo $option['option_name']; ?></div>
	
	<dl class="dropdown">	  
		<dt>
			<a href="javascript:void(0);">
			  <span class="hida">Locations</span>
			  <i class="fa fa-caret-down"></i>
			</a>
		</dt>
	  
		<dd>
			<div class="mutliSelect scrollable-container">
				<ul>
					<?php foreach ($option['values'] as $value) { ?>
						<?php if ($option['display_mode'] === 'list'): ?>
							<li><label class="clearfix"><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><span class="sf-name"><?php echo $value['option_value_name']; ?></span> </label></li>
						<?php elseif ($option['display_mode'] === 'image'): ?>
							<li><label><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><img width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" src="<?php echo $value['image']; ?>" alt="<?php echo $value['option_value_name']; ?>" title="<?php echo $value['option_value_name']; ?>"/></label></li>
						<?php else: ?>
							<li><label><input data-keyword="<?php echo $value['keyword']?>" type="checkbox" name="option[<?php echo $option['option_id']?>]" value="<?php echo $value['option_value_id']; ?>"><img width="<?php echo $image_width; ?>" height="<?php echo $image_height; ?>" src="<?php echo $value['image']; ?>" alt="<?php echo $value['option_value_name']; ?>"/><span class="sf-name"><?php echo $value['option_value_name']; ?></span> </label></li>
						<?php endif; ?>
					<?php } ?>
				</ul>
			</div>
		</dd>
	</dl>
</div>

