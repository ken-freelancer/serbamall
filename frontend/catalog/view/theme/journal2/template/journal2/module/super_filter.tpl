<div id="journal-super-filter-<?php echo $module_id; ?>"
     class="journal-sf"
     data-filters-action="<?php echo $filter_action_url; ?>"
     data-products-action="<?php echo $products_action_url; ?>"
     data-route="<?php echo $route; ?>"
     data-path="<?php echo $path; ?>"
     data-manufacturer="<?php echo $manufacturer_id; ?>"
     data-search="<?php echo $search; ?>"
     data-tag="<?php echo $tag; ?>"
     data-vendor="<?php echo $vendor_id; ?>"
     data-loading-text="<?php echo $loading_text; ?>"
     data-currency-left="<?php echo $currency_left; ?>"
     data-currency-right="<?php echo $currency_right; ?>"
     data-currency-decimal="<?php echo $currency_decimal; ?>"
     data-currency-thousand="<?php echo $currency_thousand; ?>"
     data-st="E.R.">
    <?php if ($reset): ?>
    <a class="sf-reset hint--top sf-<?php echo $this->journal2->settings->get('filter_reset_display'); ?>" data-hint="<?php echo $reset_btn_text; ?>"><span class="sf-reset-text"><?php echo $reset_btn_text; ?></span><i class="sf-reset-icon"></i></a>
    <?php endif; ?>
    <input type="hidden" class="sf-page" value="" />
    <?php foreach ($filter_groups as $filter_group): ?>
    <?php echo $filter_group['html']; ?>
    <?php endforeach; ?>

    <?php /*mv vendor name */ ?>
	<?php /*
    <div class="box sf-vendor">
        <div class="box-heading">Merchants</div>
        <div class="box-content">
            <select>
                <?php foreach ($vendor_data as $vendor): ?>
                <option value=""><?php echo $vendor['vendor_name']?></option>
                <?php endforeach; ?>
            </select>
        </div>
    </div>
	*/ ?>
	
	<div class="box-multi-dropdown box sf-option sf-vendor">
		<div class="box-heading">Shop by Merchant</div>
		
		<dl class="dropdown">	  
			<dt>
				<a href="javascript:void(0);">
				  <span class="hida">Merchants</span>
				  <i class="fa fa-caret-down"></i>
				</a>
			</dt>		  
			<dd>
				<div class="mutliSelect scrollable-container">
					<ul>
						<?php foreach ($vendor_data as $vendor){ ?>
							<li><label class="clearfix"><input data-keyword="<?php echo strtolower($vendor['vendor_name']); ?>" type="checkbox" name="vendor" value="vendor<?php echo $vendor['vendor_id']; ?>" <?php echo (isset($_GET['vendor_id']) && $_GET['vendor_id'] == $vendor['vendor_id'] ? 'checked' : ''); ?>><span class="sf-name"><?php echo $vendor['vendor_name']; ?></span> </label></li>
						<?php } ?>
					</ul>
				</div>
			</dd>
		</dl>
	</div>
	
    <?php /*.mv vendor name */ ?>
	
</div>
<?php if (!$ajax) { ?>
<script>
    Journal.SuperFilter.init($('#journal-super-filter-<?php echo $module_id; ?>'));
</script>
<?php } ?>