<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_vendor_info; ?></h1>
<div class="box">
  <div class="box-heading"><?php echo $heading_vendor_info; ?></div>
  <div class="box-content">
      <?php //echo '<pre>'; print_r($package); echo '</pre>';  ?>
	  
	  <?php 
		$col = count($package); 
		
		$col_class = 'lg-25 md-25 sm-45 xs-45';
		
		if($col == 2) { $col_class = 'lg-20 md-20 sm-40 xs-100'; }
		if($col == 3) { $col_class = 'lg-30 md-30 sm-30 xs-100'; }
		if($col == 4) { $col_class = 'lg-25 md-25 sm-45 xs-100'; }
	  ?>
	  
	  <div class="row-fluid">
		  <!-- Row2 start -->
			
			<?php foreach($package as $key=>$a_Package){ ?>
				
				<div class="<?php echo $col_class; ?> PlanPricing template4 <?php echo ($key == 2 ? 'preset' : ''); ?>"> 
				  <div class="planName"> <span class="price">RM <?php echo $a_Package['commission']; ?></span>
					<h3><?php echo $a_Package['commission_name']; ?></h3>
					<p><?php echo $a_Package['duration']; ?> Monthly Plan</p>
				  </div>
				  <div class="planFeatures">
					<ul>
						<li><img src="<?php echo $this->config->get('config_url'); ?>catalog/view/theme/journal2/image/<?php echo $key; ?>.jpg"></li>
						<li>Pellentesque odio nisi, euismod in.</li>
						<li><span class="label">Product Limit:</span> <?php echo $a_Package['product_limit']; ?></li>
						<?php if(count($a_Package['option']) > 0 ){ ?>
							<?php foreach($a_Package['option'] as $opt){ ?>
								<li><span class="label"><?php echo $opt['label_text']; ?>:</span> <?php echo $opt['label_value']; ?></li>
							<?php } ?>
						<?php } ?>
					</ul>
				  </div>
				  <p> <a href="<?php echo $a_Package['signup_href']; ?>" role="button" data-toggle="modal" class="btn btn-success btn-large">Sign Up </a> </p>
				</div>
				
			<?php } ?>
		  
		  </div>  <!-- Row2 ends -->
  
	  
  </div>
</div>
</div>
<?php echo $footer; ?>
