<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_all_vendors; ?></h1>

  <?php if ($vendor_data) { ?>
  <div class="vendor-filter">
    <div class="display"><b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display('grid');"><?php echo $text_grid; ?></a></div>
  </div><br/>
  <div class="product-list">
    <?php foreach ($vendor_data as $vendor) { ?>
    <div>
      <?php if ($vendor['thumb']) { ?>
      <div class="image"><a href="<?php echo $vendor['href']; ?>"><img src="<?php echo $vendor['thumb']; ?>" title="<?php echo $vendor['name']; ?>" alt="<?php echo $vendor['name']; ?>" /></a></div>
      <?php } ?>
      <div class="name"><a href="<?php echo $vendor['href']; ?>"><?php echo $vendor['name']; ?></a></div>
      <div class="description"><?php echo $vendor['description']; ?></div>
	  <div class="feedback" style="font-size:80%;text-align:center;color:#666"><?php echo $vendor['review']; ?></div>
	  <?php if ($this->config->get('vendor_review')) { ?>
      <?php if ($vendor['rating']) { ?>
      <div class="rating"><img src="catalog/view/theme/default/image/star/stars-<?php echo $vendor['rating']; ?>.png" alt="<?php echo $vendor['rating']; ?>" /></div>
      <?php } ?>
	  <?php } ?>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$vendor_data) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
function display(view) {
	if (view == 'list') {
		$('.product-grid').attr('class', 'product-list');
		
		$('.product-list > div').each(function(index, element) {
			
			html = '<div class="left">';
			
			var image = $(element).find('.image').html();
			
			if (image != null) { 
				html += '<div class="image">' + image + '</div>';
			}
								
			html += '  <div class="name">' + $(element).find('.name').html() + '</div>';
			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';
			html += '  <div class="feedback" style="font-size:80%;text-align:left;color:#666">' + $(element).find('.feedback').html() + '</div>';
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
				
			html += '</div>';
						
			$(element).html(html);
		});		
		
		$('.display').html('<b><?php echo $text_display; ?></b> <?php echo $text_list; ?> <b>/</b> <a onclick="display(\'grid\');"><?php echo $text_grid; ?></a>');
		
		$.totalStorage('display', 'list'); 
	} else {
		$('.product-list').attr('class', 'product-grid');
		
		$('.product-grid > div').each(function(index, element) {
			html = '';
			
			var image = $(element).find('.image').html();
			
			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
			
			html += '<div class="name" style="text-align:center;font-weight:bold">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';					
			html += '<div class="feedback" style="font-size:80%;text-align:center;color:#666;">' + $(element).find('.feedback').html() + '</div>';
			
			var rating = $(element).find('.rating').html();
			
			if (rating != null) {
				html += '<div class="rating" style="text-align:center">' + rating + '</div>';
			}			
			
			$(element).html(html);
		});	
					
		$('.display').html('<b><?php echo $text_display; ?></b> <a onclick="display(\'list\');"><?php echo $text_list; ?></a> <b>/</b> <?php echo $text_grid; ?>');
		
		$.totalStorage('display', 'grid');
	}
}

view = $.totalStorage('display');

if (view) {
	display(view);
} else {
	display('list');
}
//--></script> 
<?php echo $footer; ?>