<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
		<?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php echo $column_left; ?><?php echo $column_right; ?>

<div id="content" class="vendor-page">

	<?php echo $content_top; ?>
  
  <!--add-->
  <div class="box-content">
	
	<div class="div_vendor_info" >
		<div class="xs-100 sm-50 md-50 lg-50 xl-50 div_vendor_name" >
			<h1><?php echo $store_name; ?></h1>
		</div>
		<div class="xs-100 sm-50 md-50 lg-50 xl-50 div_info" >
			<?php echo $address_1; ?><br>
			<?php echo $address_2; ?><br>
			<?php echo $postcode; ?> <?php echo $city; ?> <br>
			<?php echo $zone; ?>, <?php echo $country; ?> 
		</div>
		<div class="clearfix"></div>
	</div>
	
	<div class="div_vendor_banner">
		<?php if ($banner_thumb) { ?>
			<div class="image"><img src="<?php echo $banner_thumb; ?>" alt="<?php echo $store_name; ?>" /></div>
		<?php } ?>
	</div>
	
	<div class="div_vendor_info_gallery" >
		<div class="xs-100 sm-100 md-100 lg-100 xl-100 div_gallery image-gallery" >
			<?php if(isset($vendor_gallery) && count($vendor_gallery) > 0) {?>
				<ul>
				<?php foreach($vendor_gallery as $gallery_image){ ?>
					<li><a href="<?php echo $gallery_image['image']; ?>" class="swipebox"><img src="<?php echo $gallery_image['thumb']; ?>" itemprop="image" /></a></li>
				<?php } ?>
				</ul>
			<?php } ?>
		</div>
	</div>
	<?php /*
    <table>
		<tr>
		  <td><?php if ($thumb) { ?>
			<div class="image" style="border-top:1px solid #E7E7E7;border-left:1px solid #E7E7E7;border-right:1px solid #E7E7E7;border-bottom:1px solid #E7E7E7;"><img src="<?php echo $thumb; ?>" alt="<?php echo $store_name; ?>" /></div>
		    <?php } ?>
		  </td>
          <td></td>
        </tr>
		<tr>
		  <td>
		  </td>
          <td></td>
        </tr>
		<tr></tr>
		<tr>
          <td><b><?php echo $text_store_name; ?> : </b></td>
          <td><?php echo $store_name; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_company; ?> : </b></td>
          <td><?php echo $company; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_name; ?> : </b></td>
          <td><?php echo $name; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_location; ?> : </b></td>
          <td style="font-size:100%;color:#666"><?php echo $location; ?></td>
        </tr>		
		<tr>
          <td><b><?php echo $text_vendor_rating; ?> : </b></td>
          <td><div class="rating"><img src="catalog/view/theme/default/image/star/stars-<?php echo $rating; ?>.png" alt="<?php echo $rating; ?>" /> <label style="font-size:100%;color:#666"><?php echo $feedback; ?></label></div></td>
        </tr>
		<?php if ($this->config->get('vendor_information')) { ?>		
		<tr>
          <td><b><?php echo $text_fax; ?> : </b></td>
          <td><?php echo $fax; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_email; ?> : </b></td>
          <td><?php echo $email; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_address_1; ?> : </b></td>
          <td><?php echo $address_1; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_address_2; ?> : </b></td>
          <td><?php echo $address_2; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_city; ?> : </b></td>
          <td><?php echo $city; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_postcode; ?> : </b></td>
          <td><?php echo $postcode; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_country; ?> : </b></td>
          <td><?php echo $country; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_zone; ?> : </b></td>
          <td><?php echo $zone; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_store_url; ?> : </b></td>
          <td><a href="<?php echo $store_url; ?>" style="text-decoration:none"><?php echo $store_url; ?></a></td>
        </tr>
		<?php } ?>
		<tr>
          <td><b><?php echo $text_description; ?> : </b></td>
          <td><?php echo $description; ?></td>
        </tr>
		<tr>
	   </tr>
	</table>
	*/ ?>
  </div>
  <!--end add-->
  <?php //$store_name; ?>

  <?php if ($products) { ?>
  <div class="product-filter">
    <div class="display">
		<a onclick="display('grid');" class="grid-view"><?php echo $this->journal2->settings->get("category_grid_view_icon", $text_grid); ?></a>
	</div>
    <div class="product-compare"><a href="<?php echo $compare; ?>" id="compare-total"><?php echo $text_compare; ?></a></div>
    <div class="limit"><b><?php echo $text_limit; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($limits as $limits) { ?>
        <?php if ($limits['value'] == $limit) { ?>
        <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
    <div class="sort"><b><?php echo $text_sort; ?></b>
      <select onchange="location = this.value;">
        <?php foreach ($sorts as $sorts) { ?>
        <?php if ($sorts['value'] == $sort . '-' . $order) { ?>
        <option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $sorts['text']; ?></option>
        <?php } else { ?>
        <option value="<?php echo $sorts['href']; ?>"><?php echo $sorts['text']; ?></option>
        <?php } ?>
        <?php } ?>
      </select>
    </div>
  </div>
  <div class="main-products product-list">
    <?php foreach ($products as $product) { ?>
    <div>
      <?php if ($product['thumb']) { ?>
        <div class="image">
            <a href="<?php echo $product['href']; ?>" <?php if(isset($product['thumb2']) && $product['thumb2']): ?> class="has-second-image" style="background: url('<?php echo $product['thumb2']; ?>') no-repeat;" <?php endif; ?>>
                <img class="lazy first-image" width="<?php echo $this->journal2->settings->get('config_image_width'); ?>" height="<?php echo $this->journal2->settings->get('config_image_height'); ?>" src="<?php echo $this->journal2->settings->get('product_dummy_image'); ?>" data-src="<?php echo $product['thumb']; ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
            </a>
            <?php if (isset($product['labels']) && is_array($product['labels'])): ?>
            <?php foreach ($product['labels'] as $label => $name): ?>
            <?php if ($label === 'outofstock'): ?>
            <img class="outofstock" <?php echo Journal2Utils::getRibbonSize($this->journal2->settings->get('out_of_stock_ribbon_size')); ?> style="position: absolute; top: 0; left: 0" src="<?php echo Journal2Utils::generateRibbon($name, $this->journal2->settings->get('out_of_stock_ribbon_size'), $this->journal2->settings->get('out_of_stock_font_color'), $this->journal2->settings->get('out_of_stock_bg')); ?>" alt="" />
            <?php else: ?>
            <span class="label-<?php echo $label; ?>"><b><?php echo $name; ?></b></span>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>
            <?php if($this->journal2->settings->get('product_grid_wishlist_icon_position') === 'image' && $this->journal2->settings->get('product_grid_wishlist_icon_display', '') === 'icon'): ?>
                <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_wishlist; ?>"><i class="wishlist-icon"></i><span class="button-wishlist-text"><?php echo $button_wishlist;?></span></a></div>
                <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_compare; ?>"><i class="compare-icon"></i><span class="button-compare-text"><?php echo $button_compare;?></span></a></div>
            <?php endif; ?>
        </div>
      <?php } else { ?>
        <div class="image">
            <a href="<?php echo $product['href']; ?>">
                <img class="first-image" width="<?php echo $this->journal2->settings->get('config_image_width'); ?>" height="<?php echo $this->journal2->settings->get('config_image_height'); ?>" src="<?php echo $this->journal2->settings->get('product_no_image'); ?>" title="<?php echo $product['name']; ?>" alt="<?php echo $product['name']; ?>" />
            </a>
            <?php if (isset($product['labels']) && is_array($product['labels'])): ?>
            <?php foreach ($product['labels'] as $label => $name): ?>
            <?php if ($label === 'outofstock'): ?>
            <img class="outofstock" <?php echo Journal2Utils::getRibbonSize($this->journal2->settings->get('out_of_stock_ribbon_size')); ?> style="position: absolute; top: 0; left: 0" src="<?php echo Journal2Utils::generateRibbon($name, $this->journal2->settings->get('out_of_stock_ribbon_size'), $this->journal2->settings->get('out_of_stock_font_color'), $this->journal2->settings->get('out_of_stock_bg')); ?>" alt="" />
            <?php else: ?>
            <span class="label-<?php echo $label; ?>"><b><?php echo $name; ?></b></span>
            <?php endif; ?>
            <?php endforeach; ?>
            <?php endif; ?>
            <?php if($this->journal2->settings->get('product_grid_wishlist_icon_position') === 'image' && $this->journal2->settings->get('product_grid_wishlist_icon_display', '') === 'icon'): ?>
                <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_wishlist; ?>"><i class="wishlist-icon"></i><span class="button-wishlist-text"><?php echo $button_wishlist;?></span></a></div>
                <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_compare; ?>"><i class="compare-icon"></i><span class="button-compare-text"><?php echo $button_compare;?></span></a></div>
            <?php endif; ?>
        </div>
      <?php } ?>
      <div class="name"><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></div>
      <div class="description"><?php echo $product['description']; ?></div>
      <?php if ($product['price']) { ?>
      <div class="price">
        <?php if (!$product['special']) { ?>
        <?php echo $product['price']; ?>
        <?php } else { ?>
        <span class="price-old"><?php echo $product['price']; ?></span> <span class="price-new" <?php echo isset($product['date_end']) && $product['date_end'] ? "data-end-date='{$product['date_end']}'" : ""; ?>><?php echo $product['special']; ?></span>
        <?php } ?>
        <?php if ($product['tax']) { ?>
        <span class="price-tax"><?php echo $text_tax; ?> <?php echo $product['tax']; ?></span>
        <?php } ?>
      </div>
      <?php } ?>
      <?php if ($product['rating']) { ?>
      <div class="rating"><img width="83" height="15" src="<?php echo Journal2Utils::staticAsset("catalog/view/theme/default/image/stars-{$product['rating']}.png"); ?>" alt="<?php echo $product['reviews']; ?>" /></div>
      <?php } ?>

      <?php if (Journal2Utils::isEnquiryProduct($this, $product['product_id'])): ?>
      <div class="cart enquiry-button">
        <a href="<?php echo $this->journal2->settings->get('enquiry_popup_code'); ?>" data-clk="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top" data-hint="<?php echo $this->journal2->settings->get('enquiry_button_text'); ?>"><?php echo $this->journal2->settings->get('enquiry_button_icon') . '<span class="button-cart-text">' . $this->journal2->settings->get('enquiry_button_text') . '</span>'; ?></a>
      </div>
      <?php else: ?>
      <div class="cart <?php echo isset($product['labels']) && is_array($product['labels']) && isset($product['labels']['outofstock']) ? 'outofstock' : ''; ?>">
        <a onclick="addToCart('<?php echo $product['product_id']; ?>');" class="button hint--top" data-hint="<?php echo $button_cart; ?>"><i class="button-left-icon"></i><span class="button-cart-text"><?php echo $button_cart; ?></span><i class="button-right-icon"></i></a>
      </div>
      <?php endif; ?>
      <div class="wishlist"><a onclick="addToWishList('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_wishlist; ?>"><i class="wishlist-icon"></i><span class="button-wishlist-text"><?php echo $button_wishlist;?></span></a></div>
      <div class="compare"><a onclick="addToCompare('<?php echo $product['product_id']; ?>');" class="hint--top" data-hint="<?php echo $button_compare; ?>"><i class="compare-icon"></i><span class="button-compare-text"><?php echo $button_compare;?></span></a></div>
    </div>
    <?php } ?>
  </div>
  <div class="pagination"><?php echo $pagination; ?></div>
  <?php } ?>
  <?php if (!$products) { ?>
  <div class="content"><?php echo $text_empty; ?></div>
  <div class="buttons">
    <div class="right"><a href="<?php echo $continue; ?>" class="button"><?php echo $button_continue; ?></a></div>
  </div>
  <?php } ?>

    <?php  //Ken - Vendor Review ?>
    <div class="review">
        <div style="text-align: right;">Average rating: <img src="catalog/view/theme/default/image/stars-<?php echo $rating; ?>.png" alt="Review" />&nbsp;&nbsp;</div>
    </div>

    <div id="tab-review" class="tab-content">
        <div id="review"></div>
        <h2 id="review-title">Write a review</h2>
        <b>Your Name:</b><br />
        <input type="text" name="name" value="" />
        <br />
        <br />
        <b>Your Review:</b>
        <textarea name="text" cols="40" rows="8" style="width: 98%;"></textarea>
        <span style="font-size: 11px;">Note: HTML is not translated!</span><br />
        <br />
        <b>Rating:</b> <span>Bad</span>&nbsp;
        <input type="radio" name="rating" value="1" />
        &nbsp;
        <input type="radio" name="rating" value="2" />
        &nbsp;
        <input type="radio" name="rating" value="3" />
        &nbsp;
        <input type="radio" name="rating" value="4" />
        &nbsp;
        <input type="radio" name="rating" value="5" />
        &nbsp;<span>Good</span><br />
        <br />
        <b>Enter the code in the box below:</b><br />
        <input type="text" name="captcha" value="" />
        <br />
        <img src="index.php?route=module/vendorreview/captcha" alt="" id="captcha" /><br />
        <br />
        <div class="buttons">
            <div class="right"><a id="button-review" class="button">Submit Your Review</a></div>
        </div>
    </div>
    <?php  //.Ken - Vendor Review ?>

  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
display('grid');
function display(view) {
	if (view == 'list') {
		$('.main-products.product-grid').attr('class', 'main-products product-list');
        $('.display a.grid-view').removeClass('active');
        $('.display a.list-view').addClass('active');

		$('.main-products.product-list > div').each(function(index, element) {
            if ($(this).hasClass('sf-loader')) return;
            $(this).attr('class','product-list-item xs-100 sm-100 md-100 lg-100 xl-100').attr('data-respond','start: 150px; end: 300px; interval: 10px;');

            var html = '';

			html += '<div class="left">';

			var image = $(element).find('.image').html();

			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}
            html += '  <div class="name">' + $(element).find('.name').html() + '</div>';

			var price = $(element).find('.price').html();

			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}

			html += '  <div class="description">' + $(element).find('.description').html() + '</div>';

            var rating = $(element).find('.rating').html();

            if (rating != null) {
                html += '<div class="rating">' + rating + '</div>';
            }

            html += '</div>';

            html += '<div class="right">';
            html += '  <div class="' + $(element).find('.cart').attr('class') + '">' + $(element).find('.cart').html() + '</div>';
            html += '  <div class="wishlist">' + $(element).find('.wishlist').html() + '</div>';
            html += '  <div class="compare">' + $(element).find('.compare').html() + '</div>';
            html += '</div>';

			$(element).html(html);
		});

		$.totalStorage('display', 'list');
	} else {
		$('.main-products.product-list').attr('class', 'main-products product-grid');
        $('.display a.grid-view').addClass('active');
        $('.display a.list-view').removeClass('active');

		$('.main-products.product-grid > div').each(function(index, element) {
            if ($(this).hasClass('sf-loader')) return;
            $(this).attr('class',"product-grid-item <?php echo $this->journal2->settings->get('product_grid_classes'); ?> display-<?php echo $this->journal2->settings->get('product_grid_wishlist_icon_display'); ?> <?php echo $this->journal2->settings->get('product_grid_button_block_button'); ?>");

            var html = '';

			var image = $(element).find('.image').html();

			if (image != null) {
				html += '<div class="image">' + image + '</div>';
			}

            html += '<div class="product-details">';
			html += '<div class="name">' + $(element).find('.name').html() + '</div>';
			html += '<div class="description">' + $(element).find('.description').html() + '</div>';

			var price = $(element).find('.price').html();

			if (price != null) {
				html += '<div class="price">' + price  + '</div>';
			}

			var rating = $(element).find('.rating').html();

			if (rating != null) {
				html += '<div class="rating">' + rating + '</div>';
			}
            html += '<hr>';
			html += '<div class="' + $(element).find('.cart').attr('class') + '">' + $(element).find('.cart').html() + '</div>';
			html += '<div class="wishlist">' + $(element).find('.cart + .wishlist').html() + '</div>';
			html += '<div class="compare">' + $(element).find('.cart + .wishlist + .compare').html() + '</div>';

            html += '</div>';

            $(element).html('<div class="product-wrapper">'+html+'</div>');
		});

		$.totalStorage('display', 'grid');
	}

    $(window).trigger('list_grid_change');
    Journal.itemsEqualHeight();
    Journal.equalHeight($(".main-products .product-wrapper"), '.description');

    $(".main-products img.lazy").lazy({
        bind: 'event',
        visibleOnly: false,
        effect: "fadeIn",
        effectTime: 250
    });

    <?php /* enable quickview */ ?>
    <?php if ($this->journal2->settings->get('quickview_status') == '1' && !Journal2Cache::$mobile_detect->isMobile() && !Journal2Cache::$mobile_detect->isTablet() && !$this->journal2->html_classes->hasClass("ie8")): ?>
        Journal.enableQuickView();
        Journal.quickViewStatus = true;
    <?php else: ?>
        Journal.quickViewStatus = false;
    <?php endif; ?>

    <?php /* enable countdown */ ?>
    <?php if ($this->journal2->settings->get('show_countdown', 'never') !== 'never'): ?>
    $('.main-products > div').each(function () {
        var $new = $(this).find('.price-new');
        if ($new.length && $new.attr('data-end-date')) {
            $(this).find('.image').append('<div class="countdown"></div>');
        }
        Journal.countdown($(this).find('.countdown'), $new.attr('data-end-date'));
    });
    <?php endif; ?>
}

$('#review .pagination a').live('click', function() {
	$('#review').fadeOut('slow');

	$('#review').load(this.href);

	$('#review').fadeIn('slow');

	return false;
});

$('#review').load('index.php?route=module/vendorreview/review&vendor_id=<?php echo $vendor_id; ?>');

$('#button-review').bind('click', function() {
	$.ajax({
		url: 'index.php?route=module/vendorreview/write&vendor_id=<?php echo $vendor_id; ?>',
		type: 'post',
		dataType: 'json',
		data: 'name=' + encodeURIComponent($('input[name=\'name\']').val()) + '&text=' + encodeURIComponent($('textarea[name=\'text\']').val()) + '&rating=' + encodeURIComponent($('input[name=\'rating\']:checked').val() ? $('input[name=\'rating\']:checked').val() : '') + '&captcha=' + encodeURIComponent($('input[name=\'captcha\']').val()),
		beforeSend: function() {
			$('.success, .warning').remove();
			$('#button-review').attr('disabled', true);
			$('#review-title').after('<div class="attention"><img src="catalog/view/theme/default/image/loading.gif" alt="" /> <?php echo $text_wait; ?></div>');
		},
		complete: function() {
			$('#button-review').attr('disabled', false);
			$('.attention').remove();
		},
		success: function(data) {
			if (data['error']) {
				$('#review-title').after('<div class="warning">' + data['error'] + '</div>');
			}

			if (data['success']) {
				$('#review-title').after('<div class="success">' + data['success'] + '</div>');

				$('input[name=\'name\']').val('');
				$('textarea[name=\'text\']').val('');
				$('input[name=\'rating\']:checked').attr('checked', '');
				$('input[name=\'captcha\']').val('');
			}
		}
	});
});
//--></script> 
<?php echo $footer; ?>