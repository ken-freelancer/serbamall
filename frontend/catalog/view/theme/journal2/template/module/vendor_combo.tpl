<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content" style="text-align: center;">
	<p><?php echo $text_vendor; ?></p>
    <select name="vendor" onchange="location = this.value">
	  <option value="0"><?php echo $text_select; ?></option>
      <?php foreach ($vendor_data as $vendor) { ?>	  
      <option value="<?php echo $vendor['href']; ?>" style="background-image:url(<?php echo $vendor['thumb']; ?>)"><?php echo $vendor['vendor_name']; ?></option>
      <?php } ?>
    </select>
    <br />
    <br />
  </div>
</div>
