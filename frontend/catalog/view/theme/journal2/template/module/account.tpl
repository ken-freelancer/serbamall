<?php if ($logged){ ?>
<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <ul>
      <?php if (!$logged) { ?>
      <li><a href="<?php echo $login; ?>"><?php echo $text_login; ?></a> <a href="<?php echo $register; ?>"><?php echo $text_register; ?></a></li>
      <li><a href="<?php echo $forgotten; ?>"><?php echo $text_forgotten; ?></a></li>
     <?php } ?>
      <?php if ($logged) { ?>
      <li><a href="<?php echo $purchase; ?>"><?php echo $text_purchase; ?></a></li>
      <li><a href="<?php echo $edit; ?>"><?php echo $text_account; ?></a></li>
      <li><a href="<?php echo $password; ?>"><?php echo $text_password; ?></a></li>
      <?php } ?>
	  <?php if ($logged) { ?>
      <?php if(isset($address)): /* v1541 compatibility */ ?>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
        <?php endif; /* end v1541 compatibility */ ?>
	  <li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
      <?php /*<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li>*/ ?>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
      <li><a href="<?php echo $serbapay; ?>"><?php echo $text_serbapay; ?></a></li>
      <li><a href="<?php echo $logout; ?>"><?php echo $text_logout; ?></a></li>
      <?php } ?>
    </ul>
  </div>
</div>
<?php } ?>