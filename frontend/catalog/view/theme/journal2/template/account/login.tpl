<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<!--<h1 class="heading-title"><?php echo $heading_title; ?></h1>-->
	<?php echo $content_top; ?>
  
  <div class="login-content">
    <div class="left" style="width:100%">
      <?php if ($this->config->get('serbapay_auth_status')) { ?>
		<?php foreach ($this->config->get('serbapay_auth') as $config) { ?>
         <!-- <a onclick="window.open('<?php echo HTTPS_SERVER . 'index.php?route=serbapay/auth&provider=' . $config['provider']; ?>&redirect=<?php echo base64_encode($this->url->link('serbapay/auth/success')); ?>', 'newwindow', 'width=700, height=450,top=200, left=600'); return false;" href="<?php echo HTTPS_SERVER . 'index.php?route=serbapay/auth&provider=' . $config['provider']; ?>&redirect=<?php echo base64_encode($this->url->link('serbapay/auth/success')); ?>">
				<img src="image/templates/serbapay_login.png" style="width: 300px;"/>
			</a>-->
			<iframe width="100%" height="350px;" src="<?php echo HTTPS_SERVER . 'index.php?route=serbapay/auth&provider=' . $config['provider']; ?>" style="border:none"></iframe>
		<?php } ?>
	<?php } ?>
    </div>
    <!--<div class="right">	

    </div>-->
  </div>
  <?php echo $content_bottom; ?></div>
<script type="text/javascript"><!--
$('#login input').keydown(function(e) {
	if (e.keyCode == 13) {
		$('#login').submit();
	}
});
//--></script> 
<?php echo $footer; ?>