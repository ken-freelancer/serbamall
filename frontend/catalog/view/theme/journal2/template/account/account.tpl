<?php echo $header; ?>
<div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
</div>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content">
	<h1 class="heading-title"><?php echo $heading_title; ?></h1>	
	<?php echo $content_top; ?>
	
	<div id="customer_profile">
		<h1 class="welcome_text">Welcome Back</h1>
		
		<p class="p_profile">
			<?php echo $customer_account['firstname']; ?> <?php echo $customer_account['lastname']; ?><br>			
			<?php echo $customer_account['email']; ?><br>
			Date Joined: <?php echo $customer_account['date_added']; ?><br>			
			<?php /*
			<?php if(isset($customer_account_last_login)){ ?>
			Last Login: <?php echo $customer_account_last_login; ?>
			<?php } ?>
			*/ ?>
		</p>
	</div>
	
  <h2 class="secondary-title" style="background-color:rgb(56, 56, 56) !important;"><?php echo $text_my_account; ?></h2>
  <div class="content">
    <ul>
      <li><a target="_blank" href="<?php echo $edit; ?>"><?php echo $text_edit; ?></a></li>
      <li><a href="<?php echo $address; ?>"><?php echo $text_address; ?></a></li>
      <li><a href="<?php echo $wishlist; ?>"><?php echo $text_wishlist; ?></a></li>
    </ul>
  </div>
  <h2 class="secondary-title"  style="background-color:rgb(56, 56, 56) !important;"><?php echo $text_my_orders; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $order; ?>"><?php echo $text_order; ?></a></li>
      <?php if ($reward) { ?>
      <li><a href="<?php echo $reward; ?>"><?php echo $text_reward; ?></a></li>
      <?php } ?>
      <?php /*<li><a href="<?php echo $return; ?>"><?php echo $text_return; ?></a></li> */ ?>
      <li><a href="<?php echo $transaction; ?>"><?php echo $text_transaction; ?></a></li>      
    </ul>
  </div>
  <h2 class="secondary-title"  style="background-color:rgb(56, 56, 56) !important;"><?php echo $text_my_newsletter; ?></h2>
  <div class="content">
    <ul>
      <li><a href="<?php echo $newsletter; ?>"><?php echo $text_newsletter; ?></a></li>
    </ul>
  </div>
  <?php echo $content_bottom; ?></div>
<?php echo $footer; ?> 