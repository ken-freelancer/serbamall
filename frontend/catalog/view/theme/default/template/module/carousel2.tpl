<div id="vcarousel<?php echo $module; ?>">
  <ul class="vjcarousel-skin-opencart">
    <?php foreach ($vendor_data as $banner) { ?>
    <li><a href="<?php echo $banner['href']; ?>"><img src="<?php echo $banner['thumb']; ?>" alt="<?php echo $banner['vendor_name']; ?>" title="<?php echo $banner['vendor_name']; ?>" /></a>
	<br/><span style="font-size:80%;text-align:center;color:#666"><?php echo $banner['vendor_name']; ?></span>
	<br/><span style="font-size:80%;text-align:center;color:#666"><?php echo $banner['review']; ?></span>
	<?php if ($this->config->get('vendor_review')) { ?>
		<?php if ($banner['rating']) { ?>
		  <br/><div class="rating"><img src="catalog/view/theme/default/image/star/stars-<?php echo $banner['rating']; ?>.png" alt="<?php echo $banner['rating']; ?>" /></div>
		<?php } ?>
	<?php } ?>
	</li>	
    <?php } ?>
  </ul>
</div>
<script type="text/javascript"><!--
$('#vcarousel<?php echo $module; ?> ul').jcarousel({
	vertical: false,
	visible: <?php echo $limit; ?>,
	scroll: <?php echo $scroll; ?>
});
//--></script>