<?php echo $header; ?><?php echo $column_left; ?><?php echo $column_right; ?>
<div id="content"><?php echo $content_top; ?>
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <h1><?php echo $heading_vendor_info; ?></h1>
<div class="box">
  <div class="box-heading"><?php echo $heading_vendor_info; ?></div>
  <div class="box-content">
    <table>

		<tr>
          <td>
		  <?php if ($thumb) { ?>
			<div class="image"><img src="<?php echo $thumb; ?>" alt="<?php echo $vendor_name; ?>" /></div>
		  <?php } ?></td>
          <td></td>
        </tr>
		<tr></tr>
		
		<tr>
          <td><b><?php echo $text_company; ?> : </b></td>
          <td><?php echo $company; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_firstname; ?> : </b></td>
          <td><?php echo $firstname; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_lastname; ?> : </b></td>
          <td><?php echo $lastname; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_fax; ?> : </b></td>
          <td><?php echo $fax; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_email; ?> : </b></td>
          <td><?php echo $email; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_address_1; ?> : </b></td>
          <td><?php echo $address_1; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_address_2; ?> : </b></td>
          <td><?php echo $address_2; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_city; ?> : </b></td>
          <td><?php echo $city; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_postcode; ?> : </b></td>
          <td><?php echo $postcode; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_country; ?> : </b></td>
          <td><?php echo $country; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_zone; ?> : </b></td>
          <td><?php echo $zone; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_description; ?> : </b></td>
          <td><?php echo $description; ?></td>
        </tr>
		<tr>
          <td><b><?php echo $text_store_url; ?> : </b></td>
          <td><a href="<?php echo $store_url; ?>" style="text-decoration:none"><?php echo $store_url; ?></a></td>
        </tr>
		<tr>
		<td></td>
		<td><a href="<?php echo $href; ?>"><input type="button" value="<?php echo $button_view_all_products; ?>" class="button" /></a></td>
		</tr>
	</table>
  </div>
</div>
</div>
<?php echo $footer; ?>
