<div class="box">
  <div class="box-heading"><?php echo $heading_title; ?></div>
  <div class="box-content">
    <div class="box-product">
      <?php foreach ($vendor_data as $vendor) { ?>
      <div style="font-size:80%;text-align:center;color:#666;margin-bottom:8px;">
        <?php if ($vendor['thumb']) { ?>
        <div class="image"><a href="<?php echo $vendor['href']; ?>"><img src="<?php echo $vendor['thumb']; ?>" alt="<?php echo $vendor['vendor_name']; ?>" /></a></div>
		<?php } ?>
		<span><?php echo $vendor['vendor_name']; ?></span><br/>
		<span><?php echo $vendor['review']; ?></span>
		<?php if ($this->config->get('vendor_review')) { ?>
		<?php if ($vendor['rating']) { ?>
		<br/><div class="rating"><img src="catalog/view/theme/default/image/star/stars-<?php echo $vendor['rating']; ?>.png" alt="<?php echo $vendor['rating']; ?>" /></div>
		<?php } ?>
		<?php } ?>
      </div>
      <?php } ?>
    </div>
	<?php if (!$AllVendors) { ?>
	<div class="cart"><a href="<?php echo $viewallvendor; ?>"><input type="button" value="<?php echo $button_allvendors; ?>" class="button" /></a></div>
	<?php } ?>
  </div>
</div>
