<style type="text/css">
	tr:hover td, td > label {
		cursor:default !important;
		height:auto !important;
	}

	#mvd_usr_pv img {
		vertical-align:middle;
		padding: 5px 5px 5px 5px;
	}

	#mvd_usr_pv ttname {
		display: block;
		padding: 5px 5px 5px 5px;
		border-top:5px solid #E7E7E7;
		border-left:2px solid #E7E7E7;
		border-right:2px solid #E7E7E7;
		border-bottom:5px solid #E7E7E7;
	}

	#mvd_usr_pv cbtitle {
		border-bottom:5px solid #E7E7E7;
		border-left:2px solid #E7E7E7;
		display: block;
		padding: 3px 3px 3px 3px;
		width:35%;
	}

	#mvd_usr_pv description {
		font-size:x-small;
		padding-left:2.5em;
	}

	#mvd_usr_pv label:hover {

	}

	#mvd_usr_pv span {
		display:inline-block;
		width:50%;
	}
</style>

<div id="mvd_usr_pv" style="width:550px">
    <?php //fail to fullfill shipping, not in geozone ?>
    <?php
    //var_dump($available_vendors);
    //var_dump($fail_to_fullfil_vendors);
    //var_dump($final_courier_per_vendor);
    ?>
	<?php if ($fail_to_fullfil_vendors) { ?>
		<?php foreach ($available_vendors as $vendor) { ?>
			<?php $p=0; ?>
			<?php foreach ($fail_to_fullfil_vendors as $ftfv) { ?>
				<?php if ($vendor['vendor'] == $ftfv['vendor_id']['vendor']) { ?>
					<?php if ($p == 0) { ?>
						<?php if ($ftfv['vendor_name']) { ?>
						<ttname><b><?php echo ucwords($ftfv['vendor_name']); ?></b></ttname><br />
						<span style="width:auto;font-size:x-small;color:red"><?php echo $text_shipping_not_fullfil; ?></span>
						<?php } ?>
					<?php } ?>	
						<label>
						<?php if ($ftfv['thumb']) { ?>
						<span style="width:50%"><a href="<?php echo $ftfv['href']; ?>"><img style="border: 1px solid #DDDDDD;" src="<?php echo $ftfv['thumb']; ?>" alt="<?php echo $ftfv['name']; ?>" title="<?php echo $ftfv['name']; ?>" /></a>&nbsp;&nbsp;<a href="<?php echo $ftfv['href']; ?>"><?php echo $ftfv['name']; ?></a></span>
						<?php } ?>&nbsp;<a href="<?php echo $ftfv['remove']; ?>"><img src="catalog/view/theme/default/image/remove-small.png" alt="<?php echo $ftfv['text_remove']; ?>" title="<?php echo $ftfv['text_remove']; ?>" /></a>
						</label>
				<?php $p++ ?>
				<?php } ?>
			<?php } ?><br/>
		<?php } ?>
	<?php } ?>
	<?php if ($this->config->get('mvd_usr_pv_display')) { ?>
	<?php foreach ($available_vendors as $vendor) { ?>
	    <?php $i=0; ?>
		<?php $checked = True; ?>
		<?php foreach ($final_courier_per_vendor as $courier) { ?>	
			<?php if ($vendor['vendor'] == $courier['vendor_id']) { ?>
				<?php if ($i == 0) { ?>
				<ttname><b><?php echo ucwords($courier['vendor_name']);?></b>&nbsp;<?php echo '(' . $courier['text_weight'] . ')'; ?></ttname>
				<?php } ?>
				<label>
				<input onchange="getRates()" type="radio" name="<?php echo $courier['vendor_id']; ?>" value="<?php echo ucwords($courier['vendor_name']) . ' [ ' . $courier['name'] . ' ] - ' . ':' . $courier['shipping_rate'] . ':' . $courier['vendor_id'] . ':' . $courier['courier_id'] . ':' . $courier['shipping_rate'] . ':' . $courier['text_weight'] . ':' . $courier['name']; ?>" <?php if ($checked) echo 'checked="checked"'; ?> />&nbsp;<img src="<?php echo $courier['thumb']; ?>" alt="<?php echo $courier['name']; ?>" /> 
				&nbsp;<span><?php echo $courier['name']; ?></span>&nbsp;<?php echo $courier['text_rate']; ?><br/>
				<description><?php echo $text_deliver_within . $courier['description']; ?></description>
				</label>				
				<?php if ($checked) $cost += $courier['shipping_rate']; ?>	
				<?php $checked = False; ?>
				<?php $i++ ?>
			<?php } ?>
		<?php } ?>
	<?php } ?>
	<?php } else { ?>
		<form id="select">
		<?php foreach ($available_vendors as $vendor) { ?>
			<?php $i=0; ?>
			<?php $selected = True; ?>
			<?php foreach ($final_courier_per_vendor as $courier) { ?>	
				<?php if ($vendor['vendor'] == $courier['vendor_id']) { ?>
					<?php if ($i == 0) { ?>
					<cbtitle><b><?php echo ucwords($courier['vendor_name']);?></b>&nbsp;<?php echo '(' . $courier['text_weight'] . ')'; ?></cbtitle><br/>
					<select id="select" onchange="getOptionRates()" name="<?php echo $vendor['vendor']; ?>">
					<?php } ?>			
					<option value="<?php echo ucwords($courier['vendor_name']) . ' [ ' . $courier['name'] . ' ] - ' . ':' . $courier['shipping_rate'] . ':' . $courier['vendor_id'] . ':' . $courier['courier_id'] . ':' . $courier['shipping_rate'] . ':' . $courier['text_weight'] . ':' . $courier['name']; ?>" <?php if ($selected) echo 'selected="selected"'; ?> ><?php echo $courier['name']; ?> - <?php echo $courier['text_rate']; ?></option>
					<?php if ($selected) $cost += $courier['shipping_rate']; ?>	
					<?php $selected = False; ?>
					<?php $i++ ?>
				<?php } ?>
			<?php } ?>
		    </select><br/><br/>
		<?php } ?>
		</form>
	<?php } ?>
</div>

<?php if ($this->config->get('mvd_usr_pv_display')) { ?>
<script type="text/javascript">
	function getRates() {
		$('#mvd_usr_pv').parent().parent().prev().find('input').click();
		$.ajax({
			type: 'POST',
			url: 'index.php?route=shipping/mvd_usr_pv/getRates',
			data: $('#mvd_usr_pv input:checked'),
			success: function(data){
				$('#mvd_usr_pv').parent().parent().next().find('label').html(data);
			}
		});
	}
	setTimeout(getRates, 1);
</script>
<?php } else { ?>
<script type="text/javascript">
	function getOptionRates() {
		$.ajax({
			type: 'POST',
			url: 'index.php?route=shipping/mvd_usr_pv/getRates',
			data: $('#select').serialize(),
			success: function(data){
				$('#mvd_usr_pv').parent().parent().next().find('label').html(data);
			}
		});
	}
	setTimeout(getOptionRates, 1);
</script>
<?php } ?>

