<?php

echo '<p><a href="<?php echo HTTPS_SERBAPAY ?>" target="New"><img src="./admin/view/image/payment/serbapay.png" alt="serbapay" title="serbapay" style="border: 1px solid #EEEEEE;" /></a></p>';
echo '<p><b>'."Please note that your Payment is processed by SerbaPay. The name of SerbaPay will be shown on your Credit Card / Bank Statement and you will also receive a notification e-mail from SerbaPay on this Transaction.".'</p></b>';

?>

<form action="<?php echo $action; ?>" method="post" id="payment">
	<input type="hidden" name="merchant_id" value="<?php echo $merchant_id; ?>" />
	<input type="hidden" name="invoice_no" value="<?php echo $invoice; ?>" />
	<input type="hidden" name="basket" value="<?php echo $basket; ?>" />
	<input type="hidden" name="amount" value="<?php echo $amount; ?>" />

	<input type="hidden" name="username" value="<?php echo $username; ?>" />
	<input type="hidden" name="useremail" value="<?php echo $useremail; ?>" />
	<input type="hidden" name="usercontact" value="<?php echo $usercontact; ?>" />

	<input type="hidden" name="currency" value="<?php echo $currency; ?>" />
	<input type="hidden" name="language" value="<?php echo $lang; ?>" />
	<input type="hidden" name="url_return" value="<?php echo $ResponseURL; ?>" />
	<input type="hidden" name="url_cancel" value="<?php echo $ResponseURL; ?>" />
	<input type="hidden" name="hash" value="<?php echo $hash ?>" />
	<input type="hidden" name="shipping" value="<?php echo $shipping?>" />
	
<div class="buttons">
    <div class="right"><a onclick="$('#payment').submit();" class="button"><span><?php echo $button_confirm; ?></span></a></div>
</div>
  
</form>