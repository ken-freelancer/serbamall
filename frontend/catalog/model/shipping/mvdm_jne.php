<?php
class ModelShippingMVDMJNE extends Model {
	function getQuote($address) {
        //error no shipping
        $error_no_shipping = 'One of your shipping item - <b>%s</b> is not able to ship to your location. Please change your shipping location. Thanks';
		$this->load->language('shipping/mvdm_jne');
		$this->load->model('tool/image');
		$method_data = array();
		$valid_courier = array();
		$shipping_courier = array();		
		$final_shipping_method = array();
		
		if ($this->config->get('mvdm_jne_status')) {
			$validate_shipping_method = TRUE;
			$num_of_shipping_products = 0;

			foreach ($this->cart->getProducts() as $product) {
                //var_dump($product);
                //check required shipping or not
				if ($product['shipping'])
                {
                    //check see got result for shipping or not
                    $not_able_ship = false;

                    //need to make sure got vendor id
                    $query_vendor_info = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendors WHERE vendor_id = '" . (int)$product['vendor_id'] . "'");
                    $vendor_info = $query_vendor_info->row;

                    $from_city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int)$vendor_info['city'] . "'");

                    if ($from_city_query->num_rows) {
                        $city_name = $from_city_query->row['name'];
                        $from_jne_code = $from_city_query->row['jne_code'];
                    } else {
                        $city_name = '';
                        $from_jne_code = '';
                    }

                    //customer JNE Code
                    $to_city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int)$this->session->data['shipping_city'] . "'");
                    if ($to_city_query->num_rows) {
                        $city_name = $to_city_query->row['name'];
                        $to_jne_code = $to_city_query->row['jne_code'];
                    } else {
                        $city_name = '';
                        $to_jne_code = '';
                    }
                    if(!empty($from_jne_code) && !empty($to_jne_code))
                    {
                        //calculate the price for the distance
                        //curl for jne
                        $log = new Log(date("Y-m-d").'-jne-curl.log');
                        $log->write('[INIT cURL]');
                       /* $weight = $product['weight'];
                        $qty = $product['quantity'];
                        $total_weight = $weight * $qty;*/
                        //cart product weight already multiple with qty
                        $total_weight = $product['weight'];
                        $type = 'eskpress';
                        //$data = '{"api_key":"'.JNE_API_KEY.'","username":"'.JNE_API_USERNAME.'","from":"'.$from_jne_code.'","thru":"' . $to_jne_code . '","weight":"' . $total_weight . '"}';
                        $data = "api_key=".JNE_API_KEY."&username=".JNE_API_USERNAME."&from=".$from_jne_code."&thru=" . $to_jne_code . "&weight=" . $total_weight;
                        $log->write('[DATA cURL] ' . $data);
                        // 1. initialize
                        $ch = curl_init();

                        // 2. set the options, including the url
                        $header = 'Content-Type: application/x-www-form-urlencoded';
                        //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                        curl_setopt($ch, CURLOPT_URL, JNE_API_URL);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                        curl_setopt($ch, CURLOPT_HEADER, $header);
                        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


                        // 3. execute and fetch the resulting HTML output
                        $output = curl_exec($ch);
                        if ($output === false)
                        {
                            $log->write(curl_error($ch));
                            echo "cURL Error: " . curl_error($ch);
                        }
                        $log->write('[RESPONSE]'.$output);
                        // 4. free up the curl handle
                        curl_close($ch);
                        $log->write('[CLOSED cURL]');
                        //.Ken - add cURL to update serbapay for shipping status

                        $jne_response = json_decode($output);
                        $jne_oke = array();
                        $jne_reg = array();
                        if(isset($jne_response->price))
                        {
                            foreach($jne_response->price as $package)
                            {
                                if($package->service_display == 'OKE')
                                {
                                    $jne_oke = $package;
                                }
                                elseif($package->service_display == 'REG')
                                {
                                    $jne_reg = $package;
                                }
                            }

                        }
                        else
                        {
                            $_SESSION['ken_shipping_error'] = sprintf($error_no_shipping, $product['name']);
                            $method_data = array();
                            return $method_data;
                        }
                    }
                    else
                    {
                        $_SESSION['ken_shipping_error'] = sprintf($error_no_shipping, $product['name']);
                        $method_data = array();
                        return $method_data;
                    }
                    //get the vendor address, and get the zone id, calculate item possible to ship or not, not possible return empty



                    //Ken if no support shipping area
                    if($not_able_ship)
                    {
                        //Ken - remember translate to indo
                        $_SESSION['ken_shipping_error'] = sprintf($error_no_shipping, $product['name']);
                        $method_data = array();
                        return $method_data;
                    }

                    //Ken - I prefix using reg better shipping
                    $courier['shipping_rate'] = $jne_reg->price;
                    $courier['geo_zone_id'] = '1'; //preset zone

                    $shipping_couriers[] = array(
                        'vendor_id'     => $product['vendor_id'],
                        'vendor_name'   => $vendor_info['company'],
                        'product_id'    => $product['product_id'],
                        'courier_id'    => '4', //JNE
                        'shipping_rate' => $courier['shipping_rate'],
                        'weight'        => $product['weight'],
                        'geo_zone_id'   => $courier['geo_zone_id'],
                    );

                    $product_vendor_shipping[$product['vendor_id']][$product['product_id']]['rate'] = $courier['shipping_rate'];
                    $product_vendor_shipping[$product['vendor_id']][$product['product_id']]['weight'] = $product['weight'];

                    $num_of_shipping_products ++;
                }
			}
            $_SESSION['jne_shipping_arr'] = $shipping_couriers;

            //this is use in serbapay payment to include each product shipping cost
            $_SESSION['cart_jne_product_vendor_shipping_arr'] = $product_vendor_shipping;

            //combine vendor info, item desc
            $vendor_desc = array();
            if(count($shipping_couriers))
            {
                foreach($shipping_couriers as $vendor)
                {
                    if(!isset($vendor_desc[$vendor['vendor_id']]['weight']))
                    {
                        $vendor_desc[$vendor['vendor_id']]['weight'] = 0;
                        $vendor_desc[$vendor['vendor_id']]['rate'] = 0;
                    }
                    $vendor_desc[$vendor['vendor_id']]['vendor_id'] = $vendor['vendor_id'];
                    $vendor_desc[$vendor['vendor_id']]['name'] = $vendor['vendor_name'];
                    $vendor_desc[$vendor['vendor_id']]['weight'] += $vendor['weight'];
                    $vendor_desc[$vendor['vendor_id']]['rate'] += $vendor['shipping_rate'];
                }

                //combine to one string text
                $vendor_str = '<br />';
                $ctr = 0;
                foreach($vendor_desc as $desc)
                {
                    $_SESSION['jne_shipping'][$desc['vendor_id']]['rate'] = $desc['rate'];
                    $_SESSION['jne_shipping'][$desc['vendor_id']]['weight'] = $desc['weight'];
                    $weight = $this->weight->format($desc['weight'], $this->config->get('config_weight_class_id'));
                    $rate = $this->currency->format($this->tax->calculate($desc['rate'], $this->config->get('mvdm_jne_tax_class_id'), $this->config->get('config_tax')));
                    $vendor_str .= '<b>'.++$ctr.'. '.$desc['name'].'</b><br />'.
                                ' (' . $this->language->get('text_weight') . ' ' . $weight . ') <br />' .
                                $rate.'<br /><br />';
                }
            }

            $i=0;
            $total_weight=0;
            $total_shipping=0;
            foreach ($shipping_couriers as $shipping_courier)
            {
                $total_shipping += $shipping_courier['shipping_rate'];
                $total_weight += $shipping_courier['weight'];
                $i++;

                //last array item equal to count
                if ($num_of_shipping_products == $i)
                {
                    $getCourierData = $this->getCourierName($shipping_courier['courier_id']);
                    $final_shipping_method[] = array(
                        'courier_name' 		=> $getCourierData['courier_name'],
                        'description'		=> $getCourierData['description'],
                        'courier_id' 		=> $shipping_courier['courier_id'],
                        'courier_image' 	=> $this->model_tool_image->resize($this->getCourierImage($shipping_courier['courier_id']),'88','30'),
                        'shipping_weight'	=> $total_weight,
                        'shipping_rate' 	=> $total_shipping
                    );
                }
            }

			if ($final_shipping_method)
            {
				$quote_data = array();
				
				foreach ($final_shipping_method as $shipping_method) {
					if ($shipping_method['courier_image']) {
						$cimage = '<td><img src=' . $shipping_method['courier_image'] . ' ></tb>';
					} else {
						$cimage = false;
					}
				
					$quote_data['mvdm_jne' . $shipping_method['courier_id']] = array(
						'code'         => 'mvdm_jne.mvdm_jne' . $shipping_method['courier_id'],
						'title'        => $shipping_method['courier_name'] . ' (' . $this->language->get('text_weight') . ' ' . $this->weight->format($shipping_method['shipping_weight'], $this->config->get('config_weight_class_id')) .
                                            ') <br/><span style="font-size:smaller;color:#999">' . $shipping_method['description'] . $vendor_str . '</span>',
						'cost'         => $shipping_method['shipping_rate'],
						'cimage'	   => $cimage,
						'tax_class_id' => $this->config->get('mvdm_jne_tax_class_id'),
						'text'         => $this->currency->format($this->tax->calculate($shipping_method['shipping_rate'], $this->config->get('mvdm_jne_tax_class_id'), $this->config->get('config_tax')))
					);
					$i++;
				}
			
				$method_data = array(
					'code'       => 'mvdm_jne',
					'title'      => $this->language->get('text_title'),
					'quote'      => $quote_data,
					'sort_order' => $this->config->get('mvdm_jne_sort_order'),
					'error'      => false
				);
			}
		}

		return $method_data;
	}
	
	private function getCourierName($courier_id) {
		$query = $this->db->query("SELECT courier_name,description FROM " . DB_PREFIX . "courier WHERE courier_id = '" . (int)$courier_id . "'");
		return $query->row;	
	}
	
	private function getCourierImage($courier_id) {
		$query = $this->db->query("SELECT courier_image FROM " . DB_PREFIX . "courier WHERE courier_id = '" . (int)$courier_id . "'");
		return $query->row['courier_image'];	
	}
}