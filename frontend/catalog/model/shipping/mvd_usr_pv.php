<?php
class ModelShippingMVDUSRPV extends Model {
	function getQuote($address) {
		$this->load->language('shipping/mvd_usr_pv');
		$this->load->model('tool/image');
		$method_data = array();
		$valid_courier = array();
		$shipping_couriers = array();		
		$final_courier_per_vendor = array();
		$raw_vendors = array();
		$available_vendors = array();
		$required_shipping_product = array();
		$valid_shipping_products = array();
		$getUnique_pid = array();
		$shipping_pid = array();
        //fail for shipping validation pid means for the product didn't included in geozone
		$fail_shipping_validation_pid = array();
		$fail_to_fullfil_vendors = array();

        //check extension enable or not
		if ($this->config->get('mvd_usr_pv_status')) {
			$validate_shipping_method = TRUE;
			//$num_of_shipping_products = 0;
            //courier counter use in array
            $courier_ctr = 0;
            //read from the product cart, foreach for each product
			foreach ($this->cart->getProducts() as $product) {
                //only count shipping is it is required shipping is true
				if ($product['shipping']) {
                    //to get vendor id for the product
                    $vendor = $this->getVendorID($product['product_id']);
					if ($vendor) {
                        //get the ultimate shipping data
						$query_shipping_data = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_ultm_shipping pus LEFT JOIN " . DB_PREFIX . "vendor_ultimate_shipping vus ON (pus.vutm_shipping_id = vus.vutm_shipping_id) WHERE pus.product_id = '" . (int)$product['product_id'] . "' ORDER BY pus.vutm_shipping_id");

						if ($query_shipping_data->rows) {

							foreach ($query_shipping_data->rows as $courier) {
								$valid_destination = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int)$courier['geo_zone_id'] . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");
								if ($valid_destination->row) {
									$shipping_couriers[++$courier_ctr] = array(
										'product_id'		=>	$courier['product_id'],
										'courier_id'		=>	$courier['courier_id'],
										'vutm_shipping_id'	=>	$courier['vutm_shipping_id'],
										'weight'			=>	$product['weight'], //weight is time with qty already
										'vendor'			=>	$courier['vendor_id'],
										'geo_zone_id'		=>	$courier['geo_zone_id'],
                                        'jne_code'          =>  $vendor['jne_code']
									);
									
									$valid_shipping_products[] = $courier['product_id'];
								}
								//store raw vendor
								$raw_vendors[] = array(		  	
									'vendor'		=>	$courier['vendor_id']
								);
							}//foreach
						}//.query_shipping_row
					}//.vendor only valid vendor product

                    /*echo '$shipping_couriers'."<br />";
                    var_dump($shipping_couriers);*/

                    //$required_shipping_product is to store, vendor_id, product_id and the particular product total price (unit price x qty)
					$required_shipping_product[] = array(
						'vendor'		=>	$vendor['vendor'],
						'product_id'	=>	$product['product_id'],
						'total'		=>  $this->tax->calculate($product['total'], $product['tax_class_id'], $this->config->get('config_tax'))
					);

					//get unique shipping product ID
					$getUnique_pid = array_map("unserialize", array_unique(array_map("serialize", $valid_shipping_products)));
					$shipping_pid[] = array(
						'product_id'	=>	(int)$product['product_id']
					);
				}				
			}//. $this->cart->getProduct()
            //end of loop from all product in shopping cart
            //var_dump($required_shipping_product);

			//store product did not pass the shipping validation; means for those not in geozone
			foreach ($shipping_pid as $shipping_product) {
				if (!in_array($shipping_product['product_id'],$getUnique_pid)) {
					$fail_shipping_validation_pid[] = $shipping_product['product_id'];
				}
			}
			//finalize vendors
			$available_vendors = array_map("unserialize", array_unique(array_map("serialize", $raw_vendors)));
            //var_dump($available_vendors);

            //**foreach logic for available_vendors to build each vendor variable to store, shipping courier, product_id, total amount
            //split to seperate variable - [ID] is by vendor ID
            //$vendor_available_couriers_v[ID] - courier array info - the ending v means vendor
            //$total_product_by_vendor_[ID] - product ID
            //$total_amount_by_vendor_[ID] - total amount (from cart info)
			foreach ($available_vendors as $uni_vendor) {
				${'vendor_available_couriers_v' . $uni_vendor['vendor']} = array();
				foreach ($shipping_couriers as $shipping_courier) {
					if ($uni_vendor['vendor'] == $shipping_courier['vendor']) {
						${'vendor_available_couriers_v' . $uni_vendor['vendor']}[] = array(
							'product_id'		=>	$shipping_courier['product_id'],
							'courier_id'		=>	$shipping_courier['courier_id'],
							'vutm_shipping_id'	=>	$shipping_courier['vutm_shipping_id'],
							'weight'			=>	$shipping_courier['weight'],
							'vendor'			=>	$shipping_courier['vendor'],
							'geo_zone_id'		=>	$shipping_courier['geo_zone_id'],
							'jne_code'		    =>	isset($shipping_courier['jne_code'])?$shipping_courier['jne_code']:''
						);
					}
				}//.foreach shipping_couriers
                /*echo 'vendor_available_couriers_v' . "<br />";
                var_dump(${'vendor_available_couriers_v' . $uni_vendor['vendor']});*/

                //split the required shipping product separate by vendor variable save in array.
                //it save product id in array; it means this vendor got how many product
				foreach ($required_shipping_product as $pbv) {
					if ($uni_vendor['vendor'] == $pbv['vendor']) {
						${'total_product_by_vendor_' . $uni_vendor['vendor']}[] = array(
							'product'	=>	$pbv['product_id']
						);
					}
				}
                //var_dump(${'total_product_by_vendor_' . $uni_vendor['vendor']});
                //split the required shipping cost / TOTAL separate by vendor variable
                //it save total amount in this variable
				foreach ($required_shipping_product as $pbv) {
					if ($uni_vendor['vendor'] == $pbv['vendor']) {
						${'total_amount_by_vendor_' . $uni_vendor['vendor']}[] = array(
							'total'		=> $pbv['total']
						);
					}
				}
                //echo 'total_amount_by_vendor_'."<br />";
                //var_dump(${'total_amount_by_vendor_' . $uni_vendor['vendor']});
			}//. foreach unique vendor


            //**foreach logic for available_vendors to check each of the total product have courier ID
            //if 2 products, one product have shipping and another don't have the 'courier_per_vendor_c' will empty array!
			foreach ($available_vendors as $uni_vendor){
                //var_dump(${'total_product_by_vendor_' . $uni_vendor['vendor']});
                //initial $courier_per_vendor_c[ID] - the ending c refer to courier
				${'courier_per_vendor_c' . $uni_vendor['vendor']} = array();
                //check the vendor_available_couriers_v[ID] exist or not
				if (isset(${'vendor_available_couriers_v' . $uni_vendor['vendor']})) {
                    //loop each courier
					foreach (${'vendor_available_couriers_v' . $uni_vendor['vendor']} as $shipping_courier) {
                        //total count products per vendor
                        $x = sizeof(${'total_product_by_vendor_' . $uni_vendor['vendor']});
                        //echo "<br />\$x: is ".$x;

                        //j is total number of courier for that vendor
						$j = 0;
                        //nested loop $vendor_available_couriers_v[ID] as compare_courier
						foreach (${'vendor_available_couriers_v' . $uni_vendor['vendor']} as $compare_courier) {
							if ($shipping_courier['courier_id'] == $compare_courier['courier_id']) {
                                //echo "<br />";
                                $j++;
                                //echo '$j: '.$j."<Br />";
							}
						}

                        /*in order to have correct shipping courier, the number of product courier (j) must same as total product (x)
                         * if number count product equal to number to courier then will create the courier_per_vendor_c
                         * else will only empty array
                         */
						if ($x == $j) {
                            //courier_per_vendor_c[ID]['courier_id'] = value of courier_id
							${'courier_per_vendor_c' . $uni_vendor['vendor']}[] = array(
								'courier_id'	=> $shipping_courier['courier_id'],
                                'jne_code'      => $shipping_courier['jne_code']
							);
						}
					}
				}
                /*echo 'courier_per_vendor_c'."<br />";
                var_dump(${'courier_per_vendor_c' . $uni_vendor['vendor']});*/

				if (isset(${'courier_per_vendor_c' . $uni_vendor['vendor']}) && (${'courier_per_vendor_c' . $uni_vendor['vendor']}) != false) {
					${'final_courier_per_vendor_c' . $uni_vendor['vendor']} = array_map("unserialize", array_unique(array_map("serialize", ${'courier_per_vendor_c' . $uni_vendor['vendor']})));
				} else {
					${'final_courier_per_vendor_c' . $uni_vendor['vendor']} = false;
				}

                /*echo 'final_courier_per_vendor_c'.$uni_vendor['vendor']."<br />";
                var_dump(${'final_courier_per_vendor_c' . $uni_vendor['vendor']});*/


                /**
                 * This below part is not find out what product is fulfill the shipping
                 * Find out the courier ID and the product ID set in the array variable
                 */
                //this is for the EMPTY courier_per_vendor_c (which means got shipping not fulfill)
				if (empty(${'courier_per_vendor_c' . $uni_vendor['vendor']})) {
                    //foreach to look up what is product not support the courier
					foreach (${'vendor_available_couriers_v' . $uni_vendor['vendor']} as $product1) {
						foreach (${'vendor_available_couriers_v' . $uni_vendor['vendor']} as $product2) {
							if ($product1['product_id'] == $product2['product_id']) {
								${'my_product' . $product2['product_id']}[] = array(
									'courier_id'	=> $product2['courier_id']
								);
							}
						}
						${'final_product_c' . $product1['product_id']} = array_map("unserialize", array_unique(array_map("serialize", ${'my_product' . $product1['product_id']})));
	                    /*echo 'final_product_c'. $product1['product_id']."<br />";
                        var_dump(${'final_product_c' . $product1['product_id']});*/
                    }
                    //.foreach to look up what is product not support the courier

                    //compare nest foreach total
					if (isset(${'total_product_by_vendor_' . $uni_vendor['vendor']})) {
						foreach (${'total_product_by_vendor_' . $uni_vendor['vendor']} as $product98) {
							foreach (${'total_product_by_vendor_' . $uni_vendor['vendor']} as $product99) {
								if ($product98['product']!=$product99['product']) {
									if (!empty(${'final_product_c' . $product99['product']}) && !empty(${'final_product_c' . $product98['product']})) {
										${'final_product_c' . $product99['product']} = array();
										$p = sizeof(${'final_product_c' . $product98['product']});
										$q = 0;
										foreach (${'final_product_c' . $product98['product']} as $courier) {
											if (!in_array($courier,${'final_product_c' . $product99['product']})) {
												$q++;
											}
										}
										if ($p==$q) {
											$fail_shipping_validation_pid[] = $product98['product'];
										}//. if ($p==$q)
									}//. if (!empty
								}//. if ($product98 != 99
							}//. foreach 99
						}//. foreach 98
					}//. if isset
				}
                //.this is for the EMPTY courier_per_vendor_c (which means got shipping not fulfill)
			}//.foreach available_vendor
			
			$fail_shipping_validation_pid = array_unique($fail_shipping_validation_pid);

            /**
             * Below part is to calculate the valid + available shipping and valid + available courier
             */
            //always reset the session before start
            $_SESSION['ken_product_vendor_shipping_arr'] = array();
            unset($_SESSION['ken_product_vendor_shipping_arr']);

			//until here can find the courier per vendor correctly
			foreach ($available_vendors as $uni_vendor) {
                //$xx will be default measurement setting load from config
				$xx = $this->config->get('mvd_usr_pv_measurement');
                //total_cart value, it is to sum up all the total of each product, it will be reset for each vendor
				$total_cart=0;

                //get vendor individual set the shipping setting / measure (4 types)
				$yy = $this->getMeasurement($uni_vendor['vendor']);

                //if $yy  will use vendor setting to override the global setting
				if (($yy > 0) && ($yy)) {
					$xx = $yy;
				}

                //foreach to set total amount of the vendor
				if (isset(${'total_amount_by_vendor_' . $uni_vendor['vendor']})) {
					foreach (${'total_amount_by_vendor_' . $uni_vendor['vendor']} as $total_amount) {
						$total_cart += $total_amount['total'];
					}
				}

				if (${'final_courier_per_vendor_c' . $uni_vendor['vendor']}) {
                    //foreach valid courier for vendor
					foreach (${'final_courier_per_vendor_c' . $uni_vendor['vendor']} as $final_courier) {
                        //var_dump($final_courier);
						$total_weight=0;
						$total_shipping=0;

                        //Ken - Implement JNE price pre-store in vendor_available_couriers_v
						foreach (${'vendor_available_couriers_v' . $uni_vendor['vendor']} as $available_courier) {
                            /*var_dump($available_courier);
                            echo "<br />";
                            echo 'final_courier';
                            echo "<br />";
                            echo $final_courier['courier_id'];
                            echo "<br />";
                            echo 'available_courier';
                            echo "<br />";
                            echo $available_courier['courier_id'];*/
							if ($final_courier['courier_id'] == $available_courier['courier_id']) {
								$my_vutm_id = $available_courier['vutm_shipping_id'];
                                //sum total weight per courier and per vendor
                                /*Ken - change from sum up calculate cost split to individual shipping cost then sum up,
                                 * It is easy for doing refund
                                 */
                                /*echo $final_courier['courier_id'];
                                echo "<br />";*/
                                //this is use in serbapay payment to include each product shipping cost
                                $this->getSingleShipping($available_courier,$final_courier['jne_code'],$my_vutm_id);
                                if(isset($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']][$available_courier['courier_id']]['rate']) &&
                                !empty($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']][$available_courier['courier_id']]['rate']))
                                    $total_shipping += $_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']][$available_courier['courier_id']]['rate'];
								$total_weight += $available_courier['weight'];
							}
						}//.foreach vendor_available_couriers_v

                        /* Ken - change from using total weight for each one, calculate by each and sum up later
                        //once got the total weight then call JNE - ** this method can't use anymore, because each
                        //product shipping cost need to separated easy when doing refund.
                        //Ken - add JNE auto calculation
                        $from_jne_code = $final_courier['jne_code'];
                        //customer JNE Code
                        $to_city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int)$this->session->data['shipping_city'] . "'");
                        if ($to_city_query->num_rows) {
                            $to_jne_code = $to_city_query->row['jne_code'];
                        } else {
                            $to_jne_code = '';
                        }
                        if(!empty($from_jne_code) && !empty($to_jne_code))
                        {
                            //calculate the price for the distance
                            //curl for jne
                            $log = new Log(date("Y-m-d") . '-jne-curl.log');
                            $log->write('[INIT cURL]');
                            //total weight get from above calculation
                            $total_weight = $product['weight'];
                            $type = 'eskpress';
                            //$data = '{"api_key":"'.JNE_API_KEY.'","username":"'.JNE_API_USERNAME.'","from":"'.$from_jne_code.'","thru":"' . $to_jne_code . '","weight":"' . $total_weight . '"}';
                            $data = "api_key=" . JNE_API_KEY . "&username=" . JNE_API_USERNAME . "&from=" . $from_jne_code . "&thru=" . $to_jne_code . "&weight=" . $total_weight;
                            $log->write('[DATA cURL] ' . $data);
                            // 1. initialize
                            $ch = curl_init();

                            // 2. set the options, including the url
                            $header = 'Content-Type: application/x-www-form-urlencoded';
                            //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                            curl_setopt($ch, CURLOPT_URL, JNE_API_URL);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                            curl_setopt($ch, CURLOPT_HEADER, $header);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


                            // 3. execute and fetch the resulting HTML output
                            $output = curl_exec($ch);
                            if ($output === false)
                            {
                                $log->write(curl_error($ch));
                                echo "cURL Error: " . curl_error($ch);
                            }
                            $log->write('[RESPONSE]' . $output);
                            // 4. free up the curl handle
                            curl_close($ch);
                            $log->write('[CLOSED cURL]');
                            //.Ken - add cURL to update serbapay for shipping status

                            $jne_response = json_decode($output);
                            //var_dump( $jne_response );
                            if (isset($jne_response->price))
                            {
                                foreach ($jne_response->price as $package)
                                {
                                    if ($package->service_display == 'OKE')
                                    {
                                        $jne_oke = $package;
                                    }
                                    if ($package->service_display == 'REG')
                                    {
                                        $jne_reg = $package;
                                    }
                                    if ($package->service_display == 'YES')
                                    {
                                        $jne_yes = $package;
                                    }
                                }
                            }
                        }//end of jne
                        //.Ken - add JNE auto calculation

                        //get shipping rate,  explode by ',' first then explode by ':'
						$getRates = $this->getUltimateRate($my_vutm_id);

                        //calculate for each shipping calculation method, the weight already total up. just find the rate range
                        //1 = weight base shipping
                        //e.g 10:10,20:20
						if ($xx == 1) {
							$rates = explode(',', $getRates);
							foreach ($rates as $rate) {
								$data = explode(':', $rate);
								if ($data[0] > $total_weight) {
									if (isset($data[1])) {
										$total_shipping = $data[1];
									}
									break;
								}
							}
                        //2 = Weight Base Shipping Plus
						} elseif ($xx == 2) {
							$parcel_weight = $total_weight;
							$weight_diff = 0;
							$prev_weight = 0;
							$parcel_rates = array();
							$x=0;

							$rates = explode(',', $getRates);
							foreach ($rates as $rate) {
								$data = explode(':', $rate);
								if ($data[0] > $parcel_weight) {
									if (isset($data[1])) {
										if ($x==0) {
											$parcel_rates[] = $data[1];
										} else {
											$parcel_rates[] = $data[1] * $parcel_weight;
										}
									}
									break;
								} else {
										if ($x==0) {
											$prev_weight =  $data[0];   
											$parcel_rates[] = $data[1]; 
											$parcel_weight = $parcel_weight - $rate[0]; 
											$x++;
										} else {
											$weight_diff = $data[0]-$prev_weight; 
											$prev_weight = $data[0]; 
											if ($parcel_weight > $weight_diff) { 
												$parcel_rates[] = $data[1] * $weight_diff; 
												$parcel_weight = $parcel_weight - $weight_diff;
											}
										}
								}
							}
                            foreach ($parcel_rates as $parcel_rate) {
                                $total_shipping = $total_shipping + $parcel_rate;
                            }
						}
                        //3 = Total Amount Per Vendor one fix fast
                        else {
							if ($xx > 2) {
								$rates = explode(',', $getRates);
								foreach ($rates as $rate) {
									$data = explode(':', $rate);
									if ($data[0] > $total_cart) {
										if (isset($data[1])) {
											$total_shipping = $data[1];
										}
										break;
									}
								}
							}
						}*/

                        //Ken - if JNE / other courier will override the flat shipping
                        /*if ($final_courier['courier_id'] == '9111' || $final_courier['courier_id'] == '9112' || $final_courier['courier_id'] == '9113' )
                        {
                            if ($final_courier['courier_id'] == '9113')
                            {
                                if(isset($jne_oke->price))
                                    $total_shipping = $jne_oke->price;
                            }
                            elseif ($final_courier['courier_id'] == '9111')
                            {
                                if(isset($jne_reg->price))
                                    $total_shipping = $jne_reg->price;
                            }
                            elseif ($final_courier['courier_id'] == '9112')
                            {
                                if(isset($jne_yes->price))
                                    $total_shipping = $jne_yes->price;
                            }
                        }*/
                        //.Ken - if JNE / other courier will override the flat shipping

						$getCourierData = $this->getCourierName($final_courier['courier_id']);
						
						if ($this->getVendorName($uni_vendor['vendor'])) {
							$fc_vname = $this->getVendorName($uni_vendor['vendor']);
						} else {
							$fc_vname = $this->config->get('config_name');
						}

                        $final_courier_per_vendor[$final_courier['courier_id']] = array(
                            'vendor_id'			=> $uni_vendor['vendor'],
                            'vendor_name'		=> $fc_vname,
                            'name' 				=> $getCourierData['courier_name'],
                            'description'		=> $getCourierData['description'],
                            'courier_id' 		=> $final_courier['courier_id'],
                            'thumb' 			=> $this->model_tool_image->resize($this->getCourierImage($final_courier['courier_id']),'88','30'),
                            'shipping_weight'	=> $total_weight,
                            'shipping_rate' 	=> $total_shipping,
                            'tax_class_id' 		=> $this->config->get('mvd_usr_pv_tax_class_id'),
                            'text_weight'		=> $this->weight->format($total_weight, $this->config->get('config_weight_class_id')),
                            'text_rate'			=> $this->currency->format($this->tax->calculate($total_shipping, $this->config->get('mvd_usr_pv_tax_class_id'), $this->config->get('config_tax')))
                        );

                        if($final_courier['courier_id'] == '9111' || $final_courier['courier_id'] == '9112' || $final_courier['courier_id'] == '9113')
                        {
                            switch ($final_courier['courier_id'])
                            {
                                case '9111':
                                    if(!isset($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9111']['rate']) ||
                                       empty($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9111']['rate']))
                                        unset($final_courier_per_vendor[$final_courier['courier_id']]); //if dont have price, remove the array element out, no option to select
                                    break;
                                case '9112':
                                    if(!isset($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9112']['rate']) ||
                                        empty($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9111']['rate']))
                                        unset($final_courier_per_vendor[$final_courier['courier_id']]);
                                    break;
                                case '9113':
                                    if(!isset($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9113']['rate']) ||
                                    empty($_SESSION['ken_product_vendor_shipping_arr'][$available_courier['vendor']][$available_courier['product_id']]['9111']['rate']))
                                        unset($shipping_couriers[$final_courier['courier_id']]);
                                    break;
                            }
                        }

					}//.foreach final_courier_per_vendor
				}
			}
            //print_r($_SESSION['ken_product_vendor_shipping_arr']);
            //Ken - Store in session for verified purpose.
            $_SESSION['ken_final_courier_per_vendor'] = $final_courier_per_vendor;
		
			$products = array();
			$products = $this->cart->getProducts();
			
			$text_shipping_not_fullfil = $this->language->get('text_shipping_not_fullfil');
			$text_title_not_fullfil = $this->language->get('text_title_not_fullfil');
			$text_deliver_within = $this->language->get('text_deliver_within');
			
			//store fail to fulfil shipment vendors
			if ($fail_shipping_validation_pid) {
					foreach ($fail_shipping_validation_pid as $fail_pd) {
						foreach ($products as $product) {
							if ($fail_pd == $product['product_id']) {
								if ($product['image']) {
									$image = $this->model_tool_image->resize($product['image'], $this->config->get('config_image_cart_width'), $this->config->get('config_image_cart_height'));
								} else {
									$image = '';
								}
								
								// Display prices
								if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
									$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')));
								} else {
									$price = false;
								}

								// Display prices
								if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
									$total = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')) * $product['quantity']);
								} else {
									$total = false;
								}
								
								$fail_to_fullfil_vendors[] = array(
									'vendor_id'			  => $this->getVendorID($product['product_id']),
									'vendor_name'		  => $this->getName($product['product_id']),
									'product_id'		  => $product['product_id'],
									'key'                 => $product['key'],
									'thumb'               => $image,
									'name'                => $product['name'],
									'quantity'            => $product['quantity'],
									'price'               => $price,
									'total'               => $total,
									'href'                => $this->url->link('product/product', 'product_id=' . $product['product_id']),
									'remove'              => $this->url->link('checkout/cart', 'remove=' . $product['key']),
									'text_remove'		  => 'Remove'
								);
								
							}
						}
					}
			}

			$cost = 0;
			ob_start();
			$template = (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/' . $this->type . '/shipping/mvd_usr_pv.tpl')) ? $this->config->get('config_template') : 'default';
			require_once(DIR_TEMPLATE . $template . '/template/' . $this->type . '/shipping/mvd_usr_pv.tpl');
			$html = ob_get_contents();
			ob_end_clean();
			
			if ($final_courier_per_vendor) {
				$quote_data = array();
				$quote_data['mvd_usr_pv'] = array(
					'code'         => 'mvd_usr_pv.mvd_usr_pv',
					'title'        => $html,
					'cost'         => $cost,
					'tax_class_id' => $this->config->get('mvd_usr_pv_tax_class_id'),
					'text'         => $this->currency->format($this->tax->calculate($cost, $this->config->get('mvd_usr_pv_tax_class_id'), $this->config->get('config_tax')))
				);

				if ($this->config->get('mvd_usr_pv_checkout_name')) {
					$checkout_name = $this->config->get('mvd_usr_pv_checkout_name');
				} else {
					$checkout_name = $this->language->get('text_title');
				}
				
				$method_data = array(
					'code'       => 'mvd_usr_pv',
					'title'      => $checkout_name,
					'quote'      => $quote_data,
					'coverage'   => $fail_to_fullfil_vendors,
					'sort_order' => $this->config->get('mvd_usr_pv_sort_order'),
					'error'      => false
				);
			}
			
		}
		return $method_data;
	}

    private function getSingleShipping($product, $jne_from, $my_vutm_id = null)
    {
        //var_dump($product);
        $product_id = $product['product_id'];
        $total_weight = $product['weight'];
        $product_vendor = $product['vendor'];
        $product_courier_id = $product['courier_id'];

        if($product['courier_id']>=9111 && $product['courier_id']<=9113)
        {
            $from_jne_code = $jne_from;
            //customer JNE Code
            $to_city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int) $this->session->data['shipping_city'] . "'");
            if ($to_city_query->num_rows)
            {
                $to_jne_code = $to_city_query->row['jne_code'];
            } else
            {
                $to_jne_code = '';
            }
            if (!empty($from_jne_code) && !empty($to_jne_code))
            {
                //calculate the price for the distance
                //curl for jne
                $log = new Log(date("Y-m-d") . '-jne-curl.log');
                $log->write('[INIT cURL]');
                //total weight get from above calculation
                /*$weight = $product['weight'];
                $qty = $product['quantity'];
                $total_weight = $weight * $qty;*/
                $type = 'eskpress';
                //$data = '{"api_key":"'.JNE_API_KEY.'","username":"'.JNE_API_USERNAME.'","from":"'.$from_jne_code.'","thru":"' . $to_jne_code . '","weight":"' . $total_weight . '"}';
                $data = "api_key=" . JNE_API_KEY . "&username=" . JNE_API_USERNAME . "&from=" . $from_jne_code . "&thru=" . $to_jne_code . "&weight=" . $total_weight;
                $log->write('[DATA cURL] ' . $data);
                // 1. initialize
                $ch = curl_init();

                // 2. set the options, including the url
                $header = 'Content-Type: application/x-www-form-urlencoded';
                //curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
                curl_setopt($ch, CURLOPT_URL, JNE_API_URL);
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
                curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_HEADER, $header);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data);


                // 3. execute and fetch the resulting HTML output
                $output = curl_exec($ch);
                if ($output === false)
                {
                    $log->write(curl_error($ch));
                    echo "cURL Error: " . curl_error($ch);
                }
                $log->write('[RESPONSE]' . $output);
                // 4. free up the curl handle
                curl_close($ch);
                $log->write('[CLOSED cURL]');
                //.Ken - add cURL to update serbapay for shipping status

                $jne_response = json_decode($output);
                //var_dump( $jne_response );
                if (isset($jne_response->price))
                {
                    foreach ($jne_response->price as $package)
                    {
                        if ($package->service_display == 'OKE' && $product_courier_id == '9113')
                        {
                            $jne_oke = $package;

                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['vendor_id'] = $product_vendor;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['product_id'] = $product_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['rate'] = $package->price;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['weight'] = $total_weight;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['courier_id'] = $product_courier_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['type'] = 'OKE';
                        }
                        if ($package->service_display == 'REG' && $product_courier_id == '9111')
                        {
                            $jne_reg = $package;

                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['vendor_id'] = $product_vendor;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['product_id'] = $product_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['rate'] = $package->price;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['weight'] = $total_weight;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['courier_id'] = $product_courier_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['type'] = 'REG';
                        }
                        if ($package->service_display == 'YES' && $product_courier_id == '9112')
                        {
                            $jne_yes = $package;

                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['vendor_id'] = $product_vendor;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['product_id'] = $product_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['rate'] = $package->price;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['weight'] = $total_weight;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['courier_id'] = $product_courier_id;
                            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['type'] = 'YES';
                        }
                    }
                }
            }//end of jne
        }
        //.Ken - add JNE auto calculation
        else
        {
            $getRates = $this->getUltimateRate($my_vutm_id);
            //this one only calculate for vutm_id which is base on fix weight rate
            $rates = explode(',', $getRates);
            foreach ($rates as $rate) {
                $data = explode(':', $rate);
                if ($data[0] > $total_weight) {
                    if (isset($data[1])) {
                        $total_shipping = $data[1];
                    }
                    break;
                }
            }
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['vendor_id'] = $product_vendor;
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['product_id'] = $product_id;
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['rate'] = $total_shipping;
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['weight'] = $total_weight;
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['courier_id'] = $product_courier_id;
            $_SESSION['ken_product_vendor_shipping_arr'][$product_vendor][$product_id][$product_courier_id]['type'] = 'vutm';
        }
    }

    //Internal Calculation / Get Reference Function
	private function getCourierName($courier_id) {
		$query = $this->db->query("SELECT courier_name,description FROM " . DB_PREFIX . "courier WHERE courier_id = '" . (int)$courier_id . "'");
		return $query->row;	
	}
	
	private function getCourierImage($courier_id) {
		$query = $this->db->query("SELECT courier_image FROM " . DB_PREFIX . "courier WHERE courier_id = '" . (int)$courier_id . "'");
		return $query->row['courier_image'];	
	}

    /**
     * Return Vendor ID for the Product
     * @param $product_id
     * @return bool
     */
	private function getVendorID($product_id) {
		$query = $this->db->query("SELECT vendor, city FROM " . DB_PREFIX . "vendor v JOIN " . DB_PREFIX . "vendors vds ON (vds.vendor_id = v.vendor) WHERE v.vproduct_id = '" . (int)$product_id . "'");
		if ($query->row) {
            $from_city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int)$query->row['city'] . "'");

            if ($from_city_query->num_rows) {
                $city_name = $from_city_query->row['name'];
                $from_jne_code = $from_city_query->row['jne_code'];
            } else {
                $city_name = '';
                $from_jne_code = '';
            }
            $query->row['jne_code'] = $from_jne_code;
            $query->row['city_name'] = $city_name;
            return $query->row;
		} else {
			return false;
		}
	}
	
	private function getVendorName($vendor_id) {
		$query = $this->db->query("SELECT vendor_name FROM " . DB_PREFIX . "vendors WHERE vendor_id = '" . (int)$vendor_id . "'");
		if ($query->row) {
			return $query->row['vendor_name'];	
		} else {
			return false;
		}
	}
	
	private function getUltimateRate($vutm_shipping_id) {
		$query = $this->db->query("SELECT shipping_rate FROM " . DB_PREFIX . "vendor_ultimate_shipping WHERE vutm_shipping_id = '" . (int)$vutm_shipping_id . "'");
		if ($query->row) {
			return $query->row['shipping_rate'];	
		} else {
			return false;
		}
	}
	
	private function getName($product_id) {
		$query = $this->db->query("SELECT vds.vendor_name as name FROM " . DB_PREFIX . "vendor vd LEFT JOIN " . DB_PREFIX . "vendors vds ON (vd.vendor = vds.vendor_id) WHERE vproduct_id = '" . (int)$product_id . "'");
		if ($query->row) {
			return $query->row['name'];	
		} else {
			return false;
		}
	}
	
	private function getMeasurement($vendor_id) {
		$query = $this->db->query("SELECT measurement_id FROM " . DB_PREFIX . "vendors WHERE vendor_id = '" . (int)$vendor_id . "'");
		if ($query->row) {
			return $query->row['measurement_id'];	
		} else {
			return false;
		}
	}

}