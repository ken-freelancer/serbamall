<?php
class ModelCatalogVendorLogo extends Model {
    public function getTotalVendors($data = array()) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1'");
		return $query->rows;
	} 
	
	public function getAllVendors($data = array()) {
		$query = $this->db->query("SELECT COUNT(*) as total FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1'");
		return $query->row['total'];
	} 
	
	public function getVendor($vendor_id) {
		$query = $this->db->query("SELECT *, vds.email as email, vds.firstname as firstname, vds.lastname as lastname FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1' AND vds.vendor_id = '" . $vendor_id . "'");
		
		return $query->row;
	} 
	
	public function getAllVendor() {
		$query = $this->db->query("SELECT count(*) as total FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1'");
		return $query->row['total'];
	} 
		
	//vendor gallery
	public function getVendorGallery($vendors) {
		$vendor_gallery = array();		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "vendor_gallery WHERE vendor_id = '" . (int)$vendors . "'");
		
		return $query->rows;
	}
	
	public function getProducts($data = array()) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
		
		$cache = md5(http_build_query($data));
		
		$product_data = $this->cache->get('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache);
		
		if (!$product_data) {
			$sql = "SELECT p.product_id, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)"; 
			
			$sql .= " LEFT JOIN " . DB_PREFIX . "vendor v ON (p.product_id = v.vproduct_id)";	
			
			if (!empty($data['filter_tag'])) {
				$sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)";			
			}
			
			$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'"; 
			
			if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
				$sql .= " AND (";
											
				if (!empty($data['filter_name'])) {
					$implode = array();
					
					$words = explode(' ', $data['filter_name']);
					
					foreach ($words as $word) {
						if (!empty($data['filter_description'])) {
							$implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
						} else {
							$implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
						}				
					}
					
					if ($implode) {
						$sql .= " " . implode(" OR ", $implode) . "";
					}
				}
				
				if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
					$sql .= " OR ";
				}
				
				if (!empty($data['filter_tag'])) {
					$implode = array();
					
					$words = explode(' ', $data['filter_tag']);
					
					foreach ($words as $word) {
						$implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
					}
					
					if ($implode) {
						$sql .= " " . implode(" OR ", $implode) . "";
					}
				}
			
				$sql .= ")";
			}
			
			$sql .= " AND v.vendor = '" . $data['vendor_id'] . "'";
			
			$sql .= " GROUP BY p.product_id";
			
			$sort_data = array(
				'pd.name',
				'p.model',
				'p.quantity',
				'p.price',
				'rating',
				'p.sort_order',
				'p.date_added'
			);	
			
			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				if ($data['sort'] == 'pd.name' || $data['sort'] == 'p.model') {
					$sql .= " ORDER BY LCASE(" . $data['sort'] . ")";
				} else {
					$sql .= " ORDER BY " . $data['sort'];
				}
			} else {
				$sql .= " ORDER BY p.sort_order";	
			}
			
			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC, LCASE(pd.name) DESC";
			} else {
				$sql .= " ASC, LCASE(pd.name) ASC";
			}
		
			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}				
	
				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}	
			
				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}
			
			$product_data = array();
					
			$query = $this->db->query($sql);
		
			foreach ($query->rows as $result) {
				$product_data[$result['product_id']] = $this->getProduct($result['product_id']);
			}
			
			$this->cache->set('product.' . (int)$this->config->get('config_language_id') . '.' . (int)$this->config->get('config_store_id') . '.' . (int)$customer_group_id . '.' . $cache, $product_data);
		}
		
		return $product_data;
	}
	
	public function getProduct($product_id) {
		if ($this->customer->isLogged()) {
			$customer_group_id = $this->customer->getCustomerGroupId();
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}	
				
		$query = $this->db->query("SELECT DISTINCT *, pd.name AS name, p.image, m.name AS manufacturer, (SELECT price FROM " . DB_PREFIX . "product_discount pd2 WHERE pd2.product_id = p.product_id AND pd2.customer_group_id = '" . (int)$customer_group_id . "' AND pd2.quantity = '1' AND ((pd2.date_start = '0000-00-00' OR pd2.date_start < NOW()) AND (pd2.date_end = '0000-00-00' OR pd2.date_end > NOW())) ORDER BY pd2.priority ASC, pd2.price ASC LIMIT 1) AS discount, (SELECT price FROM " . DB_PREFIX . "product_special ps WHERE ps.product_id = p.product_id AND ps.customer_group_id = '" . (int)$customer_group_id . "' AND ((ps.date_start = '0000-00-00' OR ps.date_start < NOW()) AND (ps.date_end = '0000-00-00' OR ps.date_end > NOW())) ORDER BY ps.priority ASC, ps.price ASC LIMIT 1) AS special, (SELECT points FROM " . DB_PREFIX . "product_reward pr WHERE pr.product_id = p.product_id AND customer_group_id = '" . (int)$customer_group_id . "') AS reward, (SELECT ss.name FROM " . DB_PREFIX . "stock_status ss WHERE ss.stock_status_id = p.stock_status_id AND ss.language_id = '" . (int)$this->config->get('config_language_id') . "') AS stock_status, (SELECT wcd.unit FROM " . DB_PREFIX . "weight_class_description wcd WHERE p.weight_class_id = wcd.weight_class_id AND wcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS weight_class, (SELECT lcd.unit FROM " . DB_PREFIX . "length_class_description lcd WHERE p.length_class_id = lcd.length_class_id AND lcd.language_id = '" . (int)$this->config->get('config_language_id') . "') AS length_class, (SELECT AVG(rating) AS total FROM " . DB_PREFIX . "review r1 WHERE r1.product_id = p.product_id AND r1.status = '1' GROUP BY r1.product_id) AS rating, (SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review r2 WHERE r2.product_id = p.product_id AND r2.status = '1' GROUP BY r2.product_id) AS reviews, p.sort_order FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id) LEFT JOIN " . DB_PREFIX . "manufacturer m ON (p.manufacturer_id = m.manufacturer_id) WHERE p.product_id = '" . (int)$product_id . "' AND pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'");
		
		if ($query->num_rows) {
			$query->row['price'] = ($query->row['discount'] ? $query->row['discount'] : $query->row['price']);
			$query->row['rating'] = (int)$query->row['rating'];
			
			return $query->row;
		} else {
			return false;
		}
	}
	
	public function getvendorproducts($product_id) {
		$query = $this->db->query("SELECT vds.vendor_id, vds.vendor_image, vds.vendor_banner, vds.vendor_name, vds.address_1, vds.address_2, vds.city, vds.postcode, vds.zone_id, vds.country_id, vds.telephone, vds.email FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "vendor vd ON (pd.product_id = vd.vproduct_id) LEFT JOIN " . DB_PREFIX . "vendors vds ON (vd.vendor = vds.vendor_id) WHERE p.product_id = '" . $product_id . "'");
        $city_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "city` WHERE city = '" . (int)$query->row['city'] . "'");

        if ($city_query->num_rows) {
            $city_name = $city_query->row['name'];
            $city_code = $city_query->row['code'];
        } else {
            $city_name = '';
            $city_code = '';
        }
        $query->row['city'] = $city_name;
		return $query->row;
	}
		
	public function getTotalProducts($data = array()) {
		$sql = "SELECT COUNT(DISTINCT p.product_id) AS total FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_store p2s ON (p.product_id = p2s.product_id)";
		
		$sql .= " LEFT JOIN " . DB_PREFIX . "vendor v ON (p.product_id = v.vproduct_id)";	
		
		if (!empty($data['filter_tag'])) {
			$sql .= " LEFT JOIN " . DB_PREFIX . "product_tag pt ON (p.product_id = pt.product_id)";			
		}
					
		$sql .= " WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p.status = '1' AND p.date_available <= NOW() AND p2s.store_id = '" . (int)$this->config->get('config_store_id') . "'";
		
		$sql .= " AND v.vendor = '" . $data['vendor_id'] . "'";
		
		if (!empty($data['filter_name']) || !empty($data['filter_tag'])) {
			$sql .= " AND (";
								
			if (!empty($data['filter_name'])) {
				$implode = array();
				
				$words = explode(' ', $data['filter_name']);
				
				foreach ($words as $word) {
					if (!empty($data['filter_description'])) {
						$implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' OR LCASE(pd.description) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
					} else {
						$implode[] = "LCASE(pd.name) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%'";
					}				
				}
				
				if ($implode) {
					$sql .= " " . implode(" OR ", $implode) . "";
				}
			}
			
			if (!empty($data['filter_name']) && !empty($data['filter_tag'])) {
				$sql .= " OR ";
			}
			
			if (!empty($data['filter_tag'])) {
				$implode = array();
				
				$words = explode(' ', $data['filter_tag']);
				
				foreach ($words as $word) {
					$implode[] = "LCASE(pt.tag) LIKE '%" . $this->db->escape(utf8_strtolower($word)) . "%' AND pt.language_id = '" . (int)$this->config->get('config_language_id') . "'";
				}
				
				if ($implode) {
					$sql .= " " . implode(" OR ", $implode) . "";
				}
			}
		
			$sql .= ")";
		}
		
		$query = $this->db->query($sql);
		
		return $query->row['total'];
	}
	
	public function getVendorRating($vendor_id) {
		$query = $this->db->query("SELECT AVG(rating) AS total FROM " . DB_PREFIX . "vendor_review WHERE status = '1' AND vendor_id = '" . (int)$this->db->escape(isset($vendor_id) ? $vendor_id : 0) . "' GROUP BY vendor_id");
		
		if (isset($query->row['total'])) {
			return (int)$query->row['total'];
		} else {
			return 0;
		}
	}
		
	public function getVendorAverageRating($product_id) {
		$vendor = $this->db->query("SELECT v.vendor as vendor_id FROM " . DB_PREFIX . "vendor v WHERE v.vproduct_id = '" . (int)$product_id . "'");
		$query = $this->db->query("SELECT AVG(rating) AS total FROM " . DB_PREFIX . "vendor_review WHERE status = '1' AND vendor_id = '" . (int)$this->db->escape(isset($vendor->row['vendor_id']) ? $vendor->row['vendor_id'] : 0) . "' GROUP BY vendor_id");
		
		if (isset($query->row['total'])) {
			return (int)$query->row['total'];
		} else {
			return 0;
		}
	}
	
	public function getTheVendors($data) {
		$sql = "SELECT *, vds.firstname as firstname, vds.lastname as lastname, vds.vendor_description as description FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1' AND vds.vendor_id IN (" . "'" . implode("','", $data['vendors']) . "'" . ")";
				
		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}				

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}
			
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
			
		$query = $this->db->query($sql);		
		return $query->rows;
	}
	
	public function getTotalFeedback($vendor_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "review WHERE status = '1' AND vendor_id = '" . (int)$this->db->escape(isset($vendor_id) ? $vendor_id : 0) . "' GROUP BY vendor_id");
		
		if (isset($query->row['total'])) {
			return (int)$query->row['total'];
		} else {
			return 0;
		}
	}
	
	public function getTotalVendorOrders($vendor_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_product WHERE order_status_id > 0 AND vendor_id = '" . (int)$this->db->escape($vendor_id) . "' GROUP BY vendor_id");
		
		if (isset($query->row['total'])) {
			return (int)$query->row['total'];
		} else {
			return 0;
		}
	}
	
	public function getTotalVendorOrdersDetail($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total, SUM(quantity) as quantity FROM " . DB_PREFIX . "order_product WHERE order_status_id > 0 AND product_id = '" . (int)$this->db->escape($product_id) . "' GROUP BY product_id");
		if ($query->row) {
			return $query->row;
		} else {
			return false;
		}
	}
	
	public function getOldTotalVendorOrders($vendor_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "order_product WHERE vendor_id = '" . (int)$this->db->escape($vendor_id) . "' GROUP BY vendor_id");
		
		if (isset($query->row['total'])) {
			return (int)$query->row['total'];
		} else {
			return 0;
		}
	}
	
	public function getOldTotalVendorOrdersDetail($product_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total, SUM(quantity) as quantity FROM " . DB_PREFIX . "order_product WHERE product_id = '" . (int)$this->db->escape($product_id) . "' GROUP BY product_id");
		if ($query->row) {
			return $query->row;
		} else {
			return false;
		}
	}
	
	public function getDefaultVendorsID() {
		$query = $this->db->query("SELECT vds.vendor_id as vendor_id FROM " . DB_PREFIX . "vendors vds LEFT JOIN " . DB_PREFIX . "user u ON (vds.user_id = u.user_id) WHERE u.status = '1' ORDER BY vds.vendor_name ASC");
		if ($query->rows) {
			return $query->rows;
		} else {
			return false;
		}
	}
	
	public function preCheck() {
		$query = $this->db->query("SHOW COLUMNS FROM " . DB_PREFIX . "order_product LIKE 'order_status_id'");
		if ($query->row) {
			return true;
		} else {
			return false;
		}
	}
}
?>