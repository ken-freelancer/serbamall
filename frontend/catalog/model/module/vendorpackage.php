<?php
class ModelModuleVendorPackage extends Model {

	public function getCommission($commission_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission WHERE commission_id = '" . (int)$commission_id . "'");
		return $query->row;
	}

	public function getCommissionProductLimit($commission_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission c LEFT JOIN " . DB_PREFIX . "product_limit pc
									ON (c.product_limit_id = pc.product_limit_id) WHERE commission_id = '" . (int)$commission_id . "'");
		return $query->row;
	}



	public function getCommissionOption($commission_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission_option WHERE commission_id = '" . (int)$commission_id . "'");
		return $query->rows;
	}

	public function getCommissions($data = array()) {
		if ($data) {
			$sql = "SELECT * FROM " . DB_PREFIX . "commission c LEFT JOIN " . DB_PREFIX . "product_limit pc ON (c.product_limit_id = pc.product_limit_id) ";
			$sort_data = array(
				'c.commission_name',
				'c.commission_type',
				'c.commission',
				'c.sort_order'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY c.commission_name";
			}


			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;

		} else {
			$commission_data = $this->cache->get('commission');
			if (!$commission_data) {
				$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "commission c LEFT JOIN " . DB_PREFIX . "product_limit pc ON (c.product_limit_id = pc.product_limit_id)  ORDER BY commission_id");
				$commission_data = $query->rows;
				$this->cache->set('commission', $commission_data);
			}
			return $commission_data;
		}
	}

	public function getTotalVendorsByCommissionId($commission_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendors WHERE commission_id = '" . (int)$commission_id . "'");

		return $query->row['total'];
	}

	public function getTotalCommissions($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "commission";
		$query = $this->db->query($sql);
		return $query->row['total'];
	}
}