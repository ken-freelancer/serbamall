<?php
class ModelModuleVendorReview extends Model {
	public function addReview($vendor_id, $data) {
		$this->db->query("INSERT INTO " . DB_PREFIX . "vendor_review SET
		author = '" . $this->db->escape($data['name']) . "',
		customer_id = '" . (int)$this->customer->getId() . "',
		vendor_id = '" . (int)$vendor_id  . "',
		text = '" . $this->db->escape($data['text']) . "',
		rating = '" . (int)$data['rating'] . "',
		date_added = NOW()");
	}
		
	public function getReviewsByVendorId($vendor_id, $start = 0, $limit = 20) {
		if ($start < 0) {
			$start = 0;
		}
		
		if ($limit < 1) {
			$limit = 20;
		}		
		
		$query = $this->db->query("SELECT r.vreview_id, r.author, r.rating, r.text, vds.vendor_id, r.date_added
							FROM " . DB_PREFIX . "vendor_review r
							LEFT JOIN " . DB_PREFIX . "vendors vds ON (r.vendor_id = vds.vendor_id)
							WHERE vds.vendor_id = '" . (int)$vendor_id . "' AND r.status = '1'
							ORDER BY r.date_added DESC LIMIT " . (int)$start . "," . (int)$limit);

		return $query->rows;
	}

	public function getTotalReviewsByVendorId($vendor_id) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "vendor_review r
									LEFT JOIN " . DB_PREFIX . "vendors vds ON (r.vendor_id = vds.vendor_id)
									WHERE vds.vendor_id = '" . (int)$vendor_id . "' AND r.status = '1'");
		
		return $query->row['total'];
	}
}
?>