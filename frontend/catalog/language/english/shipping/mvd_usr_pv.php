<?php
// Text
$_['text_title']       			= 'Multi Vendor / Drop Shipper Ultimate Shipping Module Per Vendor';
$_['text_weight'] 	   			= 'Weight:';
$_['text_title_2']       		= '<b>Total Multi Vendor Ultimate Shipping Rate</b> - ';
$_['text_ship_amount'] 			= 'Total Shipping : ';
$_['text_ship_amount_tax'] 		= 'Total Shipping + Tax : ';
$_['text_shipping_not_fullfil'] = 'Please remove either one of the product within the vendor to adjust the shipping courier fulfilment.';
$_['text_title_not_fullfil']    = ' <b><font color="red">- Shipment Outside Delivery Coverage</font></b>';
$_['text_deliver_within'] 		= 'Delivery within ';
?>