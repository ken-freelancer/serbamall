<?php
// Heading 
$_['heading_title']    = 'Account';

// Text
$_['text_register']    = 'Register';
$_['text_login']       = 'Login';
$_['text_logout']      = 'Logout';
$_['text_forgotten']   = 'Forgotten Password';
$_['text_account']     = 'My Profile';
$_['text_edit']        = 'Edit Account';
$_['text_password']    = 'My Password';
$_['text_address']     = 'My Address Books';
$_['text_wishlist']    = 'My Wish List';
$_['text_order']       = 'Order History';
$_['text_download']    = 'Downloads';
$_['text_return']      = 'Returns';
$_['text_transaction'] = 'Transactions';
$_['text_newsletter']  = 'Newsletter';
$_['text_recurring']   = 'Recurring payments';
$_['text_purchase']    = 'My Purchase';
$_['text_return']      = 'My Returns';
$_['text_serbapay']    = 'SerbaPay';
?>
